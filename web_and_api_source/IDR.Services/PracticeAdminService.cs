﻿using System;
using System.Data.Entity;
//using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.PracticeUsers.Dto;

namespace IDR.Services
{
    public class PracticeAdminService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
        //private readonly DateService _dateService;
        //private readonly PhysiciansService _physiciansService;

        public PracticeAdminService(ApplicationDbContext context,
            IAuditService auditService
            //DateService dateService, PhysiciansService physiciansService
			)
        {
            _context = context;
            _auditService = auditService;
            //_dateService = dateService;
            //_physiciansService = physiciansService;
        }

        public async Task<PracticeAdminDto> GetAdmin(string key)
        {
            var item = await _context.PracticeAdmins
                .FirstOrDefaultAsync(x => x.DeletedOn == null &&
                !string.IsNullOrEmpty(key) && x.ProvisionalKey.ToString() == key);

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

            var dto = Mapper.Map<PracticeAdminDto>(item);

            dto.UserName = user != null ? user.UserName : string.Empty;

            dto.LoadContacts(_context);

            return dto;
        }

        public async Task<PracticeAdminDto> GetAdmin(int id)
        {
            var item = await _context.PracticeAdmins
                .FirstOrDefaultAsync(x => x.DeletedOn == null &&
                x.Id == id);

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

            var dto = Mapper.Map<PracticeAdminDto>(item);

            dto.UserName = user != null ? user.UserName : string.Empty;

            dto.LoadContacts(_context);

            return dto;
        }

        public async Task<PracticeAdminDto> Create(int practiceId)
        {
            var practice = await _context.Companies.FirstOrDefaultAsync(x => x.Id == practiceId);
            var data = new PracticeAdmin
            {
                CompanyId = practiceId,
                IsProvisional = true,
                ProvisionalKey = Guid.NewGuid(),
            };
            data.Audit(_auditService);

            _context.Entry(data).State = EntityState.Added;
            data.IsProvisional = true;

            await _context.SaveChangesAsync();

            var dto = Mapper.Map<PracticeAdminDto>(data);
            dto.Practice = Mapper.Map<CompanyDto>(practice);
            dto.SaveContacts(_context, _auditService);

            return dto;
        }

        public async Task<PracticeAdminDto> Delete(int id)
        {
            var data = await _context.PracticeAdmins.FirstOrDefaultAsync(x => x.Id == id);

            data.DeletedOn = DateTime.Now;
            _auditService.Audit(data);
            await _context.SaveChangesAsync();

            return Mapper.Map<PracticeAdminDto>(data);
        }

        public async Task<PracticeAdminDto> Update(PracticeAdminDto practiceAdmin)
        {
            var existing = await _context
                .PracticeAdmins
                .FirstOrDefaultAsync(x => x.Id == practiceAdmin.Id);

            //var originalEmail = existing.ProvisionalEmail;
            //var newEmail = practiceAdmin.ProvisionalEmail;

            if (!string.IsNullOrEmpty(practiceAdmin.UserId))
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == practiceAdmin.UserId);

                if (user != null)
                {
                    user.Firstname = practiceAdmin.FirstName;
                    user.Lastname = practiceAdmin.LastName;
                }
            }

            var admin = Mapper.Map<PracticeAdmin>(practiceAdmin);
            admin.Audit(_auditService);

            _context.Entry(existing).State = EntityState.Detached;
            _context.Entry(admin).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            var dto = Mapper.Map<PracticeAdminDto>(admin);
            dto = practiceAdmin.SaveContacts(_context, _auditService);

            return dto;
        }
    }
}
