﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using IDR.Data;
using IDR.Infrastructure;
using IDR.Services.Models.PDFs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDR.Services
{
    public class PDFService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;

        public PDFService(ApplicationDbContext context, IAuditService auditService)
        {
            _context = context;
            _auditService = auditService;
        }

        //public async Task<PagedQueryResults<PDFFileDto>> SearchPDFs(PagingParameters paging, int companyId)
        //{
        //    var terms = new string[] { };

        //    if (!string.IsNullOrEmpty(paging.Filter))
        //    {
        //        terms = paging.Filter.Split(' ').ToArray();
        //    }

        //    var pdfs = _context.PDFFiles
        //        .Where(x => x.DeletedOn == null && x.CompanyId == companyId)
        //        .Where(x => !string.IsNullOrEmpty(x.FileName))
        //        .Where(x =>
        //            (string.IsNullOrEmpty(paging.Filter) ||
        //             (x.FileName.Contains(paging.Filter) ||
        //              terms.Any(t => x.Alias.Contains(t))
        //             )));

        //    var totalRecords = pdfs.Count();

        //    var model = await pdfs
        //        .Project().To<PDFFileDto>()
        //        .OrderByDescending(x => x.ModifiedOn)
        //        .Skip(paging.Offset * paging.Limit)
        //        .Take(paging.Limit)
        //        .ToListAsync();

        //    return new PagedQueryResults<PDFFileDto>(model, totalRecords);
        //}

        public async Task<List<PDFFileDto>> SearchPDFs(int? companyId)
        {

            List<PDFFile> existingPDFFiles = new List<PDFFile>();

            existingPDFFiles = _context.PDFFiles.Where(x => x.DeletedOn == null && x.CompanyId == companyId).ToList();

            List<PDFFileDto> PDFFiles = existingPDFFiles.Select(p => new PDFFileDto()
            {
                Id = p.Id,
                FileName = p.FileName,
                ModifiedOn = p.ModifiedOn.Value,
                Alias = p.Alias,
                FileSize = p.FileSize,
                PageCount = p.PageCount
            })
            .ToList();

            return PDFFiles;
        }

        public async Task<PDFFile> GetPDFForFile(int pdfId)
        {
            var pdf = await _context.PDFFiles.FirstOrDefaultAsync(x => x.Id == pdfId);

            return pdf;
        }

        public async Task<bool> CreatePDFToFile(byte[] pdf, string fileName, int companyId, double fileSize, int numberOfPages)
        {
            var existing = _context.PDFFiles.Where(p => p.FileName.Equals(fileName) && p.DeletedOn == null && p.CompanyId == companyId).FirstOrDefault();
            if (existing != null)
            {
                return false;
            }

            var newPDFFileUpload = new PDFFile
            {
                FileName = fileName,
                PDF = pdf,
                CompanyId = companyId,
                Alias = null,
                FileSize = fileSize,
                PageCount = numberOfPages
            };

            _auditService.Audit(newPDFFileUpload);

            _context.PDFFiles.Add(newPDFFileUpload);

            await _context.SaveChangesAsync();

            var pdfFileDTO = Mapper.Map<PDFFileDto>(newPDFFileUpload);

            return true;
        }

        public async Task<string> UpdatePDFAlias(int pdfId, string alias)
        {
            var pdf = await _context.PDFFiles.FirstOrDefaultAsync(x => x.Id == pdfId);

            pdf.Alias = alias;

            pdf.Audit(_auditService);
            await _context.SaveChangesAsync();

            return alias;
        }

        public async Task<bool> UpdatePDFToFile(int pdfId, byte[] pdf, string fileName, double fileSize, int numberOfPages)
        {
            try
            {

                var existing = await _context.PDFFiles.FirstOrDefaultAsync(x => x.Id == pdfId);

                if (existing != null)
                {
                    existing.PDF = pdf;
                    existing.FileName = fileName;
                    existing.FileSize = fileSize;
                    existing.PageCount = numberOfPages;
                    existing.Audit(_auditService);

                    var alreadyExisting = await _context.PDFFiles.FirstOrDefaultAsync(x => x.FileName == existing.FileName && x.CompanyId == existing.CompanyId && x.DeletedOn != null);
                    if (alreadyExisting != null)
                    {
                        return false;
                    }

                    await _context.SaveChangesAsync();
                    return true;
                }

                return false;

            }
            catch (Exception e)
            {
                return false;
            }
            
        }

        public async Task RemovePDF(int pdfId)
        {
            var pdf = await _context.PDFFiles
                .FirstAsync(i => i.Id == pdfId);

            pdf.DeletedOn = DateTime.UtcNow;
            _auditService.Audit(pdf);

            await _context.SaveChangesAsync();
        }
    }
}
