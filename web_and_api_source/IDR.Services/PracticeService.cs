﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using IDR.Data;
using IDR.Data.CallCenter;
using IDR.Domain.Addresses.Helpers;
using IDR.Domain.CallCenter;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.Disciplines.Helper;
using IDR.Domain.ImageFiles.Helpers;
using IDR.Domain.Portal.Dto;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.web.Models;

namespace IDR.Services
{

    public class PracticeService
    {
        private readonly ApplicationDbContext _db;
        private readonly IAuditService _auditService;
        private readonly DateService _dateService;
        private readonly PhysiciansService _physiciansService;

        public PracticeService(ApplicationDbContext db,
            IAuditService auditService,
            DateService dateService, PhysiciansService physiciansService)
        {
            _db = db;
            _auditService = auditService;
            _dateService = dateService;
            _physiciansService = physiciansService;
        }

        public async Task<PracticeDto> GetPractice(string userId, int id)
        {
            var isPracticeAdmin = false;
            var admin = await _db.PracticeAdmins.FirstOrDefaultAsync(x => x.UserId == userId);
            if (admin != null)
            {
                isPracticeAdmin = true;
            }

            var remotes = await _db.RemoteFacilities
                .Where(rf => rf.CompanyId == id && rf.IsNew == false && rf.DeletedOn == null)
                .ToListAsync();

            var remoteFacilityIds = remotes.Select(r => r.FacilityId).ToList();

            var practiceQuery = _db.Companies
                .Where(c => c.Id == id);

            var practice = await practiceQuery
                .ProjectTo<PracticeDto>()
                .FirstOrDefaultAsync();

            var physicians = await _physiciansService.GetPhysiciansForPractice(id);

            var model = Mapper.Map<PracticeDto>(practice);
            model.Physicians = physicians.ToList();
            model.IsPracticeAdmin = isPracticeAdmin;

            model.Physicians.ToList()
                .LoadContacts(_db)
                .LoadImages(_db);

            var practiceFacilities = _db.Facilities.Where(f =>
                    !f.IsNew && f.DeletedOn == null &&
                    f.CompanyId == practice.Id)
                .ProjectTo<FacilityDto>()
                .ToList();

            practice.Facilities = practiceFacilities;

            var remoteFacilities = await _db.Facilities
                .Where(f => remoteFacilityIds.Contains(f.Id))
                .ToListAsync();

            model.Facilities.AddRange(practiceFacilities.Select(Mapper.Map<FacilityDto>).ToList());
            model.RemoteFacilities.AddRange(remoteFacilities.Select(Mapper.Map<FacilityDto>).ToList());

            model.Facilities.LoadAddresses(_db).LoadContacts(_db).LoadImages(_db);
            model.LoadAddresses(_db).LoadContacts(_db).LoadImages(_db);

            // Call Center Details - model.
            var callCenter = await _db.CallCenterPracticeReferences.FirstOrDefaultAsync(c =>
                c.PracticeId == model.Id && c.DeletedOn == null
            );

            if (callCenter != null)
            {
                model.CallCenterId = callCenter.CallCenterId;
                model.EndCalls = callCenter.EndCalls;
                model.CallCenterNotes = callCenter.Notes;
                model.SatisfactionFinal = callCenter.SatisfactionFinal;
                model.SatisfactionInitial = callCenter.SatisfactionInitial;
                model.StartCalls = callCenter.StartCalls;
                model.PracticeCode = callCenter.PracticeCode;
                model.InfuFacilityId = callCenter.InfuFacilityId;
                model.Passive = callCenter.Passive;
                model.FacilityGroup = callCenter.FacilityGroup;
                model.Addl_URL = callCenter.Addl_URL;
            }

            return model;
        }

        public async Task<PracticeDto> UpdatePractice(PracticeDto model)
        {
            var existing = await _db.Companies.FirstOrDefaultAsync(x => x.Id == model.Id);

            existing.Name = model.Name;
            existing.IsNew = false;
            existing.TimeZone = model.TimeZone;
            existing.Discount = model.Discount;
            existing.LicenseExpiration = model.LicenseExpiration;
            existing.MaxFacilities = model.MaxFacilities;
            existing.MaxNamedUsers = model.MaxNamedUsers;
            existing.MaxPhysicians = model.MaxPhysicians;
            existing.MaxPlans = model.MaxPlans;

            existing.CostPerUser = model.CostPerUser;
            existing.CostPerFacility = model.CostPerFacility;
            existing.CostPerPhysician = model.CostPerPhysician;
            existing.CostPerPlan = model.CostPerPlan;
            existing.Audit(_auditService);

            existing.PatientEmailTemplate = model.PatientEmailTemplate;
            existing.PhysicianEmailTemplate = model.PhysicianEmailTemplate;
            existing.PhysiciansAssistantEmailTemplate = model.PhysiciansAssistantEmailTemplate;
            existing.PracticeAdminEmailTemplate = model.PracticeAdminEmailTemplate;
            existing.CallSiteAdminEmailTemplate = model.CallSiteAdminEmailTemplate;
            existing.PayorId = model.PayorId;

            existing.Division = model.Division.Equals("PAIN") ? "PAIN" : "ONC";

            var existingFacility = await _db.Facilities.FirstOrDefaultAsync(f => f.CompanyId == model.Id);
            existingFacility.Name = existing.Name;
            existingFacility.Division = existing.Division;
            existingFacility.Audit(_auditService);

            await _db.SaveChangesAsync();

            model.SaveContacts(_db, _auditService);
            model.SaveAddresses(_db, _auditService);
            model.SaveImages(_db, _auditService);

            foreach (var remote in model.RemoteFacilities)
            {
                if (remote.IsNew)
                {
                    _db.RemoteFacilities.Add(new RemoteFacility
                    {
                        CompanyId = remote.CompanyId,
                        FacilityId = remote.Id
                    });
                }
                else
                {
                    var existingRemotes = _db.RemoteFacilities.Where(
                        rf => rf.CompanyId == model.Id && rf.FacilityId == remote.Id)
                        .ToList();

                    foreach (var toRemove in existingRemotes)
                    {
                        toRemove.DeletedOn = remote.DeletedOn;
                    }
                }
            }

            // Call Center Updates
            var callCenterRef = await _db.CallCenterPracticeReferences.FirstOrDefaultAsync(cc => cc.PracticeId == model.Id);

            if (model.CallCenterId <= 0 && callCenterRef != null)
            {
                // No ref.  Flag deleted
                callCenterRef.DeletedOn = DateTime.UtcNow;
            }
            else if (callCenterRef == null)
            {
                if (model.CallCenterId > 0)
                {
                    var newCC = new CallCenterPracticeReference
                    {
                        PracticeId = model.Id,
                        EndCalls = model.EndCalls,
                        StartCalls = model.StartCalls,
                        Notes = model.CallCenterNotes,
                        CallCenterId = model.CallCenterId,
                        SatisfactionFinal = model.SatisfactionFinal,
                        SatisfactionInitial = model.SatisfactionInitial,
                        PracticeCode = model.PracticeCode,
                        Passive = model.Passive,
                        InfuFacilityId = model.InfuFacilityId,
                        FacilityGroup = model.FacilityGroup,
                        Addl_URL = model.Addl_URL
                    };
                    _db.CallCenterPracticeReferences.Add(newCC);
                }
            }
            else
            {
                if (model.CallCenterId > 0)
                {
                    callCenterRef.EndCalls = model.EndCalls;
                    callCenterRef.StartCalls = model.StartCalls;
                    callCenterRef.Notes = model.CallCenterNotes;
                    callCenterRef.CallCenterId = model.CallCenterId;
                    callCenterRef.SatisfactionFinal = model.SatisfactionFinal;
                    callCenterRef.SatisfactionInitial = model.SatisfactionInitial;
                    callCenterRef.DeletedOn = null;
                    callCenterRef.PracticeCode = model.PracticeCode;
                    callCenterRef.Passive = model.Passive;
                    callCenterRef.InfuFacilityId = model.InfuFacilityId;
                    callCenterRef.FacilityGroup = model.FacilityGroup;
                    callCenterRef.Addl_URL = model.Addl_URL;

                }
            }

            await _db.SaveChangesAsync();

            return model;
        }

        public async Task<CompanyDto> Create(bool fromInfuSystem = false)
        {
            var company = new Company
            {
                Name = "New Practice",
                CompanyType = CompanyType.Practice,
                IsNew = true,
                TimeZone = _dateService.DefaultTimeZone
            };

            var mailTemplates = await _db.EmailTemplates.ToListAsync();
            company = _db.Companies.Add(company);
            company.Discount = 0;
            company.LicenseExpiration = DateTime.UtcNow.AddYears(1);
            company.MaxFacilities = 1;
            company.MaxNamedUsers = 1;
            company.MaxPhysicians = 1;
            company.CostPerUser = 1;
            company.CostPerFacility = 1;
            company.CostPerPhysician = 1;
            company.PatientEmailTemplate = mailTemplates.FirstOrDefault(t => t.Alias == "patientinvitation")?.Content ?? "";
            company.PhysicianEmailTemplate = mailTemplates.FirstOrDefault(t => t.Alias == "physicianinvitation")?.Content ?? "";
            company.PhysiciansAssistantEmailTemplate = mailTemplates.FirstOrDefault(t => t.Alias == "painvitation")?.Content ?? "";
            company.PracticeAdminEmailTemplate = mailTemplates.FirstOrDefault(t => t.Alias == "admininvitation")?.Content ?? "";
            company.CallSiteAdminEmailTemplate = mailTemplates.FirstOrDefault(t => t.Alias == "calladmininvitation")?.Content ?? "";
            company.CallSiteMobileUserEmailTemplate = mailTemplates.FirstOrDefault(t => t.Alias == "callmobileuser")?.Content ?? "";

            if (fromInfuSystem)
            {
                company.Division = "ONC";
            }
            else
            {
                company.Division = "PAIN";
            }

            company.Audit(_auditService);

            await _db.SaveChangesAsync();

            var dto = Mapper.Map<CompanyDto>(company);

            dto.SaveContacts(_db, _auditService)
               .SaveAddresses(_db, _auditService)
               .SaveImages(_db, _auditService);

            if (company.Facilities == null || !company.Facilities.Any())
            {
                var fa = new Facility
                {
                    CompanyId = company.Id,
                    Name = company.Name ?? "Default Facility",
                    IsNew = false
                };
                fa = _db.Facilities.Add(fa);

                if (fromInfuSystem)
                {
                    fa.Division = "ONC";
                }
                else
                {
                    fa.Division = "PAIN";
                }

                await _db.SaveChangesAsync();
            }

            return dto;
        }

        public async Task<CompanyDto> Copy(int sourceId)
        {
            var old = await _db.Companies.FirstOrDefaultAsync(x => x.Id == sourceId);

            var newPractice = new Company { Name = old.Name.Replace(" (copy)", string.Empty) + " (copy)" };

            newPractice.Copy(old).Audit(_auditService);

            newPractice = _db.Companies.Add(newPractice);

            await _db.SaveChangesAsync();

            var dto = Mapper.Map<CompanyDto>(newPractice);

            dto.CopyAddresses(Mapper.Map<CompanyDto>(old), _db, _auditService)
                .CopyContacts(Mapper.Map<CompanyDto>(old), _db, _auditService)
                .CopyImages(Mapper.Map<CompanyDto>(old), _db, _auditService);

            // Call Center Updates
            var callCenterRef = await _db.CallCenterPracticeReferences.FirstOrDefaultAsync(cc => cc.PracticeId == old.Id);


            if (callCenterRef != null)
            {
                var newCC = new CallCenterPracticeReference
                {
                    PracticeId = newPractice.Id,
                    EndCalls = callCenterRef.EndCalls,
                    StartCalls = callCenterRef.StartCalls,
                    Notes = callCenterRef.Notes,
                    CallCenterId = callCenterRef.CallCenterId,
                    SatisfactionFinal = callCenterRef.SatisfactionFinal,
                    SatisfactionInitial = callCenterRef.SatisfactionInitial,
                    PracticeCode = callCenterRef.PracticeCode,
                    Passive = callCenterRef.Passive
                };
                _db.CallCenterPracticeReferences.Add(newCC);
            }

            await _db.SaveChangesAsync();

            return dto;
        }

        public void Delete(int id)
        {
            var practice = _db.Companies.First(x => x.Id == id);
            practice.DeletedOn = DateTime.Now;
            _auditService.Audit(practice);
            _db.SaveChanges();
        }

        public async Task<PagedQueryResults<CompanyDto>> GetPractices(PagingParameters paging)
        {
            var temps = await _db.Companies
                .Where(c => !string.IsNullOrEmpty(paging.Filter) && c.Name.Contains(paging.Filter) ||
                            string.IsNullOrEmpty(paging.Filter))
                .Where(c => c.DeletedOn == null && !c.IsNew && c.CompanyType == CompanyType.Practice)
                .ToListAsync();

            var totalRecords = temps.Count;

            var dtos = temps
                .Skip(paging.Offset * paging.Limit)
                .Take(paging.Limit)
                .Select(Mapper.Map<CompanyDto>)
                .ToList();

            var callCenterPracticeReferences = await _db.CallCenterPracticeReferences.ToListAsync();
            foreach (CallCenterPracticeReference ccpr in callCenterPracticeReferences)
            {
                if (string.IsNullOrEmpty(ccpr.InfuFacilityId))
                {
                    ccpr.InfuFacilityId = "N/A";
                }
            }
            dtos.All(c =>
            {
                c.InfuFacilityId = (callCenterPracticeReferences.Where(pc => pc.PracticeId == c.Id)
                .Select(pc => pc.InfuFacilityId).FirstOrDefault()); return true;
            });

            dtos.LoadAddresses(_db).LoadContacts(_db).LoadImages(_db);

            var result = new PagedQueryResults<CompanyDto>(dtos, totalRecords);

            return result;
        }

        public async Task<PagedQueryResults<BaseUserDto>> GetPracticeUsers(int id, PagingParameters pagingParameters)
        {
            var dtos = new List<BaseUserDto>();

            var paging = (FilterByTypePagedResults)pagingParameters;

            var totalRecords = 0;

            // Get physicians
            if (paging.LoadPhysicians)
            {
                var docDtos = await BaseUserHelper.GetFor<Physician, PhysicianDto>(_db, paging,
                    id, count => totalRecords += count);

                dtos.AddRange(docDtos.LoadContacts(_db).LoadDisciplines(_db));
            }

            // Get phys assists
            if (paging.LoadPhysicanAssistants)
            {
                var physDtos = await BaseUserHelper.GetFor<PhysiciansAssistant, PhysiciansAssistantDto>(_db, paging, id,
                    count => totalRecords += count);

                dtos.AddRange(physDtos.LoadContacts(_db));
            }

            // Get practice admins
            if (paging.LoadPracticeAdmins)
            {
                var adminDtos = await BaseUserHelper.GetFor<PracticeAdmin, PracticeAdminDto>(_db, paging, id,
                    count => totalRecords += count);

                dtos.AddRange(adminDtos.LoadContacts(_db));
            }

            // Get Schedulers
            if (paging.LoadSchedulers)
            {
                var adminDtos = await BaseUserHelper.GetFor<Scheduler, SchedulerDto>(_db, paging, id,
                    count => totalRecords += count);

                dtos.AddRange(adminDtos.LoadContacts(_db));
            }

            // Get Call Site Admins
            if (paging.LoadSiteAdmins)
            {
                var adminDtos = await BaseUserHelper.GetFor<CallSiteAdmin, CallSiteAdminDto>(_db, paging, id,
                    count => totalRecords += count);

                dtos.AddRange(adminDtos.LoadContacts(_db));
            }

            var final = dtos
                .Skip(paging.Limit * paging.Offset)
                .Take(paging.Limit);

            var response = new PagedQueryResults<BaseUserDto>(final.ToList(), totalRecords);

            return response;

        }

        public async Task<List<CompanyDto>> GetPracticesForFacilityGroup(String id)
        {
            List<CompanyDto> companyDetails = new List<CompanyDto>();
            CompanyDto sample;
            int? companyId = 0;
            List<int> companyList = new List<int>();
            var user = _db.Users.FirstOrDefault(u => u.Id == id || u.UserName == id);
            var csa = _db.CallSiteAdmins.FirstOrDefault(c => c.UserId == user.Id);
            if (csa == null)
            {
                var practicadmin = _db.PracticeAdmins.FirstOrDefault(p => p.UserId == user.Id);
                companyId = await _db.PracticeAdmins.Where(p => p.UserId == user.Id).Select(p => p.CompanyId).FirstOrDefaultAsync();
            }
            else
            {
                companyId = await _db.CallSiteAdmins.Where(c => c.UserId == user.Id).Select(c => c.CompanyId).FirstOrDefaultAsync();
            }
            string facilityGroup = await _db.CallCenterPracticeReferences.Where(c => c.PracticeId == companyId).Select(c => c.FacilityGroup).FirstOrDefaultAsync();
            companyList = await _db.CallCenterPracticeReferences.Where(ccpr => ccpr.FacilityGroup.Equals(facilityGroup)).Select(ccpr => ccpr.PracticeId).ToListAsync();

           foreach (int co in companyList)
            {
                sample = await _db.Companies.Where(c => c.Id == co).ProjectTo<CompanyDto>().FirstOrDefaultAsync();
                companyDetails.Add(sample);

            }
            return companyDetails;
        }

        public async Task<bool> UpdatePractice(String userId, int companyId)
        {
            try
            {
                var user = _db.Users.FirstOrDefault(u => u.Id == userId);
                var csa = _db.CallSiteAdmins.FirstOrDefault(c => c.UserId == user.Id);
                if (csa == null)
                {
                    var pa = await _db.PracticeAdmins.Where(p => p.UserId == user.Id).FirstOrDefaultAsync();
                    pa.CompanyId = companyId;
                }
                else
                {
                    csa.CompanyId = companyId;
                }

                await _db.SaveChangesAsync();
                return true;

            }
            catch(Exception e)
            {
                return false;
            }
        }

        public async Task<CompanyDto> GetPractice(String userId)
        {
            int? practiceId = 0;
            var user = _db.Users.FirstOrDefault(u => u.Id == userId);
            var csa = _db.CallSiteAdmins.FirstOrDefault(c => c.UserId == user.Id);
            if (csa == null)
            {
                var pa = await _db.PracticeAdmins.Where(p => p.UserId == user.Id).FirstOrDefaultAsync();
                practiceId = pa.CompanyId;
            }
            else
            {
                practiceId = csa.CompanyId;
            }
            string companyName = await _db.Companies.Where(c => c.Id == practiceId).Select(c => c.Name).FirstOrDefaultAsync();
            return new CompanyDto
            {
                Id = practiceId.Value,
                Name = companyName
            };
        }

    }

    public class FilterByTypePagedResults : PagingParameters
    {
        public FilterByTypePagedResults()
        {
            LoadPhysicanAssistants = true;
            LoadPhysicians = true;
            LoadPracticeAdmins = true;
            LoadPatients = true;
            LoadSchedulers = true;
            LoadSiteAdmins = true;
        }

        public bool LoadPhysicians { get; set; }
        public bool LoadPhysicanAssistants { get; set; }
        public bool LoadPatients { get; set; }
        public bool LoadPracticeAdmins { get; set; }
        public bool LoadSchedulers { get; set; }
        public bool LoadSiteAdmins { get; set; }
    }

    public static class BaseUserHelper
    {
        public static async Task<List<TDtot>> GetFor<T, TDtot>(this ApplicationDbContext context, FilterByTypePagedResults paging, int practiceId, Action<int> counterA) where T : InvitableEntity
        {
            var query = context.Set<T>()
                .Where(x => (x.CompanyId == practiceId && x.DeletedOn == null) &&
                    x.IsNew == false &&
                    (string.IsNullOrEmpty(paging.Filter) ||
                    (x.FirstName.Contains(paging.Filter) ||
                    x.LastName.Contains(paging.Filter) ||
                    x.ProvisionalEmail.Contains(paging.Filter)))
                );

            var docDtos = await query.Project().To<TDtot>().ToListAsync();
            var totalRecords = await query.CountAsync();
            counterA(totalRecords);
            return docDtos;
        }

        public static async Task<OperationResponse<ApplicationUser>> RegisterUser(this ApplicationDbContext context, string provisionalKey, string userName
            , Func<OperationResponse<ApplicationUser>, BaseUserDto, Task<ApplicationUser>> createUserFn, Func<ApplicationUser, string, Task> assignRoles)
        {
            var response = new OperationResponse<ApplicationUser>();
            try
            {
                var userResponse = await context.GetUserForRegister(provisionalKey);
                if (userResponse.State == ResponseState.Succeeded)
                {
                    Guid key;
                    if (Guid.TryParse(provisionalKey, out key))
                    {
                        var roleName = "Physician";
                        var user = await createUserFn(response, userResponse.Subject);

                        response.SetSubject(user);

                        var old = await context.Physicians.FirstOrDefaultAsync(x => x.ProvisionalKey == key) as InvitableEntity;
                        if (old == null)
                        {
                            roleName = "Physicians Assistant";
                            old = await context.PhysiciansAssistants.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                        }
                        if (old == null)
                        {
                            roleName = "Practice Admin";
                            old = await context.PracticeAdmins.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                        }
                        if (old == null)
                        {
                            roleName = "Scheduler";
                            old = await context.Schedulers.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                        }
                        if (old == null)
                        {
                            roleName = "Call Site Admin";
                            old = await context.CallSiteAdmins.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                        }
                        if (old == null)
                        {
                            roleName = "Call Site Mobile";
                            old = await context.CallSiteMobileUsers.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                        }

                        old.UserId = user.Id;
                        old.ProvisionalEmail = user.UserName;
                        old.IsProvisional = false;
                        await context.SaveChangesAsync();

                        await assignRoles(user, roleName);
                        response.Succeed(string.Empty);
                    }
                }
                else
                {
                    response.Fail("User was not found or already exists");
                }
            }
            catch
            {

            }
            return response;
        }

        public static async Task<OperationResponse<BaseUserDto>> GetUserForRegister(this ApplicationDbContext context, string provisionalKey)
        {
            var response = new OperationResponse<BaseUserDto>();
            try
            {
                Guid key;
                if (Guid.TryParse(provisionalKey, out key))
                {
                    var user = await context.Physicians.FirstOrDefaultAsync(x => x.ProvisionalKey == key) as InvitableEntity;
                    if (user == null)
                    {
                        user = await context.PhysiciansAssistants.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                    }
                    if (user == null)
                    {
                        user = await context.PracticeAdmins.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                    }
                    if (user == null)
                    {
                        user = await context.Schedulers.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                    }
                    if (user == null)
                    {
                        user = await context.CallSiteAdmins.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                    }
                    if (user == null)
                    {
                        user = await context.CallSiteMobileUsers.FirstOrDefaultAsync(x => x.ProvisionalKey == key);
                    }

                    if (user == null)
                    {
                        response.Fail("user not found");
                    }
                    else
                    {
                        var dto = Mapper.Map<BaseUserDto>(user);

                        response.SetSubject(dto);
                        response.Succeed(string.Empty);
                    }
                }
                else
                {
                    response.Fail("Invalid Provisional Data");
                }
            }
            catch (Exception ex)
            {
                response.Exception(ex);
            }
            return response;
        }

        public static async Task<OperationResponse<int>> Delete<T>(this ApplicationDbContext context, IAuditService auditService, int id) where T : InvitableEntity
        {
            var response = new OperationResponse<int>();

            var user = await context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            user.DeletedOn = DateTime.Now;

            user.Audit(auditService);

            // Remove the app user
            Deactivate<T>(context, id);

            user.ProvisionalEmail = null;
            user.ProvisionalKey = null;
            user.IsProvisional = false;

            await context.SaveChangesAsync();

            return response;
        }

        public static void Deactivate<T>(this ApplicationDbContext context, int id) where T : InvitableEntity
        {
            var user = context.Set<T>().FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                return;
            }

            user.ProvisionalEmail = null;
            DeleteAppUser(context, user.UserId);
            user.UserId = null;

        }

        public static void DeleteAppUser(this ApplicationDbContext context, string userId)
        {
            var user = context.Users.FirstOrDefault(u => u.Id == userId);

            if (user == null)
            {
                return;
            }

            context.Users.Remove(user);
            context.SaveChanges();
        }

        public static async Task<PhysicianDto> GetPhysicianUser(this ApplicationDbContext context, string userid)
        {
            int id;

            int.TryParse(userid, out id);

            var user = await context.Physicians.Project().To<PhysicianDto>().FirstOrDefaultAsync(x => x.UserId == userid || x.Id == id);

            if (user == null)
            {
                var assist = await context.PhysiciansAssistants
                    .Project().To<PhysiciansAssistantDto>()
                    .FirstOrDefaultAsync(x => x.UserId == userid || x.Id == id)
                    ;

                if (assist != null)
                {
                    return assist.Physician;
                }
            }

            if (user == null)
            {
                var admin = await context.PracticeAdmins.FirstOrDefaultAsync(pa => pa.UserId == userid);

                if (admin != null)
                {
                    user = await context.Physicians
                        .Project().To<PhysicianDto>()
                        .FirstOrDefaultAsync(p =>
                            p.CompanyId == admin.CompanyId && !p.IsNew);
                }
            }

            if (user == null)
            {
                var admin = await context.Schedulers.FirstOrDefaultAsync(pa => pa.UserId == userid);

                if (admin != null)
                {
                    user = await context.Physicians
                        .Project().To<PhysicianDto>()
                        .FirstOrDefaultAsync(p =>
                            p.CompanyId == admin.CompanyId && !p.IsNew);
                }
            }

            return user;
        }

        public static async Task<List<ApplicationUser>> GetPracticeUsers(this ApplicationDbContext context, int? practiceId)
        {
            var physiciansQuery = await context.Physicians.Where(x => x.CompanyId == practiceId && !x.IsProvisional).ToListAsync();
            var practiceAdminQuery = await context.PracticeAdmins.Where(x => x.CompanyId == practiceId && !x.IsProvisional).ToListAsync();
            var assistantsQuery = await context.PhysiciansAssistants.Where(x => x.CompanyId == practiceId && !x.IsProvisional).ToListAsync();
            var schedulersQuery = await context.Schedulers.Where(x => x.CompanyId == practiceId && !x.IsProvisional).ToListAsync();
            var callsiteAdminQuery = await context.CallSiteAdmins.Where(x => x.CompanyId == practiceId && !x.IsProvisional).ToListAsync();

            var union = physiciansQuery.Select(x => (InvitableEntity)x)
                .Union(practiceAdminQuery.Select(x => (InvitableEntity)x))
                .Union(assistantsQuery.Select(x => (InvitableEntity)x))
                .Union(schedulersQuery.Select(x => (InvitableEntity)x))
                .Union(callsiteAdminQuery.Select(x => (InvitableEntity)x))
                .ToList()
                .Select(x => x.UserId).ToArray();

            var query = context.Users.Where(x => union.Contains(x.Id));

            return await query.ToListAsync();
        }

        public static async Task SwitchRoles(this ApplicationDbContext context, IAuditService auditService, string userid, string roleName, bool isInRole)
        {
            var physician = await context.Physicians.Include(x => x.Company).FirstOrDefaultAsync(x => x.UserId == userid);
            var assistant = await context.PhysiciansAssistants.Include(x => x.Company).FirstOrDefaultAsync(x => x.UserId == userid);
            var practiceAdmin = await context.PracticeAdmins.Include(x => x.Company).FirstOrDefaultAsync(x => x.UserId == userid);
            var callsiteAdmin = await context.CallSiteAdmins.Include(x => x.Company).FirstOrDefaultAsync(x => x.UserId == userid);
            var callsiteMobile = await context.CallSiteMobileUsers.Include(x => x.Company).FirstOrDefaultAsync(x => x.UserId == userid);
            var scheduler = await context.Schedulers.Include(x => x.Company).FirstOrDefaultAsync(x => x.UserId == userid);
            var user = await context.Users.FirstOrDefaultAsync(x => x.Id == userid);

            // Physician's company if one exists
            // Otherwise Assistant's compay if one exists
            // Otherwise Practice Admin's company if one exists
            // Otherwise Scheduler's company if one exists
            // Otherwise Site Admin's company if one exists
            // Otherwise the default "Unassigned" company
            var practice =
                physician != null && physician.Company != null ? physician.Company
                    : assistant != null && assistant.Company != null ? assistant.Company
                    : practiceAdmin != null && practiceAdmin.Company != null ? practiceAdmin.Company
                    : scheduler != null && scheduler.Company != null ? scheduler.Company
                    : callsiteAdmin != null && scheduler.Company != null ? scheduler.Company
                    : context.Companies.FirstOrDefault(x => x.Name == "Unassigned");

            InvitableEntity CreateUser(InvitableEntity u)
            {
                u.ProvisionalEmail = user.UserName;
                u.FirstName = user.Firstname;
                u.LastName = user.Lastname;
                u.UserId = userid;

                if (practice != null)
                {
                    u.CompanyId = practice.Id;
                }

                u.Audit(auditService);
                return u;
            }

            FlagDeleted(physician, "Physician", roleName, isInRole);
            FlagDeleted(assistant, "Physicians Assistant", roleName, isInRole);
            FlagDeleted(practiceAdmin, "Practice Admin", roleName, isInRole);
            FlagDeleted(scheduler, "Scheduler", roleName, isInRole);
            FlagDeleted(callsiteAdmin, "Call Site Admin", roleName, isInRole);
            FlagDeleted(callsiteMobile, "Call Site Mobile", roleName, isInRole);

            SetModified(context, practiceAdmin ?? CreateUser(new PracticeAdmin()) as PracticeAdmin, "Practice Admin", roleName);
            SetModified(context, assistant ?? CreateUser(new PhysiciansAssistant()) as PhysiciansAssistant, "Physicians Assistant", roleName);
            SetModified(context, physician ?? CreateUser(new Physician()) as Physician, "Physician", roleName);
            SetModified(context, scheduler ?? CreateUser(new Scheduler()) as Scheduler, "Scheduler", roleName);
            SetModified(context, callsiteAdmin ?? CreateUser(new CallSiteAdmin()) as CallSiteAdmin, "Call Site Admin", roleName);
            SetModified(context, callsiteMobile ?? CreateUser(new CallSiteMobileUser()) as CallSiteMobileUser, "Call Site Mobile", roleName);

            await context.SaveChangesAsync();
        }

        private static void FlagDeleted(InvitableEntity user, string role, string roleName, bool isInRole)
        {
            if (user != null && role == roleName && !isInRole)
            {
                user.DeletedOn = DateTime.Now;
            }
            else if (user != null && role == roleName)
            {
                user.DeletedOn = null;
            }
        }

        private static void SetModified(ApplicationDbContext context, InvitableEntity user, string role, string roleName)
        {
            if (user == null || role != roleName) return;
            if (user.Id <= 0)
            {
                context.Entry(user).State = EntityState.Added;
            }
        }
    }
}

