﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using IDR.Data;
using IDR.Domain.Membership;
using IDR.Domain.Membership.Dto;
using IDR.Infrastructure;
using IDR.web.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IDR.Services
{
	public class UsersService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;

		public UsersService(ApplicationDbContext context, IAuditService auditService)
		{
			_context = context;
			_auditService = auditService;
		}

		public async Task<OperationResponse<UserDto>> UpdateUser(UserDto model)
		{
			var physicianRole = await _context.Roles.FirstOrDefaultAsync(x => x.Name == "Physician");

			var user = await _context
				.Users
				.Include(x => x.Roles)
				.SingleOrDefaultAsync(x => x.Id == model.Id);

			user.Firstname = model.Firstname;

			user.Lastname = model.Lastname;

			user.PhoneNumber = model.PhoneNumber;
			user.Bilingual = model.Bilingual;
			user.Active = model.Active;

			if (user.Roles.Any(x => x.RoleId == physicianRole.Id))
			{
				var physician = _context.Physicians.FirstOrDefault(x => x.UserId == user.Id);
				if (physician != null)
				{
					physician.FirstName = user.Firstname;

					physician.LastName = user.Lastname;
				}
			}

			await _context.SaveChangesAsync();

			return new OperationResponse<UserDto>().SetSubject(model).Succeed("User was successfully updated!");
		}

		public async Task<PagedQueryResults<UserDto>> QueryUsers(PagingParameters paging)
		{
            var adminRoles = new[] { "Practice Admin", "Physician", "Physicians Assistant", "Scheduler", "Call Site Admin", "Call Site Mobile" };

            var roles = await _context.Roles
				.Where(x => !adminRoles.Contains(x.Name))
				.ToListAsync();

			var sql = @"select * from AspNetUsers as U
where
not exists(select * from Physicians where UserId = U.Id)
and
not exists(select * from PhysiciansAssistants where UserId = U.Id)
and
not exists(select * from Schedulers where UserId = U.Id)
and
not exists(select * from PracticeAdmins where UserId = U.Id)
and
not exists(select * from CallSiteAdmins where UserId = U.Id)
";

			var cn = _context.Database.Connection;
			cn.Open();
			var users = cn.Query<ApplicationUser>(sql).ToList();

			var userRoles = cn.Query<UserRoleDto>("Select * from AspNetUserRoles").ToList();
			cn.Close();

			foreach (var user in users)
			{
				var usersRoles = userRoles.Where(r => r.UserId == user.Id).ToList();
				var identityRoles = usersRoles.Select(r => new IdentityUserRole
				{
					UserId = r.UserId,
					RoleId = r.RoleId
				}).ToList();

				foreach (var idRole in identityRoles)
				{
					user.Roles.Add(idRole);
				}
			}

			var totalRecords = users.Count;

			var dtos = users
				.Skip(paging.Offset * paging.Limit)
				.Take(paging.Limit)
				.Select(Mapper.Map<UserDto>).ToList()
				.Select(dto =>
				{
					var user = users.FirstOrDefault(x => x.Id == dto.Id);
					var newRoles = roles.Select(x => new UserRoleDto
					{
						IsInRole = user.Roles.Any(r => r.RoleId == x.Id),
						RoleId = x.Id,
						RoleName = x.Name,
						UserId = user.Id,
					}).ToList();
					dto.Roles = newRoles;
					return dto;
				}).ToList()
				;

			var result = new PagedQueryResults<UserDto>(dtos, totalRecords);

			return result;
		}

		public UserDto GetUser(string id)
		{
			var roles = _context.Roles.ToList();

			var user = _context.Users
				.FirstOrDefault(u => u.Id == id || u.UserName == id);

			var model = Mapper.Map<UserDto>(user);

			var physician = _context.Physicians.FirstOrDefault(p => p.UserId == user.Id);

			PracticeAdmin admin = null;
			PhysiciansAssistant pa = null;
			Scheduler sch = null;
            CallSiteAdmin csa = null;
			if (physician == null)
			{
				pa = _context.PhysiciansAssistants.FirstOrDefault(p => p.UserId == user.Id);
			}
			if (physician == null && pa == null)
			{
				admin = _context.PracticeAdmins.FirstOrDefault(p => p.UserId == user.Id);
			}
			if (physician == null && pa == null && admin == null)
			{
				sch = _context.Schedulers.FirstOrDefault(p => p.UserId == user.Id);
			}
            if (physician == null && pa == null && admin == null && sch == null)
            {
                csa = _context.CallSiteAdmins.FirstOrDefault(p => p.UserId == user.Id);
            }

			var companyId = physician?.CompanyId;

			if (!companyId.HasValue)
			{
				companyId = pa?.CompanyId;
			}

			if (!companyId.HasValue)
			{
				companyId = admin?.CompanyId;
			}

			if (!companyId.HasValue)
			{
				companyId = sch?.CompanyId;
			}
            if (!companyId.HasValue)
            {
                companyId = csa?.CompanyId;
            }
            model.IsDemoPhysician = physician != null && physician.DemoPhysician;

			var newRoles = roles.Select(x => new UserRoleDto
			{
				IsInRole = user.Roles.Any(r => r.RoleId == x.Id),
				RoleId = x.Id,
				RoleName = x.Name,
				UserId = user.Id
			}).ToList();

			if (companyId.HasValue)
			{
				model.CompanyId = companyId.Value;
				// Determine the user's type
				var physicians = _context.Physicians
					.Where(p => p.CompanyId == companyId.Value &&
						!p.IsNew && p.DeletedOn == null)
					.ToList();

				model.Physicians = physicians.Select(p => (object)new
				{
					p.Title,
					p.FirstName,
					p.LastName,
					p.UserId,
					p.Id,
				}).ToList();
			}


			model.Roles = newRoles;

			return model;
		}

		public async Task<OperationResponse<UserRoleDto>> UpdateUserRole(UserRoleDto userRole)
		{
			var response = new OperationResponse<UserRoleDto>();
			// get user
			var user = await _context.Users.SingleOrDefaultAsync(x => x.Id == userRole.UserId);

			// remove roles, add new assignments
			var matchingRole = user.Roles.FirstOrDefault(role => role.RoleId == userRole.RoleId);

			// add a new one
			if (userRole.IsInRole && matchingRole == null)
			{
				_context.Entry(new IdentityUserRole { RoleId = userRole.RoleId, UserId = userRole.UserId }).State = System.Data.Entity.EntityState.Added;
				response.Succeed("User " + user.Email + " was added to role: " + userRole.RoleName);
			}
			// remove if exists and removing
			else if (!userRole.IsInRole && matchingRole != null)
			{
				_context.Entry(matchingRole).State = System.Data.Entity.EntityState.Deleted;
				response.Succeed("User " + user.Email + " was removed from role: " + userRole.RoleName);
			}
			// finally, do nothing if is in role and it already exists ... or if is not in role and doesn't exist.
			//else { }

			await _context.SaveChangesAsync();

			await _context.SwitchRoles(_auditService, userRole.UserId, userRole.RoleName, userRole.IsInRole);

			return response.SetSubject(userRole);
		}

		public async Task<List<RoleDto>> GetRoles(string userId, bool adminOnly = false)
		{
			var practiceAdmin = await _context.PracticeAdmins.FirstOrDefaultAsync(x => x.UserId == userId);

			var isPracticeAdmin = practiceAdmin != null;
			var adminRoles = new[] { "Practice Admin", "Physician", "Physicians Assistant", "Scheduler", "Call Site Admin", "Call Site Mobile" };

			var items = await _context.Roles
				.Where(x => !isPracticeAdmin || (isPracticeAdmin && adminRoles.Contains(x.Name)))
				.ToListAsync();

			var dtos = items.Select(Mapper.Map<RoleDto>).ToList();

			if (adminOnly)
			{
				dtos = dtos.Where(r => r.Name != "Physician" &&
									   r.Name != "Physicians Assistant" &&
									   r.Name != "Scheduler" &&
									   r.Name != "Call Site Admin" &&
									   r.Name != "Call Site Mobile" &&
									   r.Name != "Practice Admin").ToList();
			}

			return dtos;
		}
	}
}