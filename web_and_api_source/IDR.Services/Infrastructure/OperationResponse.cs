﻿using System;
using System.Collections.Generic;

namespace IDR.Infrastructure
{
    public enum ResponseState
    {
        New = 0, Succeeded = 1, Warned = 2, Failed = 3, Exception = 4
    }

    public class ResponseMessage
    {
        public ResponseState State { get; set; }

        public string Message { get; set; }
    }

    public class OperationResponse<SUBJECTT>
    {
        private SUBJECTT _subject;
        private ResponseState _state;
        private List<ResponseMessage> _messages;
        public OperationResponse()
        {
            _messages = new List<ResponseMessage>();

            _state = ResponseState.New;
        }

        public ResponseState State => _state;

        public List<ResponseMessage> Messages => _messages;

        private OperationResponse<SUBJECTT> AddMessage(ResponseState newState, string message)
        {
            if ((int)State <= (int)newState)
            {
                _state = newState;
            }

            _messages.Add(new ResponseMessage { State = newState, Message = message });

            return this;
        }

        public OperationResponse<SUBJECTT> Succeed(string message)
        {
            return AddMessage(ResponseState.Succeeded, message);
        }

        public OperationResponse<SUBJECTT> Warn(string message)
        {
            return AddMessage(ResponseState.Warned, message);
        }

        public OperationResponse<SUBJECTT> Fail(string message)
        {
            return AddMessage(ResponseState.Failed, message);
        }

        public OperationResponse<SUBJECTT> Exception(Exception ex)
        {
            return AddMessage(ResponseState.Exception, "We were unable to complete your request. Please try again");// ex.ToString());
        }

        public OperationResponse<SUBJECTT> SetSubject(SUBJECTT subject)
        {
            _subject = subject;

            return this;
        }

        public SUBJECTT Subject => _subject;
    }
}
