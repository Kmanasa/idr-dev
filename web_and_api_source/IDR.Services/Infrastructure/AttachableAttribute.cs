﻿using System;

namespace IDR.Infrastructure
{
	[AttributeUsage(AttributeTargets.Class)]
	public class AttachableAttribute : Attribute
	{
		public string EntityName { get; set; }
	}

	[AttributeUsage(AttributeTargets.Property)]
	public class AttachableInfoAttribute : Attribute
	{
		public string Title { get; set; }

		public string FormType { get; set; }

	}
}
