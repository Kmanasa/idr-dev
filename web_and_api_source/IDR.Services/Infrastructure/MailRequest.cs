﻿using System.Collections.Generic;

namespace IDR.Infrastructure
{
	public abstract class MailRequest
	{
		public List<string> Recipients { get; set; }
	}
}
