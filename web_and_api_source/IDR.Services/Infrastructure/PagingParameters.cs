﻿namespace IDR.Infrastructure
{
	public class PagingParameters
	{
		public PagingParameters() : this("", limit: 10)
		{

		}

		public PagingParameters(string filter = "", string order = null,
			bool ascending = true, int offset = 0, int limit = 10, int filterType = 0)
		{
			Filter = filter;
			Order = order;
			Ascending = ascending;
			Offset = offset;
			Limit = limit;
			FilterType = filterType;
		}

		public int ProxyId { get; set; }
		public string Filter { get; set; }
		public string Order { get; set; }
		public bool Ascending { get; set; }
		public int Offset { get; set; }
		public int Limit { get; set; }
		public int FilterType { get; set; }
	}
}
