﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.PracticeUsers.Dto;

namespace IDR.Services
{
	public class CallSiteAdminService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;

		public CallSiteAdminService(ApplicationDbContext context, IAuditService auditService)
		{
			_context = context;
			_auditService = auditService;
		}

		public async Task<CallSiteAdminDto> GetAdmin(string key)
		{
			var item = await _context.CallSiteAdmins
				.FirstOrDefaultAsync(x => x.DeletedOn == null &&
				                          !string.IsNullOrEmpty(key) && x.ProvisionalKey.ToString() == key);

			var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

			var dto = Mapper.Map<CallSiteAdminDto>(item);

			dto.UserName = user != null ? user.UserName : string.Empty;

			dto.LoadContacts(_context);

			return dto;
		}

		public async Task<CallSiteAdminDto> GetAdmin(int id)
		{
			var item = await _context.CallSiteAdmins
				.FirstOrDefaultAsync(x => x.DeletedOn == null &&
				                          x.Id == id);

			var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

			var dto = Mapper.Map<CallSiteAdminDto>(item);

			dto.UserName = user != null ? user.UserName : string.Empty;

			dto.LoadContacts(_context);

			return dto;
		}

		public async Task<CallSiteAdminDto> Create(int practiceId)
		{
			var practice = await _context.Companies.FirstOrDefaultAsync(x => x.Id == practiceId);
			var data = new CallSiteAdmin
			{
				CompanyId = practiceId,
				IsProvisional = true,
				ProvisionalKey = Guid.NewGuid(),
			};
			data.Audit(_auditService);

			_context.Entry(data).State = EntityState.Added;
			data.IsProvisional = true;

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<CallSiteAdminDto>(data);
			dto.Practice = Mapper.Map<CompanyDto>(practice);
			dto.SaveContacts(_context, _auditService);

			return dto;
		}

		public async Task<CallSiteAdminDto> Delete(int id)
		{
			var data = await _context.CallSiteAdmins.FirstOrDefaultAsync(x => x.Id == id);

			data.DeletedOn = DateTime.Now;
			_auditService.Audit(data);
			await _context.SaveChangesAsync();

			return Mapper.Map<CallSiteAdminDto>(data);
		}

		public async Task<CallSiteAdminDto> Update(CallSiteAdminDto calladmin)
		{
			var existing = await _context
				.CallSiteAdmins
				.FirstOrDefaultAsync(x => x.Id == calladmin.Id);

			if (!string.IsNullOrEmpty(calladmin.UserId))
			{
				var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == calladmin.UserId);

				if (user != null)
				{
					user.Firstname = calladmin.FirstName;
					user.Lastname = calladmin.LastName;
				}
			}

			var admin = Mapper.Map<CallSiteAdmin>(calladmin);
			admin.Audit(_auditService);

			_context.Entry(existing).State = EntityState.Detached;
			_context.Entry(admin).State = EntityState.Modified;

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<CallSiteAdminDto>(admin);
			dto = calladmin.SaveContacts(_context, _auditService);

			return dto;
		}
	}
}