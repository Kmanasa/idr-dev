﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using IDR.Data;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Dtos;
using IDR.Infrastructure;
using AutoMapper;
using IDR.Domain.Addresses.Helpers;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.ImageFiles.Helpers;

namespace IDR.Services
{
	public class CompanyService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;
		private readonly DateService _dateService;

		public CompanyService(ApplicationDbContext context, IAuditService auditService, DateService dateService)
		{
			_context = context;
			_auditService = auditService;
			_dateService = dateService;
		}

		public async Task<PagedQueryResults<CompanySearchDto>> Search(PagingParameters paging)
		{
			var query = await _context.Companies
				.AsNoTracking()
				.Project().To<CompanySearchDto>()
				.Where(c => c.DeletedOn == null && !c.IsNew &&
							c.CompanyType == CompanyType.Group)
				.Where(x =>
					(!string.IsNullOrEmpty(paging.Filter) && x.Name.Contains(paging.Filter)) ||
					string.IsNullOrEmpty(paging.Filter))
				.ToListAsync();

			var totalRecords = query.Count;

			var companies = query
				.Skip(paging.Offset * paging.Limit)
				.Take(paging.Limit)
				.ToList();

			var ids = companies.Select(c => c.Id).ToList();

			//dtos.LoadAddresses(Context).LoadContacts(Context).LoadImages(Context);
			var addresses = await _context.Addresses
				.AsNoTracking()
				.Project().To<AddressDto>()
				.Where(a => ids.Contains(a.EntityId) && a.EntityName == "CompanyDto")
				.ToListAsync();

			var contacts = await _context.Contacts
				.AsNoTracking()
				.Project().To<ContactDto>()
				.Where(a => ids.Contains(a.EntityId) && a.EntityName == "CompanyDto")
				.ToListAsync();

			foreach (var co in companies)
			{
				var companyAddresses = addresses.Where(a => a.EntityId == co.Id).ToList();
				var companyContacts = contacts.Where(c => c.EntityId == co.Id).ToList();

				co.MailingAddress = companyAddresses.FirstOrDefault(a => a.AddressType == "MailingAddress");
				co.CorporateAddress = companyAddresses.FirstOrDefault(a => a.AddressType == "CorporateAddress");
				co.BillingAddress = companyAddresses.FirstOrDefault(a => a.AddressType == "BillingAddress");
				co.StreetAddress = companyAddresses.FirstOrDefault(a => a.AddressType == "StreetAddress");

				co.BillingContact = companyContacts.FirstOrDefault(a => a.ContactType == "BillingContact");
				co.PrimaryContact = companyContacts.FirstOrDefault(a => a.ContactType == "PrimaryContact");
				co.SalesContact = companyContacts.FirstOrDefault(a => a.ContactType == "SalesContact");
				co.SchedulingContact = companyContacts.FirstOrDefault(a => a.ContactType == "SchedulingContact");
			}

			var result = new PagedQueryResults<CompanySearchDto>(companies, totalRecords);

			return result;

		}

		public async Task<CompanyDto> GetCompany(int id)
		{
			var company = await _context.Companies
				.Include(x => x.Facilities)
				.Where(x => x.DeletedOn == null)
				.Where(x => x.Id == id)
				.FirstOrDefaultAsync();

			var model = Mapper.Map<CompanyDto>(company);
			model.Facilities = company.Facilities.Where(f => f.DeletedOn == null).Select(Mapper.Map<FacilityDto>).ToList();
			model.Facilities.LoadAddresses(_context).LoadContacts(_context).LoadImages(_context);
			model.LoadAddresses(_context).LoadContacts(_context).LoadImages(_context);

			return model;
		}

		public async Task<CompanyDto> UpdateCompany(CompanyDto model)
		{
			var company = await _context.Companies.FirstOrDefaultAsync(x => x.Id == model.Id);

			company.Name = model.Name;
			company.IsNew = false;
			company.TimeZone = model.TimeZone;
			company.Discount = model.Discount;
			company.LicenseExpiration = model.LicenseExpiration;
			company.MaxFacilities = model.MaxFacilities;
			company.MaxNamedUsers = model.MaxNamedUsers;
			company.MaxPhysicians = model.MaxPhysicians;
			company.MaxPlans = model.MaxPlans;
			company.CostPerUser = model.CostPerUser;
			company.CostPerFacility = model.CostPerUser;
			company.CostPerPhysician = model.CostPerUser;
			company.CostPerPlan = model.CostPerUser;

			_auditService.Audit(company);

			await _context.SaveChangesAsync();

			model.SaveContacts(_context, _auditService)
				.SaveAddresses(_context, _auditService)
				.SaveImages(_context, _auditService);

			return model;
		}

		public async Task<CompanyDto> AddCompany()
		{
			var company = new Company
			{
				IsNew = true,
				Name = "New Company",
				MaxFacilities = 1,
				MaxNamedUsers = 1,
				MaxPhysicians = 1,
				MaxPlans = 1,
				LicenseExpiration = DateTime.UtcNow.AddYears(1),
				Discount = 0,
				TimeZone = _dateService.DefaultTimeZone,
			};
			company = _context.Companies.Add(company);

			_auditService.Audit(company);

			await _context.SaveChangesAsync();

			var model = Mapper.Map<CompanyDto>(company);

			model.SaveContacts(_context, _auditService)
				.SaveAddresses(_context, _auditService)
				.SaveImages(_context, _auditService);

			return model;
		}

		public async Task<CompanyDto> CopyCopmany(int id)
		{
			var old = await _context.Companies.FirstOrDefaultAsync(x => x.Id == id);

			var newCompany = new Company { Name = old.Name.Replace(" (copy)", string.Empty) + " (copy)", };

			newCompany.Copy(old).Audit(_auditService);

			newCompany = _context.Companies.Add(newCompany);

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<CompanyDto>(newCompany);

			dto.CopyAddresses(Mapper.Map<CompanyDto>(old), _context, _auditService)
				.CopyContacts(Mapper.Map<CompanyDto>(old), _context, _auditService)
				.CopyImages(Mapper.Map<CompanyDto>(old), _context, _auditService);

			return dto;
		}

		public void DeleteCompany(int id)
		{
			var company = _context.Companies.First(x => x.Id == id);

			company.DeletedOn = DateTime.Now;
			_auditService.Audit(company);

			_context.SaveChanges();
		}

		public async Task<FacilityDto> AddFacility(int id)
		{
			var facility = new Facility
			{
				IsNew = true,
				Name = "New Facility",
				CompanyId = id
			};
			facility = _context.Facilities.Add(facility);
			facility.Audit(_auditService);

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<FacilityDto>(facility);

			dto.SaveContacts(_context, _auditService)
				.SaveAddresses(_context, _auditService)
				.SaveImages(_context, _auditService);

			return dto;
		}

		public async Task<FacilityDto> CopyFacility(int id)
		{
			var old = await _context.Facilities.FirstOrDefaultAsync(x => x.Id == id);

			var newFacility =
				new Facility { Name = old.Name.Replace(" (copy)", string.Empty) + " (copy)", CompanyId = old.CompanyId };

			newFacility.Copy(old).Audit(_auditService);

			newFacility = _context.Facilities.Add(newFacility);

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<FacilityDto>(newFacility);
			dto
				.CopyAddresses(Mapper.Map<FacilityDto>(old), _context, _auditService)
				.CopyContacts(Mapper.Map<FacilityDto>(old), _context, _auditService)
				.CopyImages(Mapper.Map<FacilityDto>(old), _context, _auditService);


			return dto;
		}

		public async Task<int> DeleteFacility(int id)
		{
			var facility = await _context.Facilities.FirstOrDefaultAsync(x => x.Id == id);

			facility.DeletedOn = DateTime.Now;
			facility.Audit(_auditService);
			_context.SaveChanges();

			return id;
		}

		public async Task<FacilityDto> UpdateFacility(FacilityDto disc)
		{
			var old = await _context.Facilities.FirstOrDefaultAsync(x => x.Id == disc.Id);

			old.Name = disc.Name;
			old.IsNew = false;

			old.Audit(_auditService);

			await _context.SaveChangesAsync();

			disc.SaveContacts(_context, _auditService)
				.SaveAddresses(_context, _auditService);

			return disc;
		}

		public async Task<List<FacilityDto>> GetFacilities(int id)
		{
			var temps = await _context.Facilities
			.Include(c => c.Company)
			.Where(c => c.DeletedOn == null && !c.IsNew)
			.Where(c => (id > 0 && c.CompanyId == id) || id <= 0)
			.ToListAsync();

			var dtos = temps
			.Select(Mapper.Map<FacilityDto>)
			.ToList();

			dtos.LoadAddresses(_context)
			.LoadContacts(_context)
			.LoadImages(_context);

			return dtos;
		}

		public async Task<FacilityDto> GetFacility(int id)
		{
			var temp = await _context.Facilities
				.Include(c => c.Company)
				.Where(c => c.DeletedOn == null)
				.Where(c => c.Id == id)
				.FirstOrDefaultAsync();

			var dto = Mapper.Map<FacilityDto>(temp);

			dto.LoadAddresses(_context)
				.LoadContacts(_context)
				.LoadImages(_context);

			return dto;
		}
	}
}
