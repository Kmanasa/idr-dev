﻿using IDR.Data;

namespace IDR.Services
{
    public interface IAuditService
    {
        void Audit(IAuditEntity entity);
    }
}
