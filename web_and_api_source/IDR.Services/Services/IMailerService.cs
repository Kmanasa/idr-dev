﻿using IDR.Data;
using IDR.Domain.Mailings;
using IDR.Domain.Mailings.Dtos;

namespace IDR.Services
{
	//public class MailMessage
	//{
	//	public string Subject { get; set; }

	//	public string Body { get; set; }
	//}

	//public interface IMailer<T>
	//{
	//	MailMessage Mail(T subject);
	//}


	//public static class MailerService
	//{
		//public static T Mail<T>(this T subject)
		//{
		//	var instances = ObjectFactory.GetAllInstances<IMailer<T>>();

		//	foreach (var instance in instances)
		//	{
		//		var final = instance.Mail(subject);
		//	}

		//	return subject;
		//}

		//public async static Task<T> LoadMailings<T>(this T subject, ApplicationDbContext context) where T : IMailing
		//{
		//	var emails = await context.Mailings
		//		.Where(x => x.DeletedOn == null)
		//		.Where(x => x.EntityId == subject.Id && x.EntityName == typeof(T).Name)
		//		.OrderByDescending(x => x.CreatedOn)
		//		.Project().To<MailingDto>()

		//		.ToListAsync();

		//	subject.Mailings = emails;

		//	return subject;
		//}
	
		//public async static Task<T> MailAndTrack<T, SUBJECTT>(this T subject, ApplicationDbContext context, IAuditService auditService, SUBJECTT mailing) 
		//	where T : MailRequest
		//	where SUBJECTT : IMailing
		//{
		//	var instances = ObjectFactory.GetAllInstances<IMailer<T>>();

		//	foreach (var instance in instances)
		//	{
		//		var final = instance.Mail(subject);

		//		var mailingRecord = new Mailing {
		//			EntityId = mailing.Id, 
		//			EntityName = typeof(SUBJECTT).Name, 
		//			RequestName = typeof(T).Name, 
		//			Subject = final.Subject, 
		//			Body = final.Body, 
		//			Email = string.Join(";", subject.Recipients),
		//		};

		//		mailingRecord.Audit(auditService);
		//		context.Mailings.Add(mailingRecord);
		//		await context.SaveChangesAsync();
		//	}

		//	return subject;
		//}
	//}
}
