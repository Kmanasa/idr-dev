﻿using IDR.Data;

namespace IDR.Services
{
	public static class AuditHelper
	{
        public static T Audit<T>(this T subject, IAuditService auditService) where T : IAuditEntity
		{
			auditService.Audit(subject);

			return subject;
		}

        public static T Copy<T> (this T destination, T source) where T : AuditEntity
		{
			destination.CopyOfId = source.Id;

			return destination;
		}
	}
}
