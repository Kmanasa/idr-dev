﻿using IDR.web.Models;
using Microsoft.AspNet.Identity;

namespace IDR.Services
{
	public interface IUserManagerService
	{
		IdentityResult Create(ApplicationUser user);
		IdentityResult Create(ApplicationUser user, string password);
		ApplicationUser FindById(string userId);
		ApplicationUser FindByName(string userName);
		void AddToRole(string userId, string roleName);
		void RemoveFromRole(string userId, string roleName);
		IdentityResult RemovePassword(string userId);
		IdentityResult AddPassword(string userId, string password);
		IdentityResult Update(ApplicationUser user);
	}
}
