﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using IDR.Data;
using IDR.Data.CallCenter;
using IDR.Domain;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Notifications;
using IDR.Domain.Portal.Dto;
using IDR.Messaging;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace IDR.Services
{
	public class ProcedureService
	{
		private readonly ApplicationDbContext _db;
		private readonly DateService _dateService;
		private readonly IAuditService _auditService;
		private readonly INewProcedureMessageProvider _messageProvider;
        private readonly NotificationService _notificationService;

		public ProcedureService(ApplicationDbContext context, DateService dateService, IAuditService auditService,
			INewProcedureMessageProvider messageProvider, NotificationService notificationService)
		{
			_db = context;
			_dateService = dateService;
			_auditService = auditService;
			_messageProvider = messageProvider;
            _notificationService = notificationService;
        }

		public async Task<PatientProcedureDto> AddPatientProcedure(string userId, PatientProcedureDto model, bool omitSetupInstructions = false)
		{
			if (model.StartDate.HasValue)
			{
				model.StartDate = _dateService.NormalizeDay(model.StartDate.Value);
				model.StartDate = model.StartDate.Value
					.AddHours(model.StartTime.Hour)
					.AddMinutes(model.StartTime.Minute);
			}

			var physician = await _db.GetPhysicianUser(userId);
			var practice = await _db.Companies.FirstAsync(c => c.Id == physician.CompanyId);
			var patient = await _db.Patients.FirstAsync(c => c.Id == model.PatientId);

			var procedure = new Procedure
			{
				FacilityId = model.FacilityId,
				PatientId = model.PatientId,
				PhysicianId = physician.Id,
				CptCode = model.CptCode,
				StartDate = model.StartDate,
				NodeLibraryId = model.ProcedureId,
				AccessCode = model.AccessCode,
				SendEmail = model.SendEmail,
				SendSms = model.SendSms
			};

			if (string.IsNullOrEmpty(model.AccessCode))
			{
				bool isUnique;
				string generated;

				do
				{
					generated = AccessCodeService.Generate();

					var count = _db.Procedures.Count(p => p.AccessCode == generated);

					isUnique = count == 0;
				} while (!isUnique);

				procedure.AccessCode = generated;
			}

			_auditService.Audit(procedure);
			_db.Procedures.Add(procedure);
			await _db.SaveChangesAsync();
			await CreateNotifications(model.StartDate.Value, practice.TimeZone, procedure);
			await _db.SaveChangesAsync();

			model.Id = procedure.Id;
			model.AccessCode = procedure.AccessCode;

			if (!omitSetupInstructions)
			{
				SendAppInstructions(patient, model.AccessCode, practice.Name, model.SendEmail, model.SendSms);
			}
            
			return model;
		}

		private void SendAppInstructions(Patient patient, string accessCode, string practiceName, bool sendEmail, bool sendSms)
		{
			try
			{
				var request = new ProcedureCreatedRequest
				{
					Email = patient.Email ?? "",
					FirstName = patient.FirstName ?? "",
					PhoneNumber = patient.Phone ?? "",
					AccessCode = accessCode ?? "",
					PracticeName = practiceName ?? "",
					Recipients = new List<string> { patient.Email ?? "" }
				};

				_messageProvider.SendProcedureCreated(request, sendEmail, sendSms);
			}
			catch
			{
				// 
			}
		}

		public async Task UpdatePatientProcedure(string userId, PatientProcedureDto model)
		{
			var physician = await _db.GetPhysicianUser(userId);
			var practice = await _db.Companies.FirstAsync(c => c.Id == physician.CompanyId);
			var patient = await _db.Patients.FirstAsync(c => c.Id == model.PatientId);

			var procedure = await _db.Procedures.FirstAsync(p => p.Id == model.ProcedureId);

			var diff = model.StartDate - procedure.StartDate;
			var span = diff;

			if (span.Value.Days != 0)
			{
				var existingNotifications = _db.ProcedureNotifications
					.Where(pn => pn.ProcedureId == model.ProcedureId).ToList();

				_db.ProcedureNotifications.RemoveRange(existingNotifications);
				await _db.SaveChangesAsync();

				// Notification that the day has changed
				NotifyProcedureReschedule(model.StartDate.Value, practice.TimeZone, procedure, true);

				await CreateNotifications(model.StartDate.Value, practice.TimeZone, procedure);
			}
			else if (span.Value.TotalMinutes != 0)
			{
				NotifyProcedureReschedule(model.StartDate.Value, practice.TimeZone, procedure);
			}

			procedure.CptCode = model.CptCode;
			procedure.FacilityId = model.FacilityId;
			procedure.NodeLibraryId = model.Id;
			procedure.StartDate = model.StartDate;
			procedure.AccessCode = model.AccessCode;
			procedure.SendSms = model.SendSms;
			procedure.SendEmail = model.SendEmail;

			_auditService.Audit(procedure);

			SendAppInstructions(patient, model.AccessCode, practice.Name, model.SendEmail, model.SendSms);

			await _db.SaveChangesAsync();
		}

		/// <summary>
		/// Get the dynamic field replacement values for a procedure
		/// </summary>
		public async Task<ProcedureRefDto> GetProcedureRefs(string id, string uuid = null)
		{
			var procedure = await _db.Procedures.Include(p => p.NodeLibrary)
				.FirstOrDefaultAsync(p => p.AccessCode == id && p.DeletedOn == null);

			if (procedure == null)
			{
				return null;
			}

			var lib = await _db.NodeLibraries.FirstOrDefaultAsync(n => n.Id == procedure.NodeLibraryId);
			var duration = lib.Duration;

			// 4/4/2018 Removed due to the new InfuSystem plans created at runtime 
			// which would be immediately expired since they have no duration
			// Is plan expired?
			//if (procedure.StartDate.HasValue)
			//{
			//    // Find the last possible day
			//    var end = procedure.StartDate.Value.AddDays(duration);

			//    // Subtract last day from today
			//    var span = end - DateTime.Now;

			//    // If expired, return null.
			//    if (span.TotalDays < 0)
			//    {
			//        return null;
			//    }
			//}

			var facility = procedure.Facility;
			var physician = procedure.Physician;
			var practice = await _db.Companies.FirstAsync(c => c.Id == physician.CompanyId);

			var address = await _db.Addresses.FirstOrDefaultAsync(a => a.EntityId == practice.Id && a.EntityName == "CompanyDto" &&
																   a.AddressType == "StreetAddress");
			if (address == null)
			{
				address = await _db.Addresses.FirstOrDefaultAsync(a => a.EntityId == practice.Id &&
				                                                       a.EntityName == "CompanyDto" &&
				                                                       a.AddressType == "PracticeDto");
				address = address ?? new Address();
			}

			var patient = procedure.Patient;

			var model = new ProcedureRefDto
			{
				PatientIdentifier = patient.Identifier ?? "Unknown",
				AccessCode = procedure.AccessCode,
				PhysicianId = physician.Id.ToString(),
				LibraryId = procedure.NodeLibraryId,
				LibraryName = procedure.NodeLibrary.Name,
				StartDate = procedure.StartDate,
				DurationInDays = duration,
				PatientName = patient.FirstName + " " + patient.LastName,
				FacilityStreetAddress1 = address.Address1,
				FacilityCityStZip = $"{address.City}, {address.State} {address.PostalCode}"
			};

			await SetFacilityDetails(model, facility);
			await SetPracticeDetails(model, practice);
			await SetPhysicianDetails(model, physician);

			await GetCallCenterProcedure(model, procedure.Id);


			if (model.FacilityCityStZip == ",  ")
			{
				model.FacilityCityStZip =
					$"{model.FacilityStreetAddressCity}, {model.FacilityStreetAddressState} {model.FacilityStreetAddressPostalCode}";
			}

			return model;
		}

		private async Task GetCallCenterProcedure(ProcedureRefDto model, int id)
		{
			var ccProc = await _db.CallCenterProcedures.FirstOrDefaultAsync(p => p.ProcedureId == id);

			if (ccProc != null)
			{
				model.InfuCck = ccProc.InfuCck;
				model.InfuHaveCosent = ccProc.InfuHaveConsent;
				model.InfuPumpModel = ccProc.InfuPumpModel;
				model.InfuRate = ccProc.InfuRate;
				model.InfuTreatmentPlan = ccProc.InfuTreatmentPlan;
				model.InfuVolume = ccProc.InfuVolume;
				model.SpanishSpeaking = ccProc.SpanishSpeaking;
				model.InfuImSafetyFeatures = ccProc.InfuImSafetyFeatures;
				model.InfuDeliveredAob = ccProc.DeliveredAoB;
				model.InfuDeliveredAobDate = ccProc.DeliveredAoBDate;

				if (ccProc.PracticeFacilityId > 0)
				{
					var co = await _db.CallCenterPracticeReferences.FirstOrDefaultAsync(c =>
						c.PracticeId == ccProc.PracticeFacilityId);
					if (co != null)
					{
						model.InfuFacilityId = co.InfuFacilityId;
					}
				}
			}
		}

		/// <summary>
		/// Gets all unread inbox items for a procedure
		/// </summary>
		/// <param name="procedureCode"></param>
		/// <returns></returns>
		public async Task<List<InboxItemDto>> GetInboxItems(string procedureCode)
		{
			var procedure = _db.Procedures.FirstOrDefault(p => p.AccessCode == procedureCode);

			if (procedure == null)
			{
				return new List<InboxItemDto>();
			}

			var notifications = await _db.ProcedureNotifications
				.Include(pn => pn.ProcedureNotificationOptions)
				.Project().To<InboxItemDto>()
				.Where(pn => pn.ProcedureId == procedure.Id && pn.Sent)
				.OrderBy(p => p.SendTime)
				.ToListAsync();

			return notifications;
		}

		public Procedure GetProcedureById(int id)
		{
			return _db.Procedures.First(p => p.Id == id);
		}

		/// <summary>
		/// Flag an inbox item as read
		/// </summary>
		public async Task UpdateProcedureNotification(ProcedureNotificationOption model)
		{

			var notification = await _db.ProcedureNotifications
			  .FirstOrDefaultAsync(n => n.Id == model.ProcedureNotificationId);

			notification.DateResponded = DateTime.UtcNow;
			notification.Response = model.Text;
			notification.Triggered = model.Trigger;

			if (model.Trigger)
			{
				// handle trigger
			}

			await _db.SaveChangesAsync();
		}

		private async Task SetFacilityDetails(ProcedureRefDto model, Facility facility)
		{
			var facilityAddress = await _db.Addresses
			  .Where(a => a.EntityId == facility.Id &&
						  a.EntityName == typeof(FacilityDto).Name &&
						  a.AddressType == "StreetAddress" &&
						  a.DeletedOn == null && !a.IsNew)
			  .FirstOrDefaultAsync();

			if (facilityAddress == null)
			{
				facilityAddress = new Address();
			}

			var facilityContacts = await _db.Contacts
				.Where(c => c.EntityId == facility.Id &&
							c.EntityName == typeof(FacilityDto).Name &&
							c.DeletedOn == null && !c.IsNew)
							.ToListAsync();

			var facilityManager = facilityContacts.FirstOrDefault(fc => fc.ContactType == "FacilityContact")
				?? new Contact();
			var patientInfo = facilityContacts.FirstOrDefault(fc => fc.ContactType == "PatientContact")
				?? new Contact();


			// Facility
			model.FacilityName = facilityAddress.Name;
			model.FacilityStreetAddress1 = facilityAddress.Address1;
			model.FacilityStreetAddress2 = facilityAddress.Address2;
			model.FacilityStreetAddressCity = facilityAddress.City;
			model.FacilityStreetAddressState = facilityAddress.State;
			model.FacilityStreetAddressPostalCode = facilityAddress.PostalCode;
			model.FacilityManagerEmail = facilityManager.Email;
			model.FacilityManagerContactPhone = facilityManager.OfficePhone;
			model.FacilityPatientInformationEmail = patientInfo.Email;
			model.FacilityPatientInformationPhone = patientInfo.PersonalPhone;
		}

		private async Task SetPracticeDetails(ProcedureRefDto model, Company practice)
		{
			var practiceAddress = await _db.Addresses
			 .Where(a => a.EntityId == practice.Id &&
						 a.EntityName == typeof(CompanyDto).Name &&
						 a.AddressType == "StreetAddress" &&
						 a.DeletedOn == null && !a.IsNew)
			 .FirstOrDefaultAsync();

			if (practiceAddress == null)
			{
				practiceAddress = new Address();
			}

			var practiceContacts = await _db.Contacts
				.Where(c => c.EntityId == practice.Id &&
							c.EntityName == typeof(CompanyDto).Name &&
							c.DeletedOn == null && !c.IsNew)
				.ToListAsync();

			var primaryContact = practiceContacts.FirstOrDefault(fc => fc.ContactType == "PrimaryContact")
				?? new Contact();
			var schedulingContact = practiceContacts.FirstOrDefault(fc => fc.ContactType == "SchedulingContact")
				?? new Contact();


			model.PracticeName = practice.Name?.Trim();
			model.PracticeContactEmail = primaryContact.Email;
			model.PracticeContactPhone = primaryContact.OfficePhone;
			model.PracticeSchedulingContactEmail = schedulingContact.Email;
			model.PracticeSchedulingContactName = schedulingContact.FirstName;
			model.PracticeSchedulingContactPhone = schedulingContact.OfficePhone;
			model.PracticeAddress1 = practiceAddress.Address1;
			model.PracticeAddress2 = practiceAddress.Address2;
			model.PracticeCity = practiceAddress.Address2;
			model.PracticeState = practiceAddress.State;
			model.PracticePostalCode = practiceAddress.PostalCode;

			var logo = await _db.ImageFiles
			   .FirstOrDefaultAsync(i => i.EntityId == practice.Id && i.EntityName == typeof(CompanyDto).Name);

			if (logo != null)
			{
				model.PracticeImageRef = logo.Id.ToString();
			}
		}

		private async Task SetPhysicianDetails(ProcedureRefDto model, Physician physician)
		{
			var physicianContacts = await _db.Contacts
			  .Where(c => c.EntityId == physician.Id &&
						  c.EntityName == typeof(PhysicianDto).Name &&
						  c.DeletedOn == null && !c.IsNew)
						  .ToListAsync();

			var directContact = physicianContacts.FirstOrDefault(fc => fc.ContactType == "DirectContact")
				?? new Contact();
			var oncallContact = physicianContacts.FirstOrDefault(fc => fc.ContactType == "OnCallContact")
				?? new Contact();
			var schedulingContact = physicianContacts.FirstOrDefault(fc => fc.ContactType == "SchedulingContact")
				?? new Contact();

			var portrait = await _db.ImageFiles
				.FirstOrDefaultAsync(i => i.EntityId == physician.Id &&
				i.EntityName == typeof(PhysicianDto).Name);

			if (portrait != null)
			{
				model.PhysicianImageRef = portrait.Id.ToString();
			}

			model.PhysicianTitle = physician.Title;
			model.PhysicianFirstName = physician.FirstName;
			model.PhysicianLastName = physician.LastName;
			model.PhysicianCompanyId = physician.CompanyId;

			model.PhysicianDirectContactEmail = directContact.Email;
			model.PhysicianDirectContactPhone = directContact.PersonalPhone;

			model.PhysicianSchedulingContactEmail = schedulingContact.Email;
			model.PhysicianSchedulingContactName = schedulingContact.FirstName;
			model.PhysicianSchedulingContactExt = schedulingContact.OfficeExt;
			model.PhysicianSchedulingContactPhone = schedulingContact.OfficePhone;

			model.PracticeOnCallServiceEmail = oncallContact.Email;
			model.PracticeOnCallServicePhone = oncallContact.OfficePhone;
		}

		private async Task CreateNotifications(DateTime startDate, string timeZone, Procedure procedure)
		{
			var libraryNotifications = await _db.Notifications
				.Include(n => n.NotificationResponses)
				.Where(n => n.NodeLibraryId == procedure.NodeLibraryId && n.DeletedOn == null)
				// Order by the send day, hour, and minute
				.OrderBy(n => n.DaysOffset)
				.ThenBy(n => n.Hour)
				.ThenBy(n => n.Minute)
				.ToListAsync();

			var zone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);

			var userLocalDate = _dateService.ToLocalTime(startDate, timeZone);

			foreach (var notification in libraryNotifications)
			{
				var targetDay = userLocalDate.AddDays(notification.DaysOffset);
				var targetDayAtMidnight = new DateTime(targetDay.Year, targetDay.Month, targetDay.Day, 0, 0, 0, DateTimeKind.Local); // .AddHours(userLocalDate.Hour*-1).AddHours(userLocalDate.Minute*-1);
				var targetDayAtHour = targetDayAtMidnight.AddHours(notification.Hour);
				var targetDayAtHourAndMinute = targetDayAtHour.AddMinutes(notification.Minute);

				var hoursOffset = zone.GetUtcOffset(DateTime.UtcNow);
				// Reverse the UTC offset
				var zoneAdjusted = targetDayAtHourAndMinute.AddHours(-1 * hoursOffset.TotalHours);

				var sendTime = zoneAdjusted;//targetDayAtHourAndMinute.ToUniversalTime();

				var procedureNotification = new ProcedureNotification
				{
					NotificationId = notification.Id,
					SendTime = sendTime,
				};

				// Add options the patient can pick in response to the notification
				foreach (var notif in notification.NotificationResponses)
				{
					var option = new ProcedureNotificationOption
					{
						Text = notif.Text,
						Trigger = notif.Trigger
					};

					procedureNotification.ProcedureNotificationOptions.Add(option);
				}

				procedure.ProcedureNotifications.Add(procedureNotification);

                await _db.SaveChangesAsync();
            }
		}

		private void NotifyProcedureReschedule(DateTime startDate, string timeZone, Procedure procedure, bool dateChanged = false)
		{
			var messageFormat = "Your procedure has been rescheduled to {0} at {1}.";

			if (dateChanged)
			{
				messageFormat += " All notifications will be repeated.";
			}

			var userLocalDate = _dateService.ToLocalTime(startDate, timeZone);

			var messageText = string.Format(messageFormat, userLocalDate.ToShortDateString(),
				userLocalDate.ToShortTimeString());

			var note = new ProcedurePushNotification
			{
				ProcedureId = procedure.Id,
				ProcedureCode = procedure.AccessCode,
				Message = messageText
			};

			var storageString = CloudConfigurationManager.GetSetting("StorageConnectionString");
			var storageAccount = CloudStorageAccount.Parse(storageString);

			// Create the queue client
			var queueClient = storageAccount.CreateCloudQueueClient();

			var message = new CloudQueueMessage(JsonConvert.SerializeObject(note));
			var queue = queueClient.GetQueueReference("procedurenotifications");
			queue.CreateIfNotExists();
			queue.AddMessage(message);

			_db.SaveChanges();
		}

		public void NotifyAoB(string url, string accessCode)
		{
			var storageString = CloudConfigurationManager.GetSetting("StorageConnectionString");
			var storageAccount = CloudStorageAccount.Parse(storageString);

			// Create the queue client
			var queueClient = storageAccount.CreateCloudQueueClient();

			var queue = queueClient.GetQueueReference("aob");
			queue.CreateIfNotExists();

			var note = new AobNotification
			{
				Url = url,
				AccessCode = accessCode
			};

			var message = new CloudQueueMessage(JsonConvert.SerializeObject(note));
			queue.EncodeMessage = true;
			queue.AddMessage(message);
		}

		public ProcedureRefDto GetMockProcedureRefs(string accessCode, int libId)
		{
			return new ProcedureRefDto
			{
				AccessCode = accessCode,
				DurationInDays = 99999,
				PatientIdentifier = "demo",
				StartDate = DateTime.Now,
				LibraryId = libId,
				FacilityId = 0,
				FacilityManagerContactPhone = "333333333",
				FacilityManagerEmail = "demo@office.com",
				FacilityName = "Demo Facility",
				FacilityPatientInformationEmail = "demo@email.com",
				FacilityPatientInformationPhone = "3035551212",
				FacilityStreetAddress1 = "123 Facility Ln.",
				FacilityStreetAddress2 = "Suite 200",
				FacilityStreetAddressCity = "Denver",
				FacilityStreetAddressPostalCode = "80001",
				FacilityStreetAddressState = "CO",
				LibraryName = "Demo Plan",
				PhysicianDirectContactEmail = "demo@direct.com",
				PhysicianDirectContactPhone = "3035552323",
				PhysicianFirstName = "Jane",
				PhysicianId = "1",
				PhysicianLastName = "Doe",
				PhysicianSchedulingContactEmail = "drjame@doe.com",
				PhysicianSchedulingContactExt = "345",
				PhysicianSchedulingContactName = "Mrs. Scheduler",
				PhysicianSchedulingContactPhone = "3035554545",
				PhysicianTitle = "Dr.",
				PracticeAddress1 = "123 Summit Dr.",
				PracticeAddress2 = "Unit B",
				PracticeCity = "Vail",
				PracticeContactEmail = "primary@contact.com",
				PracticeContactPhone = "3035556767",
				PracticeName = "Example Practice",
				PracticeOnCallServiceEmail = "oncall@email.com",
				PracticeOnCallServicePhone = "3035557878",
				PracticePostalCode = "80002",
				PracticeSchedulingContactEmail = "practice@scheduler.com",
				PracticeSchedulingContactName = "Mrs. Scheduler",
				PracticeSchedulingContactPhone = "3035558989",
				PracticeState = "CO"
			};
		}

		public async Task<IList<Notification>> GetNotifications(string id)
		{
			var procedure = await _db.Procedures.FirstOrDefaultAsync(p => p.AccessCode == id);

			var notificationIds = await _db.ProcedureNotifications
				.Where(pn => pn.ProcedureId == procedure.Id)
				.Select(pn => pn.NotificationId)
				.ToListAsync();

			var notifications = await _db.Notifications
				.OrderBy(n => n.DaysOffset)
				.ThenBy(n => n.Hour)
				.ThenBy(n => n.Minute)
				.Where(n => notificationIds.Contains(n.Id))
				.ToListAsync();

			return notifications;
		}

		public void LogDevice(LogDeviceModel model)
		{
			try
			{
				var proc = _db.Procedures.First(p => p.AccessCode == model.AccessCode);

				var log = new ProcedureAccessLog
				{
					DeviceId = model.Uuid,
					ProcedureId = proc.Id,
					Model = model.Model,
					Platform = model.Platform,
					Version = model.Version,
					Manufacturer = model.Manufacturer,
					TimeStamp = DateTime.UtcNow
				};
				_db.ProcedureAccessLog.Add(log);
				_db.SaveChanges();
			}
			catch { }
		}

		public void LogTimerStart(LogTimerModel model)
		{
			var proc = _db.Procedures.FirstOrDefault(p => p.AccessCode == model.AccessCode);

			if (proc != null)
			{
				var ccProc = _db.CallCenterProcedures.FirstOrDefault(c => c.ProcedureId == proc.Id);
				
				if (ccProc != null)
				{
					var now = DateTime.UtcNow;
					var previous = now.AddHours(-12);

					var existing = _db.CallCenterProcedureTimers.FirstOrDefault(t =>
						t.ProcedureId == proc.Id &&
						t.CallCenterProcedureId == ccProc.Id &&
						t.Start > previous && t.Start < now);

					if (existing != null)
					{
						existing.Start = now;
						existing.NotificationSent = false;
						existing.RecordedVolume = 0;
						existing.End = null;
						existing.Safe = false;
					    existing.CCV = null;

						_auditService.Audit(existing);
					}
					else
					{
						var timer = Mapper.Map<CallCenterProcedureTimer>(model);
						timer.Start = now;
						timer.End = null;
						timer.ProcedureId = proc.Id;
						
						timer.CallCenterProcedureId = ccProc.Id;
						_auditService.Audit(timer);
						_db.CallCenterProcedureTimers.Add(timer);
					}

					_db.SaveChanges();
				}
			}
		}

		public void LogTimer(LogTimerModel model)
		{
			var proc = _db.Procedures.FirstOrDefault(p => p.AccessCode == model.AccessCode);

			if (proc != null)
			{
				var ccProc = _db.CallCenterProcedures.FirstOrDefault(c => c.ProcedureId == proc.Id);

				if (ccProc != null)
				{
					var now = DateTime.UtcNow;
					var previous = now.AddHours(-12);

					var timer = _db.CallCenterProcedureTimers.FirstOrDefault(t =>
						t.ProcedureId == proc.Id &&
						t.CallCenterProcedureId == ccProc.Id &&
						t.End == null &&
						t.Start > previous && t.Start < now);

					if (timer != null)
					{
						timer.End = now;
						timer.Duration = model.Duration;
						timer.Rate = model.Rate;
						timer.Safe = model.Safe;
						timer.Unit = model.Unit;
						timer.InitialVolume = model.InitialVolume;
						timer.RecordedVolume = model.RecordedVolume;
						timer.NotificationSent = false;
					    timer.CCV = model.CCV;
					    timer.ActualDuration = model.ActualDuration;

                        _auditService.Audit(timer);
                        _db.SaveChanges();

                        //Check if in range and warn
                        if (!model.Safe)
						{
							new NonResponsiveMessageService(_db).SendNonResponsiveWarning(timer);
						}
					}
				}
			}
		}
        public async Task<bool> SendPushNotification(int patientId, string phone, int messageType, string messageContent)
        {
            var onDemandMessage = new OnDemandMessage
            {
                PatientId = patientId,
                Phone = phone,
                AccessCode = _db.Procedures.Where(p => p.PatientId == patientId).Select(p => p.AccessCode).FirstOrDefault(),
                MessageType = messageType == 0 ? "Text Message" : "Push Notification",
                MessageContent = messageContent
            };

            _auditService.Audit(onDemandMessage);

            _db.OnDemandMessages.Add(onDemandMessage);

            await _db.SaveChangesAsync();

            var currentTime = DateTime.UtcNow;
            var companyId = _db.Patients.Where(p => p.Id == patientId).Select(p => p.CompanyId).FirstOrDefault();
            var timeZone = _db.Companies.Where(c => c.Id == companyId).Select(c => c.TimeZone).FirstOrDefault();
            var procedure = _db.Procedures.Where(proc => proc.PatientId == patientId).FirstOrDefault();

            try
            {
                var newNotification = new NotificationDto ()
                {
                    DaysOffset = 0,
                    Hour = 12,
                    Minute = 0,
                    Message = onDemandMessage.MessageContent,
                    NodeLibraryId = procedure.NodeLibraryId,
                    Repeat = true,
                    RepetitionMinutes = 30,
                    Repetitions = 1,
                    Required = false,
                    ResponseType = 1,
                    Title = null,
                    TriggerText = null,
                };

                var id = await _notificationService.CreateNotification(newNotification);

                await CreateNotifications(currentTime, timeZone, procedure);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

	public class LogDeviceModel
	{
		public string AccessCode { get; set; }
		public string Model { get; set; }
		public string Platform { get; set; }
		public string Uuid { get; set; }
		public string Version { get; set; }
		public string Manufacturer { get; set; }
	}

	public class LogTimerModel
	{
		public string AccessCode { get; set; }
		public double Rate { get; set; }
		public decimal InitialVolume { get; set; }
		public decimal RecordedVolume { get; set; }
		public bool Safe { get; set; }
		public int Duration { get; set; }
		public int ActualDuration { get; set; }
        public string Unit { get; set; }
        public decimal? CCV { get; set; }
	}
}
