﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Dapper;
using IDR.Data;
using IDR.Domain;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.Disciplines.Helper;
using IDR.Domain.External.Dto;
using IDR.Domain.ImageFiles.Helpers;
using IDR.Domain.Portal.Dto;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.web.Models;
using Microsoft.AspNet.Identity;

namespace IDR.Services
{
    public class PhysiciansService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
        private readonly ProcedureService _procedureService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly INewProcedureMessageProvider _messageProvider;

        public PhysiciansService(ApplicationDbContext context,
            IAuditService auditService,
            ProcedureService procedureService,
            UserManager<ApplicationUser> userManager,
            INewProcedureMessageProvider messageProvider)
        {
            _context = context;
            _auditService = auditService;

            _procedureService = procedureService;
            _userManager = userManager;
            _messageProvider = messageProvider;
        }

        public async Task<PhysicianDto> GetPhysician(int id)
        {
            var item = await _context.Physicians
                .FirstOrDefaultAsync(x => x.DeletedOn == null &&
                x.Id == id);

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

            var dto = Mapper.Map<PhysicianDto>(item);

            dto.UserName = user != null ? user.UserName : string.Empty;

            dto.LoadDiscipline(_context).LoadContacts(_context).LoadImages(_context);

            return dto;
        }

        public async Task<PhysicianDto> AddPhysician(int practiceId, bool addRef = false)
        {
            var physician = new Physician
            {
                FirstName = ConfigurationManager.AppSettings["DefaultPhysicianFirstName"],
                LastName = ConfigurationManager.AppSettings["DefaultPhysicianLastName"],
                CompanyId = practiceId,
                IsProvisional = true,
                ProvisionalKey = Guid.NewGuid()
            };

            physician.Audit(_auditService);
            physician = _context.Physicians.Add(physician);
            await _context.SaveChangesAsync();

            var dto = Mapper.Map<PhysicianDto>(physician);
            await _context.SaveChangesAsync();

            dto.SaveContacts(_context, _auditService).SaveImages(_context, _auditService);

            if (addRef)
            {
                //_context.CallCenterProviders
            }

            return dto;
        }

        public async Task<PhysicianDto> UpdatePhysician(PhysicianDto model)
        {
            var physician = await _context
                .Physicians
                .FirstOrDefaultAsync(x => x.Id == model.Id);

            physician.ImageRef = model.ImageRef;
            physician.ProvisionalEmail = model.ProvisionalEmail;
            physician.FirstName = model.FirstName;
            physician.LastName = model.LastName;
            physician.Title = model.Title;

            if (physician.UserId == null && !string.IsNullOrEmpty(model.UserId))
            {
                physician.UserId = model.UserId;

                _userManager.AddToRole(model.UserId, "Physician");

                physician.IsProvisional = false;
            }

            physician.UserId = model.UserId;
            physician.Audit(_auditService);

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == physician.UserId);
            if (user == null)
            {
                physician.IsProvisional = true;
                physician.ProvisionalKey = Guid.NewGuid();
            }
            else
            {
                user.Firstname = model.FirstName;
                user.Lastname = model.LastName;
            }

            await _context.SaveChangesAsync();

            model = model.AddDiscipline(_context, _auditService)
                .SaveContacts(_context, _auditService).SaveImages(_context, _auditService);

            return model;
        }

        public async Task<IEnumerable<PhysicianDto>> GetPhysiciansForPractice(int practiceId)
        {
            var query =
                (await (from physician in _context.Physicians
                        join user in _context.Users on physician.UserId equals user.Id into us
                        from subuser in us.DefaultIfEmpty()
                        where physician.DeletedOn == null && physician.CompanyId == practiceId &&
                        physician.IsNew == false
                        select new { physician, subuser })
                .ToArrayAsync());

            var items = query.Select(x =>
               {
                   var phys = Mapper.Map<PhysicianDto>(x.physician);
                   phys.UserName = x.subuser == null ? string.Empty : x.subuser.UserName;
                   return phys;
               })
               .ToArray();

            var dtos = items
                .LoadDisciplines(_context).ToList()
                .LoadContacts(_context)
                .LoadImages(_context);

            return dtos;
        }

        public async Task<PhysicianDto> GetPhysicianForUserQuery(string userId)
        {
            var item = await _context
                .Physicians
                .Include("FacilityAssignments.Facility")
                .Include("Patients")
                .FirstOrDefaultAsync(x => x.DeletedOn == null && x.UserId == userId);

            if (item == null)
            {
                var assist = await _context.PhysiciansAssistants
                    .Project().To<PhysiciansAssistantDto>()
                    .FirstOrDefaultAsync(x => x.UserId == userId);

                item = await _context
                .Physicians
                .Include("Patients")
                .FirstOrDefaultAsync(x => x.DeletedOn == null && x.Id == assist.PhysicianId);
            }

            if (item == null)
            {
                return new PhysicianDto { NotAssigned = true };
            }

            item.Patients = item.Patients.Where(x => x.DeletedOn == null).ToList();

            var dto = Mapper.Map<PhysicianDto>(item);

            // TODO: refactor
            dto = dto.LoadDiscipline(_context)
                .LoadContacts(_context)
                .LoadImages(_context);

            return dto;
        }

        public async Task<PagedQueryResults<MasterTemplateExternalDto>> GetPhysicianPlans(string userId, PagingParameters paging)
        {
            PhysicianDto physician;
            if (paging.ProxyId != 0)
            {
                physician = await GetPhysician(paging.ProxyId);
            }
            else
            {
                physician = await _context.GetPhysicianUser(userId);
            }

            var query = _context.NodeLibraries
                .Project().To<MasterTemplateExternalDto>()
                .Where(lib =>
                    lib.DeletedOn == null &&
                    lib.IsNew == false &&
                    lib.Active &&
                    lib.PhysicianId == physician.Id &&
                    lib.LibraryType == Constants.NodeLibraryTypes.MasterTemplate);

            var libraries = await query.ToListAsync();

            var totalRecords = libraries.Count;

            var dtos = libraries
                .ToList();

            var response = new PagedQueryResults<MasterTemplateExternalDto>(dtos, totalRecords);

            try
            {
                var libIds = dtos.Select(l => l.Id).ToList();

                var sql =
        @"select p.NodeLibraryid, count(p.NodeLibraryId) as Count
from Procedures p 
join NodeLibraries l on p.NodeLibraryid = l.id
where p.NodeLibraryId in @LibraryIds AND p.DeletedOn is null AND p.IsNew = 0 
and StartDate <= GetDate() and DATEADD(day, l.Duration, p.StartDate) >= GetDate()
group by p.NodeLibraryId";

                var cn = _context.Database.Connection;
                cn.Open();
                var counts = cn.Query<LibraryCount>(sql, new { LibraryIds = libIds });
                cn.Close();

                if (counts != null)
                {
                    foreach (var count in counts)
                    {
                        var lib = dtos.First(d => d.Id == count.NodeLibraryId);
                        lib.PlanCount = count.Count;
                    }
                }
            }
            catch { }


            return response;
        }

        public async Task<List<MasterTemplateExternalDto>> GetAllPhysicianPlans(string userId)
        {
            if (userId.ToLower() == "new")
            {
                return new List<MasterTemplateExternalDto>();
            }

            var physician = await _context.GetPhysicianUser(userId);

            var query = _context.NodeLibraries
                .Project().To<MasterTemplateExternalDto>()
                .Where(lib =>
                    lib.DeletedOn == null &&
                    lib.IsNew == false &&
                    lib.Active &&
                    lib.PhysicianId == physician.Id);//&&
                                                     //lib.LibraryType == Constants.NodeLibraryTypes.MasterTemplate);

            var libraries = await query.ToListAsync();

            return libraries.ToList();
        }

        public async Task<PagedQueryResults<PatientDto>> GetPhysicianPatients(string userId, PagingParameters paging)
        {
            PhysicianDto physician;

            if (paging.ProxyId != 0)
            {
                physician = await GetPhysician(paging.ProxyId);
            }
            else
            {
                physician = await _context.GetPhysicianUser(userId);
            }

            var query = _context.Patients
                .Project().To<PatientDto>()
                .Where(p =>
                    p.DeletedOn == null &&
                    p.PhysicianId == physician.Id
                );

            if (!string.IsNullOrEmpty(paging.Filter))
            {
                var terms = paging.Filter.Split(' ').ToArray();
                query = query.Where(p => terms.Any(term =>
                    p.MedicalRecordNumber.Contains(term) ||
                    p.FirstName.Contains(term) ||
                    p.LastName.Contains(term) ||
                    p.Email.Contains(term)));
            }

            query = query.OrderByDescending(p => p.Id);

            var libraries = await query.ToListAsync();

            var totalRecords = libraries.Count;

            var patients = libraries
                .Skip(paging.Offset * paging.Limit)
                .Take(paging.Limit)
                .ToList();


            return new PagedQueryResults<PatientDto>(patients, totalRecords);
        }

        public async Task<PhysiciansAssistantDto> GetPhysicianAssistant(int id)
        {
            var item = await _context.PhysiciansAssistants
                .FirstOrDefaultAsync(x => x.DeletedOn == null && x.Id == id);

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

            var dto = Mapper.Map<PhysiciansAssistantDto>(item);

            dto.UserName = user != null ? user.UserName : string.Empty;

            dto.LoadContacts(_context);

            return dto;
        }

        public async Task<PatientDto> GetPatient(int patientId)
        {
            var patient = await _context.Patients
                .Project().To<PatientDto>()
                .FirstOrDefaultAsync(p => p.Id == patientId);

            return patient;
        }

        public async Task<PatientDto> GetPhysicianPatient(string userId, int patientId)
        {
            var patient = await _context.Patients
                .Project().To<PatientDto>()
                .FirstOrDefaultAsync(p => p.Id == patientId);

            return patient;
        }

        public async Task<PatientDto> CreatePhysicianPatient(string userId, PatientDto model, bool omitSetupInstructions = false)
        {
            PhysicianDto physicianUser;

            if (model.ProxyId != 0)
            {
                physicianUser = await GetPhysician(model.ProxyId);
            }
            else
            {
                physicianUser = await _context.GetPhysicianUser(userId);
            }

            var physician = await _context.Physicians.FirstAsync(p => p.UserId == physicianUser.UserId && p.Id == physicianUser.Id);
            var company = await _context.Companies.FirstOrDefaultAsync(c => c.Id == physician.CompanyId);

            var existingPatient = await _context.Patients
                .FirstOrDefaultAsync(p =>
                    p.DeletedOn == null &&
                    p.MedicalRecordNumber == model.MedicalRecordNumber &&
                    p.PhysicianId == physician.Id);

            Patient patient;

            if (existingPatient != null)
            {
                patient = existingPatient;
            }
            else
            {
                patient = new Patient
                {
                    Age = model.Age,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    MiddleInitial = model.MiddleInitial,
                    Phone = model.Phone,
                    PhysicianId = physicianUser.Id,
                    CompanyId = physician.CompanyId,
                    MedicalRecordNumber = model.MedicalRecordNumber
                };

                string generated;
                bool isUnique;

                do
                {
                    generated = AccessCodeService.Generate();

                    var count = _context.Patients.Count(p => p.Identifier == generated);

                    isUnique = count == 0;
                } while (!isUnique);

                patient.Identifier = generated;

                if (string.IsNullOrWhiteSpace(patient.Phone))
                {
                    patient.Phone = model.Phone;
                }

                _auditService.Audit(patient);

                _context.Patients.Add(patient);

                await _context.SaveChangesAsync();
            }

            model.Id = patient.Id;

            if (model.Procedure != null && model.Procedure.ProcedureId > 0)
            {
                model.Procedure.PatientId = patient.Id;
                model.Procedure.PhysicianId = physician.Id;
                // Suppress notifications
                await _procedureService.AddPatientProcedure(userId, model.Procedure, true);
            }

            if (string.IsNullOrWhiteSpace(patient.Phone))
            {
                patient.Phone = model.Phone;
            }

            if (!omitSetupInstructions)
            {
                SendAppInstructions(patient, model.Procedure.AccessCode, company.Name, model.SendEmail, model.SendSms);
            }

            return model;
        }

        private void SendAppInstructions(Patient patient, string accessCode, string practiceName, bool sendEmail, bool sendSms)
        {
            try
            {
                var request = new ProcedureCreatedRequest
                {
                    FirstName = patient.FirstName ?? "",
                    PhoneNumber = patient.Phone ?? "",
                    AccessCode = accessCode ?? "",
                    PracticeName = practiceName ?? "",
                    Recipients = new List<string> { patient.Email ?? "" }
                };

                _messageProvider.SendProcedureCreated(request, sendEmail, sendSms);
            }
            catch
            {
                // 
            }
        }

        public async Task UpdatePhysicianPatient(PatientDto model)
        {
            var patient = await _context.Patients.FirstAsync(p => p.Id == model.Id);

            if (patient != null)
            {
                patient.Age = model.Age;
                patient.Email = model.Email;
                patient.FirstName = model.FirstName;
                patient.LastName = model.LastName;
                patient.MedicalRecordNumber = model.MedicalRecordNumber;
                patient.MiddleInitial = model.MiddleInitial;
                patient.Phone = model.Phone;

                _auditService.Audit(patient);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<PatientLibraryDto>> GetPatientLibraries(int patientId)
        {
            var procedures = await _context.Procedures
                .AsNoTracking()
                .Project().To<PatientLibraryDto>()
                .Where(p => p.DeletedOn == null && p.PatientId == patientId)
                .ToListAsync();

            return procedures;
        }

        public async Task<List<FacilityDto>> GetFacilities(string userId, int? proxyId)
        {
            PhysicianDto physician;

            if (proxyId.HasValue && proxyId != 0)
            {
                physician = await GetPhysician(proxyId.Value);
            }
            else
            {
                physician = await _context.GetPhysicianUser(userId);
            }

            if (!physician.CompanyId.HasValue)
            {
                return new List<FacilityDto>();
            }

            var company = await _context.Companies
                .AsNoTracking()
                .FirstAsync(co => co.Id == physician.CompanyId.Value);

            var facilities = await _context.Facilities
                .AsNoTracking()
                .Project().To<FacilityDto>()
                .Where(f => f.CompanyId == company.Id && f.DeletedOn == null && f.IsNew == false)
                .ToListAsync();

            var remoteIds = _context.RemoteFacilities
                .Where(rf => rf.CompanyId == physician.CompanyId)
                .Select(rf => rf.FacilityId)
                .ToList();

            var remotes = await _context.Facilities
                .AsNoTracking()
                .Project().To<FacilityDto>()
                .Where(c => remoteIds.Contains(c.Id) && c.DeletedOn == null && c.IsNew == false)
                .ToListAsync();

            facilities.AddRange(remotes);

            return facilities;

        }

        public async Task<MasterTemplateExternalDto> GetPlanForCptCode(string userId, string id, int? proxyId)
        {
            PhysicianDto physician;

            if (proxyId.HasValue && proxyId.Value != 0)
            {
                physician = await GetPhysician(proxyId.Value);
            }
            else
            {
                physician = await _context.GetPhysicianUser(userId);
            }

            var library = await _context.NodeLibraries
                .Project().To<MasterTemplateExternalDto>()
                .FirstOrDefaultAsync(lib =>
                    lib.DeletedOn == null &&
                    lib.IsNew == false &&
                    lib.Active &&
                    lib.PhysicianId == physician.Id &&
                    lib.LibraryType == Constants.NodeLibraryTypes.MasterTemplate &&
                    lib.CptCodes.Any(cpt => cpt.Code == id));

            return library;
        }

        public async Task RemovePhysicianPatient(int id)
        {
            var patient = await _context.Patients.FirstOrDefaultAsync(p => p.Id == id);

            if (patient != null)
            {
                patient.DeletedOn = DateTime.Now;

                var procedures = await _context.Procedures.Where(p => p.PatientId == patient.Id).ToListAsync();

                foreach (var procedure in procedures)
                {
                    procedure.DeletedOn = DateTime.Now;
                }

                await _context.SaveChangesAsync();

            }
        }

        public async Task RemoveProcedure(int id)
        {
            var procedure = await _context.Procedures.FirstAsync(p => p.Id == id);

            procedure.DeletedOn = DateTime.Now;
            procedure.AccessCode = $"{procedure.AccessCode}|{Guid.NewGuid()}";
            if (procedure.AccessCode.Length > 255)
            {
                procedure.AccessCode = procedure.AccessCode.Substring(0, 255);
            }
            await _context.SaveChangesAsync();
        }

        public async Task<PatientDto> GetPatientByMrn(string mrn, string userId, int? proxyId)
        {
            PhysicianDto physician;

            if (proxyId.HasValue && proxyId != 0)
            {
                physician = await GetPhysician(proxyId.Value);
            }
            else
            {
                physician = await _context.GetPhysicianUser(userId);
            }

            var patient = _context.Patients
                .Project().To<PatientDto>()
                .FirstOrDefault(p => p.PhysicianId == physician.Id &&
                                    p.MedicalRecordNumber.Contains(mrn) &&
                                    p.DeletedOn == null);

            return patient;
        }

        public async Task<PhysiciansAssistantDto> AddPhysiciansAssistant(int practiceId, int physicianId = 0)
        {
            #region 1. get practice
            var practice = await _context.Companies.FirstOrDefaultAsync(x => x.Id == practiceId);
            Physician physician = null;
            if (physicianId > 0)
            {
                physician = await _context.Physicians.FirstOrDefaultAsync(x => x.Id == physicianId);
            }

            #endregion

            #region 2. build new phys assistant
            var data = new PhysiciansAssistant
            {
                CompanyId = practiceId,
                IsProvisional = true,
                ProvisionalKey = Guid.NewGuid(),
            };
            if (physicianId > 0)
            {
                data.PhysicianId = physicianId;
            }
            data.Audit(_auditService);
            #endregion

            #region 3. save 
            _context.Entry(data).State = EntityState.Added;
            await _context.SaveChangesAsync();
            #endregion

            #region 4. convert and return
            var dto = Mapper.Map<PhysiciansAssistantDto>(data);
            dto.Physician = Mapper.Map<PhysicianDto>(physician);
            dto.Practice = Mapper.Map<CompanyDto>(practice);

            dto.SaveContacts(_context, _auditService);
            #endregion

            return dto;
        }

        public async Task<PhysiciansAssistantDto> UpdatePhysiciansAssistant(PhysiciansAssistantDto pa)
        {
            var old = await _context
                .PhysiciansAssistants
                .FirstOrDefaultAsync(x => x.Id == pa.Id);

            #region 1. get the user if exists and update first and last name
            if (!string.IsNullOrEmpty(pa.UserId))
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == pa.UserId);

                if (user != null)
                {
                    user.Firstname = pa.FirstName;
                    user.Lastname = pa.LastName;
                }
            }
            #endregion

            #region 2. update the old phys assistant
            var data = Mapper.Map<PhysiciansAssistant>(pa);
            if (pa.Physician != null)
            {
                data.PhysicianId = pa.Physician.Id;
            }
            data.Audit(_auditService);

            _context.Entry(old).State = EntityState.Detached;
            _context.Entry(data).State = EntityState.Modified;
            #endregion

            #region 2.5 save
            await _context.SaveChangesAsync();
            #endregion

            #region 3. convert back to dto and update contacts
            var dto = Mapper.Map<PhysiciansAssistantDto>(data);
            dto = dto.SaveContacts(_context, _auditService);
            #endregion

            return dto;

        }
    }


    public class LibraryCount
    {
        public int NodeLibraryId { get; set; }
        public int Count { get; set; }
    }
}
