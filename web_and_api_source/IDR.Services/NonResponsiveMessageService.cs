﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text.RegularExpressions;
using IDR.Data;
using IDR.Data.CallCenter;

namespace IDR.Services
{
    public class NonResponsiveMessageService
    {
        private ApplicationDbContext _db;

        public NonResponsiveMessageService(ApplicationDbContext db)
        {
            _db = db;
        }

        public void SendNonResponsiveWarning(CallCenterProcedureTimer timer)
        {
            timer.NotificationSent = true;

            var template = _db.EmailTemplates.FirstOrDefault(t => t.Alias == "timerelapsed");
            var content = template?.Content ?? "";
            var ccProc = _db.CallCenterProcedures.FirstOrDefault(p => p.Id == timer.CallCenterProcedureId);
            var proc = _db.Procedures.FirstOrDefault(p => p.Id == timer.ProcedureId);
            var ccPatient = _db.CallCenterPatients.FirstOrDefault(p => p.CallCenterProcedureId == ccProc.Id);
            var patient = _db.Patients.FirstOrDefault(p => p.Id == ccPatient.PatientId);
            var facility = _db.Facilities.FirstOrDefault(c => c.Id == proc.FacilityId);

            var physician = _db.Physicians.FirstOrDefault(p => p.Id == proc.PhysicianId);
            var practice = _db.Companies.FirstOrDefault(c => c.Id == physician.CompanyId);
            var ccRef = _db.CallCenterPracticeReferences.FirstOrDefault(c => c.PracticeId == practice.Id);

            var phone = patient?.Phone ?? "0000000000";
            var digitsOnly = new Regex(@"[^\d]");
            var cleanPhone = digitsOnly.Replace(phone, "");
            var formattedPhone = "0000000000";

            if (!string.IsNullOrWhiteSpace(cleanPhone))
            {
                try
                {
                    formattedPhone = Convert.ToInt64(cleanPhone).ToString("(###) ###-####");
                }
                catch { }
            }

            var ccv = "";
            try
            {
                var val = timer?.CCV;
                ccv = string.Format("{0:0.00}", val);
            }
            catch { }

            var adjustedStartDate = "";

            try
            {
                // "{{InfusionDate}} is Start Date formatted for local time
                if (timer != null && practice != null)
                {
                    var dateService = new DateService();
                    var localDate = dateService.ToLocalTime(timer.Start, practice.TimeZone);

                    adjustedStartDate = localDate.ToString("MM-dd-yyyy hh:mm tt");
                    adjustedStartDate += " " + practice.TimeZone;
                }
            }
            catch
            {
                //TODO: Handle date/time if not found in the DB.
            }

            var email = content
                .Replace("{{PatientFirstName}}", patient?.FirstName ?? "")
                .Replace("{{PatientLastName}}", patient?.LastName ?? "")
                .Replace("{{PatientPhone}}", formattedPhone)
                .Replace("{{FacilityName}}", facility?.Name ?? "")
                //.Replace("{{InfusionDate}}", timer?.CreatedOn?.ToString("MM-dd-yyyy hh:mm tt") ?? "" + " UCT")
                .Replace("{{InfusionDate}}", adjustedStartDate)
                .Replace("{{Volume}}", ccProc?.InfuVolume.ToString() ?? "")
                .Replace("{{Rate}}", timer?.Rate.ToString() ?? "")
                .Replace("{{CCV}}", ccv)
                .Replace("{{RRV}}", timer?.RecordedVolume.ToString() ?? "")
                .Replace("{{Duration}}", timer?.Duration.ToString() ?? "")
                .Replace("{{ActualDuration}}", timer?.ActualDuration.ToString() ?? "")
                .Replace("{{Pump}}", ccProc?.InfuPumpModel ?? "")
                .Replace("{{PracticeCode}}", ccRef?.PracticeCode ?? "")
                .Replace("{{TreatmentPlan}}", ccProc?.InfuTreatmentPlan ?? "")
                ;

            SmtpSection smtpSection = (SmtpSection)ConfigurationManager.GetSection("mailSettings/smtp_2");
            var recipient = ConfigurationManager.AppSettings["RrvEmailRecipient"];
            var client = new SmtpClient(smtpSection.Network.Host, smtpSection.Network.Port);
            client.EnableSsl = smtpSection.Network.EnableSsl;
            client.UseDefaultCredentials = smtpSection.Network.DefaultCredentials;
            client.DeliveryMethod = smtpSection.DeliveryMethod;
            client.Credentials = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
            var mail = new MailMessage(smtpSection.From, recipient)
            {
                IsBodyHtml = true,
                Subject = "Infusion Timer Alert Message <2129>",
                Body = email
            };
            client.Send(mail);
        }
    }
}
