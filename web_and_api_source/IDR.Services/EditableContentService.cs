﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using IDR.Data;

namespace IDR.Services
{
    public class EditableContentService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;

        public EditableContentService(ApplicationDbContext context, IAuditService auditService)
        {
            _context = context;
            _auditService = auditService;
        }

        public List<EditableContent> GetContent()
        {
            return _context.EditableContent
                .Where(c => !c.IsNew )
                .OrderBy(c => c.Sequence)
                .ToList();
        }

        public EditableContent GetContent(int id)
        {
            return _context.EditableContent.FirstOrDefault(c => c.Id == id);
        }

        public EditableContent CreateContent(EditableContent model)
        {
            var highest = _context.EditableContent.OrderByDescending(c => c.Sequence).FirstOrDefault();
            var sequence = 0;

            if (highest != null)
            {
                sequence = highest.Sequence + 1;
            }

            model.Title = "New Page Content";
            model.Sequence = sequence;
            _auditService.Audit(model);
            _context.EditableContent.AddOrUpdate(model);
            _context.SaveChanges();
            return model;
        }

        public EditableContent UpdateContent(EditableContent model)
        {
            _auditService.Audit(model);
            model.IsNew = false;
            _context.EditableContent.AddOrUpdate(model);
            _context.SaveChanges();
            return model;
        }
    }
}
