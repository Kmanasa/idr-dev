﻿using System.Threading.Tasks;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Cms.Dto;

namespace IDR.Services
{
	public class CmsService
	{
		private readonly IAuditService _auditService;
		private readonly ApplicationDbContext _db;

		public CmsService(ApplicationDbContext db, IAuditService auditService)
		{
			_auditService = auditService;
			_db = db;
		}

		public async Task<AttributeDto> AddAttribute()
		{
			var attr = new Data.Attribute
			{
				Name = "New Attribute",
				IsNew = true
			};
			attr = _db.Attributes.Add(attr);
			attr.Audit(_auditService);
			_auditService.Audit(attr);

			await _db.SaveChangesAsync();

			return Mapper.Map<AttributeDto>(attr);
		}
	}
}
