﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.PracticeUsers.Dto;

namespace IDR.Services
{
    public class SchedulersService
    {
        private readonly ApplicationDbContext __context;
        private readonly IAuditService _auditService;

        public SchedulersService(ApplicationDbContext _context,IAuditService auditService)
        {
            __context = _context;
            _auditService = auditService;
        }

        public async Task<SchedulerDto> GetScheduler(string key)
        {
            var item = await __context.Schedulers
                .FirstOrDefaultAsync(x => x.DeletedOn == null && 
                !string.IsNullOrEmpty(key) && x.ProvisionalKey.ToString() == key);

            var user = await __context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

            var dto = Mapper.Map<SchedulerDto>(item);

            dto.UserName = user != null ? user.UserName : string.Empty;

            dto.LoadContacts(__context);

            return dto;
        }

        public async Task<SchedulerDto> GetScheduler(int id)
        {
            var item = await __context.Schedulers
                .FirstOrDefaultAsync(x => x.DeletedOn == null &&
                x.Id == id);

            var user = await __context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

            var dto = Mapper.Map<SchedulerDto>(item);

            dto.UserName = user != null ? user.UserName : string.Empty;

            dto.LoadContacts(__context);

            return dto;
        }

        public async Task<SchedulerDto> Create(int practiceId)
        {
            var practice = await __context.Companies.FirstOrDefaultAsync(x => x.Id == practiceId);
            var data = new Scheduler
            {
                CompanyId = practiceId,
                IsProvisional = true,
                ProvisionalKey = Guid.NewGuid(),
            };
            data.Audit(_auditService);

            __context.Entry(data).State = EntityState.Added;
            data.IsProvisional = true;

            await __context.SaveChangesAsync();

            var dto = Mapper.Map<SchedulerDto>(data);
            dto.Practice = Mapper.Map<CompanyDto>(practice);
            dto.SaveContacts(__context, _auditService);

            return dto;
        }

        public async Task<SchedulerDto> Delete(int id)
        {
            var data = await __context.Schedulers.FirstOrDefaultAsync(x => x.Id == id);

            data.DeletedOn = DateTime.Now;
            _auditService.Audit(data);
            await __context.SaveChangesAsync();

            return Mapper.Map<SchedulerDto>(data);
        }

        public async Task<SchedulerDto> Update(SchedulerDto scheduler)
        {
            var existing = await __context
                .Schedulers
                .FirstOrDefaultAsync(x => x.Id == scheduler.Id);

            if (!string.IsNullOrEmpty(scheduler.UserId))
            {
                var user = await __context.Users.FirstOrDefaultAsync(x => x.Id == scheduler.UserId);

                if (user != null)
                {
                    user.Firstname = scheduler.FirstName;
                    user.Lastname = scheduler.LastName;
                }
            }

            var admin = Mapper.Map<Scheduler>(scheduler);
            admin.Audit(_auditService);

            __context.Entry(existing).State = EntityState.Detached;
            __context.Entry(admin).State = EntityState.Modified;

            await __context.SaveChangesAsync();

            var dto = Mapper.Map<SchedulerDto>(admin);
            dto = scheduler.SaveContacts(__context, _auditService);

            return dto;
        }
    }
}
