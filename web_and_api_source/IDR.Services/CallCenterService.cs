﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Dapper;
using IDR.Data;
using IDR.Data.CallCenter;
using IDR.Domain;
using IDR.Domain.CallCenter;
using IDR.Domain.Portal.Dto;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.Services.Models.CallCenter;
using IDR.web.Models;
using MvcSiteMapProvider.Reflection;
using Stimulsoft.Report;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace IDR.Services
{
    public class CallCenterService
    {
        private readonly ApplicationDbContext _db;
        private readonly PhysiciansService _physiciansService;
        private readonly DateService _dateService;
        private readonly IAuditService _auditauditService;
        private readonly INewProcedureMessageProvider _messageProvider;
        private static readonly string Sid = ConfigurationManager.AppSettings["TwilioSid"];
        private static readonly string Token = ConfigurationManager.AppSettings["TwilioToken"];

        public CallCenterService(ApplicationDbContext db,
            PhysiciansService physiciansService,
            DateService dateService,
            IAuditService auditauditService, INewProcedureMessageProvider messageProvider)
        {
            _db = db;
            _physiciansService = physiciansService;
            _dateService = dateService;
            _auditauditService = auditauditService;
            _messageProvider = messageProvider;
        }

        public async Task<List<CallCenterDto>> GetCallCenters()
        {
            var ccs = await _db.CallCenters
                .ProjectTo<CallCenterDto>()
                .ToListAsync();

            return ccs;
        }

        public async Task<CallCenterRefDto> GetCallCenterRefs(string userId)
        {
            // Get the user
            var user = _db.Users.FirstOrDefault(u => u.Id == userId);

            var facilities = await GetFacilities(user);
            var payors = await GetPayors();

            var providersQuery = @"
SELECT 
	ccp.Id
	, ccp.ProviderType
	, ccp.FirstName
	, ccp.LastName
	, ccp.Title
	, ccpf.Id as FID 
	, ccpf.FacilityId 
	
FROM
CallCenterProviderFacilities ccpf
INNER JOIN CallCenterProviders 
	ccp ON ccpf.CallCenterProviderId = ccp.Id
";

            var cn = _db.Database.Connection;
            cn.Open();
            var providers = (await cn.QueryAsync<CallCenterProviderRef>(providersQuery)).ToList();

            var physicians = new List<CallCenterProvider>();
            var anes = new List<CallCenterProvider>();
            var nurses = new List<CallCenterProvider>();

            foreach (var p in providers)
            {
                var provider = new CallCenterProvider
                {
                    Id = p.Id,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    ProviderType = p.ProviderType,
                    Suffix = p.Suffix,
                    Title = p.Title,
                    CallCenterProviderFacilities = providers.Where(f => f.Id == p.Id)
                        .Select(f => new CallCenterProviderFacility
                        {
                            Id = f.FID,
                            FacilityId = f.FacilityId
                        }).ToList()
                };

                switch (p.ProviderType)
                {
                    case ProviderType.Anesthesiologist:
                        if (anes.All(a => a.Id != p.Id))
                        {
                            anes.Add(provider);
                        }
                        break;

                    case ProviderType.Physician:
                        if (physicians.All(a => a.Id != p.Id))
                        {
                            physicians.Add(provider);
                        }
                        break;

                    case ProviderType.Nurse:
                        if (nurses.All(a => a.Id != p.Id))
                        {
                            nurses.Add(provider);
                        }
                        break;

                }
            }

            cn.Close();

            return new CallCenterRefDto
            {
                Facilities = facilities,
                Payors = payors,
                Physicians = physicians,
                Nurses = nurses,
                Anesthesiologists = anes
            };
        }

        #region Patients
        public async Task<PagedQueryResults<CallCenterPatientDto>> GetPatients(string userId, CallCenterPatientSearchModel filter, bool forManagePatients = false)
        {
            if (filter == null)
            {
                filter = new CallCenterPatientSearchModel();
            }

            // Get the user
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                return new PagedQueryResults<CallCenterPatientDto>(new List<CallCenterPatientDto>(), 0);
            }

            var practiceId = await RequiresPracticeFilter(userId);

            var sql = GetPatientsQuery(practiceId.HasValue, filter, forManagePatients);
            var countSql = GetPatientsCountQuery(practiceId.HasValue, filter, forManagePatients);

            var cn = _db.Database.Connection;
            cn.Open();
            var patients = (await cn.QueryAsync<CallCenterPatientDto>(sql, new
            {
                CompanyId = practiceId,
                PumpSerial = filter.GetFilter(),
                FirstName = filter.GetFilter(),
                LastName = filter.GetFilter(),
                Phone = filter.GetFilter(),
                filter.Paging.Offset,
                filter.Paging.Limit
            })).ToList();

            var count = cn.ExecuteScalar<int>(countSql, new
            {
                CompanyId = practiceId,
                PumpSerial = filter.GetFilter(),
                FirstName = filter.GetFilter(),
                LastName = filter.GetFilter(),
                Phone = filter.GetFilter(),
                filter.Paging.Offset,
                filter.Paging.Limit
            });

            cn.Close();

            return new PagedQueryResults<CallCenterPatientDto>(patients, count);
        }

        private async Task<int?> RequiresPracticeFilter(string userId)
        {
            var callSiteAdmin = await _db.CallSiteAdmins.FirstOrDefaultAsync(sa => sa.UserId == userId);
            var callSiteMobile = await _db.CallSiteMobileUsers.FirstOrDefaultAsync(sa => sa.UserId == userId);

            //var requiresFacilityFilter = false;
            int? practiceId = null;

            if (callSiteAdmin != null)
            {
                //requiresFacilityFilter = true;
                if (callSiteAdmin.CompanyId.HasValue)
                {
                    practiceId = callSiteAdmin.CompanyId.Value;
                }
            }
            if (callSiteMobile != null)
            {
                // = true;
                if (callSiteMobile.CompanyId.HasValue)
                {
                    practiceId = callSiteMobile.CompanyId.Value;
                }
            }

            return practiceId;
        }

        public async Task<CallCenterPatientDto> GetPatient(int id)
        {
            var sql = GetPatientQuery();

            var cn = _db.Database.Connection;
            cn.Open();
            var patient = (await cn.QueryAsync<CallCenterPatientDto>(sql, new { Id = id })).First();
            cn.Close();

            patient.StartTime = patient.StartDate;

            return patient;
        }

        public async Task<CallCenterPatientDto> CreatePatient(CallCenterPatientDto patient,
            bool preventDefaultPhysicain = false, bool sendSms = false, bool preventCallCenterSchedule = false)
        {
            var cpt = await _db.CptCodes.FirstAsync(c => c.Id == patient.CptId);
            var lib = await _db.NodeLibraries.FirstAsync(l => l.Id == cpt.NodeLibraryId); 
            var physician = await _db.Physicians.FirstAsync(p => p.Id == lib.PhysicianId);
            var practice = await _db.Companies
                .Include(p => p.Facilities)
                .FirstAsync(c => c.Id == patient.FacilityId);

            var facility = practice.Facilities.First();

            #region Patient
            var patientDto = new PatientDto
            {
                Age = patient.Age,
                Email = patient.Email,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                MiddleInitial = patient.MiddleInitial,
                Phone = patient.Phone,
                MedicalRecordNumber = DateTime.Now.Ticks.ToString(),
                // SendSms = true,

                Procedure = new PatientProcedureDto
                {
                    ProcedureId = lib.Id,
                    FacilityId = facility.Id,
                    StartDate = patient.StartDate,
                    StartTime = patient.StartTime,
                    SendSms = sendSms
                }
            };

            // suppress notifications
            var patientModel = await _physiciansService.CreatePhysicianPatient(physician.Id.ToString(), patientDto, true);
            #endregion

            // Create the call center counterpart
            var ccRef = await _db.CallCenterPracticeReferences.FirstAsync(c => c.PracticeId == practice.Id);

            var callCenter = await _db.CallCenters.FirstAsync(cc => cc.Id == ccRef.CallCenterId);

            #region Procedure

            var proc = new CallCenterProcedure
            {
                CallCenterId = callCenter.Id,
                ProcedureId = patientModel.Procedure.Id,
                CallCenterPayorId = patient.PayorId,
                PhysicianId = patient.PhysicianId == 0 ? (int?)null : patient.PhysicianId,
                AnesthesiologistId = patient.AnesthesiologistId == 0 ? (int?)null : patient.AnesthesiologistId,
                NurseId = patient.NurseId == 0 ? (int?)null : patient.NurseId,
                PumpSerial = patient.PumpSerial,
                PumpSerial2 = patient.PumpSerial2,
                IsDualCath = patient.IsDualCath,

                Cancelled = patient.Cancelled,
                Completed = patient.Completed,
                NoMonitoring = patient.NoMonitoring,
                DoNotDisturb = patient.DoNotDisturb,
                PracticeFacilityId = practice.Id,
                CptId = patient.CptId,
                SpanishSpeaking = patient.SpanishSpeaking,
                Outpatient = patient.Outpatient,

                InitialContactDate = DateTime.UtcNow,
                DiscontinueDate = DateTime.UtcNow,
                IsNew = false,

                InfuVolume = patient.InfuVolume,
                InfuRate = patient.InfuRate,
                InfuPumpModel = patient.InfuPumpModel,
                InfuHaveConsent = patient.InfuHaveConsent,
                InfuCck = patient.InfuCck,
                InfuTreatmentPlan = patient.InfuTreatmentPlan,
                InfuImSafetyFeatures = patient.InfuImSafetyFeatures
            };

            #endregion


            #region Call Schedule
            int counter = 0;
            if (!preventCallCenterSchedule)
            {
                var calls = new List<CallCenterScheduledCall>();
                var index = 1;

                var normalizedDate = _dateService.NormalizeDay(patientModel.Procedure.StartDate.Value);

                for (var i = ccRef.StartCalls; i < ccRef.EndCalls + 1; i++)
                {
                    // Passive sites only create a call on the last day
                    if (ccRef.Passive && i != ccRef.EndCalls)
                    {
                        continue;
                        ;
                    }

                    // Adjusted date for Year, Month, Day
                    var date = normalizedDate;

                    // Increment date according to Outpatient value set
                    counter = i;
                    if (patient.Outpatient)
                    {
                         date = normalizedDate.AddDays(i+1);
                         counter = i + 1;
                    }
                    else
                    {
                         date = normalizedDate.AddDays(i+2);
                         counter = i + 2;
                    }

                    calls.Add(new CallCenterScheduledCall
                    {
                        Index = index++,
                        CallDay = date.Day,
                        CallMonth = date.Month,
                        CallYear = date.Year,
                        CallStatus = CallStatus.Scheduled,
                        // Actual call time UTC
                        CallTime = patientModel.Procedure.StartDate.Value.AddDays(counter)
                    });
                }

                proc.CallCenterScheduledCalls.AddRange(calls);
            }

            #endregion

            proc.Audit(_auditauditService);

            if (preventDefaultPhysicain)
            {
                proc.PhysicianId = null;
                proc.Physician = null;
            }

            proc.DeliveredAoBDate = DateTime.MaxValue;
            _db.CallCenterProcedures.Add(proc);

            _db.SaveChanges();

            var pat = new CallCenterPatient
            {
                IsNew = false,
                AltContact = patient.AltContact,
                AltPhone = patient.AltPhone,
                Gender = patient.Gender,
                CallCenterProcedureId = proc.Id,
                PatientId = patientModel.Id,
                Mobile = patient.Mobile,
                InfuPatientId = patient.InfuPatientId
            };

            pat.Audit(_auditauditService);
            _db.CallCenterPatients.Add(pat);
            _db.SaveChanges();

            patient.Id = patientModel.Id;
            patient.PracticeName = practice.Name;
            patient.AccessCode = patientModel.Procedure.AccessCode;

            var newPatient = await _db.Patients.FirstAsync(p => p.Id == patient.Id);

            SendAppInstructions(newPatient, patient.AccessCode, practice.Name, patient.SendEmailChecked, patient.SendSmsChecked);

            return patient;
        }

        private void SendAppInstructions(Patient patient, string accessCode, string practiceName, bool sendEmail, bool sendSms)
        {
            try
            {
                var request = new ProcedureCreatedRequest
                {
                    FirstName = patient.FirstName ?? "",
                    PhoneNumber = patient.Phone ?? "",
                    AccessCode = accessCode ?? "",
                    PracticeName = practiceName ?? "",
                    Recipients = new List<string> { patient.Email ?? "" }
                };

                _messageProvider.SendProcedureCreated(request, sendEmail, sendSms);
            }
            catch (Exception ex)
            {
                //
            }
        }

        public async Task<CallCenterPatientDto> UpdatePatient(CallCenterPatientDto patient,
            bool skipPhysician = false, bool preventCallCenterSchedule = false)
        {
            patient.StartDate = _dateService.NormalizeDay(patient.StartDate);
            patient.StartDate = patient.StartDate
                .AddHours(patient.StartTime.Hour)
                .AddMinutes(patient.StartTime.Minute);

            var cpt = await _db.CptCodes.FirstAsync(c => c.Id == patient.CptId);
            var lib = await _db.NodeLibraries.FirstAsync(l => l.Id == cpt.NodeLibraryId);
            var practice = await _db.Companies
                .Include(p => p.Facilities)
                .FirstAsync(c => c.Id == patient.FacilityId);

            var patientDto = new PatientDto
            {
                Id = patient.PatientId,
                Age = patient.Age,
                Email = patient.Email,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                MiddleInitial = patient.MiddleInitial,
                Phone = patient.Phone
            };

            var procedure = _db.Procedures.First(p => p.PatientId == patient.PatientId);
            procedure.NodeLibraryId = lib.Id;

            await _physiciansService.UpdatePhysicianPatient(patientDto);

            var pat = _db.CallCenterPatients.First(p => p.Id == patient.Id);
            var proc = pat.CallCenterProcedure;

            proc.CallCenterPayorId = patient.PayorId;

            if (!skipPhysician)
            {
                proc.PhysicianId = patient.PhysicianId == 0 ? (int?)null : patient.PhysicianId;
            }

            proc.AnesthesiologistId = patient.AnesthesiologistId == 0 ? (int?)null : patient.AnesthesiologistId;
            proc.NurseId = patient.NurseId == 0 ? (int?)null : patient.NurseId;

            proc.PumpSerial = patient.PumpSerial;
            proc.PumpSerial2 = patient.PumpSerial2;
            proc.IsDualCath = patient.IsDualCath;

            proc.Cancelled = patient.Cancelled;
            proc.Completed = patient.Completed;
            proc.NoMonitoring = patient.NoMonitoring;
            proc.DoNotDisturb = patient.DoNotDisturb;
            proc.PracticeFacilityId = practice.Id;
            proc.CptId = patient.CptId;
            proc.SpanishSpeaking = patient.SpanishSpeaking;
            proc.Outpatient = patient.Outpatient;

            proc.InitialContactDate = DateTime.UtcNow;
            proc.DiscontinueDate = DateTime.UtcNow;

            pat.AltContact = patient.AltContact;
            pat.AltPhone = patient.AltPhone;
            pat.Gender = patient.Gender;
            pat.CallCenterProcedureId = proc.Id;
            pat.Mobile = patient.Mobile;

            proc.InfuVolume = patient.InfuVolume;
            proc.InfuCck = patient.InfuCck;
            proc.InfuHaveConsent = patient.InfuHaveConsent;
            proc.InfuPumpModel = patient.InfuPumpModel;
            proc.InfuRate = patient.InfuRate;
            proc.InfuTreatmentPlan = patient.InfuTreatmentPlan;
            proc.InfuImSafetyFeatures = patient.InfuImSafetyFeatures;

            #region Call Schedule

            if (!preventCallCenterSchedule)
            {
                if (patient.StartDate.Day != procedure.StartDate.Value.Day ||
                    patient.StartDate.Month != procedure.StartDate.Value.Month ||
                    patient.StartDate.Year != procedure.StartDate.Value.Year)
                {
                    // Create the call center counterpart
                    var ccRef = await _db.CallCenterPracticeReferences.FirstAsync(c => c.PracticeId == practice.Id);

                    procedure.StartDate = patient.StartDate;

                    _db.CallCenterScheduledCalls.RemoveRange(proc.CallCenterScheduledCalls);

                    var calls = new List<CallCenterScheduledCall>();
                    var index = 1;
                    var procedureDate = _dateService.NormalizeDay(procedure.StartDate.Value);

                    for (var i = ccRef.StartCalls; i < ccRef.EndCalls + 1; i++)
                    {
                        var date = patient.StartDate.AddDays(i);
                        var adjustedTime = _dateService.ToUniversalTime(procedureDate.AddDays(i), practice.TimeZone);

                        var newCall = new CallCenterScheduledCall
                        {
                            Index = index++,
                            CallDay = date.Day,
                            CallMonth = date.Month,
                            CallYear = date.Year,
                            CallStatus = CallStatus.Scheduled,
                            CallTime = adjustedTime
                        };
                        newCall.Audit(_auditauditService);
                        calls.Add(newCall);
                    }

                    proc.Audit(_auditauditService);
                    proc.CallCenterScheduledCalls.AddRange(calls);

                }
            }
            #endregion

            _db.SaveChanges();

            var updatedPatient = await _db.Patients.FirstAsync(p => p.Id == patient.PatientId);


            SendAppInstructions(updatedPatient, patient.AccessCode, practice.Name, patient.SendEmailChecked, patient.SendSmsChecked);

            return patient;
        }

        public async Task<CallCenterPatientDto> RegisterMobileProcedure(CallCenterPatientDto model)
        {
            // Find facility by special code
            var practiceRef = _db.CallCenterPracticeReferences.First(p => p.PracticeCode.ToLower() == model.PracticeCode.ToLower());
            var practice = _db.Companies.First(c => c.Id == practiceRef.PracticeId);
            var physician = _db.Physicians.First(p => p.CompanyId == practice.Id);
            var libraries = _db.NodeLibraries
                .Include(l => l.CptCodes)
                .Where(l => l.PhysicianId == physician.Id)
                .ToList();

            var library = libraries.First(l => l.CptCodes.Any(c => c.Code.ToLower() == model.CptCode.ToLower()));
            var cptCode = library.CptCodes.First(c => c.Code == model.CptCode);

            model.CptId = cptCode.Id;
            model.FacilityId = practice.Id;
            model.PayorId = practice.PayorId;

            return await CreatePatient(model);
        }

        public async Task<bool> DeletePatient(string currentUserId, int id)
        {
            var patient = await _db.CallCenterPatients.FirstAsync(p => p.Id == id);

            patient.Audit(_auditauditService);
            patient.DeletedOn = DateTime.UtcNow;


            var procId = patient.CallCenterProcedure.ProcedureId;

            var procs = await _db.CallCenterProcedures.Where(p => p.ProcedureId == procId).ToListAsync();

            foreach (var proc in procs)
            {
                proc.DeletedOn = DateTime.UtcNow;
                proc.Audit(_auditauditService);
            }

            await _db.SaveChangesAsync();

            return true;
        }

        private string GetPatientQuery()
        {
            return @"

SELECT 
	ccp.Id,   
	p.Id as PatientId,
	p.FirstName,
	p.LastName,
	p.MiddleInitial,
	p.Email,
	p.Age,
	p.Phone,
	ccp.Gender,
	ccp.AltContact,
	ccp.AltPhone,
	cproc.DoNotDisturb,
	cproc.Cancelled,
	cproc.Completed,
	cproc.NoMonitoring,
	cproc.Outpatient,
	cproc.PracticeFacilityId as FacilityId, 
	cproc.CallCenterPayorId as PayorId,
	cproc.PhysicianId,
	cproc.CallCenterPayorId,
	cproc.AnesthesiologistId,
	cproc.NurseId,
	cproc.PumpSerial,
	cproc.PumpSerial2,
	cproc.IsDualCath,
	cproc.CptId,
	prc.StartDate,
	prc.AccessCode,
	c.[Name] as FacilityName
FROM  
	Patients p   
INNER JOIN
	CallCenterPatients ccp ON ccp.PatientId = p.Id
INNER JOIN
	Procedures prc on prc.PatientId = p.Id
INNER JOIN 
	CallCenterProcedures cproc on cproc.ProcedureId = prc.Id
INNER JOIN 
	Facilities f on prc.FacilityId = f.Id
INNER JOIN 
	Companies c on f.CompanyId = c.Id
WHERE
	ccp.Id = @Id;";
        }

        private string GetPatientsQuery(bool filterPractice, CallCenterPatientSearchModel search, bool forManagePatients)
        {

            #region Patient Query

            var patientSql = "";
            if (!forManagePatients)
            {
                patientSql = @"
SELECT
	pat.Id,  
	Calls.Id as FirstCallId, 
	ccp.Id as ProcedureId,
	p.FirstName,
	p.LastName,
	--p.MiddleInitial,
	--p.PhysicianId,
	p.Age,
	p.Email,
	p.CompanyId,
	p.Phone,
	--p.MedicalRecordNumber,
	p.Identifier,
	prc.FacilityId,
    ccpr.InfuFacilityId as InfuFacilityId,
	co.Name as FacilityName,
	--prc.PhysicianId,
	--prc.CptCode,
	--prc.AccessCode,
	pat.Id as CallCenterPatientId, 
	pat.CallCenterProcedureId, 
	pat.PatientId
	--pat.Gender,
	--pat.IsNew
FROM  
	Patients p   
INNER JOIN
	CallCenterPatients pat ON pat.PatientId = p.Id
INNER JOIN
	Procedures prc on prc.PatientId = p.Id
INNER JOIN
	CallCenterProcedures ccp on ccp.ProcedureId = prc.Id
INNER JOIN
	Facilities f on f.Id = prc.FacilityId
INNER JOIN 
	Companies co on co.Id = f.CompanyId
INNER JOIN
    CallCenterPracticeReferences ccpr on co.Id = ccpr.PracticeId
CROSS APPLY
        (
        SELECT  TOP 1 CallCenterScheduledCalls.Id
       FROM    CallCenterScheduledCalls
        WHERE  CallCenterScheduledCalls.CallCenterProcedureId = ccp.Id
        ) Calls
WHERE
	ccp.IsNew = 0   
AND
	pat.DeletedOn IS NULL";

            }
            else
            {
                patientSql = @"
SELECT
	pat.Id,  
	Calls.Id as FirstCallId, 
	ccp.Id as ProcedureId,
	p.FirstName,
	p.LastName,
	--p.MiddleInitial,
	--p.PhysicianId,
	p.Age,
	p.Email,
	p.CompanyId,
	p.Phone,
	--p.MedicalRecordNumber,
	p.Identifier,
	prc.FacilityId,
    ccpr.InfuFacilityId as InfuFacilityId,
	co.Name as FacilityName,
	--prc.PhysicianId,
	--prc.CptCode,
	--prc.AccessCode,
	pat.Id as CallCenterPatientId, 
	pat.CallCenterProcedureId, 
	pat.PatientId
	--pat.Gender,
	--pat.IsNew
FROM  
	Patients p   
INNER JOIN
	CallCenterPatients pat ON pat.PatientId = p.Id
INNER JOIN
	Procedures prc on prc.PatientId = p.Id
INNER JOIN
	CallCenterProcedures ccp on ccp.ProcedureId = prc.Id
INNER JOIN
	Facilities f on f.Id = prc.FacilityId
INNER JOIN 
	Companies co on co.Id = f.CompanyId
INNER JOIN
    CallCenterPracticeReferences ccpr on co.Id = ccpr.PracticeId
CROSS APPLY
        (
        SELECT  TOP 1 CallCenterScheduledCalls.Id
       FROM    CallCenterScheduledCalls
        WHERE  CallCenterScheduledCalls.CallCenterProcedureId = ccp.Id
        ) Calls
WHERE
	ccp.IsNew = 0 AND DateDiff(day, p.CreatedOn, getDate()) between 0 and 30
AND
	pat.DeletedOn IS NULL";

            }
            
            #endregion


            if (filterPractice)
            {
                patientSql += " AND f.CompanyId = @CompanyId ";
            }

            if (search != null && search.FilterType != PatientFilterType.None && !string.IsNullOrWhiteSpace(search.Filter))
            {
                if (search.FilterType == PatientFilterType.PumpSerial)
                {
                    patientSql += " and prc.PumpSerial like @PumpSerial ";
                }
                else if (search.FilterType == PatientFilterType.FirstName)
                {
                    patientSql += " and p.FirstName like @FirstName ";
                }
                else if (search.FilterType == PatientFilterType.LastName)
                {
                    patientSql += " and p.LastName like @LastName ";
                }
                else if (search.FilterType == PatientFilterType.Phone)
                {
                    patientSql += " and p.Phone like @Phone ";
                }
            }

            patientSql += " ORDER BY Calls.Id OFFSET(@Offset * @Limit) ROWS FETCH NEXT @Limit ROWS ONLY; ";

            return patientSql;
        }

        private string GetPatientsCountQuery(bool filterPractice, CallCenterPatientSearchModel search, bool forManagePatients)
        {
            #region Count Query
            var countSql = "";

            if (!forManagePatients)
            {
                countSql = @"
SELECT 
	 COUNT (p.Id)
FROM  
	Patients p   
INNER JOIN
	CallCenterPatients pat ON pat.PatientId = p.Id
INNER JOIN
	Procedures prc on prc.PatientId = p.Id
INNER JOIN
	CallCenterProcedures ccp on ccp.ProcedureId = prc.Id
INNER JOIN
	Facilities f on f.Id = prc.FacilityId
INNER JOIN 
	Companies co on co.Id = f.CompanyId
--INNER JOIN
--    CallCenterPracticeReferences ccpr on co.Id = ccpr.PracticeId
CROSS APPLY
        (
        SELECT  TOP 1 CallCenterScheduledCalls.Id
        FROM    CallCenterScheduledCalls
        WHERE  CallCenterScheduledCalls.CallCenterProcedureId = ccp.Id
        ) Calls
WHERE
	ccp.IsNew = 0   
AND
	pat.DeletedOn IS NULL";

            }
            else
            {
                countSql = @"
SELECT 
	 COUNT (p.Id)
FROM  
	Patients p   
INNER JOIN
	CallCenterPatients pat ON pat.PatientId = p.Id
INNER JOIN
	Procedures prc on prc.PatientId = p.Id
INNER JOIN
	CallCenterProcedures ccp on ccp.ProcedureId = prc.Id
INNER JOIN
	Facilities f on f.Id = prc.FacilityId
INNER JOIN 
	Companies co on co.Id = f.CompanyId
INNER JOIN
    CallCenterPracticeReferences ccpr on co.Id = ccpr.PracticeId
CROSS APPLY
        (
        SELECT  TOP 1 CallCenterScheduledCalls.Id
        FROM    CallCenterScheduledCalls
        WHERE  CallCenterScheduledCalls.CallCenterProcedureId = ccp.Id
        ) Calls
WHERE
	ccp.IsNew = 0 AND DateDiff(day, p.CreatedOn, getDate()) between 0 and 30
AND
	pat.DeletedOn IS NULL";

            }
            
            #endregion

            if (filterPractice)
            {
                countSql += " AND f.CompanyId = @CompanyId ";
            }

            if (search != null && search.FilterType != PatientFilterType.None && !string.IsNullOrWhiteSpace(search.Filter))
            {
                if (search.FilterType == PatientFilterType.PumpSerial)
                {
                    countSql += " and prc.PumpSerial like @PumpSerial ";
                }
                else if (search.FilterType == PatientFilterType.FirstName)
                {
                    countSql += " and p.FirstName like @FirstName ";
                }
                else if (search.FilterType == PatientFilterType.LastName)
                {
                    countSql += " and p.LastName like @LastName ";
                }
                else if (search.FilterType == PatientFilterType.Phone)
                {
                    countSql += " and p.Phone like @Phone ";
                }
            }


            return countSql;
        }
        #endregion

        #region Providers
        public async Task<PagedQueryResults<CallCenterProviderDto>> GetProviders(PagingParameters paging)
        {
            var q = _db.CallCenterProviders
                .Where(f => f.DeletedOn == null)
                .ProjectTo<CallCenterProviderDto>();

            if (!string.IsNullOrWhiteSpace(paging.Filter))
            {
                var fitlerType = (ProviderFilterType)paging.FilterType;

                switch (fitlerType)
                {
                    case ProviderFilterType.FirstName:
                        q = q.Where(c => c.FirstName.Contains(paging.Filter));
                        break;

                    case ProviderFilterType.LastName:
                        q = q.Where(c => c.LastName.Contains(paging.Filter));
                        break;

                    default: //ID sort
                        break;

                }
            }


            var query = q;

            var count = query.Count();

            var providers = await query
                .OrderBy(c => c.Id)
                .Skip(paging.Offset * paging.Limit)
                .Take(paging.Limit)
                .ToListAsync();

            return new PagedQueryResults<CallCenterProviderDto>(providers, count);
        }

        public async Task<CallCenterProviderDto> CreateProvider(CallCenterProviderDto provider, string currentUserId)
        {
            var caregivrer = new CallCenterProvider
            {
                ProviderType = provider.ProviderType,
                FirstName = provider.FirstName,
                LastName = provider.LastName,
                IsNew = false,
                Title = provider.Title,
                Suffix = provider.Suffix
            };

            foreach (var facility in provider.CallCenterProviderFacilities)
            {
                caregivrer.CallCenterProviderFacilities.Add(new CallCenterProviderFacility
                {
                    IsNew = false,
                    FacilityId = facility.Id
                });
            }

            caregivrer.Audit(_auditauditService);
            _db.CallCenterProviders.Add(caregivrer);
            await _db.SaveChangesAsync();

            provider.Id = caregivrer.Id;
            return provider;
        }

        public async Task<CallCenterProviderDto> UpdateProvider(CallCenterProviderDto model, string currentUserId)
        {
            var provider = _db.CallCenterProviders
                .Include(c => c.CallCenterProviderFacilities)
                .First(c => c.Id == model.Id);

            provider.ProviderType = model.ProviderType;
            provider.FirstName = model.FirstName;
            provider.LastName = model.LastName;
            provider.IsNew = false;
            provider.Suffix = model.Suffix;
            provider.Title = model.Title;

            //1,2
            var providerFacIds = model.CallCenterProviderFacilities
                .Where(f => f.FacilityId > 0)
                .Select(f => f.FacilityId);

            //1,2,3,4
            var facilitiesToRemove = provider.CallCenterProviderFacilities
                .Where(f => !providerFacIds.Contains(f.FacilityId))
                .ToList();

            // remove 1,2
            if (facilitiesToRemove.Any())
            {
                _db.CallCenterProviderFacilities.RemoveRange(facilitiesToRemove);
            }

            // Add 3,4
            foreach (var newFacility in model.CallCenterProviderFacilities.Where(f => f.FacilityId == 0))
            {
                if (provider.CallCenterProviderFacilities.Any(f => f.FacilityId == newFacility.Id))
                {
                    continue;
                }

                var facil = new CallCenterProviderFacility
                {
                    FacilityId = newFacility.Id,
                    IsNew = false
                };
                facil.Audit(_auditauditService);
                provider.CallCenterProviderFacilities.Add(facil);
            }

            provider.Audit(_auditauditService);
            await _db.SaveChangesAsync();

            return await GetProvider(model.Id);
        }

        public async Task<bool> DeleteProvider(int id)
        {
            var provider = await _db.CallCenterProviders.FirstAsync(p => p.Id == id);
            provider.Audit(_auditauditService);
            provider.DeletedOn = DateTime.UtcNow;
            await _db.SaveChangesAsync();

            return true;
        }

        public async Task<CallCenterProviderDto> GetProvider(int id)
        {
            var caregivrer = await _db.CallCenterProviders.FirstAsync(c => c.Id == id);

            var facilityIds = caregivrer.CallCenterProviderFacilities
                .Select(f => f.FacilityId).ToList();

            var facilities = _db.Companies.Where(c => facilityIds.Contains(c.Id));

            return new CallCenterProviderDto
            {
                Id = caregivrer.Id,
                ProviderType = caregivrer.ProviderType,
                FirstName = caregivrer.FirstName,
                LastName = caregivrer.LastName,
                Title = caregivrer.Title,
                CallCenterProviderFacilities =
                    caregivrer.CallCenterProviderFacilities.Select(
                        f => new CallCenterProviderFacilityDto
                        {
                            Id = f.Id,
                            FacilityId = f.FacilityId,
                            Name = facilities.First(fc => fc.Id == f.FacilityId).Name
                        }).ToList()
            };

        }

        private async Task<List<CallCenterPayor>> GetPayors()
        {
            return await _db.CallCenterPayors
                .Where(f => f.DeletedOn == null)
                .ToListAsync();
        }

        #endregion

        #region Schedule
        public async Task<List<CallCenterScheduleDto>> GetSchecule(int year, int month, int day)
        {
            var cn = _db.Database.Connection;
            cn.Open();
            var schedule = (await cn.QueryAsync<CallCenterScheduleDto>(GetSeheduleSql(), new
            {
                Year = year,
                Month = month,
                Day = day
            })).ToList();

            var procedureIds = schedule.Select(s => s.CallCenterProcedureId).Distinct().ToArray();

            var associatedCalls = (await cn.QueryAsync<AssociatedCallDto>(GetAssociatedSql(),
                new { Ids = procedureIds })).ToList();

            cn.Close();

            foreach (var instance in schedule)
            {
                // Find associated calls
                var calls = associatedCalls.Where(i =>
                    i.Id != instance.Id &&
                    i.CallCenterProcedureId == instance.CallCenterProcedureId
                ).ToList();

                foreach (var call in calls)
                {
                    instance.AssociatedCalls.Add(call);
                }

                // Find offset for time zones
                instance.TimeZoneOffset = GetTimeZoneOffset(instance.TimeZone);
                instance.CallTime = _dateService.ToLocalTime(instance.CallTime, instance.TimeZone);
            }

            var hotlineCalls = schedule.Where(c => c.HasHotlineInLastDay)
                .OrderBy(c => c.TimeZoneOffset)
                .ThenBy(c => c.CallTime)
                .ToList();

            var hotLineOrdered = new List<CallCenterScheduleDto>();

            hotLineOrdered.AddRange(hotlineCalls.Where(c => !c.Mobile));
            hotLineOrdered.AddRange(hotlineCalls.Where(c => c.Mobile));

            var standardCalls = schedule.Except(hotlineCalls)
                .OrderBy(c => c.TimeZoneOffset)
                .ThenBy(c => c.CallTime)
                .ToList();

            var standardOrdrerd = new List<CallCenterScheduleDto>();
            standardOrdrerd.AddRange(standardCalls.Where(c => !c.Mobile));
            standardOrdrerd.AddRange(standardCalls.Where(c => c.Mobile));


            // Merge the lists with hotline calls first
            hotLineOrdered.AddRange(standardOrdrerd);

            return hotLineOrdered;
        }

        public async Task<NextCallId> GetNextCall(string currentUserId, int year, int month, int day)
        {
            // Do I have an assigned call?
            var cn = _db.Database.Connection;
            cn.Open();

            var sql = @"
SELECT COUNT(*) FROM CallCenterScheduledCalls 
WHERE 
	CallYear = @Year
AND
	CallMonth = @Month
AND
	CallDay = @Day 
AND
	AssignedUserId = @UserId";

            var count = (await cn.ExecuteScalarAsync<int>(sql,
                new { Year = year, Month = month, Day = day, UserId = currentUserId }));
            cn.Close();

            if (count > 0)
            {
                return new NextCallId(-1, year, month, day);
            }

            var schedule = (await GetSchecule(year, month, day))
                .FirstOrDefault(s => s.Assigned == false);

            if (schedule == null)
            {
                return new NextCallId(0, year, month, day);
            }

            cn.Open();

            await cn.ExecuteAsync("UPDATE CallCenterScheduledCalls SET AssignedUserId = @UserId, Assigned = 1 WHERE Id = @Id",
                new { UserId = currentUserId, schedule.Id });

            cn.Close();

            return new NextCallId(schedule.Id, year, month, day);
        }

        public async Task<NextCallId> LockCall(string currentUserId, int id, bool isSuperAdmin)
        {
            // Do I have an existing call?
            var alreadyAssigned = await _db.CallCenterScheduledCalls.FirstOrDefaultAsync(c =>
                c.CallStatus == CallStatus.Scheduled && c.AssignedUserId == currentUserId && c.Id == id);

            if (alreadyAssigned != null)
            {
                // User has an uncompleted call; use that one
                return new NextCallId(alreadyAssigned.Id, "", "");
            }

            // Get the call in question
            var call = await _db.CallCenterScheduledCalls.FirstAsync(c => c.Id == id);

            if (call.CallStatus == CallStatus.Scheduled)
            {
                // Call belongs to someone else;
                if (string.IsNullOrWhiteSpace(call.AssignedUserId))
                {
                    call.AssignedUserId = currentUserId;
                    call.Assigned = true;
                    await _db.SaveChangesAsync();
                    return new NextCallId(id, "", "");
                }
            }
            var user = await _db.Users.FirstAsync(c => c.Id.Equals(call.AssignedUserId));
            var firstName = user.Firstname;
            var lastName = user.Lastname;
            if (isSuperAdmin)
            {
                return new NextCallId(id, "", "");
            }
            else
            {
                return new NextCallId(0, firstName, lastName);
            }
        }

        public async Task<CallCenterCallDetailDto> GetCall(int id, string userId)
        {
            var cn = _db.Database.Connection;
            cn.Open();

            var call = (await cn.QueryAsync<CallCenterCallDetailDto>(GetCallQuery(), new { Id = id }))
                .FirstOrDefault();

            call.ContextUserId = userId;

            var comments = (await cn.QueryAsync<CallCenterCommentDto>(
                "SELECT * FROM CallCenterComments WHERE CallCenterProcedureId = @Id ORDER BY Id DESC",
                new { Id = call.CallCenterProcedureId }))
                .ToList();

            var commentTypes = (await cn.QueryAsync<CallCenterCommentType>("select * from CallCenterCommentTypes order by [index]")).ToList();


            var dischargeDate = (await cn.QueryAsync<DischargeDate>(
                "SELECT TOP 1 CallYear, CallMonth, CallDay FROM CallCenterScheduledCalls WHERE CallCenterProcedureId = @CallCenterProcedureId ORDER BY CallYear DESC, CallMonth DESC, CallDay DESC", new { call.CallCenterProcedureId }))
                .FirstOrDefault();

            var assessments = (await cn.QueryAsync<AssociatedCallDto>(
                    "SELECT * FROM CallCenterScheduledCalls WHERE CallStatus = @Status AND CallCenterProcedureId = @CallCenterProcedureId ORDER BY [Index]",
                    new
                    {
                        call.CallCenterProcedureId,
                        call.Id,
                        Status = (int)CallStatus.Completed
                    }))
                .ToList();

            call.Comments.AddRange(comments);
            call.AssociatedCalls.AddRange(assessments);
            cn.Close();

            foreach (var comment in call.Comments)
            {
                if (comment.CreatedOn == null)
                {
                    continue;
                }

                comment.CreatedOn = _dateService.ToLocalTime(comment.CreatedOn.Value, call.TimeZone);
            }

            call.CommentTypes = commentTypes;
            call.DischargeDate = dischargeDate.ToString();
            return call;
        }

        public async Task<bool> DeleteCall(int id)
        {
            var call = await _db.CallCenterScheduledCalls.FirstAsync(c => c.Id == id);
            call.Audit(_auditauditService);
            call.DeletedOn = DateTime.UtcNow;
            await _db.SaveChangesAsync();

            return true;
        }

        public async Task<CallCenterCommentDto> CreateComment(CallCenterCommentDto model)
        {
            var typeId = _db.CallCenterCommentTypes.First(ct => ct.Index == model.CallTypeIndex).Id;

            var comment = new CallCenterComment
            {
                CallCenterProcedureId = model.CallCenterProcedureId,
                CallCenterScheduledCallId = model.CallCenterScheduledCallId,
                Comment = model.Comment,
                CallCenterCommentTypeId = typeId
            };
            comment.Audit(_auditauditService);
            _db.CallCenterComments.Add(comment);

            await _db.SaveChangesAsync();

            model.Id = comment.Id;
            model.CreatedOn = comment.CreatedOn;
            model.CreatedBy = comment.CreatedBy;

            return model;
        }

        public async Task<CallCenterAggregates> GetAggregates(string currentUserId, int year, int month, int day)
        {
            var cn = _db.Database.Connection;
            cn.Open();
            var schedule = (await cn.QueryAsync<CallCenterScheduleDto>(GetAggregatesSql(), new
            {
                Year = year,
                Month = month,
                Day = day
            })).ToList();

            cn.Close();

            return new CallCenterAggregates
            {
                ScheduledMonitor = schedule.Count(s => s.Mobile == false && s.CallStatus == (int)CallStatus.Scheduled),
                CompletedMonitor = schedule.Count(s => s.Mobile == false && s.CallStatus == (int)CallStatus.Completed),

                ScheduledMobile = schedule.Count(s => s.Mobile && s.CallStatus == (int)CallStatus.Scheduled),
                CompletedMobile = schedule.Count(s => s.CallStatus == (int)CallStatus.CompletedMobile),
            };
        }

        public async Task<bool> SaveAssessment(AssociatedCallDto model)
        {
            var call = await _db.CallCenterScheduledCalls.FirstAsync(c => c.Id == model.Id);

            call.CallStatus = CallStatus.Completed;

            var proc = await _db.CallCenterProcedures.FirstAsync(p => p.Id == call.CallCenterProcedureId);

            // Assessment
            proc.Satisfaction = model.Satisfaction;

            call.PainScoreCurrent = model.PainScoreCurrent;
            call.Perscription24Hour = model.Perscription24Hour;
            call.Otc24Hour = model.Otc24Hour;
            call.TolerableToilet = model.TolerableToilet.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableToilet.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableToilet == 2 ? (int)AssessmentValues.Intolerable : model.TolerableToilet));
            call.TolerableConsumption = model.TolerableConsumption.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableConsumption.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableConsumption == 2 ? (int)AssessmentValues.Intolerable : model.TolerableConsumption));
            call.TolerableSleeping = model.TolerableSleeping.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableSleeping.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableSleeping == 2 ? (int)AssessmentValues.Intolerable : model.TolerableSleeping));
            call.TolerableWalking = model.TolerableWalking.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableWalking.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableWalking == 2 ? (int)AssessmentValues.Intolerable : model.TolerableWalking));
            call.TolerableDressing = model.TolerableDressing.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableDressing.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableDressing == 2 ? (int)AssessmentValues.Intolerable : model.TolerableDressing));
            call.TolerableStairs = model.TolerableStairs.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableStairs.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableStairs == 2 ? (int)AssessmentValues.Intolerable : model.TolerableStairs));
            call.TolerablePtOt = model.TolerablePtOt.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerablePtOt.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerablePtOt == 2 ? (int)AssessmentValues.Intolerable : model.TolerablePtOt));
            call.TolerableReturnToWork = model.TolerableReturnToWork.Value == 0 ? (int)AssessmentValues.Tolerable : (model.TolerableReturnToWork.Value == 1 ? (int)AssessmentValues.SlightlyDifficult : (model.TolerableReturnToWork == 2 ? (int)AssessmentValues.Intolerable : model.TolerableReturnToWork));
            call.EffectiveUse = model.EffectiveUse;
            call.DoseButton = model.DoseButton;
            await _db.SaveChangesAsync();

            return true;
        }

        public async Task<bool> CancelCall(int id)
        {
            var call = _db.CallCenterScheduledCalls.First(c => c.Id == id);
            call.AssignedUserId = null;
            call.Assigned = false;
            await _db.SaveChangesAsync();
            return true;
        }

        public async Task<bool> CallBack(int id, int hour, int minute)
        {
            var call = _db.CallCenterScheduledCalls.First(c => c.Id == id);
            var newTime = new DateTime(call.CallYear, call.CallMonth, call.CallDay, hour, minute, 0,
                DateTimeKind.Local);
            var zone = "";

            try
            {
                var ccp = _db.CallCenterProcedures.First(p => p.Id == call.CallCenterProcedureId);
                var proc = _db.Procedures.First(p => p.Id == ccp.ProcedureId);
                var fac = _db.Facilities.First(f => f.Id == proc.FacilityId);
                var co = _db.Companies.First(c => c.Id == fac.CompanyId);
                zone = co.TimeZone;
            }
            catch (Exception ex)
            {
            }

            var utc = _dateService.ToUniversalTime(newTime, zone);

            call.CallTime = utc;

                var note = new CallCenterComment
                {
                    CallCenterProcedureId = call.CallCenterProcedureId,
                    CallCenterCommentTypeId = (int)CallType.General,
                    Comment = "Patient contacted. Later call back requested.",
                    CallCenterScheduledCallId = call.Id
                };
            note.Audit(_auditauditService);
            call.Audit(_auditauditService);
            _db.CallCenterComments.Add(note);

            await _db.SaveChangesAsync();
            return true;
        }

        public async Task<CallTomorrow> CallTomorrow(int id)
        {
            var call = _db.CallCenterScheduledCalls.First(c => c.Id == id);
            var zone = "";

            try
            {
                var ccp = _db.CallCenterProcedures.First(p => p.Id == call.CallCenterProcedureId);
                var proc = _db.Procedures.First(p => p.Id == ccp.ProcedureId);
                var fac = _db.Facilities.First(f => f.Id == proc.FacilityId);
                var co = _db.Companies.First(c => c.Id == fac.CompanyId);
                zone = co.TimeZone;
            }
            catch (Exception ex)
            {
            }

            call.CallStatus = CallStatus.Completed;

            var note = new CallCenterComment
            {
                CallCenterProcedureId = call.CallCenterProcedureId,
                CallCenterCommentTypeId = (int)CallType.General,
                Comment = "Additional Call scheduled for tomorrow at patient request.",
                CallCenterScheduledCallId = call.Id
            };
            note.Audit(_auditauditService);
            call.Audit(_auditauditService);

            _db.CallCenterComments.Add(note);

            await _db.SaveChangesAsync();

            var tomorrow = _db.CallCenterScheduledCalls.FirstOrDefault(c =>
                c.CallCenterProcedureId == call.CallCenterProcedureId
                && c.CallStatus == CallStatus.Scheduled && c.Index == (call.Index + 1));


            if (tomorrow == null)
            {
                return new CallTomorrow(false);
            }

            return new CallTomorrow(true);
        }

        public async Task<bool> Discharge(int id, int satisfaction, bool followUp, int? followUpHour = null, int? followUpMinute = null)
        {
            var call = _db.CallCenterScheduledCalls.First(c => c.Id == id);
            var callsToRemove = _db.CallCenterScheduledCalls
                .Where(c => c.CallCenterProcedureId == call.CallCenterProcedureId && c.Index > call.Index);

            // Set this call as discharged and delete all future calls
            call.CallStatus = CallStatus.Discharged;

            var pc = await _db.CallCenterProcedures.FirstAsync(p => p.Id == call.CallCenterProcedureId);
            pc.Satisfaction = satisfaction;

            var callIds = callsToRemove.Select(c => c.Id).ToList();

            // Remove calls that would otherwise be scheduled
            var comments = _db.CallCenterComments.Where(c => callIds.Any(i => c.CallCenterScheduledCallId == i)).ToList();
            _db.CallCenterComments.RemoveRange(comments);
            await _db.SaveChangesAsync();
            _db.CallCenterScheduledCalls.RemoveRange(callsToRemove);

            var ccp = _db.CallCenterProcedures.First(p => p.Id == call.CallCenterProcedureId);
            ccp.DiscontinueDate = DateTime.UtcNow;

            if (followUp)
            {
                try
                {
                    var proc = _db.Procedures.First(p => p.Id == ccp.ProcedureId);
                    var fac = _db.Facilities.First(f => f.Id == proc.FacilityId);
                    var co = _db.Companies.First(c => c.Id == fac.CompanyId);
                    var zone = co.TimeZone;

                    var date = new DateTime(call.CallYear, call.CallMonth, call.CallDay, followUpHour ?? 9, followUpMinute ?? 0, 0);
                    var tomorrowDate = date.AddDays(1);

                    var normalizedDate = _dateService.ToUniversalTime(tomorrowDate, zone);

                    var newCall = new CallCenterScheduledCall
                    {
                        CallCenterProcedureId = call.CallCenterProcedureId,
                        CallDay = normalizedDate.Day,
                        CallMonth = normalizedDate.Month,
                        CallYear = normalizedDate.Year,
                        CallTime = normalizedDate,
                        Index = call.Index + 1,

                        CallStatus = CallStatus.Scheduled,
                        IsNew = false
                    };
                    newCall.Audit(_auditauditService);
                    _db.CallCenterScheduledCalls.Add(newCall);
                }
                catch (Exception ex)
                {
                }
            }

            var note = new CallCenterComment
            {
                CallCenterProcedureId = call.CallCenterProcedureId,
                CallCenterCommentTypeId = (int)CallType.General,
                Comment = followUp ? "Additional Post Discharge call scheduled for tomorrow at patient request"
                    : "Patient successfully discharged.",
                CallCenterScheduledCallId = call.Id
            };

            note.Audit(_auditauditService);
            ccp.Audit(_auditauditService);
            _db.CallCenterComments.Add(note);

            await _db.SaveChangesAsync();

            return true;
        }

        public async Task<List<IGrouping<string, SummaryDto>>> GetSummary(string userId, int year, int month, int day)
        {
            var practiceId = await RequiresPracticeFilter(userId);

            var cn = _db.Database.Connection;
            cn.Open();
            var schedule = (await cn.QueryAsync<SummaryDto>(GetSummarySql(practiceId.HasValue), new
            {
                Year = year,
                Month = month,
                Day = day,
                PracticeId = practiceId ?? 0,
                CompanyId = practiceId ?? 0,
            })).ToList();


            foreach (var s in schedule)
            {
                s.IsToday = s.CallYear == year && s.CallMonth == month && s.CallDay == day;
            }


            var scheduleGroups = schedule.GroupBy(s => s.Identifier).ToList();

            return scheduleGroups;
        }

        public async Task<SummaryOverviewDto> GetSummaryOverview(string currentUserId, int year, int month, int day)
        {
            var practiceId = await RequiresPracticeFilter(currentUserId);

            #region Pain score averages for today
            var cn = _db.Database.Connection;
            cn.Open();
            var schedule = (await cn.QueryAsync<SummaryDto>(GetSummarySql(practiceId.HasValue), new
            {
                Year = year,
                Month = month,
                Day = day,
                CompanyId = practiceId ?? 0
            })).ToList();

            var groupedByIdent = schedule.GroupBy(s => s.Identifier).ToList();

            var assessmentGroups = groupedByIdent.Select(
                grp => new
                {
                    grp.Key,
                    LastAssessment = grp.OrderByDescending(x => x.Index).FirstOrDefault()
                }).ToList();

            double averagePainScore = 0;

            if (assessmentGroups.Any())
            {
                assessmentGroups.Average(a => a.LastAssessment.PainScoreCurrent);
            }

            #endregion

            #region Pain scores averaged for all days in schedule
            var groupedByIndex = schedule.GroupBy(s => s.Index).ToList();

            var dailyAverages = new Dictionary<int, double>();

            foreach (var group in groupedByIndex)
            {
                var avgIndexPain = group.Average(g => g.PainScoreCurrent);

                var index = group.FirstOrDefault()?.Index ?? 0;

                if (index != 0)
                {
                    dailyAverages.Add(index, Math.Round(avgIndexPain, 1));
                }
            }
            #endregion

            #region 180 day satisfaction 
            var satisfaction = await cn.ExecuteScalarAsync<double>(
                @"SELECT
			ROUND(AVG(Cast(Satisfaction as float)),2) FROM
				CallCenterProcedures ccp

				WHERE Satisfaction > 0 AND
			ID IN(
				SELECT DISTINCT CallCenterProcedureId FROM
			CallCenterScheduledCalls
				WHERE CallStatus > 1 AND CallTime > @date
				)",
                new { date = new DateTime(year, month, day).AddDays(-180) });


            #endregion

            return new SummaryOverviewDto
            {
                AveragePainScore = Math.Round(averagePainScore, 1),
                DailyAverages = dailyAverages,
                Satisfaction = satisfaction
            };
        }

        public async Task<List<SummaryDto>> GetCallHistory(int procedureId)
        {
            var cn = _db.Database.Connection;
            cn.Open();
            var history = (await cn.QueryAsync<SummaryDto>(GetCallHistorySql(), new
            {
                ProcedureId = procedureId
            })).ToList();

            return history;
        }

        private string GetSeheduleSql()
        {
            return @"
SELECT  
	ccsc.Id
	,p.Id as ProcedureId
	,ccp.Id as CallCenterProcedureId
	,ccp.SpanishSpeaking
	,ccp.Outpatient
	,ccsc.[Index]
	,ccsc.CallStatus
	,ccsc.CallDay
	,ccsc.CallMonth
	,ccsc.CallYear
	,ccsc.Assigned
	,ccsc.AssignedUserId 
	,ccsc.CallTime
	,p.StartDate

	,co.Name as FacilityName
	,co.TimeZone

	,pat.Age
	,pat.CompanyId
	,pat.Email
	,pat.FirstName
	,pat.Identifier
	,pat.LastName
	,pat.MiddleInitial
	,pat.Phone
	,cpat.Gender
	,cpat.AltContact
	,cpat.AltPhone
	,cpat.Mobile

	,phys.Id as PhysicianId
	,phys.FirstName as PhysicianFirstName
	,phys.LastName as PhysicianLastName
	,phys.Title as PhysicianTitle
	,phys.Suffix as PhysicianSuffix

	,ane.Id as AnesthesiologistId
	,ane.FirstName as AnesthesiologistFirstName
	,ane.LastName as AnesthesiLastName
	,ane.Title as AnesthesiTitle
	,ane.Suffix as AnesthesiSuffix

	,nurse.Id as NurseFirstName
	,nurse.FirstName as NurseFirstName
	,nurse.LastName as NurseLastName
	,nurse.Title as NurseTitle
	,nurse.Suffix as NurseSuffix

FROM            
	CallCenterScheduledCalls ccsc
	INNER JOIN 
	CallCenterProcedures ccp on ccp.Id = ccsc.CallCenterProcedureId
	INNER JOIN
	Procedures p on ccp.ProcedureId = p.Id
	INNER JOIN
	CallCenterPatients cpat on cpat.CallCenterProcedureId = ccp.id
	INNER JOIN
	Patients pat on pat.Id = cpat.PatientId
	LEFT JOIN
	CallCenterProviders phys on phys.Id = ccp.PhysicianId 
	LEFT JOIN
	CallCenterProviders ane on ane.Id = ccp.AnesthesiologistId 
	LEFT JOIN
	CallCenterProviders nurse on nurse.Id = ccp.NurseId 
	INNER JOIN 
	Companies co on co.Id = ccp.PracticeFacilityId
WHERE
	ccp.NoMonitoring = 0
	AND
	ccsc.CallStatus = 1
	AND
	CallYear = @Year
	AND
	CallMonth = @Month
	AND
	CallDay = @Day
	AND
	ccsc.DeletedOn IS NULL
	AND
	ccp.DeletedOn IS NULL
	AND
	cpat.DeletedOn IS NULL
";
        }

        private string GetSummarySql(bool filterPractice)
        {
            var sql = @"
SELECT  
	ccsc.Id
	,ccp.Id as CallCenterProcedureId
	,cpt.Code
	,ccsc.[Index]
	,ccsc.Assigned
	,ccsc.CallYear
	,ccsc.CallMonth
	,ccsc.CallDay
	,ccsc.CallStatus
	,ccsc.PainScoreCurrent
	,ccsc.TolerableConsumption
	,ccsc.TolerableDressing
	,ccsc.TolerablePtOt
	,ccsc.TolerableReturnToWork
	,ccsc.TolerableSleeping
	,ccsc.TolerableStairs
	,ccsc.TolerableToilet
	,ccsc.TolerableWalking
	,ccp.Satisfaction
	,pat.FirstName
	,pat.Identifier
	,pat.LastName
	,pat.Phone
	,cpat.Mobile
	,phys.Id as PhysicianId
	,phys.FirstName as PhysicianFirstName
	,phys.LastName as PhysicianLastName
	,phys.Title as PhysicianTitle
	,phys.Suffix as PhysicianSuffix

FROM            
	CallCenterScheduledCalls ccsc
	INNER JOIN 
	CallCenterProcedures ccp on ccp.Id = ccsc.CallCenterProcedureId
	INNER JOIN
	CallCenterPatients cpat on cpat.CallCenterProcedureId = ccp.id
	INNER JOIN
	Patients pat on pat.Id = cpat.PatientId
	INNER JOIN
	Procedures prc on prc.PatientId = pat.Id
	INNER JOIN
	Facilities f on f.Id = prc.FacilityId
	LEFT JOIN
	CallCenterProviders phys on phys.Id = ccp.PhysicianId 
	INNER JOIN
	CptCodes cpt on cpt.Id = ccp.CptId
WHERE 
	ccsc.DeletedOn IS NULL
	AND
	ccp.DeletedOn IS NULL
	AND
	cpat.DeletedOn IS NULL
	AND
	ccsc.CallCenterProcedureId IN (
		SELECT 
			sc.CallCenterProcedureId
		FROM            
			CallCenterScheduledCalls sc
			INNER JOIN 
			CallCenterProcedures cp on cp.Id = sc.CallCenterProcedureId
		WHERE
			cp.NoMonitoring = 0
			AND
			CallYear = @Year
			AND
			CallMonth = @Month
			AND
			CallDay = @Day
	)";

            if (filterPractice)
            {
                sql += " AND f.CompanyId = @CompanyId ";
            }

            return sql;
        }

        private string GetCallHistorySql()
        {
            return @"
SELECT  
	ccsc.Id
	,ccsc.Assigned
	,ccsc.CallStatus
	,ccp.Id as CallCenterProcedureId
	,cpt.Code
	,ccsc.[Index]
	,ccsc.CallStatus
	,ccsc.PainScoreCurrent
	,ccsc.TolerableConsumption
	,ccsc.TolerableDressing
	,ccsc.TolerablePtOt
	,ccsc.TolerableReturnToWork
	,ccsc.TolerableSleeping
	,ccsc.TolerableStairs
	,ccsc.TolerableToilet
	,ccsc.TolerableWalking
	,ccp.Satisfaction
	,pat.FirstName
	,pat.Identifier
	,pat.LastName
	,pat.Phone
	,cpat.Mobile
	,phys.Id as PhysicianId
	,phys.FirstName as PhysicianFirstName
	,phys.LastName as PhysicianLastName
	,phys.Title as PhysicianTitle
	,phys.Suffix as PhysicianSuffix
	,c.[Name] as FacilityName
	,pr.StartDate
FROM            
	CallCenterScheduledCalls ccsc
	INNER JOIN 
	CallCenterProcedures ccp on ccp.Id = ccsc.CallCenterProcedureId
	INNER JOIN
	CallCenterPatients cpat on cpat.CallCenterProcedureId = ccp.id
	INNER JOIN
	Patients pat on pat.Id = cpat.PatientId
	LEFT JOIN
	CallCenterProviders phys on phys.Id = ccp.PhysicianId 
	INNER JOIN
	CptCodes cpt on cpt.Id = ccp.CptId
	INNER JOIN 
	Procedures pr on pr.Id = ccp.ProcedureId
	INNER JOIN
	Facilities f on f.Id = pr.FacilityId
	INNER JOIN 
	Companies c on c.Id = f.CompanyId
WHERE 

	ccsc.CallCenterProcedureId = @ProcedureId";
        }

        private string GetAggregatesSql()
        {
            return @"
SELECT  
	ccsc.Id
	,p.Id as ProcedureId
	,ccp.Id as CallCenterProcedureId
	,ccp.SpanishSpeaking
	,ccp.Outpatient
	,ccsc.[Index]
	,ccsc.CallStatus
	,ccsc.CallDay
	,ccsc.CallMonth
	,ccsc.CallYear
	,ccsc.Assigned
	,ccsc.AssignedUserId 
	,ccsc.CallTime
	
	,p.StartDate

	,co.Name as FacilityName
	,co.TimeZone

	,pat.Age
	,pat.CompanyId
	,pat.Email
	,pat.FirstName
	,pat.Identifier
	,pat.LastName
	,pat.MiddleInitial
	,pat.Phone
	,cpat.Gender
	,cpat.AltContact
	,cpat.AltPhone
	,cpat.Mobile

	,phys.Id as PhysicianId
	,phys.FirstName as PhysicianFirstName
	,phys.LastName as PhysicianLastName
	,phys.Title as PhysicianTitle
	,phys.Suffix as PhysicianSuffix

	,ane.Id as AnesthesiologistId
	,ane.FirstName as AnesthesiologistFirstName
	,ane.LastName as AnesthesiLastName
	,ane.Title as AnesthesiTitle
	,ane.Suffix as AnesthesiSuffix

	,nurse.Id as NurseFirstName
	,nurse.FirstName as NurseFirstName
	,nurse.LastName as NurseLastName
	,nurse.Title as NurseTitle
	,nurse.Suffix as NurseSuffix

FROM            
	CallCenterScheduledCalls ccsc
	INNER JOIN 
	CallCenterProcedures ccp on ccp.Id = ccsc.CallCenterProcedureId
	INNER JOIN
	Procedures p on ccp.ProcedureId = p.Id
	INNER JOIN
	CallCenterPatients cpat on cpat.CallCenterProcedureId = ccp.id
	INNER JOIN
	Patients pat on pat.Id = cpat.PatientId
	LEFT JOIN
	CallCenterProviders phys on phys.Id = ccp.PhysicianId 
	LEFT JOIN
	CallCenterProviders ane on ane.Id = ccp.AnesthesiologistId 
	LEFT JOIN
	CallCenterProviders nurse on nurse.Id = ccp.NurseId 
	INNER JOIN 
	Companies co on co.Id = ccp.PracticeFacilityId
WHERE
	ccp.NoMonitoring = 0
	AND
	CallYear = @Year
	AND
	CallMonth = @Month
	AND
	CallDay = @Day
	AND
	ccsc.DeletedOn IS NULL
	AND
	ccp.DeletedOn IS NULL
	AND
	cpat.DeletedOn IS NULL
";
        }

        private string GetAssociatedSql()
        {
            return @"select * from CallCenterScheduledCalls
where CallCenterProcedureId in @Ids
order by CallCenterProcedureId, [Index]";
        }

        private string GetCallQuery()
        {
            return @"
SELECT  
	ccsc.Id
	,ccsc.CallStatus
	,ccsc.AssignedUserId
	,ccp.Id as CallCenterProcedureId
	,p.Id as ProcedureId
	,p.StartDate
	,ccsc.[Index]
	,pat.Age
	,pat.CompanyId
	,pat.Email
	,pat.FirstName
	,pat.Identifier
	,pat.LastName
	,pat.MiddleInitial
	,pat.Phone
	,cpat.Gender
	,cpat.AltContact
	,cpat.AltPhone
	,cpat.Mobile
	,cpat.Mobile
	,ccp.Cancelled
	,ccp.Completed
	,ccp.NoMonitoring
	,ccp.DoNotDisturb

	,co.Name as FacilityName
	,co.TimeZone

	,ccpr.Notes as FacilityNotes
	,cpt.Code as ProcedureName
	,p.StartDate as ProcedureDate

	,phys.FirstName as PhysicianFirstName
	,phys.LastName as PhysicianLastName
	,phys.Title as PhysicianTitle
	,phys.Suffix as PhysicianSuffix

	,ane.FirstName as AnesthesiologistFirstName
	,ane.LastName as AnesthesiologistLastName
	,ane.Title as AnesthesiologistTitle
	,ane.Suffix as AnesthesiologistSuffix

	,nurse.FirstName as NurseFirstName
	,nurse.LastName as PhysicianNurseLastName
	,nurse.Title as NurseTitle
	,nurse.Suffix as NurseSuffix

FROM            
	CallCenterScheduledCalls ccsc
	INNER JOIN 
	CallCenterProcedures ccp on ccp.Id = ccsc.CallCenterProcedureId
	INNER JOIN
	Procedures p on ccp.ProcedureId = p.Id
	INNER JOIN 
	CallCenterPracticeReferences ccpr on ccpr.CallCenterId = ccp.CallCenterId
	INNER JOIN
	CallCenterPatients cpat on cpat.CallCenterProcedureId = ccp.id
	INNER JOIN
	Patients pat on pat.Id = cpat.PatientId
	LEFT JOIN
	CallCenterProviders phys on phys.Id = ccp.PhysicianId 
	LEFT JOIN
	CallCenterProviders ane on ane.Id = ccp.AnesthesiologistId 
	LEFT JOIN
	CallCenterProviders nurse on nurse.Id = ccp.NurseId 
	INNER JOIN 
	Companies co on co.Id = ccp.PracticeFacilityId
	INNER JOIN
	CptCodes cpt on cpt.Id = ccp.CptId
WHERE
	ccsc.Id = @Id";
        }
        #endregion

        #region Facilities
        private async Task<List<CallCenterFacilityDto>> GetFacilities(ApplicationUser user)
        {
            var callSiteAdmin = await _db.CallSiteAdmins.FirstOrDefaultAsync(sa => sa.UserId == user.Id);
            var callSiteMobile = await _db.CallSiteMobileUsers.FirstOrDefaultAsync(sa => sa.UserId == user.Id);
            var facilities = new List<CallCenterFacilityDto>();
            List<int> companyIds = new List<int>();
            CallCenterFacilityDto facility;

            var requiresFilter = false;
            var practiceId = 0;

            if (callSiteAdmin != null)
            {
                requiresFilter = true;
                if (callSiteAdmin.CompanyId.HasValue)
                {
                    practiceId = callSiteAdmin.CompanyId.Value;
                }
            }

            if (callSiteMobile != null)
            {
                requiresFilter = true;
                if (callSiteMobile.CompanyId.HasValue)
                {
                    practiceId = callSiteMobile.CompanyId.Value;
                }
            }

            var facilityQuery = _db.Companies.Where(f => f.DeletedOn == null && f.Division.Equals("PAIN") &&
                !f.IsNew && f.CompanyType == CompanyType.Practice);

            // Only show facilities for the given facility for non-admins
            if (requiresFilter)
            {
                    String facilityGroup = await _db.CallCenterPracticeReferences.Where(c => c.PracticeId == practiceId).Select(c => c.FacilityGroup).FirstOrDefaultAsync();
                    companyIds = await _db.CallCenterPracticeReferences.Where(ccpr => ccpr.FacilityGroup.Equals(facilityGroup)).Select(ccpr => ccpr.PracticeId).ToListAsync();
                    foreach (var cID in companyIds)
                    {
                        var facilityGroupQuery = _db.Companies.Where(f => f.DeletedOn == null && f.Division.Equals("PAIN") &&
                        !f.IsNew && f.CompanyType == CompanyType.Practice && f.Id == cID);
                        facility = await facilityGroupQuery.ProjectTo<CallCenterFacilityDto>().FirstOrDefaultAsync();
                         facilities.Add(facility);
                    }
            } else
            {
                facilities = facilityQuery.ProjectTo<CallCenterFacilityDto>().ToList();
            }

            var codes = await GetProcedureCptCodes();

            foreach (var fac in facilities)
            {
                fac.CptCodes = codes.Where(c => c.FacilityId == fac.Id)
                    .Select(c => new CallCenterFacilityCpt
                    {
                        Id = c.Id,
                        NodeLibraryId = c.NodeLibraryId,
                        Code = c.Code
                    })
                    .ToList();
            }

            return facilities;
        }

        public async Task<List<Company>> GetAllFacilities()
        {
            return await _db.Companies.Where(c => c.IsNew == false &&
                    c.CompanyType == CompanyType.Practice &&
                    c.DeletedOn == null)
                .ToListAsync();
        }

        public async Task<List<CallCenterFacilityTypeaheadDto>> GetFilteredFacilities(string filter)
        {
            if (string.IsNullOrWhiteSpace(filter))
            {
                return new List<CallCenterFacilityTypeaheadDto>();
            }

            var lower = filter.ToLowerInvariant();

            return await _db.Companies
                .ProjectTo<CallCenterFacilityTypeaheadDto>()
                .Where(c => c.IsNew == false &&
                    c.CompanyType == CompanyType.Practice &&
                    c.DeletedOn == null &&
                    c.Name.Contains(lower))
                .ToListAsync();
        }

        public async Task<List<CallCenterFacilityCpt>> GetProcedureCptCodes()
        {
            var sql = @"
SELECT distinct 
	cpt.Id,
	cpt.Code, 
	c.Id as FacilityId, 
	cpt.NodeLibraryId,
	c.Name, cpr.PracticeCode, 
	p.Id as PhysicianId
FROM CptCodes cpt
INNER JOIN NodeLibraries nl on nl.Id = cpt.NodeLibraryId
INNER JOIN Physicians p on p.Id = nl.PhysicianId
INNER JOIN Companies c on c.Id = p.CompanyId
INNER JOIN CallCenterPracticeReferences cpr on cpr.PracticeId = c.Id
WHERE p.Id = (SELECT TOP 1 Id FROM Physicians WHERE CompanyId in (c.Id))
AND cpr.PracticeCode IS NOT NULL
";

            var cn = _db.Database.Connection;
            cn.Open();
            var codes = (await cn.QueryAsync<CallCenterFacilityCpt>(sql)).Distinct(new CptCodeComparer()).ToList();
            cn.Close();

            return codes;
        }
        #endregion

        #region Reports
        public async Task<List<Report>> GetReports()
        {
            return await _db.Reports.ToListAsync();
        }
        #endregion

        private int GetTimeZoneOffset(string zone)
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById(zone);
            return (int)tz.BaseUtcOffset.TotalHours;
        }

        public async Task<bool> SendTextMessage(int patientId, string phone, int messageType, string messageContent)
        {
            if (string.IsNullOrEmpty(phone))
            {
                return false;
            }

            var onDemandMessage = new OnDemandMessage
            {
                PatientId = patientId,
                Phone = phone,
                AccessCode = _db.Procedures.Where(p => p.PatientId == patientId).Select(p => p.AccessCode).FirstOrDefault(),
                MessageType = messageType == 0 ? "Text Message" : "Push Notification",
                MessageContent = messageContent
            };

            _auditauditService.Audit(onDemandMessage);

            _db.OnDemandMessages.Add(onDemandMessage);

            await _db.SaveChangesAsync();

            try
            {
                var regex = new Regex("[^0-9]");
                var toPhone = onDemandMessage.Phone;

                var text = onDemandMessage.MessageContent;

                TwilioClient.Init(Sid, Token);

                var message = MessageResource.Create(
                body: text,
                messagingServiceSid: ConfigurationManager.AppSettings["MessagingServiceSid"],
                to: new Twilio.Types.PhoneNumber(toPhone)
                );

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task ScheduleACallTomorrow(int id)
        {
            var call = _db.CallCenterScheduledCalls.First(c => c.Id == id);
            var zone = "";

            try
            {
                var ccp = _db.CallCenterProcedures.First(p => p.Id == call.CallCenterProcedureId);
                var proc = _db.Procedures.First(p => p.Id == ccp.ProcedureId);
                var fac = _db.Facilities.First(f => f.Id == proc.FacilityId);
                var co = _db.Companies.First(c => c.Id == fac.CompanyId);
                zone = co.TimeZone;
            }
            catch (Exception ex)
            {
            }

            var date = new DateTime(call.CallYear, call.CallMonth, call.CallDay, 0, 0, 0);
            var tomorrowDate = date.AddDays(1);

            var normalizedDate = _dateService.ToUniversalTime(tomorrowDate, zone);

            var newCall = new CallCenterScheduledCall
            {
                CallCenterProcedureId = call.CallCenterProcedureId,
                CallDay = normalizedDate.Day,
                CallMonth = normalizedDate.Month,
                CallYear = normalizedDate.Year,
                CallTime = normalizedDate,
                Index = call.Index + 1,
                CallType = CallType.Assessment,
                CallStatus = CallStatus.Scheduled,
                IsNew = true
            };
            newCall.Audit(_auditauditService);
            _db.CallCenterScheduledCalls.Add(newCall);

            await _db.SaveChangesAsync();
        }
        public async Task<bool> ScheduleANewTimeTomorrow(int id, int Hour, int Minute)
        {
            var call = _db.CallCenterScheduledCalls.First(c => c.Id == id);

            var nextCall = _db.CallCenterScheduledCalls.First(c => c.Index == (call.Index + 1) && c.CallCenterProcedureId == call.CallCenterProcedureId);

            var newTime = new DateTime(nextCall.CallYear, nextCall.CallMonth, nextCall.CallDay, Hour, Minute, 0,
                DateTimeKind.Local);

            var zone = "";

            try
            {
                var ccp = _db.CallCenterProcedures.First(p => p.Id == call.CallCenterProcedureId);
                var proc = _db.Procedures.First(p => p.Id == ccp.ProcedureId);
                var fac = _db.Facilities.First(f => f.Id == proc.FacilityId);
                var co = _db.Companies.First(c => c.Id == fac.CompanyId);
                zone = co.TimeZone;
            }
            catch (Exception ex)
            {
            }

            var utc = _dateService.ToUniversalTime(newTime, zone);

            nextCall.CallTime = utc;
            nextCall.Audit(_auditauditService);

            await _db.SaveChangesAsync();
            return true;
        }

        public async Task<CallCenterReportDto> AuthenticateForCallCenterReports()
        {
            try
            {
                var stimulsofthost = ConfigurationManager.AppSettings["StimulSoftReportingURL"];
                var username = ConfigurationManager.AppSettings["StimulSoftReportingUsername"];
                var password = ConfigurationManager.AppSettings["StimulSoftReportingPassword"];
                var connection = new Stimulsoft.Server.Connect.StiServerConnection(stimulsofthost);

                await connection.Accounts.Users.LoginAsync(username, password);
                return new CallCenterReportDto
                {
                    IsAuthenticatedForGeneratingReports = true
                };
            }
            catch (Exception ex)
            {
                return new CallCenterReportDto
                {
                    IsAuthenticatedForGeneratingReports = false
                };
            }

        }
        public async Task<PracticeDto> CheckForAdditionalQuestions(int practiceId)
        {
            var practiceDetail = await _db.CallCenterPracticeReferences.FirstOrDefaultAsync(p => p.PracticeId == practiceId);
            return new PracticeDto()
            {
                InfuFacilityId = practiceDetail.InfuFacilityId,
                Addl_URL = practiceDetail.Addl_URL
            };
        }

        public async Task<Additional_URL> CheckForAddURL(int practiceId)
        {
            var practiceDetail = await _db.CallCenterPracticeReferences.FirstOrDefaultAsync(p => p.PracticeId == practiceId);
            var addlURL = practiceDetail.Addl_URL;
            if (addlURL == null)
            {
                return new Additional_URL(false);
            }
            return new Additional_URL(true);
        }
    }

    public class CallCenterProviderRef : CallCenterProvider
    {
        public int FID { get; set; }
        public int FacilityId { get; set; }
    }

    public class FacilityCpt
    {
        public int FacilityId { get; set; }
        public int LibraryId { get; set; }
        public int CptId { get; set; }
        public string Code { get; set; }
    }

    public class CptCodeComparer : IEqualityComparer<CallCenterFacilityCpt>
    {
        public bool Equals(CallCenterFacilityCpt x, CallCenterFacilityCpt y)
        {
            if (x == null && y == null)
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Code.ToLowerInvariant() == y.Code.ToLowerInvariant() && x.FacilityId == y.FacilityId;
        }

        public int GetHashCode(CallCenterFacilityCpt obj)
        {
            return obj.Code.GetHashCode();
        }
    }

    public class CallCenterRefDto
    {
        public List<CallCenterFacilityDto> Facilities { get; set; }
        public List<CallCenterPayor> Payors { get; set; }

        public List<CallCenterProvider> Physicians { get; set; }
        public List<CallCenterProvider> Nurses { get; set; }
        public List<CallCenterProvider> Anesthesiologists { get; set; }
        //public List<CallCenterCommentType> CommentTypes { get; set; }
    }

    public class DischargeDate
    {
        public int CallYear { get; set; }
        public int CallMonth { get; set; }
        public int CallDay { get; set; }

        public override string ToString()
        {
            return new DateTime(CallYear, CallMonth, CallDay).ToShortDateString();
        }
    }

    public class CallCenterPatientSearchModel
    {
        public string Filter { get; set; }
        public PatientFilterType FilterType { get; set; }
        public PagingParameters Paging { get; set; }

        public string GetFilter()
        {
            return $"%{Filter}%";
        }
    }

    public class CallCenterCommentTypes
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public int Title { get; set; }
    }

    public enum ProviderFilterType
    {
        Id = 0,
        FirstName = 1,
        LastName = 2
    }

    public enum PatientFilterType
    {
        None = 0,
        PumpSerial = 1,
        FirstName = 2,
        LastName = 3,
        Phone = 4
    }
}