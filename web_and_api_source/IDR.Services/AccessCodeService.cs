using System;
using System.Linq;

namespace IDR.Services
{
    public static class AccessCodeService
    {
        public static string Generate(int size = 8)
        {
            //var chars = "ABCDEF0123456789";
            var chars = "ABCDEF23456789";

            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, size)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
    }
}