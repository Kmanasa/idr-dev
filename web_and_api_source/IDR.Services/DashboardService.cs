﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IDR.Data;
using IDR.Domain.Dashboard;
using IDR.Domain.Portal.Dto;

namespace IDR.Services
{
    public class DashboardService
    {
        private readonly ApplicationDbContext _context;
        private readonly PhysiciansService _physiciansService;

        public DashboardService(ApplicationDbContext context,PhysiciansService physiciansService)
        {
            _context = context;
            _physiciansService = physiciansService;
        }

        public async Task<List<DashboardItem>> GetDashboard(string userId, int? proxyId)
        {
            PhysicianDto physician;

            if (proxyId.HasValue && proxyId != 0)
            {
                physician = await _physiciansService.GetPhysician(proxyId.Value);
            }
            else
            {
                physician = await _context.GetPhysicianUser(userId);
            }

            #region Patient Procedure Sql
            var sql =
@"SELECT 
	p.Id as ProcedureId, p.PhysicianId, p.PatientId, p.NodeLibraryId, p.CptCode, p.AccessCode, p.FacilityId, p.StartDate, DATEADD(day, l.Duration, p.StartDate) as EndDate,
	dr.FirstName as PhysicianFirstName, dr.LastName as PhysicianLastName,
	l.Name as ProcedureName,
	pa.FirstName as PatientFirstName, pa.LastName as PatientLastName, pa.MedicalRecordNumber, pa.Email PatientEmail, pa.Phone as PatientPhone
FROM Procedures p 
JOIN Patients pa ON pa.Id = p.PatientId
JOIN NodeLibraries l ON p.NodeLibraryid = l.id
JOIN Physicians dr ON p.PhysicianId = dr.id
WHERE 
p.NodeLibraryId in 
	(SELECT Id FROM NodeLibraries 
		WHERE 
			DeletedOn is null 
			AND isnew = 0 
			AND Active = 1 
			AND LibraryType = 'master-template' 
			AND PhysicianId = @PhysicianId
	) 
AND
	p.DeletedOn is null 
AND 
	p.IsNew = 0 
--AND 
--	p.StartDate <= GetDate() 
AND
	DATEADD(day, l.Duration, p.StartDate) >= GetDate()
";
            #endregion

            #region Procedure Notification Sql

            var nsql = @"SELECT 
	pn.Id, pn.NotificationId, pn.ProcedureId, pn.Sent, pn.SendTime, pn.NextAttemptTime, pn.Attempts, pn.Response, pn.DateResponded, pn.Triggered,
    n.Message, n.Title, n.Required, n.Repetitions
FROM 
ProcedureNotifications pn
JOIN Notifications n on n.Id = pn.NotificationId
JOIN Procedures p on p.Id = pn.ProcedureId
JOIN NodeLibraries l ON p.NodeLibraryid = l.id
JOIN Physicians dr ON p.PhysicianId = dr.id
WHERE 
p.NodeLibraryId in 
	(SELECT Id FROM NodeLibraries 
		WHERE 
			DeletedOn is null 
			AND isnew = 0 
			AND Active = 1 
			AND LibraryType = 'master-template' 
			AND PhysicianId = @PhysicianId
	) 
AND
	p.DeletedOn is null 
AND 
	p.IsNew = 0 
--AND 
--	p.StartDate <= GetDate() 
AND
	DATEADD(day, l.Duration, p.StartDate) >= GetDate()
ORDER BY
    n.DaysOffset, hour, Minute
";
            #endregion 

            var cn = _context.Database.Connection;
            cn.Open();
            var items = (await cn.QueryAsync<DashboardItem>(sql, new { PhysicianId = physician.Id })).ToList();
            var responses = (await cn.QueryAsync<DashboardNotification>(nsql, new { PhysicianId = physician.Id })).ToList();
            cn.Close();

            foreach (var response in responses)
            {
                var dashboardItem = items.FirstOrDefault(d => d.ProcedureId == response.ProcedureId);

                dashboardItem?.DashboardNotifications.Add(response);
            }

            items = items.OrderBy(i => i.StartDate).ToList();

            items = items.Where(i => i.DashboardNotifications.Any()).ToList();

            SetOpFlags(items);

            return items;
        }

        private void SetOpFlags(List<DashboardItem> items)
        {
            foreach (var procedure in items)
            {
                foreach (var notification in procedure.DashboardNotifications)
                {
                    notification.IsPreOp = notification.SendTime < procedure.StartDate;
                }
            }
        }

        public async Task<DashboardItem> GetProcedureDashboard(int procedureId)
        {
            #region Patient Procedure Sql
            var sql =
@"SELECT 
	p.Id as ProcedureId, p.PhysicianId, p.PatientId, p.NodeLibraryId, p.CptCode, p.AccessCode, p.FacilityId, p.StartDate, DATEADD(day, l.Duration, p.StartDate) as EndDate,
	dr.FirstName as PhysicianFirstName, dr.LastName as PhysicianLastName,
	l.Name as ProcedureName,
	pa.FirstName as PatientFirstName, pa.LastName as PatientLastName, pa.MedicalRecordNumber, pa.Email PatientEmail, pa.Phone as PatientPhone
FROM Procedures p 
JOIN Patients pa ON pa.Id = p.PatientId
JOIN NodeLibraries l ON p.NodeLibraryid = l.id
JOIN Physicians dr ON p.PhysicianId = dr.id
WHERE 
    p.Id = @ProcedureId
";
            #endregion

            #region Procedure Notification Sql

            var nsql = @"SELECT 
	pn.Id, pn.NotificationId, pn.ProcedureId, pn.Sent, pn.SendTime, pn.NextAttemptTime, pn.Attempts, pn.Response, pn.DateResponded, pn.Triggered, pn.Attempts,
    n.Message, n.Title, n.Required, n.Repetitions
FROM 
ProcedureNotifications pn
JOIN Notifications n on n.Id = pn.NotificationId
JOIN Procedures p on p.Id = pn.ProcedureId
JOIN NodeLibraries l ON p.NodeLibraryid = l.id
JOIN Physicians dr ON p.PhysicianId = dr.id
WHERE 
    p.Id = @ProcedureId
ORDER BY
    n.DaysOffset, hour, Minute
";
            #endregion 

            var cn = _context.Database.Connection;
            cn.Open();
            var dashboard = (await cn.QueryAsync<DashboardItem>(sql, new { ProcedureId = procedureId })).Single();
            var responses = (await cn.QueryAsync<DashboardNotification>(nsql, new { ProcedureId = procedureId })).ToList();
            cn.Close();

            foreach (var response in responses)
            {
                dashboard.DashboardNotifications.Add(response);
            }

            return dashboard;
        }
    }
}
