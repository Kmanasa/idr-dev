﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Helpers;
using IDR.Domain.PracticeUsers.Dto;

namespace IDR.Services
{
	public class CallSiteMobileUserService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;

		public CallSiteMobileUserService(ApplicationDbContext context, IAuditService auditService)
		{
			_context = context;
			_auditService = auditService;
		}

		public async Task<CallSiteMobileUserDto> GetUser(string key)
		{
			var item = await _context.CallSiteMobileUsers
				.FirstOrDefaultAsync(x => x.DeletedOn == null &&
				                          !string.IsNullOrEmpty(key) && x.ProvisionalKey.ToString() == key);

			var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

			var dto = Mapper.Map<CallSiteMobileUserDto>(item);

			dto.UserName = user != null ? user.UserName : string.Empty;

			dto.LoadContacts(_context);

			return dto;
		}

		public async Task<CallSiteMobileUserDto> GetUser(int id)
		{
			var item = await _context.CallSiteMobileUsers
				.FirstOrDefaultAsync(x => x.DeletedOn == null &&
				                          x.Id == id);

			var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.UserId);

			var dto = Mapper.Map<CallSiteMobileUserDto>(item);

			dto.UserName = user != null ? user.UserName : string.Empty;

			dto.LoadContacts(_context);

			return dto;
		}

		public async Task<CallSiteMobileUserDto> Create(int practiceId)
		{
			var practice = await _context.Companies.FirstOrDefaultAsync(x => x.Id == practiceId);
			var data = new CallSiteMobileUser
			{
				CompanyId = practiceId,
				IsProvisional = true,
				ProvisionalKey = Guid.NewGuid(),
			};
			data.Audit(_auditService);

			_context.Entry(data).State = EntityState.Added;
			data.IsProvisional = true;

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<CallSiteMobileUserDto>(data);
			dto.Practice = Mapper.Map<CompanyDto>(practice);
			dto.SaveContacts(_context, _auditService);

			return dto;
		}

		public async Task<CallSiteMobileUserDto> Delete(int id)
		{
			var data = await _context.CallSiteMobileUsers.FirstOrDefaultAsync(x => x.Id == id);

			data.DeletedOn = DateTime.Now;
			_auditService.Audit(data);
			await _context.SaveChangesAsync();

			return Mapper.Map<CallSiteMobileUserDto>(data);
		}

		public async Task<CallSiteMobileUserDto> Update(CallSiteMobileUserDto calluser)
		{
			var existing = await _context
				.CallSiteMobileUsers
				.FirstOrDefaultAsync(x => x.Id == calluser.Id);

			if (!string.IsNullOrEmpty(calluser.UserId))
			{
				var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == calluser.UserId);

				if (user != null)
				{
					user.Firstname = calluser.FirstName;
					user.Lastname = calluser.LastName;
				}
			}

			var admin = Mapper.Map<CallSiteMobileUser>(calluser);
			admin.Audit(_auditService);

			_context.Entry(existing).State = EntityState.Detached;
			_context.Entry(admin).State = EntityState.Modified;

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<CallSiteMobileUserDto>(admin);
			dto = calluser.SaveContacts(_context, _auditService);

			return dto;
		}
	}
}