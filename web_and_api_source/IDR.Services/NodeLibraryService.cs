﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Dapper;
using IDR.Data;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Cms.Dto;
using IDR.Domain.Disciplines.Helper;
using IDR.Domain.External.Dto;
using IDR.Infrastructure;
using IDR.Services.Models.PDFs;

namespace IDR.Services
{
	public class NodeLibraryService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;
		private List<NodeLibraryDto> _replacements = new List<NodeLibraryDto>();
        private readonly PDFService _pdfService;
        private readonly GoogleMapService _googleMapService;

        #region Node Libraries
        public NodeLibraryService(ApplicationDbContext context, IAuditService auditService, 
            PDFService pdfService, GoogleMapService googleMapService)
		{
			_context = context;
			_auditService = auditService;
            _pdfService = pdfService;
            _googleMapService = googleMapService;

        }

		/// <summary>
		/// Gets a list of avaialble node libraries
		/// </summary>
		public async Task<List<NodeLibraryDto>> GetNodeLibraries(string libraryType)
		{
			var noMasters = string.IsNullOrEmpty(libraryType);

			var apps = await _context.NodeLibraries
				.AsNoTracking()
				.Include("Nodes.Properties")
				.Include("Nodes.Type.Properties")
				.Include("Nodes.Properties.Type")
				.Where(lib => lib.DeletedOn == null && !lib.IsNew)
				.Where(lib => lib.PhysicianId == null)
				.Where(lib =>
					(noMasters && lib.LibraryType != Constants.NodeLibraryTypes.MasterTemplate) ||
					(!noMasters && lib.LibraryType == libraryType))
				.ToListAsync();

			var dtos = apps.Select(f =>
			{
				var dto = Mapper.Map<NodeLibraryDto>(f);

				dto.Nodes = dto.Nodes.Where(node => node.DeletedOn == null && node.NodeId == null).ToList();

				return dto;
			}).ToList();

			dtos.LoadDisciplines(_context);

			return dtos;
		}

		/// <summary>
		/// Gets a list of node libraries
		/// </summary>
		public async Task<List<MasterTemplateExternalDto>> GetNodeLibrariesExternal()
		{
			//TODO: check for duration exceeded?
			var libraries = await _context.NodeLibraries
				.Project().To<MasterTemplateExternalDto>()
				.Where(lib =>
					lib.DeletedOn == null &&
					lib.IsNew == false &&
					lib.Active &&
					lib.LibraryType == Constants.NodeLibraryTypes.MasterTemplate &&
					lib.PhysicianId == null)
				.ToListAsync();

			return libraries;
		}

		/// <summary>
		/// Gets a node library with optionally replaced reusable content
		/// </summary>
		public async Task<NodeLibraryDto> GetNodeLibrary(int id,
			bool replaceReusableContent = false, bool loadDisciplines = false)
		{
			var library = await _context.NodeLibraries
				.AsNoTracking()
				.Include(n => n.CptCodes)
				.FirstAsync(n => n.Id == id);

			var nodes = await _context.Nodes
				.Include(n => n.Properties)
				.Include(n => n.Properties.Select(p => p.Type))
				.Include(n => n.Type)
				.Where(n => n.NodeLibraryId == id && n.DeletedOn == null)
				.ToListAsync();

            var model = new NodeLibraryDto
            {
                Id = library.Id,
                Active = library.Active,
                CtssCode = library.CtssCode,
                Description = library.Description,
                Key = library.Key,
                LibraryType = library.LibraryType,
                Name = library.Name,
                Duration = library.Duration,
				Nodes = GetNodesForParent(null, nodes)
			};

			foreach (var cptCode in library.CptCodes)
			{
				model.CptCodes.Add(new CptCodeDto
				{
					Code = cptCode.Code,
					NodeLibraryId = cptCode.NodeLibraryId,
					Id = cptCode.Id
				});
			}

			if (replaceReusableContent)
			{
				await ReplaceReusableContent(model.Nodes);
			}

			if (loadDisciplines)
			{
				model.LoadDiscipline(_context);
			}

			var attributes = await _context.NodeLibraryAttributes
				.Project().To<NodeLibraryAttributeDto>()
				.Where(at => at.NodeLibraryId == id)
				.ToListAsync();

			model.Attributes = attributes;

            int? physicianId = _context.NodeLibraries.Where(x => x.Id == id).Select(x => x.PhysicianId).FirstOrDefault();
            int? companyId = _context.Physicians.Where(y => y.Id == physicianId).Select(y => y.CompanyId).FirstOrDefault();

            List<PDFFileDto> pdfFilesForNodeLibrary = await _pdfService.SearchPDFs(companyId);
            model.PDFFilesForNodeLibrary = pdfFilesForNodeLibrary;

            AddressDto address = await _googleMapService.GetAddressForNodeLibrary(id);
            model.AddressForNodeLibrary = address;

            return model;
		}

		/// <summary>
		/// Gets a node library based on a patient's procedure access code
		/// </summary>
		public async Task<NodeLibraryDto> GetPatientNodeLibrary(string accessCode)
		{
			var procedure = await _context.Procedures
				.AsNoTracking()
				.FirstOrDefaultAsync(p => p.AccessCode == accessCode);

			return await GetNodeLibrary(procedure.NodeLibraryId, true);
		}

		private async Task ReplaceReusableContent(List<NodeDto> nodes)
		{
			var nodesWithChildType = nodes.Where(n => n.ChildTemplateId.HasValue).ToList();
			var uniqueChildIds = nodesWithChildType.Select(n => n.ChildTemplateId.Value).Distinct();
			var replacementNodes = new List<NodeLibraryDto>();

			foreach (var childId in uniqueChildIds)
			{
				var childLib = await GetNodeLibrary(childId, true);
				replacementNodes.Add(childLib);
			}

			foreach (var replacement in replacementNodes)
			{
				if (_replacements.All(r => r.Id != replacement.Id))
				{
					_replacements.Add(replacement);
				}
			}

			foreach (var replacement in _replacements)
			{
				var nodeToReplace = nodes.FirstOrDefault(n =>
					n.ChildTemplateId.HasValue &&
					n.ChildTemplateId.Value == replacement.Id);

				if (nodeToReplace == null) { continue; }

				var index = nodes.IndexOf(nodeToReplace);

				for (var i = 0; i < replacement.Nodes.Count; i++)
				{
					var preservedName = nodes[index].Name;
					var preservedTitle = nodes[index].Title;
					var preservedDescription = nodes[index].Description;
					var preservedSequence = nodes[index].Sequence;
					var menuIconStyle = nodes[index].MenuIconStyle;
					var subMenuText = nodes[index].SubMenuText;

					if (i == 0)
					{
						nodes[index] = replacement.Nodes[i];
					}
					else
					{
						nodes.Insert(index + i, replacement.Nodes[i]);
					}

					nodes[index].Name = preservedName;
					nodes[index].Title = preservedTitle;
					nodes[index].Description = preservedDescription;
					nodes[index].Sequence = preservedSequence;
					nodes[index].MenuIconStyle = menuIconStyle;
					nodes[index].SubMenuText = subMenuText;
				}
			}

			foreach (var n in nodes)
			{
				await ReplaceReusableContent(n.Nodes);
			}
		}

		/// <summary>
		/// Search node libraries with pagination
		/// </summary>
		public async Task<PagedQueryResults<MasterTemplateExternalDto>> SearchNodeLibraries(
			string term,
			PagingParameters paging,
			bool activeOnly = true)
		{
			var query = _context.NodeLibraries
				.Project().To<MasterTemplateExternalDto>()
				.Where(lib => lib.DeletedOn == null && !lib.IsNew)
				.Where(lib => lib.PhysicianId == null)
				.Where(lib => lib.LibraryType == Constants.NodeLibraryTypes.MasterTemplate);

			if (!string.IsNullOrEmpty(term))
			{
				var terms = term.Split(' ');

				query = query.Where(lib =>
					terms.Any(t => lib.CptCodes.Any(cpt => cpt.Code.Contains(t))) ||
					terms.Any(t => lib.Name.Contains(t)));
			}

			if (activeOnly)
			{
				query = query.Where(lib => lib.Active);
			}

			var count = await query.CountAsync();

			var libraries = await query
				.OrderBy(lib => lib.Name)
				.Skip(paging.Offset * paging.Limit)
				.Take(paging.Limit)
				.ToListAsync();

			return new PagedQueryResults<MasterTemplateExternalDto>(libraries, count);
		}

		/// <summary>
		/// Create a new node library
		/// </summary>
		public async Task<NodeLibraryDto> CreateNodeLibrary(string nodeType)
		{
			var library = new NodeLibrary
			{
				Name = "New Application",
				Description = "Description",
				Key = "new-application",
				LibraryType = nodeType,
				IsNew = true,
				Duration = 14
			};

			var library1 = library;
			var newApps = await _context.NodeLibraries.Where(x => x.Key.Contains(library1.Key)).ToListAsync();
			if (newApps.Any())
			{
				library.Key += "-" + newApps.Count();
			}

			if (nodeType.ToLower() == "content-template")
			{
				library.Active = true;
			}

			library = _context.NodeLibraries.Add(library);

			_auditService.Audit(library);

			_context.SaveChanges();

			return Mapper.Map<NodeLibraryDto>(library);
		}

		/// <summary>
		/// Add a new node to a library
		/// </summary>
		public async Task<NodeDto> AddNode(Node node)
		{
			node.TypeId = node.Type.Id;
			node.Type = null;

			// get and insert new props
			var node1 = node;
			var props = (await _context.NodeTypes
				.Include(x => x.Properties)
				.FirstOrDefaultAsync(x => x.Id == node1.TypeId))
				.Properties.ToList()
				.Select(x =>
					new Property
					{
						Name = x.Name,
						Title = x.Name,
						TypeId = x.Id,
						IsNew = true
					}
				).ToList();

			node.Properties = props;
			node = _context.Nodes.Add(node);

			_auditService.Audit(node);
			await _context.SaveChangesAsync();

			node = _context.Nodes.FirstOrDefault(x => x.Id == node.Id);

			return Mapper.Map<NodeDto>(node);
		}

		/// <summary>
		///  Update a node
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public async Task<NodeDto> UpdateNode(Node model)
		{
			var node = await _context.Nodes
				.Include(x => x.Properties)
				.FirstOrDefaultAsync(x => x.Id == model.Id);

			node.Name = model.Name;
			node.Title = model.Title;
			node.Description = model.Description;
			node.IsNew = false;
			node.IsPhysicianEditable = model.IsPhysicianEditable;
			node.Sequence = model.Sequence;
			node.ChildTemplateId = model.ChildTemplateId;
			node.HtmlContent = model.HtmlContent;
			node.SubMenuText = model.SubMenuText;
			node.MenuIconStyle = model.MenuIconStyle;
			node.ImageData = model.ImageData;

			foreach (var prop in model.Properties)
			{
				var p = node.Properties.FirstOrDefault(x => x.Id == prop.Id);

				if (p != null)
				{
					p.Value = prop.Value;
				}
			}

			_auditService.Audit(node);

			await _context.SaveChangesAsync();

			return Mapper.Map<NodeDto>(model);
		}

		/// <summary>
		///  Copy an existing node and it's sub structure
		/// </summary>
		public async Task<NodeDto> CopyNode(int id)
		{
			var originalNode = await _context.Nodes
				.Include("Nodes.Type")
				.Include("Nodes.Properties")
				.Include("Nodes.Type.Properties")
				.Include("Nodes.Properties.Type")
				.FirstAsync(x => x.Id == id);

			var newNode = CopyNode(originalNode, originalNode.NodeLibraryId);

			_context.Nodes.Add(newNode);

			await _context.SaveChangesAsync();

			var mapped = Mapper.Map<NodeDto>(newNode);
			var nodeType = await _context.NodeTypes
				.Project().To<NodeTypeDto>()
				.FirstOrDefaultAsync(nt => nt.Id == originalNode.TypeId);

			mapped.Type = nodeType;
			return mapped;
		}

		//Copy an entire library
		public async Task<NodeLibraryDto> CopyLibrary(int id, string userId = null, int? proxyId = null, bool isContent = false)
		{
			int? physicianId = null;

			if (proxyId.HasValue && proxyId.Value != 0)
			{
				physicianId = proxyId.Value;
			}

			else if (!string.IsNullOrEmpty(userId))
			{
				var physician = await _context.GetPhysicianUser(userId);
				if (physician != null)
				{
					physicianId = physician.Id;
				}
			}

			var lib = await _context.NodeLibraries
				.AsNoTracking()
				.Include(n => n.CptCodes)
				.Include(n => n.Notifications)
				.Include(n => n.Notifications.Select(c => c.NotificationResponses))
				.FirstOrDefaultAsync(x => x.Id == id);

			lib.Id = 0;

			var newName = lib.Name ?? string.Empty;

			if (!physicianId.HasValue)
			{
				newName = "(copy)";
				if (!string.IsNullOrEmpty(lib.Name))
				{
					newName = lib.Name.Replace(" (copy)", string.Empty) + " (copy)";
				}
			}
			else
			{
				lib.PhysicianId = physicianId.Value;
			}

			lib.Name = newName;
			if (isContent)
			{
				lib.LibraryType = "content-template";
			}

			lib.Active = true;
			_auditService.Audit(lib);
			_context.Entry(lib).State = EntityState.Added;

			_context.NodeLibraries.Add(lib);

			#region Copy CPT Codes and Notifications
			// Set the CPT codes as new
			foreach (var cpt in lib.CptCodes)
			{
				cpt.Id = 0;
				cpt.DeletedOn = null;
				_context.Entry(cpt).State = EntityState.Added;
			}

			foreach (var nt in lib.Notifications)
			{
				nt.Id = 0;
				_context.Entry(nt).State = EntityState.Added;

				foreach (var r in nt.NotificationResponses)
				{
					r.Id = 0;
					_context.Entry(r).State = EntityState.Added;
				}
			}
			#endregion

			_context.SaveChanges();

			#region Copy nodes
			// Copy the nodes from the origin library to the target library
			var cn = _context.Database.Connection;
			cn.Open();

			_context.Database.Connection.Execute("CopyNodes",
				new
				{
					baseNodeLibraryId = id,
					newNodeLibraryId = lib.Id
				},
				commandType: CommandType.StoredProcedure);
			cn.Close();
			#endregion

			return new NodeLibraryDto
			{
				Id = lib.Id,
				CtssCode = lib.CtssCode,
				Description = lib.Description,
				Key = lib.Key,
				LibraryType = lib.LibraryType,
				Name = lib.Name,
				CptCodes = lib.CptCodes.Select(c => new CptCodeDto { Code = c.Code }).ToList()
			};
		}

		/// <summary>
		/// Delete a node
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public async Task<NodeDto> DeleteNode(int id)
		{
			var node = await _context.Nodes.FirstOrDefaultAsync(x => x.Id == id);
			node.DeletedOn = DateTime.Now;
			_auditService.Audit(node);

			await _context.SaveChangesAsync();

			return Mapper.Map<NodeDto>(node);
		}

		private List<NodeDto> GetNodesForParent(int? parentNodeId, List<Node> allNodes)
		{
			var nodes = allNodes.Where(n => n.NodeId == parentNodeId).ToList();

			var models = nodes.Select(n => new NodeDto
			{
				Description = n.Description,
				ChildTemplateId = n.ChildTemplateId,
				DeletedOn = n.DeletedOn,
				HtmlContent = n.HtmlContent,
				SubMenuText = n.SubMenuText,
				Id = n.Id,
				IsNew = n.IsNew,
				IsPhysicianEditable = n.IsPhysicianEditable,
				MenuIconStyle = n.MenuIconStyle,
				Name = n.Name,
				Nodes = GetNodesForParent(n.Id, allNodes),
				NodeId = n.NodeId,
				NodeLibraryId = n.NodeLibraryId,
				OverrideSourceId = n.OverrideSourceId,
				Properties = n.Properties.Where(p => p.DeletedOn == null).Select(Mapper.Map<PropertyDto>).ToList(),
				Sequence = n.Sequence,
				Title = n.Title,
				Type = Mapper.Map<NodeTypeDto>(n.Type),
				TypeId = n.TypeId,
				ImageData = n.ImageData
			}).ToList();

			return models;
		}

		private Node CopyNode(Node node, int? libraryId)
		{
			var newNode = new Node
			{
				Nodes = new List<Node>(),
				ChildTemplateId = node.ChildTemplateId,
				CopyOfId = node.Id,
				DeletedOn = node.DeletedOn,
				Description = node.Description,
				HtmlContent = node.HtmlContent,
				SubMenuText = node.SubMenuText,
				IsNew = node.IsNew,
				IsPhysicianEditable = node.IsPhysicianEditable,
				MenuIconStyle = node.MenuIconStyle,
				Name = node.Name,
				NodeId = node.NodeId,
				// Assign the given library ID or default to the source node's ID
				NodeLibraryId = libraryId ?? node.NodeLibraryId,
				OverrideSourceId = node.OverrideSourceId,
				Properties = new List<Property>(),
				RecoveryPlanId = node.RecoveryPlanId,
				Sequence = node.Sequence,
				Title = node.Title,
				TypeId = node.TypeId
			};
			_auditService.Audit(newNode);


			#region Property mapping
			if (node.Properties != null)
			{
				foreach (var prop in node.Properties)
				{
					var newProperty = new Property
					{
						CopyOfId = prop.CopyOfId,
						CssClass = prop.CssClass,
						DeletedOn = prop.DeletedOn,
						IsNew = prop.IsNew,
						Name = prop.Name,
						PropertyId = prop.PropertyId,
						Title = prop.Title,
						TypeId = prop.TypeId,
						Value = prop.Value
					};
					_auditService.Audit(newProperty);

					newNode.Properties.Add(newProperty);
				}
			}
			#endregion

			return newNode;
		}
		#endregion

		#region Attributes
		public async Task<List<AttributeDto>> GetAttributes()
		{
			var attributes = await _context.Attributes
				.AsNoTracking()
				.Project().To<AttributeDto>()
				.Where(a => a.DeletedOn == null && !a.IsNew)
				.ToListAsync();

			return attributes;
		}

		public async Task<AttributeDto> GetAttribute(int id)
		{
			var d = await _context.Attributes
				.FirstOrDefaultAsync(x => x.DeletedOn == null && x.Id == id);

			var dto = Mapper.Map<AttributeDto>(d);

			return dto;
		}

		public async Task<AttributeDto> AddAttribute()
		{
			var attr = new Data.Attribute
			{
				Name = "New Attribute",
				IsNew = true
			};

			attr = _context.Attributes.Add(attr);

			_auditService.Audit(attr);

			await _context.SaveChangesAsync();

			return Mapper.Map<AttributeDto>(attr);
		}

		public async Task<AttributeDto> UpdateAttribute(AttributeDto attr)
		{
			var attribute = await _context.Attributes.FirstOrDefaultAsync(x => x.Id == attr.Id);
			attribute.Name = attr.Name;
			attribute.IsNew = false;
			_auditService.Audit(attribute);

			_context.SaveChanges();

			return attr;
		}

		public async Task<NodeLibraryAttributeDto> AssociateAttribute(int nodeId, int attributeId)
		{
			var attr = new NodeLibraryAttribute
			{
				AttributeId = attributeId,
				NodeLibraryId = nodeId,
				IsNew = false
			};

			_auditService.Audit(attr);

			_context.NodeLibraryAttributes.Add(attr);
			await _context.SaveChangesAsync();

			return _context.NodeLibraryAttributes
				.Project().To<NodeLibraryAttributeDto>()
				.First(at => at.Id == attr.Id);
		}

		public async Task<bool> DissociateAttribute(int nodeLibraryAttributeId)
		{
			var attr = await _context.NodeLibraryAttributes
				.FirstOrDefaultAsync(a => a.Id == nodeLibraryAttributeId);

			if (attr != null)
			{
				_context.NodeLibraryAttributes.Remove(attr);
			}

			await _context.SaveChangesAsync();

			return true;
		}

		public async Task<int> DeleteAttribute(int id)
		{
			var attr = await _context.Attributes.FirstOrDefaultAsync(x => x.Id == id);

			attr.DeletedOn = DateTime.Now;
			_auditService.Audit(attr);
			await _context.SaveChangesAsync();

			return id;
		}
		#endregion

		public async Task<NodeLibraryDto> DeleteNodeLibrary(int id)
		{
			var library = await _context.NodeLibraries.FirstOrDefaultAsync(x => x.Id == id);
			library.DeletedOn = DateTime.Now;
			_auditService.Audit(library);

			var cpts = await _context.CptCodes.Where(c => c.NodeLibraryId == library.Id).ToListAsync();

			foreach (var cpt in cpts)
			{
				cpt.DeletedOn = DateTime.UtcNow;
			}

			await _context.SaveChangesAsync();

			return Mapper.Map<NodeLibraryDto>(library);
		}

		public async Task<NodeLibraryDto> UpdateNodeLibrary(NodeLibraryDto lib)
		{
			_context.CptCodes.RemoveRange(_context.CptCodes.Where(c => c.NodeLibraryId == lib.Id));
			await _context.SaveChangesAsync();

			var library = await _context.NodeLibraries.FirstOrDefaultAsync(x => x.Id == lib.Id);
			library.Name = lib.Name;
			library.Description = lib.Description;
			library.Key = lib.Key;
			library.IsNew = false;
			library.CtssCode = lib.CtssCode;
			library.Active = lib.Active;
			library.Duration = lib.Duration;

			foreach (var code in lib.CptCodes)
			{
				library.CptCodes.Add(new CptCode
				{
					NodeLibraryId = lib.Id,
					Code = code.Code
				});
			}

			library.Audit(_auditService);
			await _context.SaveChangesAsync();
			lib.AddDiscipline(_context, _auditService);

			return Mapper.Map<NodeLibraryDto>(lib);
		}

		public async Task<List<NodeTypeDto>> GetMetaData(string libraryType)
		{
			var nodes = await _context.NodeTypes
				.Include(n => n.Properties)
				.Include(n => n.AllowedTypes)
				.Where(n => string.IsNullOrEmpty(n.LibraryType) || n.LibraryType == libraryType)
				.Where(n => n.DeletedOn == null)
				.ToListAsync();

			var dtos = nodes.Select(Mapper.Map<NodeTypeDto>).ToList();

			return dtos;
		}

		public async Task<NodeTypeDto> GetNodeType(int id)
		{
			var node = await _context.NodeTypes
				.Include(x => x.Properties)
				.Include(x => x.AllowedTypes)
				.Where(x => x.Id == id)
				.FirstOrDefaultAsync();

			var dto = Mapper.Map<NodeTypeDto>(node);

			return dto;
		}

		public async Task<List<NodeLibraryDto>> GetNodeLibraryTemplates(int[] idArray)
		{
			var query = await _context.NodeLibraries
				.Include("Attributes")
				.Where(nl => nl.Attributes.Any() &&
							 nl.Active &&
							 nl.LibraryType == Constants.NodeLibraryTypes.ReusableTemplate)
							 .ToListAsync();

			var withAttributes = query
				.Where(nl => idArray.Aggregate(true, (current, id) => current & nl.Attributes.Any(at => at.AttributeId == id)))
				.ToList();

			var tempaltes = withAttributes
				.Select(Mapper.Map<NodeLibraryDto>)
				.ToList();

			return tempaltes;
		}

		public async Task<List<MasterTemplateExternalDto>> QueryMasterTemplates(string type)
		{
			var query = _context.NodeLibraries
			.Project().To<MasterTemplateExternalDto>()
			.Where(lib =>
			lib.DeletedOn == null &&
			lib.IsNew == false &&
			lib.LibraryType == type &&
			lib.PhysicianId == null);

			var libraries = await query.ToListAsync();

			return libraries;
		}

        public async Task<bool> DeletePlan(int id)
        {
            var existingNodeLibrary = await _context.NodeLibraries.FirstOrDefaultAsync(x => x.Id == id);
            existingNodeLibrary.DeletedOn = DateTime.Now;
            _auditService.Audit(existingNodeLibrary);

            await _context.SaveChangesAsync();

            return true;
        }

    }
}
