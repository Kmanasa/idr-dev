﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using IDR.Data;

namespace IDR.Services
{
    public class EmailTemplatesService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;

        public EmailTemplatesService(ApplicationDbContext context, IAuditService auditService)
        {
            _context = context;
            _auditService = auditService;
        }

        public List<EmailTemplate> GetTemplates()
        {
            return _context.EmailTemplates.ToList();
        }

        public EmailTemplate GetTemplate(string alias)
        {
            return _context.EmailTemplates.FirstOrDefault(c => c.Alias == alias);
        }

        public EmailTemplate UpdateTemplate(EmailTemplate model)
        {
            _auditService.Audit(model);
            _context.EmailTemplates.AddOrUpdate(model);
            _context.SaveChanges();
            return model;
        }
    }
}
