﻿using System;

namespace IDR.Services
{
    public class DateService
    {
        public DateTime NormalizeDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0,0,0);
        }
		
        public DateTime ToLocalTime(DateTime uctDate, string timeZone)
        {
            if (timeZone == null)
            {
                timeZone = DefaultTimeZone;
            }
            var zone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            var adjusted = TimeZoneInfo.ConvertTimeFromUtc(uctDate, zone);

            return adjusted;
        }

	    public DateTime ToUniversalTime(DateTime localTime, string zone)
	    {
		    try
		    {
			    var timezone = TimeZoneInfo.FindSystemTimeZoneById(zone);
				var local = new DateTime(localTime.Year, localTime.Month, localTime.Day, localTime.Hour, localTime.Minute, 0);
			    var utc = TimeZoneInfo.ConvertTimeToUtc(local, timezone);

			    return utc;
		    }
		    catch
		    {
			    return localTime;
		    } 
	    }

        public string DefaultTimeZone => "Mountain Standard Time";
    }
}
