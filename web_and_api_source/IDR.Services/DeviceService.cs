﻿using System.Threading.Tasks;
using IDR.Data;
using IDR.Domain;
using Microsoft.Azure;
using Microsoft.Azure.NotificationHubs;

namespace IDR.Services
{
    public class DeviceService
    {
        private readonly NotificationHubClient _hub;

        public DeviceService()
        {
            // Find the notification hub with registrations for the user's devices
            var endpoint = CloudConfigurationManager.GetSetting("NotificationHubEndpoint");
            var hubName = CloudConfigurationManager.GetSetting("HubName");
            _hub = NotificationHubClient.CreateClientFromConnectionString(endpoint, hubName);
        }

        public async Task RegisterDevice(DeviceDto device)
        {
            string regId = null;

            var channels = await _hub.GetRegistrationsByChannelAsync(device.Handle, 100);

            foreach (var reg in channels)
            {
                if (regId == null)
                {
                    regId = reg.RegistrationId;
                }
                else
                {
                    await _hub.DeleteRegistrationAsync(reg);
                }
            }

            if (regId == null)
            {
                regId = await _hub.CreateRegistrationIdAsync();
            }

            RegistrationDescription registration;

            if (device.DeviceType == DeviceType.Apple)
            {
                registration = new AppleRegistrationDescription(device.Handle, new[] {device.ProcedureCode});
            }
            else
            {
                registration = new GcmRegistrationDescription(device.Handle, new[] {device.ProcedureCode });
            }
            
            registration.RegistrationId = regId;
            await _hub.CreateOrUpdateRegistrationAsync(registration);
        }
    }
}
