﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Disciplines.Dto;

namespace IDR.Services
{
	public class DisciplinesService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;

		public DisciplinesService(ApplicationDbContext context, IAuditService auditService)
		{
			_context = context;
			_auditService = auditService;
		}

		public async Task<DisciplineDto> AddDiscipline()
		{
			var discipline = new Discipline
			{
				Name = "New Discipline",
				Description = "Description",
				IsNew = true
			};


			discipline = _context.Disciplines.Add(discipline);

			discipline.Audit(_auditService);

			await _context.SaveChangesAsync();

			return Mapper.Map<DisciplineDto>(discipline);
		}

		public async Task<List<DisciplineDto>> Query()
		{
			var cats = await _context.Disciplines
				.Where(x => x.DeletedOn == null && !x.IsNew)
				.ToListAsync();

			var dtos = cats.Select(Mapper.Map<DisciplineDto>).ToList();

			return dtos;
		}

		public async Task<DisciplineDto> GetDiscipline(int id)
		{
			var d = await _context.Disciplines
				.FirstOrDefaultAsync(x => x.DeletedOn == null && x.Id == id);

			var dto = Mapper.Map<DisciplineDto>(d);

			return dto;
		}

		public async Task<DisciplineDto> UpdateDiscipline(DisciplineDto discipline)
		{
			var old = await _context.Disciplines.FirstOrDefaultAsync(x => x.Id == discipline.Id);
			old.Name = discipline.Name;
			old.Description = discipline.Description;
			old.IsNew = false;
			old.Audit(_auditService);

			_context.SaveChanges();

			return discipline;
		}

		public async Task<DisciplineDto> CopyDiscipline(int id)
		{
			var old = await _context.Disciplines.FirstOrDefaultAsync(x => x.Id == id);

			var newDiscipline = new Discipline
			{
				Name = old.Name.Replace(" (copy)", string.Empty) + " (copy)",
				Description = old.Description
			};

			newDiscipline.Copy(old).Audit(_auditService);

			newDiscipline = _context.Disciplines.Add(newDiscipline);

			await _context.SaveChangesAsync();

			return Mapper.Map<DisciplineDto>(newDiscipline);
		}

		public async Task<int> DeleteDiscipline(int id)
		{
			var discipline = _context.Disciplines.Find(id);
			discipline.DeletedOn = DateTime.Now;
			discipline.Audit(_auditService);

			await _context.SaveChangesAsync();

			return id;
		}
	}
}