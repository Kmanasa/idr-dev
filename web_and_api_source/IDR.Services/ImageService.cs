﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using IDR.Data;
using IDR.Domain.ImageFiles.Helpers;
using IDR.Domain.Images.Dto;
using IDR.Infrastructure;

namespace IDR.Services
{
	public class ImageService
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;

		public ImageService(ApplicationDbContext context, IAuditService auditService)
		{
			_context = context;
			_auditService = auditService;
		}

		public async Task<PagedQueryResults<ImageFileDto>> SearchImages(PagingParameters paging)
		{
			var terms = new string[] { };

			if (!string.IsNullOrEmpty(paging.Filter))
			{
				terms = paging.Filter.Split(' ').ToArray();
			}

			var images = _context.ImageFiles
				.Where(x => x.DeletedOn == null)
				.Where(x => x.EntityName == typeof(ImageDto).Name || x.IsPublic)
				.Where(x => !string.IsNullOrEmpty(x.FileName))
				.Where(x =>
					(string.IsNullOrEmpty(paging.Filter) ||
					 (x.FileName.Contains(paging.Filter) ||
					  terms.Any(t => x.Alias.Contains(t))
					 )));

			var totalRecords = images.Count();

			var model = await images
				.Project().To<ImageFileDto>()
				.OrderByDescending(x => x.ModifiedOn)
				.Skip(paging.Offset * paging.Limit)
				.Take(paging.Limit)
				.ToListAsync();

			return new PagedQueryResults<ImageFileDto>(model, totalRecords);
		}

		public async Task<ImageFileDto> GetImage(int id)
		{
			return await _context.ImageFiles
				.Project().To<ImageFileDto>()
				.FirstAsync(i => i.Id == id);
		}

		public async Task RemoveImage(int id)
		{
			var img = await _context.ImageFiles
				.FirstAsync(i => i.Id == id);

			img.DeletedOn = DateTime.UtcNow;
			_auditService.Audit(img);

			await _context.SaveChangesAsync();
		}

		public async Task<ImageFileDto> CreateImageToFile(byte[] img, string fileName)
		{
			var image = new Image();

			image.Audit(_auditService);

			_context.Entry(image).State = EntityState.Added;

			await _context.SaveChangesAsync();

			var dto = Mapper.Map<ImageDto>(image);

			dto.SaveImages(_context, _auditService);

			//var cmd = new UpdateImageToFileCommand(_context, dto.Image.Id, img, fileName, _auditService);
			//await cmd.Execute();
			await UpdateImageToFile(dto.Image.Id, img, fileName);

			dto.LoadImages(_context);

			return dto.Image;
		}

		public async Task<int> UpdateImageToFile(int imageId, byte[] img, string fileName)
		{
			var existing = await _context.ImageFiles.FirstOrDefaultAsync(x => x.Id == imageId);

			if (existing != null)
			{
				existing.Image = img;
				existing.FileName = fileName;
				existing.Audit(_auditService);
			}

			await _context.SaveChangesAsync();
			return imageId;
		}

		public async Task<ImageFile> GetImageForFile(int id)
		{
			var img = await _context.ImageFiles.FirstOrDefaultAsync(x => x.Id == id);

			return img;
		}

		public async Task<string> UpdateImageAlias(int id, string alias)
		{
			var img = await _context.ImageFiles.FirstOrDefaultAsync(x => x.Id == id);

			img.Alias = alias;

			img.Audit(_auditService);
			await _context.SaveChangesAsync();

			return alias;
		}

	    public async Task AssociatePhysicianImage(int id, int entityId)
	    {
	        var img = await _context.ImageFiles.FirstOrDefaultAsync(x => x.Id == id);

	        img.EntityName = "PhysicianDto";
	        img.EntityId = entityId;
	        img.ImageType = "Portrait";
	        img.ImageFormType = "BASIC";
	        img.ImageTitle = "Library Image";

	        var physician = _context.Physicians.FirstOrDefault(p => p.Id == entityId);
	        physician.ImageRef = img.Id.ToString();

	        await _context.SaveChangesAsync();
	    }
    }
}