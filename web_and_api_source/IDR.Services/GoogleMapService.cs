﻿using IDR.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDR.Domain.Addresses.Dtos;


namespace IDR.Services
{
    public class GoogleMapService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;

        public GoogleMapService(ApplicationDbContext context, IAuditService auditService)
        {
            _context = context;
            _auditService = auditService;
        }
        public async Task<AddressDto> GetAddressForNodeLibrary(int nodelibraryId)
        {
            int? physicianId = _context.NodeLibraries.Where(x => x.Id == nodelibraryId).Select(x => x.PhysicianId).FirstOrDefault();
            int? companyId = _context.Physicians.Where(x => x.Id == physicianId).Select(x => x.CompanyId).FirstOrDefault();
            String name = _context.Companies.Where(x => x.Id == companyId).Select(x => x.Name).FirstOrDefault();
            //String addressTitle = _context.Addresses.Where(x => x.Name.Equals(name)).Select(x => x.AddressTitle).FirstOrDefault();
            AddressDto companyAddresses = _context.Addresses.Where(x => x.Name.Equals(name)).Select(x => new AddressDto()
            {
                Id = x.Id,
                Address1 = x.Address1,
                Address2 = x.Address2,
                City = x.City,
                State = x.State,
                PostalCode = x.PostalCode,
                Country = x.Country,
                AddressTitle = x.AddressTitle
            }).FirstOrDefault();

            return companyAddresses;
        }
    }
}
