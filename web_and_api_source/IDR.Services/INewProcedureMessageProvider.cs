using IDR.Domain;

namespace IDR.Services
{
    public interface INewProcedureMessageProvider
    {
        void SendProcedureCreated(ProcedureCreatedRequest request, bool sendEmail, bool sendSms);
    }
}