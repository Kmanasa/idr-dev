﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using IDR.Data;
using IDR.Domain.Notifications;

namespace IDR.Services
{
    public class NotificationService
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;

        public NotificationService(ApplicationDbContext context, IAuditService auditService)
        {
            _context = context;
            _auditService = auditService;
        }

        public async Task<List<NotificationDto>> GetPlanNotifications(int id)
        {
            var notifications = await _context.Notifications
                .Project().To<NotificationDto>()
                .Where(n => n.NodeLibraryId == id && n.DeletedOn == null && n.IsNew == false)
                .OrderBy(n => n.DaysOffset)
                .ThenBy(n => n.Hour)
                .ThenBy(n => n.Minute)
                .ToListAsync();

            return notifications;
        }

        public async Task<int> CreateNotification(NotificationDto model)
        {
            var notification = new Notification
            {
                DaysOffset = model.DaysOffset,
                Hour = model.Hour,
                Minute = model.Minute,
                Message = model.Message,
                NodeLibraryId = model.NodeLibraryId,
                Repeat = model.Repeat,
                RepetitionMinutes = model.RepetitionMinutes,
                Repetitions = model.Repetitions,
                Required = model.Required,
                ResponseType = model.ResponseType,
                Title = model.Title,
                TriggerText = model.TriggerText,
            };

            _auditService.Audit(notification);

            foreach (var response in model.NotificationResponses)
            {
                var resp = new NotificationResponse
                {
                    Text = response.Text,
                    Trigger = response.Trigger
                };
                notification.NotificationResponses.Add(resp);
            }

            _context.Notifications.Add(notification);

            await _context.SaveChangesAsync();

            return notification.Id;
        }

        public async Task UpdateNotification(NotificationDto model)
        {
            var notification = _context.Notifications.Find(model.Id);
            notification.DaysOffset = model.DaysOffset;
            notification.Hour = model.Hour;
            notification.Minute = model.Minute;
            notification.Message = model.Message;
            notification.NodeLibraryId = model.NodeLibraryId;
            notification.Repeat = model.Repeat;
            notification.RepetitionMinutes = model.RepetitionMinutes;
            notification.Repetitions = model.Repetitions;
            notification.Required = model.Required;
            notification.ResponseType = model.ResponseType;
            notification.Title = model.Title;
            notification.TriggerText = model.TriggerText;

            _context.NotificationResponses.RemoveRange(notification.NotificationResponses);

            await _context.SaveChangesAsync();

            foreach (var response in model.NotificationResponses)
            {
                var resp = new NotificationResponse
                {
                    Text = response.Text,
                    Trigger = response.Trigger
                };
                notification.NotificationResponses.Add(resp);
            }

            await _context.SaveChangesAsync();
        }

        public async Task DeleteNotification(int id)
        {
            var notification = _context.Notifications.Find(id);
            notification.DeletedOn = DateTime.UtcNow;
            _auditService.Audit(notification);
            await _context.SaveChangesAsync();
        }

        public async Task<NotificationDto> GetNotification(int id)
        {
            var notification = await _context.Notifications
                .Project().To<NotificationDto>()
                .FirstOrDefaultAsync(n => n.Id == id);

            return notification;
        }
    }
}
