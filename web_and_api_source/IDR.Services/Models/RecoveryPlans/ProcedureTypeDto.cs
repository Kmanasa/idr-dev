﻿namespace IDR.Domain.RecoveryPlans.Dto
{
	public class ProcedureTypeDto
	{
		public int Id { get; set; }

		public string CptCode { get; set; }

		public string FullDescription { get; set; }

		public string ShortDescription { get; set; }

		public int RecoveryPlanId { get; set; }
	}
}
