﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IDR.Domain.Dashboard
{
    public class DashboardItem
    {
        public DashboardItem()
        {
            DashboardNotifications = new List<DashboardNotification>();
        }

        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientPhone { get; set; }
        public string MedicalRecordNumber { get; set; }
        public int PhysicianId { get; set; }
        public int NodeLibraryId { get; set; }
        public int ProcedureId { get; set; }
        public int PatientId { get; set; }
        public int FacilityId { get; set; }
        public string CptCode { get; set; }
        public string AccessCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string ProcedureName { get; set; }

        public bool HasRequired { get { return DashboardNotifications.Any(dn => dn.Required); } }

        public List<DashboardNotification> DashboardNotifications { get; set; }
    }

    public class DashboardNotification
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int ProcedureId { get; set; }
        public bool Sent { get; set; }
        public int Repetitions { get; set; }
        public bool Required { get; set; }
        public bool Triggered { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime NextAttemptTime { get; set; }
        public int Attempts { get; set; }
        public string Response { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime DateResponded { get; set; }
        public bool IsPreOp { get; set; }

        public bool SentNotRequired => Sent && !Required && string.IsNullOrWhiteSpace(Response);
        public bool SentRespondedNotTriggered => Sent && !string.IsNullOrWhiteSpace(Response) && !Triggered;
        public bool SentRespondedTriggered => Sent && !string.IsNullOrWhiteSpace(Response) && Triggered;
        public bool SentNoResponseRepeating => Sent && Required && string.IsNullOrWhiteSpace(Response) && Attempts < Repetitions;
        public bool SentAllNoResponseRepeating => Sent && Required && string.IsNullOrWhiteSpace(Response) && Attempts >= Repetitions;

    }
}