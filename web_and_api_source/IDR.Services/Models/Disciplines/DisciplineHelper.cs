﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using IDR.Data;
using IDR.Domain.Disciplines.Dto;
using IDR.Services;

namespace IDR.Domain.Disciplines.Helper
{
	public static class DisciplineHelper
	{
		public static IEnumerable<T> LoadDisciplines<T>(this IEnumerable<T> itemsNeedingDisciplines, ApplicationDbContext context) where T : IDisciplined
		{
			var ids = itemsNeedingDisciplines.Select(x => x.Id).ToArray();
			
			var type = typeof(T).Name;

			var disciplines = context
				.DisciplineEntityItems
				.Include(x => x.Discipline)
				.Where(x => ids.Contains(x.EntityId) && x.EntityName == type)
				.ToList();

			Func<DisciplineEntityItem, T, T> resultF = (dei, item) =>
			{
				item.Discipline = Mapper.Map<DisciplineDto>(dei.Discipline);
				return item;
			};

			var parts = (from d in disciplines
						join item in itemsNeedingDisciplines on d.EntityId equals item.Id
						 select resultF(d, item)).ToList();

			return itemsNeedingDisciplines;
		}

		public static T LoadDiscipline<T>(this T item, ApplicationDbContext context) where T : IDisciplined
		{
			var type = typeof(T).Name;

			var discipline = context
			    .DisciplineEntityItems
			    .Include(x => x.Discipline)
                .FirstOrDefault(x => item.Id == x.EntityId && x.EntityName == type);

			item.Discipline = Mapper.Map<DisciplineDto>(discipline);

			return item;
		}

		public static T CopyDiscipline<T, SOURCET>(this T target, SOURCET source, ApplicationDbContext context, IAuditService auditService) 
			where T : IDisciplined
			where SOURCET : IDisciplined
		{
			var discipline = source.LoadDiscipline(context).Discipline;

			target.Discipline = discipline;

			target.AddDiscipline(context, auditService);

			return target;
		}

		public static T AddDiscipline<T>(this T item, ApplicationDbContext context, IAuditService auditService) where T : IDisciplined
		{
			if (item.Discipline == null || item.Discipline.Id <= 0) return item;

			var type = typeof(T).Name;

			var mapping = context
				.DisciplineEntityItems
				.Include(x => x.Discipline)
				.FirstOrDefault(x => x.DisciplineId == item.Discipline.Id && item.Id == x.EntityId && x.EntityName == type);

			if (mapping == null)
			{
				var items = context.DisciplineEntityItems.Where(x => x.EntityId == item.Id && x.EntityName == type).ToList();

				foreach (var oldmapping in items)
				{
					context.Entry(oldmapping).State = EntityState.Deleted;
				}

				var dei = new DisciplineEntityItem { DisciplineId = item.Discipline.Id, EntityName = type, EntityId = item.Id, };
				dei.Audit(auditService);
				dei = context.DisciplineEntityItems.Add(dei);
				context.SaveChanges();
				item.Discipline = Mapper.Map<DisciplineDto>(dei);
			}
			else
			{
				item.Discipline = Mapper.Map<DisciplineDto>(mapping.Discipline); 
			}

			return item;
		}
	}
}
