﻿using System.Collections.Generic;

namespace IDR.Domain.Disciplines.Dto
{
    public class DisciplineDto
    {
		public DisciplineDto()
        {
			Items = new List<DisciplineEntityItemDto>();
        }

		public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

		public List<DisciplineEntityItemDto> Items { get; set; }
    }
}
