﻿using IDR.Data;
using IDR.Domain.Contacts.Dtos;
using IDR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using AutoMapper.Internal;
using IDR.Infrastructure;

namespace IDR.Domain.Contacts.Helpers
{
	public static class ContactableHelper
	{
		private class PropertyAndAttribute
		{
			public PropertyAndAttribute(PropertyInfo property)
			{
				Property = property;

				ContactInfoAttribute att = property.GetCustomAttribute<ContactInfoAttribute>();

				if (att == null)
				{
					att = new ContactInfoAttribute { FormType = Constants.ContactFormTypes.Simple, Title = property.Name };
				}

				Attribute = att;
			}
			public PropertyInfo Property { get; set; }

			public ContactInfoAttribute Attribute { get; set; }
		}

		private static IEnumerable<PropertyAndAttribute> GetContactProperties<T>(this T item) where T : IContactable
		{
			var properties = typeof(T).GetProperties()
				.Where(prop => prop.PropertyType == typeof(ContactDto))
				.ToList()
				.Select(x => new PropertyAndAttribute(x))
				.ToArray()
				;

			return properties;
		}

		private static string GetTypeName<T>() where T : IContactable
		{
			AttachableAttribute att = typeof(T).GetCustomAttribute<AttachableAttribute>();

			return att == null ? typeof(T).Name : att.EntityName;
		}

		private static void ApplyContacts<T>(this List<Contact> contacts, List<T> items, IEnumerable<PropertyAndAttribute> props) where T : IContactable
		{
			var type = GetTypeName<T>();

			contacts.Each(contact =>
			{
				var prop = props.FirstOrDefault(p => p.Property.Name == contact.ContactType);
				if (prop != null)
				{
					var item = items.FirstOrDefault(x => x.Id == contact.EntityId);

					if (contact == null)
					{
						contact = new Contact
						{
							ContactFormType = prop.Attribute.FormType,
							ContactType = prop.Property.Name,
							ContactTitle = prop.Attribute.Title,
							EntityId = item.Id,
							EntityName = type
						};
					}
					var dto = Mapper.Map<ContactDto>(contact);
					prop.Property.SetValue(item, dto);
				}
			});


		}

		public static List<T> LoadContacts<T>(this List<T> itemsNeedingContacts, ApplicationDbContext context) where T : IContactable
		{
			if (itemsNeedingContacts.Count == 0) return itemsNeedingContacts;
			var first = itemsNeedingContacts.First();
			var contactProperties = first.GetContactProperties();
			var type = GetTypeName<T>();

			var ids = itemsNeedingContacts.Select(x => x.Id).ToArray();

			var contacts = context.Contacts
				.Where(x => x.EntityName == type && ids.Contains(x.EntityId))
				.ToList()
				;

			contacts.ApplyContacts<T>(itemsNeedingContacts, contactProperties.ToList());

			return itemsNeedingContacts;
		}

		public static T LoadContacts<T>(this T itemNeedingContacts, ApplicationDbContext context) where T : IContactable
		{
			var contactProperties = itemNeedingContacts.GetContactProperties();

			var type = GetTypeName<T>();

			var contacts = context.Contacts
				.Where(x => x.EntityId == itemNeedingContacts.Id && x.EntityName == type)
				.ToList()
				;

			contactProperties.Each(prop =>
			{
				var contact = contacts.FirstOrDefault(c => c.ContactType == prop.Property.Name);
				if (contact == null)
				{
					contact = new Contact
					{
						ContactFormType = prop.Attribute.FormType,
						ContactType = prop.Property.Name,
						ContactTitle = prop.Attribute.Title,
						EntityId = itemNeedingContacts.Id,
						EntityName = type
					};
				}
				var dto = Mapper.Map<ContactDto>(contact);
				prop.Property.SetValue(itemNeedingContacts, dto);
			});

			return itemNeedingContacts;
		}

		public static T CopyContacts<T>(this T destinationItem, T sourceItem, ApplicationDbContext context, IAuditService auditService) where T : IContactable
		{
			var contactProperties = destinationItem
				.GetContactProperties()
				;
			sourceItem.LoadContacts(context);

			Func<ContactDto, ContactDto> cleanContact = (contact) =>
			{
				contact.Id = 0;
				contact.EntityId = destinationItem.Id;
				return contact;
			};

			contactProperties.Each(prop =>
			{
				var val = cleanContact((ContactDto)prop.Property.GetValue(sourceItem, null));
				var existing = prop.Property.GetValue(destinationItem, null);
				if (existing == null)
				{
					prop.Property.SetValue(destinationItem, val);
				}
			});

			destinationItem.SaveContacts(context, auditService);

			return destinationItem;
		}

		public static T SaveContacts<T>(this T itemWithContacts, ApplicationDbContext context, IAuditService auditService) where T : IContactable
		{
			var contactProperties = itemWithContacts
				.GetContactProperties()
				;

			var type = GetTypeName<T>();

		    foreach (var prop in contactProperties)
		    {
		        var existing = prop.Property.GetValue(itemWithContacts, null);

		        var name = prop.Property.Name;

		        if (existing == null)
		        {
		            var newContact = new Contact
		            {
		                ContactFormType = prop.Attribute.FormType,
		                ContactType = prop.Property.Name,
		                ContactTitle = prop.Attribute.Title,
		                EntityId = itemWithContacts.Id,
		                EntityName = type
		            };

		            newContact.Audit(auditService);
		            var contact = context.Contacts.Add(newContact);
		            context.SaveChanges();
		            prop.Property.SetValue(itemWithContacts, Mapper.Map<ContactDto>(contact));
		        }
		        else
		        {
		            var newContact = Mapper.Map<Contact>(existing);
		            var old = context.Contacts.FirstOrDefault(x =>
		                x.EntityName == type && x.EntityId == itemWithContacts.Id && x.ContactType == name);

		            if (old != null)
		            {
		                old.CellPhone = newContact.CellPhone;
		                old.ContactFormType = newContact.ContactFormType;
		                old.ContactTitle = newContact.ContactTitle;
		                old.ContactType = newContact.ContactType;
		                old.Email = newContact.Email;
		                old.FirstName = newContact.FirstName;
		                old.LastName = newContact.LastName;
		                old.OfficeExt = newContact.OfficeExt;
		                old.OfficePhone = newContact.OfficePhone;
		                old.PersonalPhone = newContact.PersonalPhone;
		                old.ServiceName = newContact.ServiceName;

		                newContact.Audit(auditService);
		            }
		            else
		            {
		                newContact.ContactFormType = prop.Attribute.FormType;
		                newContact.ContactType = prop.Property.Name;
		                newContact.ContactTitle = prop.Attribute.Title;
		                newContact.EntityId = itemWithContacts.Id;
		                newContact.EntityName = type;

                        context.Entry(newContact).State = System.Data.Entity.EntityState.Added;
		            }

		            context.SaveChanges();
		            prop.Property.SetValue(itemWithContacts, Mapper.Map<ContactDto>(newContact));
		        }
            }
			
			return itemWithContacts;

		}
	}
}
