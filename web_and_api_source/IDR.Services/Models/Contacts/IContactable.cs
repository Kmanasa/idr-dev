﻿namespace IDR.Domain.Contacts
{
	public interface IContactable
	{
		int Id { get; set; }
	}
}
