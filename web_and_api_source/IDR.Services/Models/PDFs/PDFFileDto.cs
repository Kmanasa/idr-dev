﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDR.Services.Models.PDFs
{
    public class PDFFileDto
    {
        public int Id { get; set; }
        public byte[] PDF { get; set; }
        public string FileName { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CompanyId { get; set; }
        public string Alias { get; set; }
        public double FileSize { get; set; }
        public int PageCount { get; set; }
    }
}
