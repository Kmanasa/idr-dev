﻿using IDR.Domain.Contacts;

namespace IDR.Domain.PracticeUsers.Dto
{
	public class CallSiteAdminDto : BaseUserDto, IContactable
	{
		public CallSiteAdminDto()
		{
			UserTypeTitle = "Call Site Admin";

			UserType = "calladmin";
		}
	}

	public class CallSiteMobileUserDto : BaseUserDto, IContactable
	{
		public CallSiteMobileUserDto()
		{
			UserTypeTitle = "Call Site Mobile User";

			UserType = "callmobileuser";
		}
	}
}