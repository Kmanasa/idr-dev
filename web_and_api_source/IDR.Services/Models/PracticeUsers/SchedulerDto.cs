﻿namespace IDR.Domain.PracticeUsers.Dto
{
    public class SchedulerDto : PracticeAdminDto
    { 
        public SchedulerDto()
        {
            UserTypeTitle = "Scheduler";

            UserType = "scheduler";
        }
    }
}