﻿using IDR.Infrastructure;

namespace IDR.Domain.PracticeUsers.Dto
{
	public class InviteUserRequest : MailRequest
	{
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PracticeName { get; set; }
        public string ProvisionalKey { get; set; }
        public string Host { get; set; }
	}
}
