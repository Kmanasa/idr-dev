﻿using IDR.Domain.Contacts;
using IDR.Domain.Contacts.Dtos;
using IDR.Domain.Portal.Dto;

namespace IDR.Domain.PracticeUsers.Dto
{
    public class PhysiciansAssistantDto : BaseUserDto, IContactable
    {
		public PhysiciansAssistantDto()
        {
			UserTypeTitle = "Physicians Assistant";

			UserType = "physiciansassistant";
		}

		[ContactInfo(FormType = Constants.ContactFormTypes.Simple, Title = "Direct Contact")]
		public ContactDto DirectContact { get; set; }

		public PhysicianDto Physician { get; set; }

		public int? PhysicianId { get; set; }
	}
}
