﻿using System;

namespace IDR.Domain.Cms.Dto
{
	public class PropertyValueDto
	{
		public int Id { get; set; }

		public int OrderBy { get; set; }

		public string PartKey { get; set; }

		public int PropertyId { get; set; }

		public string Value { get; set; }

        public string ListStyleType { get; set; }

        public string ListStyleUrl { get; set; }

		public int Sequence { get; set; }

		public string Title { get; set; }

		public string ColumnModelKey { get; set; }

		public bool? IsFormatted { get; set; }

		public bool IsBoundValue { get; set; }

		public string BindPath { get; set; }
        
        public bool IsNew { get; set; }

        public DateTime? DeletedOn { get; set; }
	}
}
