﻿using System.Collections.Generic;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Disciplines;
using IDR.Domain.Disciplines.Dto;
using IDR.Services.Models.PDFs;

namespace IDR.Domain.Cms.Dto
{
	public class NodeLibraryDto : IMappableForm, IDisciplined
	{
		public NodeLibraryDto()
		{
			UINodeType = Constants.MOBILE_APP_NODE_TEMPLATE;
            Attributes=new List<NodeLibraryAttributeDto>();
            Nodes = new List<NodeDto>();
            CptCodes = new List<CptCodeDto>();
		}

		public int Id { get; set; }
        public bool Active { get; set; }
        public string CtssCode { get; set; }
        public string Description { get; set; }
        public List<CptCodeDto> CptCodes { get; set; }
        public string Key { get; set; }
		public string LibraryType { get; set; }
        public string Name { get; set; }
        public string UINodeType { get; set; }
        public int Duration { get; set; }
        public List<NodeDto> Nodes { get; set; }

        public List<NodeLibraryAttributeDto> Attributes { get; set; }

		public DisciplineDto Discipline { get; set; }

        public Dictionary<string, double> Diagnostics { get; set; }

        public List<PDFFileDto> PDFFilesForNodeLibrary { get; set; }

        public AddressDto AddressForNodeLibrary { get; set; }
    }
}
