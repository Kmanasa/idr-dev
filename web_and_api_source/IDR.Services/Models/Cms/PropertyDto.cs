﻿using System;
using System.Collections.Generic;
using IDR.Data;

namespace IDR.Domain.Cms.Dto
{
	public class PropertyDto : AuditEntity
	{
		public string Name { get; set; }

		public PropertyType Type { get; set; }

		public string CssClass { get; set; }

		public string Title { get; set; }

		public string Value { get; set; }

		public Guid Key { get; set; }

		public int NodeId { get; set; }

		public int TypeId { get; set; }

		//public List<PropertyValueDto> Values { get; set; }
	}
}
