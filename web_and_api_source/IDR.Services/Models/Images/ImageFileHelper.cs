﻿using IDR.Data;
using IDR.Domain.Images;
using IDR.Domain.Images.Dto;
using IDR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using AutoMapper.Internal;
using IDR.Infrastructure;
namespace IDR.Domain.ImageFiles.Helpers
{
	public static class ImagableHelper
	{
		private class PropertyAndAttribute
		{
			public PropertyAndAttribute(PropertyInfo property)
			{
				Property = property;

				ImageFileInfoAttribute att = property.GetCustomAttribute<ImageFileInfoAttribute>();

				if (att == null)
				{
					att = new ImageFileInfoAttribute { FormType = Constants.ImageFormTypes.Basic, Title = property.Name };
				}

				Attribute = att;
			}
			public PropertyInfo Property { get; set; }

			public ImageFileInfoAttribute Attribute { get; set; }
		}

		private static IEnumerable<PropertyAndAttribute> GetImageFileProperties<T>(this T item) where T : IImagable
		{
			var properties = typeof(T).GetProperties()
				.Where(prop => prop.PropertyType == typeof(ImageFileDto))
				.ToList()
				.Select(x => new PropertyAndAttribute(x))
				.ToArray()
				;

			return properties;
		}

		private static string GetTypeName<T>()
		{
			AttachableAttribute att = typeof(T).GetCustomAttribute<AttachableAttribute>();

			return att == null ? typeof(T).Name : att.EntityName;
		}

		private static void ApplyImageFiles<T>(this List<ImageFile> ImageFiles, List<T> items, IEnumerable<PropertyAndAttribute> props) where T : IImagable
		{
			foreach(var ImageFile in ImageFiles)
			{
				var prop = props.FirstOrDefault(p => p.Property.Name == ImageFile.ImageType);
				if (prop != null)
				{
					var item = items.FirstOrDefault(x => x.Id == ImageFile.EntityId);
					var type = GetTypeName<T>();

					if (ImageFile == null)
					{
						var file = new ImageFile
						{
							ImageType = prop.Property.Name,
							ImageTitle  = prop.Attribute.Title, 
							ImageFormType = prop.Attribute.FormType,
							EntityId = item.Id, 
							FileName = string.Empty,
							EntityName = type, 
							IsPublic = prop.Attribute.IsPublic,
						};
					}
					var dto = Mapper.Map<ImageFileDto>(ImageFile);
					prop.Property.SetValue(item, dto);
				}
			}


		}

		public static List<T> LoadImages<T>(this List<T> itemsNeedingImageFiles, ApplicationDbContext context) where T : IImagable
		{
			if (itemsNeedingImageFiles.Count == 0) return itemsNeedingImageFiles;
			var first = itemsNeedingImageFiles.First();
			var ImageFileProperties = first.GetImageFileProperties();
			var type = GetTypeName<T>();

			var ids = itemsNeedingImageFiles.Select(x => x.Id).ToArray();

			var ImageFiles = context.ImageFiles
				.Where(x => x.EntityName == type && ids.Contains(x.EntityId))
				.ToList()
				;

			ImageFiles.ApplyImageFiles<T>(itemsNeedingImageFiles, ImageFileProperties.ToList());

			return itemsNeedingImageFiles;
		}



		public static T LoadImages<T>(this T itemNeedingImageFiles, ApplicationDbContext context) where T : IImagable
		{
			var ImageFileProperties = itemNeedingImageFiles.GetImageFileProperties();
			
			var type = GetTypeName<T>();

			var ImageFiles = context.ImageFiles
				.Where(x => x.EntityId == itemNeedingImageFiles.Id && x.EntityName == type)
				.ToList()
				;

			ImageFileProperties.Each(prop =>
			{
				var imageFile = ImageFiles.FirstOrDefault(c => c.ImageType == prop.Property.Name);
				if (imageFile == null)
				{
					imageFile = new ImageFile
					{
						ImageType = prop.Property.Name,
						ImageTitle = prop.Attribute.Title,
						ImageFormType = prop.Attribute.FormType,
						EntityId = itemNeedingImageFiles.Id,
						FileName = string.Empty,
						EntityName = type, IsPublic = prop.Attribute.IsPublic,
					};
				}
				var dto = Mapper.Map<ImageFileDto>(imageFile);
				prop.Property.SetValue(itemNeedingImageFiles, dto);
			});

			return itemNeedingImageFiles;
		}

		public static T CopyImages<T>(this T destinationItem, T sourceItem, ApplicationDbContext context, IAuditService auditService) where T : IImagable
		{
			var ImageFileProperties = destinationItem
				.GetImageFileProperties()
				;
			sourceItem.LoadImages(context);

			Func<ImageFileDto, ImageFileDto> cleanImageFile = (imageFile) => {
				var final = new ImageFileDto { Id = 0, EntityId = destinationItem.Id, EntityName = imageFile.EntityName, FileName = imageFile.FileName, ImageFormType = imageFile.ImageFormType, ImageTitle = imageFile.ImageTitle, ImageType = imageFile.ImageType };

				return final;
			};

			ImageFileProperties.Each(prop =>
			{
				var val = cleanImageFile((ImageFileDto)prop.Property.GetValue(sourceItem, null));

				var existing = prop.Property.GetValue(destinationItem, null);
				if (existing == null)
				{
					prop.Property.SetValue(destinationItem, val);
				}
			});

			destinationItem.SaveImages(context, auditService);

			ImageFileProperties.Each(prop =>
			{
				var val = (ImageFileDto)prop.Property.GetValue(sourceItem, null);

				var newImage = (ImageFileDto)prop.Property.GetValue(destinationItem, null);

				var oldItem = context.ImageFiles.FirstOrDefault(x => x.Id == val.Id);
				var newItem = context.ImageFiles.FirstOrDefault(x => x.Id == newImage.Id);
				newItem.Image = oldItem.Image;

				newItem.FileName = oldItem.FileName;
				context.SaveChanges();
			});
			return destinationItem;
		}

		public static T SaveImages<T>(this T itemWithImageFiles, ApplicationDbContext context, IAuditService auditService) where T : IImagable
		{
			var imageFileProperties = itemWithImageFiles.GetImageFileProperties();

			var type = GetTypeName<T>();

			foreach(var prop in imageFileProperties)
			{
				var existing = prop.Property.GetValue(itemWithImageFiles, null);

				if (existing == null)
				{
					var newImageFile = new ImageFile
					{
						ImageType = prop.Property.Name,
						ImageTitle = prop.Attribute.Title,
						ImageFormType = prop.Attribute.FormType,
						EntityId = itemWithImageFiles.Id,
						FileName = string.Empty,
						EntityName = type,
						IsPublic = prop.Attribute.IsPublic,
					};

					newImageFile.Audit(auditService);
					var imageFile = context.ImageFiles.Add(newImageFile);
					context.SaveChanges();
					prop.Property.SetValue(itemWithImageFiles, Mapper.Map<ImageFileDto>(imageFile));
				}
			    else
			    {
			        var newImageFile = Mapper.Map<ImageFile>(existing);
			        var old = context.ImageFiles.FirstOrDefault(x =>
			            x.EntityName == type && x.EntityId == itemWithImageFiles.Id &&
			            x.ImageType == prop.Property.Name);
			        if (old != null)
			        {
			            old.ImageType = newImageFile.ImageType;
			            old.ImageTitle = newImageFile.ImageTitle;
			            old.ImageFormType = newImageFile.ImageFormType;
			            old.FileName = newImageFile.FileName;
			            old.IsPublic = newImageFile.IsPublic;
			        }
			        else
			        {
			            newImageFile.ImageType = prop.Property.Name;
			            newImageFile.ImageTitle = prop.Attribute.Title;
			            newImageFile.ImageFormType = prop.Attribute.FormType;
			            newImageFile.EntityId = itemWithImageFiles.Id;
			            newImageFile.FileName = string.Empty;
			            newImageFile.EntityName = type;
			            newImageFile.IsPublic = prop.Attribute.IsPublic;
			            context.Entry(newImageFile).State = System.Data.Entity.EntityState.Added;
			        }

			        context.SaveChanges();
			        prop.Property.SetValue(itemWithImageFiles, Mapper.Map<ImageFileDto>(newImageFile));
			    }
			}

			return itemWithImageFiles;

		}
	}
}
