﻿namespace IDR.Domain.Membership.Dto
{
	public class UserRoleDto
	{
		public string RoleId { get; set; }

		public string RoleName { get; set; }

		public bool IsInRole { get; set; }

		public string UserId { get; set; }
	}
}
