﻿using IDR.Data;
using IDR.Domain.Addresses.Dtos;
using IDR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using AutoMapper.Internal;
using IDR.Infrastructure;

namespace IDR.Domain.Addresses.Helpers
{
    public static class AddressableHelper
    {
        private class PropertyAndAttribute
        {
            public PropertyAndAttribute(PropertyInfo property)
            {
                Property = property;

                var att = property.GetCustomAttribute<AddressInfoAttribute>() ??
                          new AddressInfoAttribute
                          {
                              FormType = Constants.AddressFormTypes.Simple,
                              Title = property.Name
                          };

                Attribute = att;
            }
            public PropertyInfo Property { get; set; }

            public AddressInfoAttribute Attribute { get; set; }
        }

        private static IEnumerable<PropertyAndAttribute> GetAddressProperties<T>(this T item) where T : IAddressable
        {
            var properties = typeof(T).GetProperties()
                .Where(prop => prop.PropertyType == typeof(AddressDto))
                .ToList()
                .Select(x => new PropertyAndAttribute(x))
                .ToArray();

            return properties;
        }

        private static string GetTypeName<T>()
        {
            var att = typeof(T).GetCustomAttribute<AttachableAttribute>();

            return att == null ? typeof(T).Name : att.EntityName;
        }

        private static void ApplyAddresses<T>(this List<Address> contacts, List<T> items, IEnumerable<PropertyAndAttribute> props) where T : IAddressable
        {
            contacts.Each(contact =>
            {
                var prop = props.FirstOrDefault(p => p.Property.Name == contact.AddressType);
                if (prop != null)
                {
                    var item = items.FirstOrDefault(x => x.Id == contact.EntityId);
                    var type = GetTypeName<T>();

                    if (contact == null)
                    {
                        contact = new Address
                        {
                            AddressFormType = prop.Attribute.FormType,
                            AddressType = prop.Property.Name,
                            AddressTitle = prop.Attribute.Title,
                            EntityId = item.Id,
                            EntityName = type
                        };
                    }
                    var dto = Mapper.Map<AddressDto>(contact);
                    prop.Property.SetValue(item, dto);
                }
            });
        }

        public static List<T> LoadAddresses<T>(this List<T> itemsNeedingAddresses, ApplicationDbContext context) where T : IAddressable
        {
            if (!itemsNeedingAddresses.Any())
            {
                return itemsNeedingAddresses;
            }

            var first = itemsNeedingAddresses.First();
            var contactProperties = first.GetAddressProperties();
            var type = GetTypeName<T>();

            var ids = itemsNeedingAddresses.Select(x => x.Id).ToArray();

            var contacts = context.Addresses
                .Where(x => x.EntityName == type && ids.Contains(x.EntityId))
                .ToList();

            contacts.ApplyAddresses(itemsNeedingAddresses, contactProperties.ToList());

            return itemsNeedingAddresses;
        }

        public static T LoadAddresses<T>(this T itemNeedingAddresses, ApplicationDbContext context) where T : IAddressable
        {
            var contactProperties = itemNeedingAddresses.GetAddressProperties();

            var type = GetTypeName<T>();

            var contacts = context.Addresses
                .Where(x => x.EntityId == itemNeedingAddresses.Id && x.EntityName == type)
                .ToList();

            contactProperties.Each(prop =>
            {
                var contact = contacts.FirstOrDefault(c => c.AddressType == prop.Property.Name);
                if (contact == null)
                {
                    contact = new Address
                    {
                        AddressFormType = prop.Attribute.FormType,
                        AddressType = prop.Property.Name,
                        AddressTitle = prop.Attribute.Title,
                        EntityId = itemNeedingAddresses.Id,
                        EntityName = type
                    };
                }
                var dto = Mapper.Map<AddressDto>(contact);
                prop.Property.SetValue(itemNeedingAddresses, dto);
            });

            return itemNeedingAddresses;
        }

        public static T CopyAddresses<T>(this T destinationItem, T sourceItem, ApplicationDbContext context, IAuditService auditService) where T : IAddressable
        {
            var AddressProperties = destinationItem.GetAddressProperties();

            sourceItem.LoadAddresses(context);

            Func<AddressDto, AddressDto> cleanAddress = Address =>
            {
                Address.Id = 0;
                Address.EntityId = destinationItem.Id;
                return Address;
            };

            AddressProperties.Each(prop =>
            {
                var val = cleanAddress((AddressDto)prop.Property.GetValue(sourceItem, null));
                var existing = prop.Property.GetValue(destinationItem, null);
                if (existing == null)
                {
                    prop.Property.SetValue(destinationItem, val);
                }
            });

            destinationItem.SaveAddresses(context, auditService);

            return destinationItem;
        }


        public static T SaveAddresses<T>(this T itemWithAddresses, ApplicationDbContext context, IAuditService auditService) where T : IAddressable
        {
            var contactProperties = itemWithAddresses.GetAddressProperties();

            var type = GetTypeName<T>();

            foreach(var prop in contactProperties)
            {
                var existing = prop.Property.GetValue(itemWithAddresses, null);
                var name = prop.Property.Name;

                if (existing == null)
                {
                    var newAddress = new Address
                    {
                        AddressFormType = prop.Attribute.FormType,
                        AddressType = prop.Property.Name,
                        AddressTitle = prop.Attribute.Title,
                        EntityId = itemWithAddresses.Id,
                        EntityName = type
                    };

                    newAddress.Audit(auditService);
                    var contact = context.Addresses.Add(newAddress);
                    context.SaveChanges();
                    prop.Property.SetValue(itemWithAddresses, Mapper.Map<AddressDto>(contact));
                }
                else
                {
                    var newAddress = Mapper.Map<Address>(existing);
                    var old = context.Addresses.FirstOrDefault(x => x.EntityName == type && 
                                                                    x.EntityId == itemWithAddresses.Id && 
                                                                    x.AddressType == name);
                    if (old != null)
                    {
	                    old.Address1 = newAddress.Address1;
	                    old.Address2 = newAddress.Address2;
	                    old.AddressFormType = newAddress.AddressFormType;
	                    old.AddressTitle = newAddress.AddressTitle;
	                    old.AddressType = newAddress.AddressType;
	                    old.City = newAddress.City;
	                    old.Country = newAddress.Country;
	                    old.Name = newAddress.Name;
	                    old.PostalCode = newAddress.PostalCode;
	                    old.State = newAddress.State;
                    }
                    else
                    {
                        newAddress.AddressFormType = prop.Attribute.FormType;
                        newAddress.AddressType = prop.Property.Name;
                        newAddress.AddressTitle = prop.Attribute.Title;
                        newAddress.EntityId = itemWithAddresses.Id;
                        newAddress.EntityName = type;
                        newAddress.Audit(auditService);
                        context.Entry(newAddress).State = System.Data.Entity.EntityState.Added;
                    }
                    context.SaveChanges();
                    prop.Property.SetValue(itemWithAddresses, Mapper.Map<AddressDto>(newAddress));
                }

            }

            return itemWithAddresses;

        }
    }
}
