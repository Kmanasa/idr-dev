﻿namespace IDR.Domain.Addresses
{
	public interface IAddressable
	{
		int Id { get; set; }
	}
}
