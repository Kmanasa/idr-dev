﻿using IDR.Domain.Mailings.Dtos;
using System.Collections.Generic;

namespace IDR.Domain.Mailings
{
	public interface IMailing
	{
		int Id { get; set; }

		List<MailingDto> Mailings { get; set; }
	}
}
