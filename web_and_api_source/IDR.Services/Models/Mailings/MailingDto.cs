﻿using System;

namespace IDR.Domain.Mailings.Dtos
{
	public class MailingDto
	{
		public int Id { get; set; }

		public DateTime? CreatedOn { get; set; }

		public int EntityId { get; set; }

		public string EntityName { get; set; }

		public string Email { get; set; }

		public string Subject { get; set; }

		public string RequestName { get; set; }

		public string Body { get; set; }
	}
}
