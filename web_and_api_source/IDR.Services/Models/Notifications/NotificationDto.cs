﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Domain.Notifications
{
    public class NotificationDto
    {
        public NotificationDto()
        {
            NotificationResponses = new List<NotificationResponseDto>();
        }

        public int Id { get; set; }
        public int NodeLibraryId { get; set; }
        [StringLength(150)]
        public string Title { get; set; }
        [StringLength(100)]
        public string Message { get; set; }
        [StringLength(1000)]
        public string TriggerText { get; set; }
        public int DaysOffset { get; set; }
        public int HoursOffset { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public DateTime? CreatedOn { get; set; }
        [StringLength(128)]
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [StringLength(128)]
        public string ModifiedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsNew { get; set; }
        public bool Required { get; set; }
        public bool Repeat { get; set; }
        public int Repetitions { get; set; }
        public int RepetitionMinutes { get; set; }
        public int ResponseType { get; set; }


        public string Offset
        {
            get
            {
                var absDays = Math.Abs(DaysOffset);

                var period = DaysOffset < 0 ? "before" : "after";

                var dayText = absDays != 1 ? "days" : "day";

                return string.Format("{0} {1} {2} the procedure at {3}:{4}",
                    absDays, dayText, period, Hour, Minute.ToString("00"));
            }
        }

        public bool IsBefore => DaysOffset < 0;


        public int Period => DaysOffset < 1 ? 0 : 1;

     
        public virtual List<NotificationResponseDto> NotificationResponses { get; set; }
    }

    public class NotificationResponseDto
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        [StringLength(500)]
        public string Text { get; set; }
        public bool Trigger { get; set; }
    }
}
