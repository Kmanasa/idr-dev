﻿using IDR.Domain.Contacts;
using IDR.Domain.Contacts.Dtos;
using IDR.Domain.Disciplines;
using IDR.Domain.Disciplines.Dto;
using IDR.Domain.Images;
using IDR.Domain.Images.Dto;
using IDR.Domain.PracticeUsers.Dto;
using System.Collections.Generic;

namespace IDR.Domain.Portal.Dto
{
    public class PhysicianDto : BaseUserDto, IDisciplined, IContactable, IImagable
    {
        public PhysicianDto()
        {
            Patients = new List<PatientProcedureDto>();
			UserTypeTitle = "Physician";
			UserType = "physician";
        }

        public string Title { get; set; }

        public List<PatientProcedureDto> Patients { get; set; }

        public string ImageRef { get; set; }

		[ImageFileInfo(Title="Physician Photo", FormType=Constants.ImageFormTypes.Portrait)]
		public ImageFileDto Portrait{ get; set; }
		
		public DisciplineDto Discipline { get; set; }

		[ContactInfo(FormType=Constants.ContactFormTypes.Simple, Title="Direct Contact (internal only)")]
		public ContactDto DirectContact { get; set; }

		[ContactInfo(FormType = Constants.ContactFormTypes.Service, Title = "On-call Service")]
		public ContactDto OnCallContact { get; set; }

		[ContactInfo(FormType = Constants.ContactFormTypes.Service, Title = "Scheduling")]
		public ContactDto SchedulingContact { get; set; }

		public bool NotAssigned { get; set; }

        public bool DemoPhysician { get; set; }

        public bool IsNew { get; set; }
    }
}
