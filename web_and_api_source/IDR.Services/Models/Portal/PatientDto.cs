﻿using IDR.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;

namespace IDR.Domain.Portal.Dto
{
	[Attachable(EntityName="PatientDto")]
	public class PatientDto
	{
        public int Id { get; set; }

        public DateTime? DeletedOn { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }
        [StringLength(255)]
        public string LastName { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(50)]
        public string Identifier { get; set; }
        [StringLength(255)]
        public string Phone { get; set; }
        public int Age { get; set; }
        [StringLength(255)]
        public string MedicalRecordNumber { get; set; }
        public int PhysicianId { get; set; }
        [StringLength(1)]
        public string MiddleInitial { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? ModifiedOn { get; set; }
        [StringLength(128)]
        [ScaffoldColumn(false)]
        public string ModifiedBy { get; set; }
        public bool SendEmail { get; set; }
        public bool SendSms { get; set; }
        public PatientProcedureDto Procedure { get; set; }

        public int ProxyId { get; set; }
    }
}
