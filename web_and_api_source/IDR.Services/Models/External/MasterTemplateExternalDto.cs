﻿
using System.Collections.Generic;
using IDR.Domain.Cms.Dto;

namespace IDR.Domain.External.Dto
{
    public class MasterTemplateExternalDto : BaseExternalDto
    {
        public MasterTemplateExternalDto()
        {
            CptCodes = new List<CptCodeDto>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Key { get; set; }
        public string CtssCode { get; set; }
        public List<CptCodeDto> CptCodes { get; set; }
        public string LibraryType { get; set; }
        public int? PhysicianId { get; set; }
        public bool Active { get; set; }
        public bool IsDemoPlan { get; set; }
        public int PlanCount { get; set; }
    }
}
