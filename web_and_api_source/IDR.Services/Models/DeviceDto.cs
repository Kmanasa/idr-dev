﻿
using IDR.Data;

namespace IDR.Domain
{
    public class DeviceDto
    {
        public string Id { get; set; }
        public string ProcedureCode { get; set; }
        public string Handle { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
