﻿using IDR.Domain.Companies.Dto;

namespace IDR.Domain.CallCenter
{
	public class PracticeDto : CompanyDto
	{
		public int CallCenterId { get; set; }
		public int StartCalls { get; set; }
		public int EndCalls { get; set; }
		public int SatisfactionInitial { get; set; }
		public int SatisfactionFinal { get; set; }
		public string CallCenterNotes { get; set; }
		public string PracticeCode { get; set; }
		public bool Passive { get; set; }
		public string InfuFacilityId { get; set; }
        public string FacilityGroup { get; set; }
        public string Addl_URL { get; set; }
    }

    public class Additional_URL
    {
        public Additional_URL(bool isthereAddlURL)
        {
            isThereAddlURL = isthereAddlURL;
        }

        public bool isThereAddlURL { get; set; }
    }
}