﻿using System;
using System.Collections.Generic;
using System.Linq;
using IDR.Data;

namespace IDR.Domain
{
    public class InboxItemDto
    {
        public InboxItemDto()
        {
            ProcedureNotificationOptions = new List<ProcedureNotificationOption>();
        }

        public int Id { get; set; }
        public int ProcedureId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Response { get; set; }
        public bool Required { get; set; }
        public int NotificationId { get; set; }
        public bool Sent { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime? DateResponded { get; set; }
        public bool Triggered { get; set; }
        public string TriggerText { get; set; }

        public bool ResponseRequired => Required && DateResponded == null && ProcedureNotificationOptions.Any();

        public virtual List<ProcedureNotificationOption> ProcedureNotificationOptions { get; set; }
    }
}
