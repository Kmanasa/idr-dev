﻿using System;
using IDR.Data.CallCenter;

namespace IDR.Services.Models.CallCenter
{
	public class CallCenterPatientDto
	{
		public int Id { get; set; }
		public int FirstCallId { get; set; }
		public string ProcedureId { get; set; }

		public int PatientId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleInitial { get; set; }
		public string Email { get; set; }
		public int Age { get; set; }
		public string Phone { get; set; }
		public PatientGender Gender { get; set; }
		public string AltContact { get; set; }
		public string AltPhone { get; set; }
		public bool DoNotDisturb { get; set; }
		public bool Cancelled { get; set; }
		public int FacilityId { get; set; }
		public string FacilityName { get; set; }
		public int PhysicianId { get; set; }
		public int PayorId { get; set; }
		public int AnesthesiologistId { get; set; }
		public int NurseId { get; set; }
		public int CptId { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime StartTime { get; set; }
		public string PumpSerial { get; set; }
		public bool IsDualCath { get; set; }
		public string PumpSerial2 { get; set; }
		public bool Completed { get; set; }
		public bool Mobile { get; set; }
		public bool SpanishSpeaking { get; set; }
		public bool NoMonitoring { get; set; }
		public bool Outpatient { get; set; }
		public string InfuPatientId { get; set; }
        public string PracticeName { get; set; }

        public bool SendSmsChecked { get; set; }
        public bool SendEmailChecked { get; set; }

        public bool SendSms { get; set; }
        public bool SendEmail { get; set; }

        public string InfuFacilityId { get; set; }

        // Mobile only
        public string PracticeCode { get; set; }
		public string CptCode { get; set; }
		public string AccessCode { get; set; }

		public int InfuVolume { get; set; }
		public string InfuPumpModel { get; set; }
		public bool InfuHaveConsent { get; set; }
		public bool InfuCck { get; set; }
		public decimal InfuRate { get; set; }
		public string InfuTreatmentPlan { get; set; }
		public string InfuHomeDisconnect { get; set; }
		public bool InfuImSafetyFeatures { get; set; }
	}
}