﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace IDR.Services.Models.CallCenter
namespace IDR.Domain.CallCenter.Dto
{
    public class CallCenterPracticeReferenceDTO
    {
        public int InfuFacilityId { get; set; }
    }
}