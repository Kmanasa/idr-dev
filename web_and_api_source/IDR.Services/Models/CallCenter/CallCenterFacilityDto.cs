﻿using System.Collections.Generic;

namespace IDR.Services.Models.CallCenter
{
	public class CallCenterFacilityDto
	{
		public CallCenterFacilityDto()
		{
			CptCodes = new List<CallCenterFacilityCpt>();
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public bool IsNew { get; set; }
		public int PayorId { get; set; }

		public List<CallCenterFacilityCpt> CptCodes { get; set; }
	}

	public class CallCenterFacilityCpt
	{
		// Used for patient facility list
		public int Id { get; set; }
		public int NodeLibraryId { get; set; }

		// Used primarily for mobile 
		public string Code { get; set; }
		public int FacilityId { get; set; }
		public string Name { get; set; }
		public string PracticeCode { get; set; }
	}
}