﻿using System;
using IDR.Data;

namespace IDR.Services.Models.CallCenter
{
	public class CallCenterFacilityTypeaheadDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime? DeletedOn { get; set; }
		public bool IsNew { get; set; }
		public CompanyType CompanyType { get; set; }
	}
}