﻿using System;
using IDR.Data.CallCenter;

namespace IDR.Services.Models.CallCenter
{
	public class SummaryDto
	{
		public int Id { get; set; }
		public int CallCenterProcedureId { get; set; }
		public int CallYear { get; set; }
		public int CallMonth { get; set; }
		public int CallDay { get; set; }
		public string Code { get; set; }
		public string FacilityName { get; set; }
		public DateTime StartDate { get; set; }
		public int Index { get; set; }
		public int PainScoreCurrent { get; set; }
		public int? TolerableConsumption { get; set; }
		public int? TolerableDressing { get; set; }
		public int? TolerablePtOt { get; set; }
		public int? TolerableReturnToWork { get; set; }
		public int? TolerableSleeping { get; set; }
		public int? TolerableStairs { get; set; }
		public int? TolerableToilet { get; set; }
		public int? TolerableWalking { get; set; }
		public CallStatus CallStatus { get; set; }
		public int Satisfaction { get; set; }
		public string FirstName { get; set; }
		public string Identifier { get; set; }
		public string LastName { get; set; }
		public string Phone { get; set; }
		public bool Assigned { get; set; }

		public bool Mobile { get; set; }
		public int PhysicianId { get; set; }
		public string PhysicianFirstName { get; set; }
		public string PhysicianLastName { get; set; }
		public string PhysicianTitle { get; set; }
		public string PhysicianSuffix { get; set; }
		public bool IsToday { get; set; }
	}
}