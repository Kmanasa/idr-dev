﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDR.Services.Models.CallCenter
{
    public class CallCenterReportDto
    {
        public bool IsAuthenticatedForGeneratingReports { get; set; }
    }
}
