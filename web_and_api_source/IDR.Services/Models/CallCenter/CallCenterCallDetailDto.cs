﻿using IDR.Data.CallCenter;
using System;
using System.Collections.Generic;

namespace IDR.Services.Models.CallCenter
{
	public class CallCenterCallDetailDto
	{
		public CallCenterCallDetailDto()
		{
			Comments = new List<CallCenterCommentDto>();
			AssociatedCalls = new List<AssociatedCallDto>();
			CommentTypes = new List<CallCenterCommentType>();
		}

		public List<CallCenterCommentType> CommentTypes { get; set; }

		public int Id { get; set; }
		public int CallCenterProcedureId { get; set; }
		public int ProcedureId { get; set; }
		public int Index { get; set; }
		public string AssignedUserId { get; set; }
		// HTTP Request User Id
		public string ContextUserId { get; set; }

		public DateTime StartDate { get; set; }
		public CallStatus CallStatus { get; set; }
		public int Age { get; set; }
		public int CompanyId { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleInitial { get; set; }
		public string Phone { get; set; }
		public string TimeZone { get; set; }
		public PatientGender Gender { get; set; }
		public string AltContact { get; set; }
		public string AltPhone { get; set; }
		public bool Mobile { get; set; }
		public bool Cancelled { get; set; }
		public bool Completed { get; set; }
		public bool NoMonitoring { get; set; }
		public bool DoNotDisturb { get; set; }
		public string FacilityNotes { get; set; }
		public string PumpSerial { get; set; }
		public string PumpSerial2 { get; set; }
		public bool IsDualCath { get; set; }

		public string ProcedureName { get; set; }
		public DateTime ProcedureDate { get; set; }

		public string FacilityName { get; set; }
		public string PhysicianFirstName { get; set; }
		public string PhysicianLastName { get; set; }
		public string PhysicianTitle { get; set; }
		public string PhysicianSuffix { get; set; }

		public string AnesthesiologistFirstName { get; set; }
		public string AnesthesiologistLastName { get; set; }
		public string AnesthesiologistTitle { get; set; }
		public string AnesthesiologistSuffix { get; set; }

		public string NurseFirstName { get; set; }
		public string NurseLastName { get; set; }
		public string NurseTitle { get; set; }
		public string NurseSuffix { get; set; }

		public bool IsAssigned => !string.IsNullOrEmpty(AssignedUserId) &&
				!string.IsNullOrEmpty(ContextUserId) &&
				AssignedUserId == ContextUserId ;

		public bool HasPhysician => !string.IsNullOrWhiteSpace(PhysicianFirstName) ||
									!string.IsNullOrWhiteSpace(PhysicianLastName) ||
									!string.IsNullOrWhiteSpace(PhysicianTitle) ||
									!string.IsNullOrWhiteSpace(PhysicianSuffix);

		public bool HasAnesthesiologist => !string.IsNullOrWhiteSpace(AnesthesiologistFirstName) ||
										   !string.IsNullOrWhiteSpace(AnesthesiologistLastName) ||
										   !string.IsNullOrWhiteSpace(AnesthesiologistTitle) ||
										   !string.IsNullOrWhiteSpace(AnesthesiologistSuffix);

		public bool HasNurse => !string.IsNullOrWhiteSpace(NurseFirstName) ||
								!string.IsNullOrWhiteSpace(NurseLastName) ||
								!string.IsNullOrWhiteSpace(NurseTitle) ||
								!string.IsNullOrWhiteSpace(NurseSuffix);

		public string AnesthesiologistFullName => $"{AnesthesiologistTitle} {AnesthesiologistFirstName} {AnesthesiologistLastName} {AnesthesiologistSuffix}";
		public string PhysicianFullName => $"{PhysicianTitle} {PhysicianFirstName} {PhysicianLastName} {PhysicianSuffix}";
		public string NurseFullName => $"{NurseTitle} {NurseFirstName} {NurseLastName} {NurseSuffix}";

		public string GenderName
		{
			get
			{
				switch (Gender)
				{
					//case PatientGender.Male:
					default:
						return "Male";

					case PatientGender.Female:
						return "Female";

					case PatientGender.NonBinary:
						return "Non-Binary";
				}
			}
		}

		public List<CallCenterCommentDto> Comments { get; set; }
		public List<AssociatedCallDto> AssociatedCalls { get; set; }
		public string DischargeDate { get; set; }
	}

	public class CallCenterCommentDto
	{
		public int Id { get; set; }
		public string Comment { get; set; }
		public int CallCenterScheduledCallId { get; set; }
		public int CallCenterProcedureId { get; set; }
		public DateTime? CreatedOn { get; set; }
		public int CallTypeIndex { get; set; }
		public int CallCenterCommentTypeId { get; set; }

		public string CreatedBy { get; set; }
		//public string Title { get; set; }

		//public string CallTypeName { get; set; }
		//{
		//	get
		//	{
		//		switch (CallTypeIndex)
		//		{
		//			default:
		//				//case CallType.Assessment:
		//				return "Assessment";
		//			case (int)CallType.HotlineNote:
		//				return "Hotline Note";
		//			case (int)CallType.PatientReported:
		//				return "Patient Reported";
		//			case (int)CallType.General:
		//				return "General";
		//		}
		//	}
		//}
	}

	public class CallCenterAggregates
	{
		public int ScheduledMonitor { get; set; }
		public int CompletedMonitor { get; set; }
		public int TotalMonitor => ScheduledMonitor + CompletedMonitor;
		public int PercentMonitor
		{
			get
			{
				try
				{
					var percent = (int)Math.Round((double)(100 * CompletedMonitor) / TotalMonitor);
					if (percent < 0)
					{
						return 100;
					}
					return percent;
				}
				catch
				{
					return 100;
				}
			}
		}

		public int ScheduledMobile { get; set; }
		public int CompletedMobile { get; set; }
		public int TotalMobile => ScheduledMobile + CompletedMobile;
		public int PercentMobile
		{
			get
			{
				try
				{
					var percent = (int)Math.Round((double)(100 * CompletedMobile) / TotalMobile);
					if (percent < 0)
					{
						return 100;
					}
					return percent;
				}
				catch
				{
					return 100;
				}
			}
		}

		public int ScheduledTotal => ScheduledMonitor + ScheduledMobile;
		public int CompletedTotal => CompletedMonitor + CompletedMobile;
		public int Total => ScheduledTotal + CompletedTotal;
		public int PercentTotal
		{
			get
			{
				try
				{
					var percent = (int)Math.Round((double)(100 * CompletedTotal) / Total);
					if (percent < 0)
					{
						return 100;
					}
					return percent;
				}
				catch
				{
					return 100;
				}
			}
		}
	}
}