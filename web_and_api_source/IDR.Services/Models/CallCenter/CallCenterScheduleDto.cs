﻿using System;
using System.Collections.Generic;
using System.Linq;
using IDR.Data.CallCenter;

namespace IDR.Services.Models.CallCenter
{
    public class CallCenterScheduleDto
    {
        public CallCenterScheduleDto()
        {
            AssociatedCalls = new List<AssociatedCallDto>();
        }

        public int Id { get; set; }
        public int CallCenterProcedureId { get; set; }
        public bool SpanishSpeaking { get; set; }
        public bool Outpatient { get; set; }
        public int ProcedureId { get; set; }
        public int Index { get; set; }
        public int CallStatus { get; set; }
        public int CallDay { get; set; }
        public int CallMonth { get; set; }
        public int CallYear { get; set; }
        public bool Assigned { get; set; }
        public string AssignedUserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime CallTime { get; set; }
        public string TimeZone { get; set; }
        public int TimeZoneOffset { get; set; }
        public int Age { get; set; }
        public int CompanyId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Identifier { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string Phone { get; set; }
        public PatientGender Gender { get; set; }
        public string AltContact { get; set; }
        public string AltPhone { get; set; }
        public bool Mobile { get; set; }

        public string FacilityName { get; set; }

        public int PhysicianId { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string PhysicianTitle { get; set; }
        public string PhysicianSuffix { get; set; }

        public int AnesthesiId { get; set; }
        public string AnesthesiologistFirstName { get; set; }
        public string AnesthesiLastName { get; set; }
        public string AnesthesiTitle { get; set; }
        public string AnesthesiSuffix { get; set; }

        public string NurseId { get; set; }
        public string NurseFirstName { get; set; }
        public string NurseLastName { get; set; }
        public string NurseTitle { get; set; }
        public string NurseSuffix { get; set; }

        // True if a hotline comment exists 
        // within the last 24 hours
        public bool HasHotlineInLastDay
        {
            get
            {
                if (AssociatedCalls == null || !AssociatedCalls.Any())
                {
                    return false;
                }

                var hotlineCall = AssociatedCalls.FirstOrDefault(c =>
                    c.CallStatus == Data.CallCenter.CallStatus.Completed &&
                    c.CallType == CallType.HotlineNote &&
                    c.CreatedOn.HasValue &&
                    c.CreatedOn.Value > DateTime.UtcNow.AddDays(-1)
                );

                return hotlineCall != null;
            }
        }

        public List<AssociatedCallDto> AssociatedCalls { get; set; }

        public string CallStatusName => CallStatus == 1 ? "Scheduled" : "[Unknown]";

        public string GenderName
        {
            get
            {
                switch (Gender)
                {
                    //case PatientGender.Male:
                    default:
                        return "Male";

                    case PatientGender.Female:
                        return "Female";

                    case PatientGender.NonBinary:
                        return "Non-Binary";
                }
            }
        }
    }

    public class CallTomorrow
    {
        public CallTomorrow(bool isThereACallTomorrow)
        {
            IsThereACallTomorrow = isThereACallTomorrow;
        }
        
        public bool IsThereACallTomorrow { get; set; }
    }
}