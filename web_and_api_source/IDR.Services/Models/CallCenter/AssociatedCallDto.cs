﻿using System;
using IDR.Data.CallCenter;

namespace IDR.Services.Models.CallCenter
{
	public class AssociatedCallDto
	{
		public int Id { get; set; }
		public CallType CallType { get; set; }
		public CallStatus CallStatus { get; set; }
		public DateTime? CreatedOn { get; set; }
		public int CallCenterProcedureId { get; set; }
		public int Index { get; set; }
		public int CallDay { get; set; }
		public int CallMonth { get; set; }
		public int CallYear { get; set; }
		public bool Assigned { get; set; }
		public string AssignedUserId { get; set; }
		public DateTime CallTime { get; set; }


		// Assessment
		public int Satisfaction { get; set; }
		public int PainScoreCurrent { get; set; }
		public int Perscription24Hour { get; set; }
		public int Otc24Hour { get; set; }
        public int DoseButton { get; set; }
		public int? TolerableToilet { get; set; }
		public int? TolerableConsumption { get; set; }
		public int? TolerableSleeping { get; set; }
		public int? TolerableWalking { get; set; }
		public int? TolerableDressing { get; set; }
		public int? TolerableStairs { get; set; }
		public int? TolerablePtOt { get; set; }
		public int? TolerableReturnToWork { get; set; }
        public bool? EffectiveUse { get; set; }

    }
}