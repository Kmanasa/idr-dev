﻿using System;

namespace IDR.Services.Models.CallCenter
{
	public class CallCenterDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}

	public class NextCallId
	{
		public NextCallId(int? id, int year, int month, int day)
		{
			Id = id;
			Year = year;
			Month = month;
            Day = day;
		}

		public NextCallId(int? id, string firstName, string lastName) //: this(id, 0,0,0, "", "")
		{
            Id = id;
            Year = 0;
            Month = 0;
            Day = 0;
            FirstName = firstName;
            LastName = lastName;
        }

		public int? Id { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public int Day { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
    }
}