﻿using System.Collections.Generic;

namespace IDR.Services.Models.CallCenter
{
	public class SummaryOverviewDto
	{
		public double AveragePainScore { get; set; }
		public Dictionary<int, double> DailyAverages { get; set; }
		public double Satisfaction { get; set; }
	}
}