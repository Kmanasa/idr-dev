﻿using System;
using System.IO;
using System.Linq;
using System.Data.Entity;
using IDR.Data;
using Microsoft.Azure.WebJobs;

namespace WebJobDeletedItems
{
    public class Functions
    {
        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called queue
        [NoAutomaticTrigger]
        public static void ManualTrigger(TextWriter log)
        {
            log.WriteLine("Deleting outdated items at {0}", DateTime.UtcNow);

            DeleteLibraries(log);

            log.WriteLine("Finished deleting outdated items at {0}", DateTime.UtcNow);

        }

        private static void DeleteLibraries(TextWriter log)
        {
            var context = new ApplicationDbContext("IdrDataContext");

			var deletedLibs = context.NodeLibraries
                .Include(n => n.Nodes)
                .Where(n => n.DeletedOn != null)
                .ToList();

            // Flag all child nodes as deleted
            foreach (var lib in deletedLibs)
            {
                foreach (var node in lib.Nodes)
                {
                    SetNodeChildrenDeleted(node);
                }
            }

            // Delete all old nodes
            DeleteNodes(log);

            foreach (var lib in deletedLibs)
            {
                try
                {
                    context.NodeLibraries.Remove(lib);
                    context.SaveChanges();
                    Console.WriteLine("Removed library {0} {1}", lib.Id, lib.Name);
                }
                catch (Exception)
                {
                    Console.WriteLine("Unable to remove library {0} {1}", lib.Id, lib.Name);
                }
            }
        }

        private static void DeleteNodes(TextWriter log)
        {
            try
            {
                var context = new ApplicationDbContext("IdrDataContext");
                var deletedNodes = context.Nodes
                    .Include(n => n.Nodes)
                    .Where(n => n.DeletedOn != null)
                    .ToList();

                foreach (var node in deletedNodes)
                {
                    SetNodeChildrenDeleted(node);
                }

                context.SaveChanges();

                deletedNodes = context.Nodes
                    .Include(n => n.Nodes)
                    .Where(n => n.DeletedOn != null).ToList();

                while (deletedNodes.Any())
                {
                    foreach (var n in deletedNodes)
                    {
                        try
                        {
                            context.Nodes.Remove(n);
                            context.SaveChanges();
                            Console.WriteLine("Removed node {0} {1}", n.Id, n.Title);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Unable to remove node {0} {1}", n.Id, n.Title);
                        }
                    }

                    deletedNodes = context.Nodes
                        .Include(n => n.Nodes)
                        .Where(n => n.DeletedOn != null).ToList();
                }
            }
            catch (Exception e)
            {
                log.Write(e);
            }
        }

        private static void SetNodeChildrenDeleted(Node node)
        {
            node.DeletedOn = DateTime.Now;
            if (node.Nodes.Any())
            {
                foreach (var child in node.Nodes)
                {
                    SetNodeChildrenDeleted(child);
                }
            }
        }
    }
}
