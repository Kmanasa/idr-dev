﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;

namespace BaracudaMailTest
{
	class Program
	{
		static void Main(string[] args)
		{
			var host = new JobHost();
			// The following code will invoke a function called ManualTrigger and 
			// pass in data (value in this case) to the function
			host.Call(typeof(Functionsx).GetMethod("ManualTrigger"));
		}
	}

	public class Functionsx
	{ 
		public static void ManualTrigger(TextWriter log)
		{
			try
			{
				//var rrvSmtpHost = "mail1.infusystem.com";
				//var client = new SmtpClient(rrvSmtpHost);
				//var mail = new MailMessage("no-reply@infusystem.com", "sean.");

				//mail.IsBodyHtml = true;
				//mail.Subject = "Infusion Timer Alert Message [248-433-2129]";
				//mail.Body = "test - " + DateTime.UtcNow.ToLongTimeString();
				////mail.Bcc.Add("tyler.brinks@notionone.com");

				var client = new SmtpClient("smtp.sendgrid.net");
				var mail = new MailMessage("no-reply@infusystem.com", "tyler.brinks@notionone.com");
				client.Port = 587;
				client.Credentials = new NetworkCredential("idrmail", "idr3m@il");

				mail.IsBodyHtml = true;
				mail.Subject = "Infusion Timer Alert Message [248-433-2129]";
				mail.Body = "test - " + DateTime.UtcNow.ToLongTimeString();
				//mail.Bcc.Add("tyler.brinks@notionone.com");

				client.Send(mail);
			}
			catch (Exception ex)
			{
				log.Write(ex.Message + Environment.NewLine + ex.StackTrace);
			}
		}
	}
}
