// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using IDR.Web.App;
using IDR.Web.Mailers;
using IDR.Data;
using IDR.Services;
using IDR.web.Models;
using Microsoft.AspNet.Identity;
using StructureMap;
using StructureMap.Pipeline;
using StructureMap.Web.Pipeline;

namespace IDR.Web.DependencyResolution
{
	using StructureMap.Graph;

	public class DefaultRegistry : Registry
	{
		#region Constructors and Destructors

		public DefaultRegistry()
		{
			#region Scanning
			Scan(
				scan =>
				{

					scan.TheCallingAssembly();
					scan.AssemblyContainingType(typeof(Entity));
					scan.AssemblyContainingType(typeof(IAuditService));
					scan.With(new ControllerConvention());

					scan.WithDefaultConventions();


				});
			#endregion

			#region Implementations

			For<INewProcedureMessageProvider>().Use<NewProcedureMessageProvider>();
			For<IInvitationMailer>().Use<InvitationMailer>();
			For<IResetPasswordMailer>().Use<ResetPassword>();
			For<IAuditService>().Use<AuditService>();

			Policies.Add<ConnectionStringPolicy>();

			For<DbContext>().Use<ApplicationDbContext>()
				.LifecycleIs<HttpContextLifecycle>()
				.Ctor<string>()
				;

			For<ApplicationDbContext>().Use<ApplicationDbContext>()
				.LifecycleIs<HttpContextLifecycle>()
				.Ctor<string>()
				;

			For<IUserStore<ApplicationUser>>().Use<Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>>().LifecycleIs<HttpContextLifecycle>(); ;

			#endregion
		}

		#endregion
	}

	public class ConnectionStringPolicy : ConfiguredInstancePolicy
	{
		protected override void apply(Type pluginType, IConfiguredInstance instance)
		{
			try
			{
				var parameter = instance.Constructor.GetParameters().FirstOrDefault(x => x.Name == "connectionString");

				if (parameter != null)
				{

					var cs = Tenancy.GetConnectionForHost(HttpContext.Current?.Request?.Url?.Host ?? "IdrDataContext");
					instance.Dependencies.AddForConstructorParameter(parameter, cs);

				}
			}
			catch (Exception)
			{
				// Structuremap will handle the rest.
			}
		}
	}
}