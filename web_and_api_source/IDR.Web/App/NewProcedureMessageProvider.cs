﻿using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using IDR.Data;
using IDR.Domain;
using IDR.Services;
using IDR.Web.Mailers;
using Twilio;
using Twilio.Rest.Messaging.V1;
using Twilio.Rest.Messaging.V1.Service;
using Twilio.Rest.Api.V2010.Account;

namespace IDR.Web.App
{
	public class NewProcedureMessageProvider : INewProcedureMessageProvider
	{
		private static readonly string Sid = ConfigurationManager.AppSettings["TwilioSid"];
		private static readonly string Token = ConfigurationManager.AppSettings["TwilioToken"];
		// private static readonly string TwilioPhone = ConfigurationManager.AppSettings["TwilioPhone"];
		private static readonly string GoogleUrl = ConfigurationManager.AppSettings["AppUrlGoogle"];
		private static readonly string AppleUrl = ConfigurationManager.AppSettings["AppUrlApple"];
		private static readonly string SmsMessageUrls = ConfigurationManager.AppSettings["SmsMessageUrls"];
		private static readonly string SmsMessage = ConfigurationManager.AppSettings["SmsMessage"];

		public void SendProcedureCreated(ProcedureCreatedRequest request, bool sendEmail, bool sendSms)
		{
			if (sendEmail)
			{
				SendEmail(request);
			}

			if (sendSms)
			{
				SendSms(request);
			}
		}

		public void SendSms(ProcedureCreatedRequest request)
		{
			if (string.IsNullOrEmpty(request.PhoneNumber))
			{
				return;
			}

			try
			{
                var regex = new Regex("[^0-9]");
                var phone = regex.Replace(request.PhoneNumber, "");
                //var twilio = new TwilioRestClient(Sid, Token);

                var text = string.Format(SmsMessage, AppleUrl, GoogleUrl, request.AccessCode);
                // twilio.SendMessage(TwilioPhone, phone, text, "");

                TwilioClient.Init(Sid, Token);

                var message = MessageResource.Create(
                body: text,
                messagingServiceSid: ConfigurationManager.AppSettings["MessagingServiceSid"],
                to: new Twilio.Types.PhoneNumber(phone)
                );
            }
			catch (Exception ex)
			{
			}
		}

		public void SendEmail(ProcedureCreatedRequest request)
		{
			try
			{
				var cn = Tenancy.GetConnectionForHost(System.Web.HttpContext.Current.Request.Url.Host);
				var context = new ApplicationDbContext(cn);
				request.Email = request.Recipients.First();
				var mailer = new PlanCreatedMailer(context);
				mailer.Mail(request);
			}
			catch (Exception ex)
			{
				var a = ex;
			}
		}
	}
}
