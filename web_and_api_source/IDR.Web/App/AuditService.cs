﻿using IDR.Data;
using IDR.Services;

namespace IDR.Web.App
{
	public class AuditService : IAuditService
	{
		private readonly IUserContext _userContext;

		public AuditService(IUserContext userContext)
		{
			_userContext = userContext;
		}

		public void Audit(IAuditEntity entity)
		{
			try
			{
				entity.Audit(_userContext.GetCurrentUserName());
			}
			catch
			{
				// unable to audit which is fine for anonymous users etc.
			}
		}
	}
}