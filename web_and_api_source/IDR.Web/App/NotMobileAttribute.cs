﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace IDR.Web.App
{
	public class NotMobileAttribute : ActionFilterAttribute 
	{
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			var identity = System.Web.HttpContext.Current.User.Identity;

			if (identity != null && identity.Name != null && 
				identity.Name.ToLower() == "api@idr.com")
			{
				var resposne = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");
				actionContext.Response = resposne;
			}
		}

	}
}