﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace IDR.Web.App
{
	public class BearerRequiredAttribute : ActionFilterAttribute 
	{
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			try
			{
				var request = System.Web.HttpContext.Current.Request;

				var auth = request.Headers["Authorization"];

				var token = auth.Split(' ')[1];
				var raw = Startup.OAuthOptions.AccessTokenFormat.Unprotect(token);

				if (string.IsNullOrEmpty(raw?.Identity?.Name))
				{
					var resposne = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");
					actionContext.Response = resposne;
				}
			}
			catch
			{
				var resposne = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");
				actionContext.Response = resposne;
			}
		}
	}
}