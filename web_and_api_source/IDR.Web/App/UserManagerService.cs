﻿using IDR.Data;
using IDR.Services;
using IDR.web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace idr.App.Services
{
	public class UserManagerService : IUserManagerService
	{
		private readonly UserManager<ApplicationUser> _userManager;

		public UserManagerService(ApplicationDbContext context)
		{
			_userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
			_userManager.UserValidator = new UserValidator<ApplicationUser>(_userManager)
			{
				AllowOnlyAlphanumericUserNames = false
			};
		}

		public IdentityResult Create(ApplicationUser user)
		{
			return _userManager.Create(user);
		}

		public IdentityResult Create(ApplicationUser user, string password)
		{
			return _userManager.Create(user, password);
		}

		public ApplicationUser FindById(string userId)
		{
			return _userManager.FindById(userId);
		}

		public ApplicationUser FindByName(string userName)
		{
			return _userManager.FindByName(userName);
		}

		public void AddToRole(string userId, string roleName)
		{
			_userManager.AddToRole(userId, roleName);
		}

		public void RemoveFromRole(string userId, string roleName)
		{
			_userManager.RemoveFromRole(userId, roleName);
		}

		public IdentityResult RemovePassword(string userId)
		{
			return _userManager.RemovePassword(userId);
		}

		public IdentityResult AddPassword(string userId, string password)
		{
			return _userManager.AddPassword(userId, password);
		}

		public IdentityResult Update(ApplicationUser user)
		{
			return _userManager.Update(user);
		}
	}
}