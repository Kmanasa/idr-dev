/// <reference path="plugin.js" />

/*global tinymce:true */

tinymce.PluginManager.add('idrplaceholders', function (editor) {
    
	editor.addButton('idrplaceholder', {

	    type: 'listbox',
	    text: 'IDR Placeholders',
	    icon: false,
        onselect: function(e) {
            editor.insertContent(this.value());
        },
        values: [
        
            { text: 'Facility Name', value: '{{facility.Name}}' },
            { text: 'Facility Street Address 1', value: '{{facility.StreetAddress1}}' },    // Physical Address 1
            { text: 'Facility Street Address 2', value: '{{facility.StreetAddress2}}' },    // Physical Address 2
            { text: 'Facility Street Address City', value: '{{facility.StreetCity}}' },     // Physical Address City
            { text: 'Facility Street Address Postal', value: '{{facility.StreetPostal}}' }, // Physical Address Postal Code
            { text: 'Facility Manager Contact Phone', value: '{{facility.ManagerContactPhone}}' }, // Facility Manager Contact Phone
            { text: 'Facility Patient Information Phone', value: '{{facility.PatientInfoPhone}}' }, // Facility Patient info phone 
            { text: 'Facility Patient Information Email', value: '{{facility.PatientInfoEmail}}' }, // Facility Patient info email 
            // Facility logo

            { text: 'Practice Name', value: '{{practice.Name}}' },                          // Practice Name
            { text: 'Practice Office Address 1', value: '{{practice.StreetAddress1}}' },    // Practice Street A1
            { text: 'Practice Office Address 2', value: '{{practice.StreetAddress2}}' },    // Practice Street A2
            { text: 'Practice Office Address City', value: '{{practice.StreetCity}}' },     // Practice Street City
            { text: 'Practice Office Address State', value: '{{practice.Streetstate}}' },   // Practice Street State
            { text: 'Practice Office Address Postal', value: '{{practice.StreetPostal}}' }, // Practice Street Postal
            { text: 'Practice Contact Phone', value: '{{practice.ContactPhone}}' },         // Practice Primary contact phone
            { text: 'Practice Contact Email', value: '{{practice.ContactEmail}}' },         // Practice Primary contact email
            { text: 'Practice Scheduling Name', value: '{{practice.SchedulingName}}' },     // Practice Scheduling contact name
            { text: 'Practice Scheduling Phone', value: '{{practice.SchedulingPhone}}' },   // Practice Scheduling contact phone
            { text: 'Practice Scheduling Email', value: '{{practice.SchedulingEmail}}' },   // Practice Scheduling contact email
            { text: 'Practice Logo', value: '{{practice.PracticeImage}}' },               // Practice Logo


            { text: 'Physician First Name', value: '{{physician.FirstName}}' },     // Physician First
            { text: 'Physician Last Name', value: '{{physician.LastName}}' },       // Physician Last
            { text: 'Physician Title', value: '{{physician.Title}}' },              // Physician Title
            { text: 'Physician Image', value: '{{physician.PhysicianImage}}' },     // Physician Image
            { text: 'Physician Direct Contact Phone', value: '{{physician.DirectContactPhone}}' },  // Physician direct contact phone
            { text: 'Physician Direct Contact Email', value: '{{physician.DirectContactEmail}}' },  // Physician direct contact email
            { text: 'Physician On-call Phone', value: '{{physician.OnCallPhone}}' },         // Physician on call phone
            { text: 'Physician On-call Email', value: '{{physician.OnCallEmail}}' },         // Physician on call email
            { text: 'Physician\'s Scheduling Contact Name', value: '{{physician.SchedulingContactName}}' },     // Physician sch contact name
            { text: 'Physician\'s Scheduling Contact Phone', value: '{{physician.SchedulingContactPhone}}' },   // Physician sch contact phone
            { text: 'Physician\'s Scheduling Contact Ext', value: '{{physician.SchedulingContactExt}}' },       // Physician sch contact ext
            { text: 'Physician\'s Scheduling Contact Email', value: '{{physician.SchedulingContactEmail}}' }   // Physician sch contact email

        ]

	});


});
