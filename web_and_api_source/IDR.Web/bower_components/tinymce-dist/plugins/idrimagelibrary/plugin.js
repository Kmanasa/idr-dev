
/*global tinymce:true */

tinymce.PluginManager.add('idrimagelibrary', function (editor) {

	function showImageLibrary() {

	    var modal = idr.showImageLibrary();
	    modal.then(selectImage);
	}

    function selectImage(img) {
        console.log(img);
        var data = {},
            imgElm = editor.selection.getNode();

        if (imgElm.nodeName == 'IMG' && !imgElm.getAttribute('data-mce-object') && !imgElm.getAttribute('data-mce-placeholder')) {
            data = {
                src: editor.dom.getAttrib(imgElm, 'src')
            };
        } else {
            imgElm = null;
        }

        data = {
            src: img.Url.replace(/ /g, '%20'),
            width: '100%'
        };

        editor.undoManager.transact(function () {
            if (!data.src) {
                if (imgElm) {
                    editor.dom.remove(imgElm);
                    editor.focus();
                    editor.nodeChanged();
                }

                return;
            }

            if (!imgElm) {
                data.id = '__mcenew';
                editor.focus();
                editor.selection.setContent(editor.dom.createHTML('img', data));
                imgElm = editor.dom.get('__mcenew');
                editor.dom.setAttrib(imgElm, 'id', null);
            } else {
                editor.dom.setAttribs(imgElm, data);
            }
        });
    }

	editor.addButton('idrimage', {
		icon: 'image',
		tooltip: 'Insert/edit image',
		onclick: function () { showImageLibrary(); },
		stateSelector: 'img:not([data-mce-object],[data-mce-placeholder])'
	});

	editor.addMenuItem('imageimage', {
		icon: 'image',
		text: 'IDR Image Library',
		onclick: function() { showImageLibrary(); },
		context: 'insert',
		prependToContext: true
	});

});
