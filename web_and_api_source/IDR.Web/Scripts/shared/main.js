
(function () {
    'use strict';
    angular.module('app.controllers', [])
	  .controller('AppCtrl', [
		    '$scope', '$rootScope', 'Api', function ($scope, $rootScope, Api) {
		        var $window;
		        $window = $(window);
		        $scope.user = { Firstname: '', Lastname: '' }

		        $scope.showProxy = false;
                $rootScope.proxyChanged = function (id, show) {
                    if (show) {
                        $scope.showProxy = true;
                    }
                    $rootScope.proxyPhysicianId = id;
		            $rootScope.$broadcast('proxyChanged');
		        }

		        Api.auth.current(function (data) {
		            $scope.user = data;

		            $rootScope.isPhysician =  _.any(data.Roles, function (r) {
		                return (r.RoleName === 'Physician' && r.IsInRole) ||
		                    (r.RoleName === 'Physicians Assistant' && r.IsInRole);
		            });

		            $rootScope.isPracticeAdmin =  _.any(data.Roles, function (r) {
						return (r.RoleName === 'Practice Admin' && r.IsInRole );
					});

			        $rootScope.isCallCenterAdmin = _.any(data.Roles, function (r) {
				        return (r.RoleName === 'Call Center Admin' && r.IsInRole);
			        });
                    
		            $rootScope.isScheduler = _.any(data.Roles, function (r) {
		                return (r.RoleName === 'Scheduler' && r.IsInRole);
		            });

		            $rootScope.isSuperAdmin = _.any(data.Roles, function (r) {
		                return r.RoleName === 'Super Admin' && r.IsInRole;
		            });

		            $rootScope.iAdmin = _.any(data.Roles, function (r) {
		                return r.RoleName === 'Admin' && r.IsInRole;
                    });

                    $rootScope.isHotlineNurse = _.any(data.Roles, function (r) {
                        return r.RoleName === 'Hotline Nurse' && r.IsInRole;
                    });

                    $rootScope.isCallSiteAdmin = _.any(data.Roles, function (r) {
                        return r.RoleName === 'Call Site Admin' && r.IsInRole;
                    });

		            $rootScope.canManagePractice = $scope.isPracticeAdmin ||
                        $rootScope.isScheduler ||
                        $rootScope.isSuperAdmin ||
                        $rootScope.iAdmin;

		            if (data.Physicians) {
		                $rootScope.proxyPhysicians = data.Physicians;
		                $rootScope.proxyPhysicianId = 0;
		                
		                if (data.Physicians.length) {

		                    var physicianIndex = 0;

		                    if ($scope.isPhysician) {
		                        var phys = _.find(data.Physicians, function(p) {
		                            return p.UserId == data.Id;
		                        });
		                        if (phys) {
		                            physicianIndex = data.Physicians.indexOf(phys);
		                        }
		                    }

		                    $rootScope.proxyPhysicianId = data.Physicians[physicianIndex].Id;

		                    $rootScope.proxyChanged(data.Physicians[physicianIndex].Id);
		                }
		            }
		        });

		        $scope.main = {
		            brand: 'Slim',
		            name: 'Lisa Doe'
		        };
		        $scope.pageTransitionOpts = [
                  {
                      name: 'Fade up',
                      "class": 'animate-fade-up'
                  }, {
                      name: 'Scale up',
                      "class": 'ainmate-scale-up'
                  }, {
                      name: 'Slide in from right',
                      "class": 'ainmate-slide-in-right'
                  }, {
                      name: 'Flip Y',
                      "class": 'animate-flip-y'
                  }
		        ];
		        $scope.admin = {
		            layout: 'wide',
		            menu: 'vertical',
		            fixedHeader: true,
		            fixedSidebar: true,
		            pageTransition: $scope.pageTransitionOpts[0],
		            skin: '32'
		        };


                $scope.$watch('admin', function (newVal, oldVal) {
		            if (newVal.menu === 'horizontal' && oldVal.menu === 'vertical') {
		                $rootScope.$broadcast('nav:reset');
		                return;
		            }
		            if (newVal.fixedHeader === false && newVal.fixedSidebar === true) {
		                if (oldVal.fixedHeader === false && oldVal.fixedSidebar === false) {
		                    $scope.admin.fixedHeader = true;
		                    $scope.admin.fixedSidebar = true;
		                }
		                if (oldVal.fixedHeader === true && oldVal.fixedSidebar === true) {
		                    $scope.admin.fixedHeader = false;
		                    $scope.admin.fixedSidebar = false;
		                }
		                return;
		            }
		            if (newVal.fixedSidebar === true) {
		                $scope.admin.fixedHeader = true;
		            }
		            if (newVal.fixedHeader === false) {
		                $scope.admin.fixedSidebar = false;
		            }
		        }, true);
		        return $scope.color = {
		            primary: '#0071b4',
		            success: '#A3BE8C',
		            info: '#7FABD2',
		            infoAlt: '#B48EAD',
		            warning: '#EBCB8B',
		            danger: '#BF616A',
		            gray: '#DCDCDC'
		        };
		    }
	  ]).controller('HeaderCtrl', ['$scope', function ($scope) { }])
        .controller('NavContainerCtrl', ['$scope', function ($scope) { }]).controller('NavCtrl', [
    '$scope', 'filterFilter', 'Api', function ($scope, filterFilter, Api) {


    }
        ]).controller('DashboardCtrl', ['$scope', function ($scope) { }]);


    if (!Array.prototype.select) {
        Array.prototype.any = function (f) {
            for (var idx = 0; idx < this.length; idx++) {
                if (f(this[idx])) return true;
            }
            return false;
        };

        Array.prototype.select = function (f) {
            var final = [];
            for (var idx = 0; idx < this.length; idx++) {
                final[idx] = f(this[idx]);
            }
            return final;
        };

        Array.prototype.innerJoin = function (arr, compareF, resultF) {
            var final = [];
            for (var idx = 0; idx < this.length; idx++) {
                for (var s_idx = 0; s_idx < arr.length; s_idx++) {
                    if (compareF(this[idx], arr[s_idx])) {
                        final[idx] = resultF(this[idx], arr[s_idx]);
                    }
                }
            }
            return final;
        };

        Array.prototype.where = function (f) {
            var final = [];
            for (var idx = 0; idx < this.length; idx++) {
                if (f(this[idx])) {
                    final.push(this[idx]);
                }
            }
            return final;
        };

        Array.prototype.sum = function (f) {
            var final = 0;
            for (var idx = 0; idx < this.length; idx++) {
                if (f(this[idx])) {
                    final += f(this[idx]);
                }
            }
            return final;
        };

        Array.prototype.removeIndex = function (idx) {
            this.splice(idx, 1);
            return this;
        };

        Array.prototype.add = function (fn) {
            this.push(fn());
        };
    }

})();
