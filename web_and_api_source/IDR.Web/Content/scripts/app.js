
(function () {
	Dropzone.autoDiscover = false;
	'use strict';
	var idr;

	angular.module('idrfilters', []);


	window.idr = idr = angular.module('app', [
		'ngResource',
		'ngRoute',
		'ngAnimate',
		'ui.bootstrap',
		'ui.tree',
		'ui.mask',
		'idr.filters',
		'app.controllers',
		'app.directives',
		'app.localization',
		'app.nav',
		'ui.tinymce',
		'ui.bootstrap',
		'bootstrap-tagsinput'
	]);

	idr.config([
		'$routeProvider', function ($routeProvider) {
			var routes, setRoutes;
			routes = [
				'dashboard',
				'pages/404', 'pages/500', 'pages/blank',
				'pages/forgot-password', 'pages/physician', 'pages/invoice', 'pages/signin', 'pages/signup',
				'users/list', 'users/edit', 'users/create', 'physician/physician', 'physician/plans'
			];
			setRoutes = function (route) {
				var config, url;
				url = '/' + route;
				config = {
					templateUrl: 'partials/' + route + '.html'
				};
				$routeProvider.when(url, config);
				return $routeProvider;
			};
			routes.forEach(function (route) {
				return setRoutes(route);
			});

			return $routeProvider
				.when('/adminpractice', { template: '<div ng-controller="AdminPracticeRedirect"></div>', title: 'Admin Practice' })

				.when('/admin', { templateUrl: 'partials/landing/admin.html', title: 'Admin' })
				.when('/admin/companies', { templateUrl: 'partials/admin/companies/list.html', title: 'Companies' })
				.when('/admin/companies/:id', { templateUrl: 'partials/admin/companies/edit.html', title: 'Company Edit' })
				.when('/admin/facilities/:facilityId/:companyId', { templateUrl: 'partials/admin/facilities/edit.html', title: 'Facility Edit' })
				.when('/admin/physicians', { templateUrl: 'partials/admin/physicians/list.html', title: 'Physicians' })
				.when('/admin/physicians/:physicianId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-physician.html', title: 'Edit Physician' })
				.when('/admin/physiciansassistants/:assistantId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-assistant.html', title: 'Edit Assistant' })
				.when('/admin/physicians/:patientId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-patient.html', title: 'Edit Patient' })
				.when('/admin/practiceadmins/:adminId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-practiceadmin.html', title: 'Edit Practice Admin' })
				.when('/admin/calladmins/:adminId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-callsiteadmin.html', title: 'Edit Call Site Admin' })
				.when('/admin/callmobileusers/:adminId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-callsitemobile.html', title: 'Edit Call Site Mobile User' })
				.when('/admin/schedulers/:adminId/:practiceId', { templateUrl: 'partials/admin/practiceusers/edit-scheduler.html', title: 'Edit Scheduler' })
				.when('/admin-practices', { templateUrl: 'partials/landing/practices.html', title: 'Edit Practices' })
				.when('/admin/practices/:practiceId', { templateUrl: 'partials/admin/practices/edit.html', title: 'Edit Practice' })
				.when('/admin/practices', { templateUrl: 'partials/admin/practices/list.html', title: 'Practices' })
				.when('/admin/users', { templateUrl: 'partials/users/list.html', title: 'Users' })
                .when('/admin/users/:entityId', { templateUrl: 'partials/users/edit.html', title: 'Edit User' })

                .when('/admin/practices/edit/:id', { templateUrl: 'partials/pdf-edit.html', title: 'PDF Edit' }) 


				.when('/assistant/', { templateUrl: 'partials/landing/assistant.html', title: 'Assistant' })
				.when('/assistant/physician/editprofile/:physicianId', { templateUrl: 'partials/physician/edit.html', title: 'Edit Profile' })
				.when('/assistant/physician/not-assigned', { templateUrl: 'partials/physician/not-assigned.html', title: '' })
				.when('/assistant/physician/plans', { templateUrl: 'partials/cms/recoveryplans/list.html', title: 'Plans' })
				.when('/assistant/physician/patients/:entityId', { templateUrl: 'partials/physician/patient.html', title: 'Patients' })
				.when('/assistant/physician/patients', { redirectTo: '/physician/patients', title: 'Patients' })

				.when('/physician/patients', { templateUrl: 'partials/physician/patients.html', reloadOnSearch: false, title: 'Patients' })

				.when('/cms', { templateUrl: 'partials/landing/cms.html', title: 'Content Magagement' })
				.when('/cms/attributes', { templateUrl: 'partials/cms/attributes/list.html', title: 'Attributes' })
				.when('/cms/attributes/:attributeId', { templateUrl: 'partials/cms/attributes/edit.html', title: 'Attributes Edit' })
				.when('/cms/attributes/new', { templateUrl: 'partials/cms/attributes/edit.html', title: 'New Attributes' })
				.when('/cms/categories', { templateUrl: 'partials/cms/categories/list.html', title: 'Caregories' })
				.when('/cms/categories/:categoryId', { templateUrl: 'partials/cms/categories/edit.html', title: 'Category Edit' })
				.when('/cms/categories/new/:categoryId', { templateUrl: 'partials/cms/categories/edit.html', title: 'New Category' })
				.when('/cms/disciplines', { templateUrl: 'partials/cms/disciplines/list.html', title: 'Disciplines' })
				.when('/cms/disciplines/:disciplineId', { templateUrl: 'partials/cms/disciplines/edit.html', title: 'Disciplines Edit' })
				.when('/cms/disciplines/new', { templateUrl: 'partials/cms/disciplines/edit.html', title: 'New Discipline' })
				.when('/cms/images', { templateUrl: 'partials/cms/images/libraries.html', title: 'Images' })
				.when('/cms/images/:libId', { templateUrl: 'partials/cms/images/library.html', title: 'Image' })
				.when('/cms/images/add/:libId', { templateUrl: 'partials/cms/images/image.html', title: 'New Image' })
				.when('/cms/images/edit/:libId/:imgId', { templateUrl: 'partials/cms/images/image.html', title: 'Edit Image' })
				.when('/cms/nodes/:nodeId/:type', { templateUrl: 'partials/cms/nodes/edit.html', title: 'Content' })
				.when('/cms/libraries/:type', { templateUrl: 'partials/cms/nodes/list.html', title: 'Content' })
				.when('/cms/recoverytemplates', { templateUrl: 'partials/cms/recoverytemplates/list.html', title: 'Templates' })
				.when('/cms/recoverytemplates/:entityId', { templateUrl: 'partials/cms/recoverytemplates/edit.html', title: 'Template' })

				.when('/emailtemplates/edit/:id', { templateUrl: 'partials/admin/emailtemplates/edit.html', title: 'Email Template' })
				.when('/emailtemplates', { templateUrl: 'partials/admin/emailtemplates/list.html', reloadOnSearch: false, title: 'Email Templates' })

				.when('/images/create', { templateUrl: 'partials/cms/images/add.html', title: 'Images' })
				.when('/images/edit/:id', { templateUrl: 'partials/cms/images/edit.html', title: 'Image Edit' })
				.when('/images/list', { templateUrl: 'partials/cms/images/list.html', reloadOnSearch: false, title: 'Images' })

				.when('/meta', { templateUrl: 'partials/landing/meta.html', title: 'Entity Details' })
				.when('/meta/list', { templateUrl: 'partials/meta/list.html', title: 'Entity Details' })
				.when('/meta/:entityId', { templateUrl: 'partials/meta/edit.html', title: 'Entity' })

				.when('/physician', { templateUrl: 'partials/physician/start.html', title: 'Physician' })
				.when('/physician/help', { templateUrl: 'partials/landing/physician.html', title: 'Physician Help' })
				.when('/physician/editprofile/:physicianId', { templateUrl: 'partials/physician/edit.html', title: 'Physician' })
				.when('/physician/patient/wizard', { templateUrl: 'partials/physician/add-patient.html', title: 'Physician' })
				.when('/physician/patient/wizard/:entityId', { templateUrl: 'partials/physician/add-patient.html', title: 'Physician' })
				.when('/physician/patients/:id/edit', { templateUrl: 'partials/physician/patient.html', title: 'Physician Edit' })
				.when('/physician/plans', { templateUrl: 'partials/physician/plans.html', title: 'Physician Plans' })
				.when('/physician/plans/:id', { templateUrl: 'partials/cms/recoveryplans/edit.html', title: 'Physician Plan' })
				.when('/physician/plans/:id/:templateId', { templateUrl: 'partials/cms/recoveryplans/edit.html', title: 'Physician Plah' })
				.when('/physician/dashboard', { templateUrl: 'partials/physician/dashboard.html', title: 'Compliance Tracking' })
				.when('/plans/:id/notifications', { templateUrl: 'partials/physician/notifications.html', title: 'Plan Notifications' })
				.when('/plans/:id/notifications/:notificationId', { templateUrl: 'partials/physician/notification-edit.html', title: 'Plan Notifications' })

				.when('/procedures/:id/dashboard', { templateUrl: 'partials/procedures/dashboard.html', title: 'Patient Dashboard' })

				.when('/practiceadmin', { templateUrl: 'partials/landing/practiceadmin.html', title: 'Practice Admin' })
				.when('/practiceadmin/admin/users', { redirectTo: '/admin/users', title: 'Practice Admin Users' })
				.when('/practiceadmin/profile/edit', { redirectTo: '/admin/practices/-1', title: 'Practice Admin' })
                .when('/practiceadmin/physician/plans', { redirectTo: '/physician/plans', title: 'Practice Admin Plans' })
                .when('/facilitygroup', { templateUrl: 'partials/admin/facilities/facilitygroup.html', title: 'Practice Admin Facility Group' })
                .when('/users', { templateUrl: 'partials/landing/users.html', title: 'Users' })


				.when('/callcenter', { templateUrl: 'partials/callcenter/callcenter.html', title: 'Call Center' })
				.when('/callcenter/followup/', { templateUrl: 'partials/callcenter/followup.html', reloadOnSearch: false, title: 'Call Center Follow Up' })
				.when('/callcenter/reporting', { templateUrl: 'partials/callcenter/reporting.html', title: 'Call Center Reporting' })
				.when('/callcenter/patients', { templateUrl: 'partials/callcenter/patients.html', reloadOnSearch: false, title: 'Call Center Patients' })
				.when('/callcenter/patient/:id', { templateUrl: 'partials/callcenter/patient.html', title: 'Call Center Patient' })
				.when('/callcenter/providers', { templateUrl: 'partials/callcenter/providers.html', reloadOnSearch: false, title: 'Call Center Providers' })
				.when('/callcenter/provider/:id', { templateUrl: 'partials/callcenter/provider.html', title: 'Call Center Provider' })
				.when('/callcenter/call/:id', { templateUrl: 'partials/callcenter/call.html', title: 'Call Center Patient' })
				.when('/callcenter/summary', { templateUrl: 'partials/callcenter/summary.html', reloadOnSearch: false, title: 'Call Center Activity' })
                .when('/callcenter/callhistory/:id', { templateUrl: 'partials/callcenter/callhistory.html', title: 'Call Center History' })
                .when('/callcenter/managepatients', { templateUrl: 'partials/callcenter/managepatients.html', reloadOnSearch: false, title: 'Call Center Manage Patients' })

                .when('/monitor/summary', { templateUrl: 'partials/callcenter/summary.html', reloadOnSearch: false, title: 'Call Center Activity' })
                .when('/monitor/followup/', { templateUrl: 'partials/callcenter/followup.html', reloadOnSearch: false, title: 'Call Center Follow Up' })
                .when('/monitor/patients', { templateUrl: 'partials/callcenter/patients.html', reloadOnSearch: false, title: 'Call Center Patients' })
                .when('/monitor/reporting', { templateUrl: 'partials/callcenter/reporting.html', title: 'Call Center Reporting' })
                .when('/monitor/managepatients', { templateUrl: 'partials/callcenter/managepatients.html', title: 'Call Center Manage Patients' })

                .when('/callcenteradmin', { templateUrl: 'partials/callcenter/callcenter.html', title: 'Call Center' })
                .when('/callcenteradmin/summary', { templateUrl: 'partials/callcenter/summary.html', reloadOnSearch: false, title: 'Call Center Activity' })
                .when('/callcenteradmin/reporting', { templateUrl: 'partials/callcenter/reporting.html', title: 'Call Center Reporting' })
                .when('/callcenteradmin/followup/', { templateUrl: 'partials/callcenter/followup.html', reloadOnSearch: false, title: 'Call Center Follow Up' })
                .when('/callcenteradmin/patients', { templateUrl: 'partials/callcenter/patients.html', reloadOnSearch: false, title: 'Call Center Patients' })
                .when('/callcenteradmin/practices', { templateUrl: 'partials/admin/practices/list.html', reloadOnSearch: false, title: 'Call Center Practices' })
                .when('/callcenteradmin/providers', { templateUrl: 'partials/callcenter/providers.html', reloadOnSearch: false, title: 'Call Center Providers' })
                .when('/callcenteradmin/patient/:id', { templateUrl: 'partials/callcenter/patient.html', reloadOnSearch: false, title: 'Call Center Patient' })
        
                .when('/', {
                    resolve: {
                        "check": function ($rootScope, $location, $window) {
                            $rootScope.$watch('isHotlineNurse', function () {
                                var hotline = $rootScope.isHotlineNurse;
                                var admin = $rootScope.isCallSiteAdmin;
                                if (hotline === true) {
                                    
                                    $location.path('/hotlinenurse');
                                }
                                else if (admin === true) {
                                    $location.path('/callcenter/summary');
                                }
                                else {
                                    $location.path('/dashboard');
                                }

                            })
                        }
                    
                    }, title: 'Dashboard'
                })
                
				.when('/404', { templateUrl: 'partials/pages/404.html' })

				.when('/pagecontent/', { templateUrl: 'partials/admin/editable/list.html', title: 'Page Content' })
				.when('/pagecontent/new', { templateUrl: 'partials/admin/editable/edit.html', title: 'New Page' })
				.when('/pagecontent/:id/edit', { templateUrl: 'partials/admin/editable/edit.html', title: 'Page Edit' })

                .when('/hotlinenurse', { templateUrl: 'partials/callcenter/patients.html', title: 'Hotline Nurse' })  


                .when('/callcenter/searchpatients', { templateUrl: 'partials/callcenter/patients.html', reloadOnSearch: false, title: 'Call Center Patients' })

				.otherwise({ redirectTo: '/404' });
		}
	]);


	angular.module('idr.directives', []);
	angular.module('idr.filters', []);

	idr.run(['$rootScope', '$http', function ($rootScope, $http) {

		var tkn = localStorage.getItem('token');
		if (tkn) {
			$http.defaults.headers.common.Authorization = 'Bearer ' + tkn;
		}

		$rootScope.pageTitle = 'IDR Smart Dr.';

		$rootScope.$on("$routeChangeStart", function (event, next, current) {

			if (!localStorage.getItem('token')) {
				window.location.href = '/account/login';
			}

			if (next && next.title) {
				$rootScope.pageTitle = next.title;
			}

			try {
				Dropzone.forElement('#dropzone-control').destroy();
			} catch (e) {
				//console.log('unable to destory drop zone.');
			}

		});

		$rootScope.formatPhone = function (number) {
            /* 
            @param {Number | String} number - Number that will be formatted as telephone number
            Returns formatted number: (###) ###-####
                if number.length < 4: ###
                else if number.length < 7: (###) ###
            */
			if (!number) { return ''; }

			number = String(number);

			// Will return formattedNumber. 
			// If phonenumber isn't longer than an area code, just show number
			var formattedNumber = number;

			// if the first character is '1', strip it out and add it back
			var c = (number[0] === '1') ? '1 ' : '';
			number = number[0] === '1' ? number.slice(1) : number;

			// # (###) ###-#### as c (area) front-end
			var area = number.substring(0, 3);
			var front = number.substring(3, 6);
			var end = number.substring(6, 10);

			if (front) {
				formattedNumber = (c + "(" + area + ") " + front);
			}
			if (end) {
				formattedNumber += ("-" + end);
			}
			return formattedNumber;
		};
	}]);

})();

window.xlogoff = function () {

	localStorage.removeItem('token');
	localStorage.removeItem('token_expires');
	window.location = "/account/logoff";
};
