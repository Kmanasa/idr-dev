﻿(function () {
    idr.directive('idrConfirm', ['$modal', idrConfirm]);
    idr.controller('IdrConfirmModalCtrl', ['$scope', '$modalInstance', 'options',  modalController]);

    function modalController($scope, $modalInstance, options) {

        $scope.message = options.message || 'Are you sure?';
        $scope.title = options.title || 'Please Confirm';
        $scope.oktext = options.oktext || 'OK';

        $scope.ok = function () {
            $modalInstance.close();
        };
        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    function idrConfirm($modal) {
        var trigger, instance;

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                
                trigger = attrs.idrConfirmTrigger || 'click';

                element.bind(trigger, function() {
                    instance = $modal.open({
                        templateUrl: 'confirmModal.html',
                        controller: 'IdrConfirmModalCtrl',
                        resolve: {
                            options: function() {
                                var opt = {};

                                if (attrs.idrConfirmMessage) {
                                    opt.message = attrs.idrConfirmMessage;
                                }

                                if (attrs.idrConfirmTitle) {
                                    opt.title = attrs.idrConfirmTitle;
                                }

                                if (attrs.idrConfirmOkText) {
                                    opt.oktext = attrs.idrConfirmOkText;
                                }

                                return opt;
                            }
                        }
                    });

                    instance.result.then(function () {
                        scope.$eval(attrs.idrConfirm);
                    });
                });
            }
        };
    }

})();