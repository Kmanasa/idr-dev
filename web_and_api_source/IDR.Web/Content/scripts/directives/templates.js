﻿(function (window) {
    window.idrTemplates = {

    };


    idr.controller('MessageController', ['$rootScope', '$scope', '$routeParams', '$location', 'Api'
    , function ($rootScope, $scope) {
        var handlers = [];
        handlers.push(function (msg) {
            return msg;
        });
        function prepare(msg) {
            if (msg.State == 0) {
                msg.max = 0;
                msg.class = 'alert-info';
                msg.fa = 'fa-spinner fa-spin';
            }
            else if (msg.State == 1) {
                msg.max = 10;
                msg.class = 'alert-success';
                msg.fa = 'fa-bookmark';
            }
            else if (msg.State == 2) {
                msg.max = 15;
                msg.class = 'alert-warning';
                msg.fa = 'fa-exclamation';
            }
            else if (msg.State == 3) {
                msg.max = 0;
                msg.class = 'alert-danger';
                msg.fa = 'fa-warning';
            }
            else if (msg.State == 4) {
                msg.max = 0;
                msg.class = 'alert-danger';
                msg.fa = 'fa-warning'
                console.error(msg.Message);
                msg.Message = "A System Exception Has Occurred.  Please try your operation again.  If this continues to occur, please contact an administrator.";
            }
            return msg;
        }
        $scope.messages = [];
        function deleteForTag(tag) {
            $scope.messages.where(function (m) {
                if (m.tag == tag) {
                    m.hide = true;
                }
                return m.tag == tag;
            });
        }

        addMessage = function (msg, tag) {
            deleteForTag(tag);
            if ($scope.messages.any(function (m) { return m.Message == msg.Message; })) {
                var m = $scope.messages.where(function (m) { return m.Message == msg.Message; })[0];
                m.hide = true;
            }
            if (msg && msg.Message && msg.Message) {
                $scope.messages.push(prepare(msg));
                msg.tag = tag;
                msg.age = 0;
            }
        };

        setInterval(function () {
            var deletes = [];
            $scope.$apply(function () {
                var index = 0;
                $scope.messages.select(function (msg) {
                    msg.age++;
                    if (msg.max > 0 && msg.age >= msg.max) {
                        msg.hide = true;
                    }
                    index++;
                });
            });
        }, 1000);

    }]);


    window.handleResponse = function ($scope, data, tag) {
        if (data.State) {
            var deletes = [];
            data.Messages.select(function (message) {
                addMessage(message, tag);
            });
            return data.Subject;
        }
        return data;
    }

    /// expects config.serviceName
    window.idrTemplates.template_listEditController = function (config) {

        if (config.todos) {
            config.todos.select(function (todo) {
                console.warn("**** TODO : " + todo);
            });
        }

        function defaultNull(name, defaultValue) {
            return !config[name] ? defaultValue : config[name];
        }

        function validateConfig(names) {
            names.select(function (item) {
                if (!config[item]) {
                    throw new Error('Config value named: ' + item + ' was required but not specified!');
                }
            });
        }

        validateConfig(['serviceName', 'controllerName']);

        return function ($scope, $routeParams, $location, $sce, Api, $modal) {
            $scope.getFunc = function (name) {
                var funcName = defaultNull(name + 'FuncName', name);
                return $scope[funcName];
            }

            function runIf(name) {
                var args = [];
                for (var i = 0; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }

                if (config[name]) {
                    args.push($modal || {});
					args.push(Api);
                    args.push($sce);
                    args.push($location);
                    args.push($routeParams);
                    args.push($scope);
                    args = args.reverse();

                    return config[name].apply(config, args);
                }
                return true;
            }

            $scope[defaultNull('idName', 'entityId')] = $routeParams[defaultNull('idName', 'entityId')];

            $scope.cleanHtml = function (val) {
                var html = $sce.trustAsHtml(val);
                return html;
            };

            runIf('init');

            function loadList() {
                runIf('beforeList');
                var serviceMeth = defaultNull('listMethodName', 'query');
                if (config.isPaging) {
                    Api[config.serviceName][serviceMeth]($scope.paging, function (data) {
                        data = window.handleResponse($scope, data);
                        $scope[defaultNull('listName', 'Data')] = data;
                        var mod = data.TotalRecords % $scope.paging.Limit;
                        $scope.totalPages = (Math.floor(data.TotalRecords / $scope.paging.Limit) + (mod > 0 ? 1 : 0)) - 1;
                        $scope.pages = new Array();//$scope.totalPages;//new Array(Math.min($scope.totalPages + 1, 5));
                        if ($scope.paging.Offset > $scope.totalPages) {
                            $scope.paging.Offset = $scope.totalPages;
                        }
                        if ($scope.totalPages === 0) {
                            $scope.paging.Offset = 0;
                        } 
                        $scope.paging.currentRecords = data.Results.length;
                        $scope.paging.TotalRecords = data.TotalRecords;
                        runIf('onList');
                    });
                }
                else {
                    Api[config.serviceName][serviceMeth]().$promise.then(function (data) {
                        data = window.handleResponse($scope, data);
                        $scope[defaultNull('listName', 'Data')] = data;
                        runIf('onList');
                    });
                }
            }

            $scope.loadList = loadList;

            if (config.loadListFuncName) {
                $scope[config.loadListFuncName] = loadList;
            }

            if (config.serviceCalls) {
                config.serviceCalls.select(function (svc) {
                    svc.method = !svc.methodName ? 'query' : svc.methodName;
                    Api[svc.serviceName][svc.method]().$promise.then(function (data) {
                        data = window.handleResponse($scope, data);
                        $scope[svc.scopeName] = data;
                        if (svc.handle) {
                            svc.handle($scope, $routeParams, $location, Api, data);
                        }
                    });
                });
            }

            if ($scope[defaultNull('idName', 'entityId')] == 'new') {
                runIf('beforeCreate');
                var svc = (config.hasParent) ? Api[config.serviceName].create({ id: $routeParams[defaultNull('parentIdName', 'parentId')] }) : Api[config.serviceName].create();
                svc.$promise.then(function (data) {
                    data = window.handleResponse($scope, data);
                    $scope[defaultNull('entityName', 'Entity')] = data;

                    $scope.Entity = data;
                    runIf('onCreate');
                });
            }
            else if ($scope[defaultNull('idName', 'entityId')]) {
                runIf('beforeExisting');
                var conf = { id: $scope[defaultNull('idName', 'entityId')] };
                var serviceMeth = defaultNull('getMethodName', 'get');

                conf[defaultNull('parentIdName', 'parentId')] = $routeParams[defaultNull('parentIdName', 'parentId')];

                var svc = Api[config.serviceName][serviceMeth](conf);
                svc.$promise.then(function (data) {
                    data = window.handleResponse($scope, data);
                    $scope[defaultNull('entityName', 'Entity')] = data;
                    $scope.Entity = data;
                    runIf('onExisting', data);
                });
            }
            else {
                $scope.paging = {
                    Filter: '', Order: '', Ascending: true, Offset: 0, Limit: 10
                };
                $scope.loadList();
                $scope.boundaryLinks = true;
                $scope.directionLinks = true;
                $scope.setOffset = function (page) {
                    if (page < 0 || page > $scope.totalPages) return;
                    $scope.paging.Offset = page;
                    $scope.loadList();
                }
            }

            $scope[defaultNull('saveFuncName', 'save')] = function (item, form, force, callback) {
                if ((form && form.$dirty) || force) {
                    runIf('beforeSave');
                    Api[config.serviceName].update(item, function (data) {
                        data = window.handleResponse($scope, data);
                        $scope[defaultNull('entityName', 'Entity')].Id = data.Id; // reset the base entity in the case of a new or... 
                        if (form) { form.$setPristine; }

                        runIf('onSave');

                        if (callback) {
                            callback();
                        }
                    });
                }
            };

            $scope[defaultNull('removeFuncName', 'remove')] = function (item) {
                runIf('beforeRemove');
                Api[config.serviceName].remove({ id: item.Id }, function (data) {
                    data = window.handleResponse($scope, data);
                    $scope.loadList();
                    runIf('onRemove');
                });
            };

            $scope[defaultNull('copyFuncName', 'copy')] = function (item) {
                runIf('beforeCopy');
                Api[config.serviceName].copy({ id: item.Id }, function (data) {
                    data = window.handleResponse($scope, data);
                    $scope.loadList();
                    runIf('onCopy');
                });
            };


            $scope.goto = function (url) {
                if (runIf('onGoto')) {
                    console.warn('Controller: ' + config.controllerName + ' goto: ' + url)
                    $location.path(url);
                }
            }

            $scope.masks = { phone: '(999) 999-9999' };

            $scope.numPerPageOpt = [10, 25, 50, 100];

            runIf('after');

            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };

            $scope.dateformat = 'MM/dd/yyyy';
        };
    }
})(window);

(function () {
    idr.controller('ImageFileController', ['$scope', '$routeParams', '$location', 'Api'
        , function ($scope, $routeParams, $location, Api) {
            $scope.getUrl = function (img) {
                return '/api/imagefiles/' + img.Id;
            }
        }]);
})();
