﻿(function() {
    //scope.$parent.replacementUrl
    function dropzone() {
        return {
            restrict: 'AE',
            scope: {
                image: '=',
                onimageload: '='
            },
            link: function (scope, element, attrs) {
                var url = '/api/imagefiles/files/new';
                var img = scope.image || scope.$parent.image;
                if (img && img.Id > 0) {
                    url = '/api/imagefiles/files/' + img.Id;
                }

                var dz = new Dropzone('#' + element[0].id, {
                        url: url,
                        uploadMultiple: false,
                        maxFiles: 1,
                        init: function() {
                            this.on("maxfilesexceeded", function(file) {
                                this.removeAllFiles();
                                this.addFile(file);
                            });
                        },
                        complete: function(data) {
                            if (!img.EntityId) {
                                var xhr = data.xhr.response;
                                var finalImg = JSON.parse(xhr);
                                img = finalImg;
                                
                                if (scope.onimageload) {
                                    scope.onimageload(finalImg);
                                } else if (scope.imageUploadComplete || scope.$parent.imageUploadComplete) {
                                    var pic = JSON.parse(data.xhr.response);
                                    scope.$parent.imageUploadComplete(pic);
                                }
                            } else {
                                img.FileName = data.name;
                                scope.$apply(function() {
                                    img.FileName = data.name;
                                });
                                if (scope.onimageload) {
                                    scope.onimageload(img);
                                } else if (scope.imageUploadComplete || scope.$parent.imageUploadComplete) {
                                    var pic = JSON.parse(data.xhr.response);

                                    if (pic.Id) {
                                        scope.$parent.imageUploadComplete(pic);
                                    }

                                    if (scope.$parent.refreshEntity) {
                                        scope.$parent.refreshEntity();
                                    }
                                }
                            }
                            return true;
                        }

                    });
                
            }
        };
    }

    idr.directive('dropzone', dropzone);
})();

idr.directive('fileDropzone', function ($modal, Api) {
    return {
        scope: {
            action: "@",
            file: '=',
            practiceid: '@',
            getPDFFiles: "&callFuc" 

        },
        link: function (scope, element, attrs) {
            element.dropzone({
                url: scope.action,
                paramName: "uploadfile",

                acceptedFiles: '.pdf',
                init: function () {
                    this.on("maxfilesexceeded", function (file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });
                    this.on('success', function (file, res) {
                        var response = res;
                        var Instance = $modal.open({
                            backdrop: 'static',
                            keyboard: false,
                            templateUrl: 'file.html',
                            controller: 'fileController',
                            size: 'small',

                            resolve: {
                                response: function () {
                                    return response;
                                },
                            },

                        }) 

                    });
                },
                complete: function () {
                    scope.getPDFFiles();
                    this.removeAllFiles();
                }
            })
        }
    }
});
