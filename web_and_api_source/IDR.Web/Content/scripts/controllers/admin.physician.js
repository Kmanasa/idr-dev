﻿/// <reference path="../shared/templates.js" />

(function (window) {

    function fullPhysicianController($scope, $routeParams, $location, Api) {

        $scope.addFacility = function () {
            var facs = $scope.Entity.Facilities;
            var facility = { _new: true };
            $scope.Entity.Facilities.push(facility);
        }

        $scope.setFacilityAssign = function (fac, form) {
            fac._new = false;
            fac.PhysicianId = $scope.Entity.Id;
            fac.FacilityId = fac.Facility.Id;
            $scope.savePhysician($scope.Entity, form);
        }

        $scope.removeFacilityAssign = function (index, form) {
            $scope.Entity.Facilities.splice(index, 1);
            $scope.savePhysician($scope.Entity, form, true);
        }
    }

    idr.controller('AdminPracticesController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'AdminPracticesController'
            , serviceName: 'practices'
            , idName: 'practiceId'
            , saveFuncName: 'savePractice'
            , removeFuncName: 'removePractice'
            , copyFuncName: 'copyPractice'
            , entityName: 'Practice'
            , isPaging: true
            , listName: 'Practices'
            , loadFuncName: 'loadPractice'
            , listRoute: '/#/practices/list'
            , serviceCalls: []
            , onCreate: function ($scope) {
                var url = '/admin/practices/' + $scope.Practice.Id;
                $scope.goto(url);

                $scope.Company = { Id: $scope.Practice.Id }
            }
            , init: function ($scope) {
                $scope.refreshMce = function () {

                    if ($scope.refreshed) {
                        return;
                    }

                    $scope.refreshed = true;


                    setTimeout(function () {
                        $scope.$broadcast('$tinymce:refresh');

                    }, 500);
                };
                $scope.templateOptions = {
                    onDeactivate: function () {

                        $scope.savePractice($scope.Practice, $scope.current, true);
                    },
                    inline: false,
                    menubar: 'edit insert format table tools',
                    plugins: 'advlist table image link lists charmap colorpicker hr paste textcolor wordcount code',
                    skin: 'lightgray',
                    theme: 'modern',
                    fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 36px 38px 40px",
                    toolbar: 'image undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | fontselect | fontsizeselect | idrplaceholder',
                    height: '400px'
                };
            }
            , onExisting: function ($scope, $routeParams, $location, $sce, Api) {

                //var self = $scope;
                $scope.Company = $scope.Practice;
                $scope.Company.isPractice = true;
                $scope.canAddAdmin = false;
                $scope.canAddPhysician = false;

                function loadList() {
                    $scope.AllUsersPaging.paging.id = $scope.Practice.Id;

                    if (!$scope.AllUsersPaging || !$scope.AllUsersPaging.paging) return;

                    Api.practice.getUsers($scope.AllUsersPaging.paging, function (data) {
                        data = window.handleResponse($scope, data);
                        $scope.AllUsers = data;
                        var mod = data.TotalRecords % $scope.AllUsersPaging.paging.Limit;
                        $scope.AllUsersPaging.totalPages = (Math.floor(data.TotalRecords / $scope.AllUsersPaging.paging.Limit) + (mod > 0 ? 1 : 0)) - 1;

                        if ($scope.AllUsersPaging.totalPages < 0) {
                            $scope.AllUsersPaging.totalPages = 0;
                        }

                        $scope.AllUsersPaging.pages = new Array($scope.AllUsersPaging.totalPages); //new Array(Math.min($scope.totalPages + 1, 5));

                        if ($scope.AllUsersPaging.paging.Offset > $scope.AllUsersPaging.totalPages) {
                            $scope.AllUsersPaging.paging.Offset = $scope.AllUsersPaging.totalPages;
                        }
                        $scope.AllUsersPaging.paging.currentRecords = data.Results.length;
                        $scope.AllUsersPaging.paging.TotalRecords = data.TotalRecords;

                        var practiceAdmins = _.filter(data.Results, function (u) {
                            return u.UserType === 'practiceadmin';
                        });

                        var physicians = _.filter(data.Results, function (u) {
                            return u.UserType === 'physician';
                        });
                        var schedulers = _.filter(data.Results, function (u) {
                            return u.UserType === 'scheduler';
                        });

                        var siteAdmins = _.filter(data.Results, function (u) {
                            return u.UserType === 'siteadmin';
                        });

                        var callSiteAdmins = _.filter(data.Results, function (u) {
                            return u.UserType === 'siteadmin';
                        });

                        $scope.canAddAdmin = schedulers && schedulers.length < $scope.Company.MaxNamedUsers;
                        $scope.canAddScheduler = practiceAdmins && practiceAdmins.length < $scope.Company.MaxNamedUsers;
                        $scope.canAddPhysician = physicians && physicians.length < $scope.Company.MaxPhysicians;
                        //$scope.canAddPhysician = siteAdmins && siteAdmins.length < $scope.Company.MaxSiteAdmins;
                        $scope.canAddCallAdmin = callSiteAdmins && callSiteAdmins.length < $scope.Company.MaxNamedUsers;

                    });

                    Api.callCenters.query(function (cc) {

                        $scope.callCenters = cc;
                        $scope.callCenters.splice(0, 0, { Id: 0, Name: 'No Call Center Selected' });
                    });

                    Api.callsiteRefs.query(function (data) {
                        $scope.payors = data.Payors;
                    });
                }

                $scope.expirationChanged = function () {
                };

                $scope.resetPassword = function (user) {

                    window.addMessage({ State: 0, Message: 'Sending password reset...' }, 'RESETP');

                    Api.users.resetPassword({ id: user.UserId }, function (data) {
                        //alert('Password reset instructions sent');
                        handleResponse($scope, {
                            State: 2,
                            Messages: [{ Message: "Password reset request sent", max: 4, 'class': 'alert-success' }]
                        }, 'RESETP');
                    });
                };

                $scope.AllUsersPaging = {
                    paging: {
                        id: $scope.Practice.Id
                        , Filter: ''
                        , Order: ''
                        , Ascending: true
                        , Offset: 0
                        , Limit: 10
                        , LoadPhysicanAssistants: true,
                        LoadPhysicians: true,
                        LoadPracticeAdmins: true,
                        LoadPatients: true,
                        LoadSchedulers: true,
                        LoadSiteAdmins: true
                    },
                    totalPages: 0,
                    loadList: loadList
                    , boundaryLinks: true
                    , directionLinks: true
                    , setOffset: function (page) {
                        if (page < 0 || page > $scope.AllUsersPaging.totalPages) return;
                        $scope.AllUsersPaging.paging.Offset = page;
                        $scope.AllUsersPaging.loadList();
                    }
                };

                $scope.removeUser = function (user) {

                    var map = [];
                    map['practiceadmin'] = 'adminPracticeAdmin';
                    map['physiciansassistant'] = 'adminPhysiciansAssistants';
                    map['physician'] = 'adminPhysicians';
                    map['scheduler'] = 'adminSchedulers';
                    map['siteadmin'] = 'adminSiteAdmin';

                    Api[map[user.UserType]].remove(user, function (data) {
                        data = window.handleResponse($scope, data);
                        loadList();
                    });
                }

                $scope.deactivateUser = function (user) {

                    var map = [];
                    map['practiceadmin'] = 'adminPracticeAdmin';
                    map['physiciansassistant'] = 'adminPhysiciansAssistants';
                    map['physician'] = 'adminPhysicians';

                    Api[map[user.UserType]].deactivate({ id: user.Id }, function (data) {
                        loadList();
                    });
                }

                if ($scope.Practice) {
                    loadList();
                }
            }
            , after: function ($scope, $routeParams, $location, $sce, Api) {

                $scope.timeZones = [
                    { value: '(UTC-10:00) Hawaii', key: 'Hawaiian Standard Time' },
                    { value: '(UTC-09:00) Alaska', key: 'Alaskan Standard Time' },
                    { value: '(UTC-08:00) Pacific Time (US and Canada)', key: 'Pacific Standard Time' },
                    { value: '(UTC-07:00) Mountain Time (US and Canada)', key: 'Mountain Standard Time' },
                    { value: '(UTC-07:00) Arizona', key: 'US Mountain Standard Time' },
                    { value: '(UTC-06:00) Central Time (US and Canada)', key: 'Central Standard Time' },
                    { value: '(UTC-05:00) Eastern Time (US and Canada)', key: 'Eastern Standard Time' },
                    { value: '(UTC-04:00) Atlantic Time (Canada)', key: 'Atlantic Standard Time' }
                ];


                function getPDFFiles() {
                    $scope.pdfFiles = Api.pdfFiles.GetPdfFiles({ companyId: $scope.Practice.Id });
                }

                $scope.getPDFFiles = function (practice) {
                    getPDFFiles();
                    //$scope.pdfFiles = Api.pdfFiles.GetPdfFiles({ companyId: practice.Id });
                } 


                $scope.removePdf = function (id) {
                    Api.pdfFiles.Delete({ id: id }, getPDFFiles);
                   
                }

                $scope.setAddressesSavePractice = function () {

                    // Default the address names
                    if (!$scope.Practice.StreetAddress.Name) {
                        $scope.Practice.StreetAddress.Name = $scope.Practice.Name;
                    }

                    if (!$scope.Practice.MailingAddress.Name) {
                        $scope.Practice.MailingAddress.Name = $scope.Practice.Name;
                    }

                    if (!$scope.Practice.BillingAddress.Name) {
                        $scope.Practice.BillingAddress.Name = $scope.Practice.Name;
                    }

                    if (!$scope.Practice.CallCenterId) {
                        $scope.Practice.StartCalls = 0;
                        $scope.Practice.EndCalls = 0;
                        $scope.Practice.SatisfactionInitial = 0;
                        $scope.Practice.SatisfactionFinal = 0;
                        $scope.Practice.CallCenterNotes = null;
                    }

                    $scope.savePractice($scope.Practice, $scope.current);

                    var pr = $scope.Practice;
                    var userCost = pr.MaxNamedUsers * pr.CostPerUser;
                    var physicianCost = pr.MaxPhysicians * pr.CostPerPhysician;
                    var facilityCost = pr.MaxFacilities * pr.CostPerFacility;
                    var planCost = pr.MaxPlans * pr.CostPerPlan;

                    pr.TotalCost = planCost + userCost + physicianCost + facilityCost - pr.Discount;
                };

                $scope.copyStreetAddress = function (addr) {
                    if (!$scope.Practice.StreetAddress) {
                        return;
                    }

                    addr.Address1 = $scope.Practice.StreetAddress.Address1;
                    addr.Address2 = $scope.Practice.StreetAddress.Address2;
                    addr.City = $scope.Practice.StreetAddress.City;
                    addr.State = $scope.Practice.StreetAddress.State;
                    addr.PostalCode = $scope.Practice.StreetAddress.PostalCode;

                    $scope.savePractice($scope.Practice, $scope.current);
                };

                $scope.removeFacility = function (facility) {
                    Api.facilities.remove({ id: facility.Id }, function (result) {
                        $scope.Company.Facilities = Api.facilities.query({ companyId: facility.CompanyId });
                    });
                };

                $scope.copyFacility = function (facility) {
                    Api.facilities.copy({ id: facility.Id }, function (result) {
                        $scope.Company.Facilities = Api.facilities.query({ companyId: facility.CompanyId });
                    });
                };

                Api.companies.query(function (data) {
                    $scope.allCompanies = data.Results;
                });

                $scope.getCompanyFacilities = function (companyId) {
                    $scope.allCompanyFacilities = Api.facilities.query({ companyId: companyId });
                };

                $scope.addRemoteFacility = function (newFacility) {

                    var facilityExists = _.any($scope.Practice.RemoteFacilities, function (fc) {
                        return fc.Id === newFacility.Id;
                    });

                    if (facilityExists) {
                        alert('The selected facility is already assigned.');
                        return;
                    }

                    $scope.Practice.RemoteFacilities.push({
                        IsNew: true,
                        Id: newFacility.Id,
                        CompanyId: $scope.Practice.Id,
                        Name: newFacility.Name
                    });
                    $scope.savePractice($scope.Practice, $scope.currentm, true, function (data) {
                        _.each($scope.Practice.RemoteFacilities, function (fc) {
                            return fc.IsNew = false;
                        });
                    });
                }

                $scope.removeRemoteFacility = function (f) {
                    f.deletedOn = new Date();

                    $scope.savePractice($scope.Practice, $scope.current, true, function () {
                        $scope.Practice.RemoteFacilities = _.without($scope.Practice.RemoteFacilities, f);
                    });
                };
            }

        })]);

    idr.controller('AdminMananagePhysicianController', ['$scope', '$routeParams', '$location', '$sce', 'Api', '$modal'
        , idrTemplates.template_listEditController({
            controllerName: 'AdminMananagePhysicianController'
            , serviceName: 'adminPhysicians'
            , idName: 'physicianId'
            , saveFuncName: 'savePhysician'
            , removeFuncName: 'removePhysician'
            , copyFuncName: 'copyPhysician'
            , entityName: 'Physician'
            , parentIdName: 'practiceId'
            , hasParent: true
            , listName: 'Facilities'
            , onCopy: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.get({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , onRemove: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.query({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , after: function ($scope, $routeParams, $location, $sce, Api, $modal) {

                $scope.refreshEntity = function () {
                    window.addMessage({ State: 1, Message: 'Image Updated. Refresh to preview.' }, 'PREVIEW');
                };

                $scope.imageUploadComplete = function (img) {
                    $scope.Entity.ImageRef = img.Id;

                    Api.imageFiles.associatePhysicianImage({ id: img.Id, entityId: $scope.Entity.Id },
                        { id: img.Id, entityId: $scope.Entity.Id }, function () {
                            $scope.refreshEntity();
                        });

                };


                $scope.sendInvitation = function (id, service) {

                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');
                    Api[service].invite({ id: id }).$promise.then(function (data) {
                        handleResponse($scope, data, 'INVITE');
                    });
                };

                $scope.addFacility = function (newFacility) {

                    if (_.any($scope.Entity.Facilities, function (fc) {
                        return fc.FacilityId === newFacility.Id;
                    })) {
                        alert('The selected facility is already assigned.');
                        return;
                    }

                    $scope.Entity.Facilities.push({
                        _new: true,
                        FacilityId: newFacility.Id,
                        PhysicianId: $scope.Entity.Id,
                        Facility: newFacility
                    });
                    $scope.savePhysician($scope.Entity, $scope.current);
                }

                $scope.removeFacility = function (facilityAssignment) {
                    $scope.Entity.Facilities = _.without($scope.Entity.Facilities, facilityAssignment);

                    $scope.savePhysician($scope.Entity, $scope.current, true);
                };

                $scope.getCptCodes = function (lib) {

                    var codes = _.map(lib.CptCodes, function (cpt) {
                        return cpt.Code;
                    });

                    return codes.join(', ');
                };

                Api.practices.get({ id: $routeParams.practiceId }, function (data) {
                    $scope.Practice = data;
                });

                Api.physician.getAllPlans({
                    id: $routeParams.physicianId
                }, function (data) {
                    $scope.libraries = data;
                });




                function getPhysicianLibraries() {

                    Api.physician.getAllPlans({
                       id: $routeParams.physicianId
                    }, function (data) {
                        $scope.libraries = data;
                    });
                }

                function associateLibrary(library) {

                    Api.physician.addPlan({
                        id: library.Id,
                        proxyId: $routeParams.physicianId
                    }, library, getPhysicianLibraries);
                }

                $scope.deletePlan = function (id) {
                    response = Api.nodes.delete({ id: id }, (function (data) {
                        getPhysicianLibraries();
                    }));
                } 

                $scope.addPlan = function () {
                    var instance = $modal.open({
                        templateUrl: 'AddPhysicianLibrariesController.html',
                        controller: 'AddPhysicianLibrariesController'
                    });

                    instance.result.then(function (library) {
                        window.addMessage({ State: 1, Message: 'Please wait.  Importing a new plan may take several minutes.' }, 'ACTIVE');

                        associateLibrary(library);
                    });
                };
            }
        })]);

    idr.controller('AdminMananagePracticeAdminController',
        [
            '$scope', '$routeParams', '$location', '$sce', 'Api', function ($scope, $routeParams, $location, $sce, Api) {

                if ($routeParams.adminId === 'new') {
                    Api.adminPracticeAdmin.create({ practiceId: $routeParams.practiceId }, function (data) {
                        $scope.Entity = data;
                    });
                } else {
                    Api.adminPracticeAdmin.get({ id: $routeParams.adminId }, function (data) {
                        $scope.Entity = data;
                    });
                }

                $scope.saveAdmin = function (entity) {
                    Api.adminPracticeAdmin.update(entity);
                };

                $scope.sendInvitation = function (id) {
                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');

                    Api.adminPracticeAdmin.invite({ id: id },
                        function (data) {
                            window.handleResponse($scope, data, 'INVITE');
                        });
                };
            }
        ]);

    idr.controller('AdminMananageSchedulerController',
        ['$scope', '$routeParams', '$location', '$sce', 'Api', function ($scope, $routeParams, $location, $sce, Api) {

            if ($routeParams.adminId === 'new') {
                Api.adminSchedulers.create({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Entity = data;
                });
            } else {
                Api.adminSchedulers.get({ id: $routeParams.adminId }, function (data) {
                    $scope.Entity = data;
                });
            }


            $scope.saveAdmin = function (entity) {
                Api.adminSchedulers.update(entity);
            };

            $scope.sendInvitation = function (id) {
                window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');

                Api.adminSchedulers.invite({ id: id },
                    function (data) {
                        window.handleResponse($scope, data, 'INVITE');
                    });
            };
        }
        ]);

    idr.controller('AdminMananageCallSiteAdminController',
        [
            '$scope', '$routeParams', '$location', '$sce', 'Api', function ($scope, $routeParams, $location, $sce, Api) {

                if ($routeParams.adminId === 'new') {
                    Api.callsiteAdmins.create({ practiceId: $routeParams.practiceId }, function (data) {
                        $scope.Entity = data;
                    });
                } else {
                    Api.callsiteAdmins.get({ id: $routeParams.adminId }, function (data) {
                        $scope.Entity = data;
                    });
                }


                $scope.saveAdmin = function (entity) {
                    Api.callsiteAdmins.update(entity);
                };

                $scope.sendInvitation = function (id) {
                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');

                    Api.callsiteAdmins.invite({ id: id },
                        function (data) {
                            window.handleResponse($scope, data, 'INVITE');
                        });
                };
            }
        ]);

    idr.controller('AdminMananageCallSiteMobileUserController',
        [
            '$scope', '$routeParams', '$location', '$sce', 'Api', function ($scope, $routeParams, $location, $sce, Api) {

                if ($routeParams.adminId === 'new') {
                    Api.callsiteMobileUsers.create({ practiceId: $routeParams.practiceId }, function (data) {
                        $scope.Entity = data;
                    });
                } else {
                    Api.callsiteMobileUsers.get({ id: $routeParams.adminId }, function (data) {
                        $scope.Entity = data;
                    });
                }


                $scope.saveAdmin = function (entity) {
                    Api.callsiteMobileUsers.update(entity);
                };

                $scope.sendInvitation = function (id) {
                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');

                    Api.callsiteMobileUsers.invite({ id: id },
                        function (data) {
                            window.handleResponse($scope, data, 'INVITE');
                        });
                };
            }
        ]);


    idr.controller('AdminMananageAssistantController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'AdminMananageAssistantController'
            , serviceName: 'adminPhysiciansAssistants'
            , idName: 'assistantId'
            , saveFuncName: 'saveAssistant'
            , removeFuncName: 'removePhysician'
            , copyFuncName: 'copyPhysician'
            , entityName: 'Physician'
            , parentIdName: 'practiceId'
            , hasParent: true
            , listName: 'Facilities'
            //, after: fullPhysicianController
            , onCopy: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.get({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , onRemove: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.query({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , after: function ($scope, $routeParams, $location, $sce, Api) {
                $scope.sendInvitation = function (id, service) {

                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');
                    Api[service].invite({ id: id }, function (data) {
                        handleResponse($scope, data, 'INVITE');
                    });
                };
                Api.practices.get({ id: $routeParams.practiceId }, function (data) {
                    $scope.Practice = data;
                });

                Api.practice.getPhysicians({ id: $routeParams.practiceId }, function (data) {
                    data.Results.select(function (item) {
                        item.fullName = item.FirstName + ' ' + item.LastName + ' ' + item.Title;
                    })
                    $scope.Physicians = data.Results;
                });

            }
        })]);

    idr.controller('AdminPhysiciansController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'AdminPhysiciansController'
            , serviceName: 'adminPhysicians'
            , idName: 'physicianId'
            , saveFuncName: 'savePhysician'
            , removeFuncName: 'removeUser'
            , copyFuncName: 'copyPhysician'
            , entityName: 'Physician'
            , parentIdName: 'practiceId'
            , hasParent: true
            , listName: 'Facilities'
            , after: function ($scope, $routeParams, $location, $sce, Api) {

            }
            , onCopy: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.get({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , onRemove: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.query({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }

        })]);

    idr.controller('PhysiciansController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'PhysiciansController'
            , serviceName: 'physician'
            , idName: 'physicianId'
            , saveFuncName: 'savePhysician'
            , removeFuncName: 'removePhysician'
            , copyFuncName: 'copyPhysician'
            , entityName: 'Physician'
            , listName: 'Facilities'
            , getMethodName: 'getMyPhysicianProfile'
            //, after: fullPhysicianController
            , onCopy: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.get({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , onRemove: function ($scope, $routeParams, $location, $sce, Api) {
                Api.adminPhysicians.query({ practiceId: $routeParams.practiceId }, function (data) {
                    $scope.Practice.Physicians = data;
                });
            }
            , after: function ($scope, $routeParams, $location, $sce, Api) {
                if ($routeParams.physicianId === 0) {
                    $location.url('/assistant/physician/not-assigned');
                }


                $scope.addFacility = function () {
                    var facs = $scope.Entity.Facilities;
                    var facility = { _new: true };
                    $scope.Entity.Facilities.push(facility);
                }

                $scope.setFacilityAssign = function (fac, form) {
                    fac._new = false;
                    fac.PhysicianId = $scope.Entity.Id;
                    fac.FacilityId = fac.Facility.Id;
                    $scope.savePhysician($scope.Entity, form);
                }

                $scope.removeFacilityAssign = function (index, form) {
                    $scope.Entity.Facilities.splice(index, 1);
                    $scope.savePhysician($scope.Entity, form, true);
                }

                //			    Api.facilities.query({ companyId: -1 },function (data) {
                //			        $scope.Facilities = data;
                //			    });

            }
        })]);
    var idx = 1001;

    idr.controller('PatientProfileController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'PatientProfileController'
            , isPaging: true
            , serviceName: 'physician'
            , entityName: 'Patient'
            , getMethodName: 'getPatient'
            , listName: 'Patients'
            , after: function ($scope, $routeParams, $location, $sce, Api) {
                $scope.dobopen = false;
                $scope.protoopened = false;
                $scope.opendob = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.dobopen = true;
                };

                $scope.openproto = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.protoopened = true;
                };

                $scope.removePatient = function (pat) {

                    Api.patients.remove({ id: pat.Id }, function (data) {
                        $scope.goto('/physician/patients');
                    });
                }

                $scope.generateQr = function (patient) {
                    window.addMessage({ State: 0, Message: 'Generating Qr links ...' }, 'GENERATE');
                    Api.patientRecoveryPlans.generate(patient, function (data) {
                        data = window.handleResponse($scope, data, 'GENERATE');
                        data.HasQrCode = true;
                        patient.Code = data;
                    });
                }

                $scope.invitePatient = function (patient) {

                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');
                    Api.patientRecoveryPlans.invite(patient, function (data) {
                        data = window.handleResponse($scope, data, 'INVITE');
                        patient.Mailings = data.Mailings;
                    });
                }

                Api.plans.query().$promise.then(function (data) {
                    $scope.Plans = window.handleResponse($scope, data).Subject;
                });


                $scope.savePatient = function (frm) {
                    if (frm.$dirty && frm.$valid) {
                        Api.patients.savePatientProcedure($scope.Patient, function (data) {

                        });
                    }
                }
            }
        })]);

    idr.controller('MyPatientsController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'MyPatientsController'
            , isPaging: true
            , serviceName: 'physician'
            , listMethodName: 'getMyPatients'
            , listName: 'Patients'
            , after: function ($scope, $routeParams, $location, $sce, Api) {
                $scope.removePatient = function (p) {
                    Api.patients.remove(p, function (data) {
                        $scope.loadList();
                    })
                }
                $scope.copyPatient = function (p) {
                    Api.patients.copy({ id: p.Id }, function (data) {
                        $scope.loadList();
                    })
                }
            }
        })]);

    idr.controller('NewPatientWizardController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            controllerName: 'NewPatientWizardController'
            , isPaging: true
            , serviceName: 'physician'
            , listMethodName: 'getAllPatients'
            , listName: 'Patients'
            , after: function ($scope, $routeParams, $location, $sce, Api) {

                $scope.wizard = {
                    search: true
                    , foundPatient: false
                    , addNewProcedure: false
                    , addNew: false
                    , reset: function () {
                        this.foundPatient = false;
                        this.addNew = false;
                        this.addNewProcedure = false;
                        this.search = false;
                        return this;
                    }
                };


                if ($routeParams.entityId) {
                    $scope.wizard.foundPatient = true;
                    $scope.wizard.addNew = false;
                    $scope.wizard.search = true;
                    $scope.wizard.existing = true;
                    $scope.doneAccessCodes = true;

                    Api.patients.get({ id: $routeParams.entityId }, function (data) {
                        var d = data;
                        $scope.currentPatient = d;
                        $scope.currentProcedure = d;
                        $scope.currentCptProcedure = d.ProcedureType;
                        $scope.wizard.getAccessCodes = true;

                    });
                }
                $scope.searchProcedures = function (cpt, procedure) {
                    $scope.searchStarted = true;
                    Api.patients.loadCptProcedures({ find: cpt }, function (data) {
                        $scope.procedures = window.handleResponse($scope, data).Results;
                        $scope.showCptOverflow = data.TotalRecords > data.Results.length;

                    })
                }
                function newProcedure() {


                    $scope.currentProcedure = {

                        Id: $scope.currentPatient.Id, ProcedureName: '', DischargeProtocol: '', NumberOfDays: 0, DischargeDate: '',
                    };
                    for (key in $scope.currentPatient) {
                        if (isNaN(key)) {
                            $scope.currentProcedure[key] = $scope.currentPatient[key];
                        }
                    }
                }

                var existingProc = false;
                var existingQr = false;

                $scope.setProcedure = function (proc) {
                    if (!existingProc) newProcedure();
                    $scope.currentCptProcedure = proc;
                    $scope.currentProcedure.ProcedureTypeId = proc.Id;
                    $scope.currentProcedure.RecoveryPlan = proc.RecoveryPlan;
                    $scope.currentProcedure.RecoveryPlanId = proc.RecoveryPlan.Id;
                    window.addMessage({ State: 0, Message: 'Creating Treatment Plan...' }, 'CREATETREATMENT');
                    var svc = existingProc ? 'savePatientProcedure' : 'saveProcedure';
                    Api.patients[svc]($scope.currentProcedure, function (data) {
                        window.addMessage({ State: 1, Message: 'Treatment Plan Created!' }, 'CREATETREATMENT');
                        $scope.currentProcedure = window.handleResponse($scope, data);
                        $scope.currentProcedure.RecoveryPlan = proc.RecoveryPlan;
                        $scope.wizard.getAccessCodes = true;
                        window.addMessage({ State: 0, Message: 'Generating Qr links ...' }, 'GENERATE')
                        if (existingProc) return;

                        Api.patientRecoveryPlans.generate({ Id: $scope.currentProcedure.ProcedureId }, function (data) {
                            data = window.handleResponse($scope, data, 'GENERATE');
                            data.HasQrCode = true;
                            $scope.currentProcedure.Code = data;
                            $scope.doneAccessCodes = true;
                            existingProc = true;
                        });

                    });


                }

                $scope.openx = false;
                $scope.opendate = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.openx = true;
                };

                $scope.invitePatient = function (patient) {

                    window.addMessage({ State: 0, Message: 'Sending invitation...' }, 'INVITE');
                    Api.patientRecoveryPlans.invite({ Id: patient.ProcedureId }, function (data) {
                        $location.url('/physician/patients');
                    });
                }

                $scope.clearProcedure = function () {
                    delete $scope.currentProcedure.RecoveryPlan;
                }

                $scope.setPatient = function (patient) {
                    $scope.wizard.reset().foundPatient = true;
                    $scope.currentPatient = patient;
                }
                $scope.clearPatient = function () {
                    $scope.currentPatient = null;
                    $scope.wizard.reset().search = true;
                }

                $scope.startNewPatient = function () {
                    $scope.currentPatient = { MRN: '', FirstName: '', LastName: '', MiddleInitial: '', DateOfBirth: '', };
                    $scope.wizard.reset().addNew = true;
                }


                $scope.saveProcedure = function (frm) {
                    var svc = $scope.currentProcedure.Id > 0 ? 'savePatientProcedure' : 'saveProcedure';
                    if (frm.$dirty && frm.$valid) {

                        Api.patients[svc]($scope.currentProcedure, function (data) {

                        });
                    }
                }

                $scope.saveNewPatient = function (frm) {
                    if (frm.$dirty && frm.$valid) {

                        Api.patients.save($scope.currentPatient, function (data) {
                            $scope.currentPatient = data;
                            $scope.wizard.reset().foundPatient = true;
                        });
                    }
                }
            }
        })]);








    function addPhysicianLibrariesController($scope, $modalInstance, Api) {
        $scope.model = {
            term: ''
        };

        $scope.search = function () {
            Api.nodeLibraries.search({ term: $scope.model.term }, function (data) {
                $scope.libraries = data.Results;
            });
        };

        $scope.getCptCodes = function (lib) {

            var codes = _.map(lib.CptCodes, function (cpt) {
                return cpt.Code;
            });

            return codes.join(', ');
        };

        $scope.select = function (library) {
            $modalInstance.close(library);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    angular.module('app').controller('AddPhysicianLibrariesController', ['$scope', '$modalInstance', 'Api', addPhysicianLibrariesController]);

})(window);

function fileController($scope, $location, $modalInstance, response,  Api) {
    $scope.response = response;
    $scope.onClick = function () {
        $modalInstance.close($scope.model);
        
    }
}

angular.module('app').controller('fileController',
    ['$scope', '$location', '$modalInstance', 'response', 'Api', fileController]); 

function pdfEditController($scope, $routeParams,$window, Api) {
   
    $scope.pdfId = $routeParams.id;
    $scope.goBack = function () {
        if (window.history && $window.history.back) {
            window.history.back();
        }
    } 

    $scope.saveAlias = function () {
        Api.pdfFiles.Alias({ id: $scope.pdfId },
            {
                Id: $scope.pdfId,
                Alias: $scope.pdf.Alias
            });
    }
}
angular.module('app').controller('pdfEditController',
    ['$scope', '$routeParams', '$window', 'Api', pdfEditController]);  
