﻿

(function () {

    function adminCompanyController($scope, $routeParams, $q, $location, $controller, $attrs, $window, Api) {

        $scope.isNew = false;
        $scope.timeZones = [
                    { value: '(UTC-10:00) Hawaii', key: 'Hawaiian Standard Time' },
                    { value: '(UTC-09:00) Alaska', key: 'Alaskan Standard Time' },
                    { value: '(UTC-08:00) Pacific Time (US and Canada)', key: 'Pacific Standard Time' },
                    { value: '(UTC-07:00) Mountain Time (US and Canada)', key: 'Mountain Standard Time' },
                    { value: '(UTC-07:00) Arizona', key: 'US Mountain Standard Time' },
                    { value: '(UTC-06:00) Central Time (US and Canada)', key: 'Central Standard Time' },
                    { value: '(UTC-05:00) Eastern Time (US and Canada)', key: 'Eastern Standard Time' },
                    { value: '(UTC-04:00) Atlantic Time (Canada)', key: 'Atlantic Standard Time' }
        ];

        if ($routeParams.id && $routeParams.id != 'new') {
            $scope.Company = Api.companies.get({ id: $routeParams.id });
        } else {
            $scope.isNew = true;
            //$scope.Company = new Api.companies();
            $scope.Company = Api.companies.create();
        }

        $scope.saveCompany = function() {
            $scope.Company.$update();
        };

        $scope.removeFacility = function (facility) {
        	Api.facilities.remove({ id: facility.Id }, function (result) {
        		$scope.Company.Facilities = Api.facilities.query({ companyId: facility.CompanyId });
        	});
        };

        $scope.copyFacility = function (facility) {
        	Api.facilities.copy({ id: facility.Id }, function (result) {
        		$scope.Company.Facilities = Api.facilities.query({ companyId: facility.CompanyId });
        	});
        };

        $scope.getFunc = function (name) {
            
            if (name == 'save') {
                return $scope.saveCompany;
            }

            return function() {
               
            }
        }
    }

    angular.module('app').controller('AdminCompanyController',
        ['$scope', '$routeParams', '$q', '$location', '$controller', '$attrs', '$window', 'Api', adminCompanyController]);
}())