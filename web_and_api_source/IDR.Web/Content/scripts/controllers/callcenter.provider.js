﻿(function () {

	function callCenterEditProviderController($rootScope, $scope, $routeParams, $http, $location, Api) {

		$scope.providerTypes = [
			{ Name: 'Physician', Id: 0 },
			{ Name: 'Anesthesiologist', Id: 1 },
			{ Name: 'Nurse', Id: 2 }
		];

		$scope.provider = {
			CallCenterProviderFacilities: []
		};

		if ($routeParams.id !== 'new') {
			Api.callsiteProviders.get({ id: $routeParams.id }, function (data) {
				$scope.provider = data;

				if (!$scope.provider.Facilities) {
					$scope.provider.Facilities = [];
				}
			});
		}

		$scope.getFacilities = function (val) {
			return $http.get('/api/callcenterproviders/facilities?filter=' + val).then(function (response) {
				return response.data;
			});
		};

		$scope.selectFacility = function ($item, $model, $label) {
			var existing = _.find($scope.provider.CallCenterProviderFacilities,
				function (f) {
					return f.Id === $item.Id;
				});

			if (existing) {

				return;
			}

			$scope.selectedFacility = null;

			$scope.provider.CallCenterProviderFacilities.push({ Id: $item.Id, Name: $item.Name });
		};

		$scope.remove = function (f) {
			$scope.provider.CallCenterProviderFacilities = _.without($scope.provider.CallCenterProviderFacilities, f);
		}

		$scope.save = function () {
			window.addMessage({ State: 0, Message: 'Saving Provider...' }, 'PROVIDER');

			if (!$scope.provider.Id) {
				Api.callsiteProviders.save($scope.provider, function (data) {
					$scope.provider = data;

					saved();
					//$location.path('/callcenter/providers/');
				}, error);
			} else {
				Api.callsiteProviders.update($scope.provider, function (data) {
					$scope.provider = data;

					saved();
				}, error);
			}
		};

		function saved() {
			window.handleResponse($scope, {
				State: 2,
				Messages: [{ Message: "Provider Saved.", max: 4, 'class': 'alert-success' }]
			}, 'PROVIDER');
		}

		function error() {
			window.handleResponse($scope, {
				State: 3,
				Messages: [{ Message: "Unable to Update Provider.", max: 4, 'class': 'alert-danger' }]
			}, 'PROVIDER');
		}
	}

	angular.module('app').controller('CallCenterEditProviderController',
		['$rootScope', '$scope', '$routeParams', '$http', '$location', 'Api', callCenterEditProviderController]);

}())