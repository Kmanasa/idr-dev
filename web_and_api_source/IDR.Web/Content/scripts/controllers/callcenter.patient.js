﻿(function () {

	function callCenterEditPatientsController($rootScope, $scope, $routeParams, $location, Api) {
		$scope.patient = {
			StartTime: new moment().hour(11).minute(0).second(0).local(),
			Outpatient: true
		};

		$scope.saving = false;


		Api.auth.current(function (data) {

			$scope.isMobileUser = _.any(data.Roles, function (r) {
				return r.RoleName === 'Call Site Mobile' && r.IsInRole;
			});

			$scope.isCsa = $scope.isMobileUser || _.any(data.Roles, function (r) {
				return r.RoleName === 'Call Site Admin' && r.IsInRole;
			});
			
			Api.callsiteRefs.query(function (data) {
				$scope.refs = data;

				if ($routeParams.id !== 'new') {
					Api.callsitePatients.get({ id: $routeParams.id },
                        function (data) {
                            
                            $scope.patient = data;
                           
							if ($scope.isCsa) {
								$scope.patient.FacilityId = data.Facilities[0].Id;
							}
                           
							$scope.selectFacility($scope.patient.FacilityId);

						});

				} else {
					if ($scope.isCsa) {
						$scope.patient.FacilityId = data.Facilities[0].Id;
						$scope.selectFacility(data.Facilities[0].Id);
					}
				}
			});
		});

		$scope.physicians = [];
		$scope.anes = [];
		$scope.nurses = [];
		$scope.procedures = [];
		$scope.genders = [
			{ value: 0, key: 'Male' },
			{ value: 1, key: 'Female' },
			{ value: 2, key: 'Non-binary' }
		];

        $scope.selectFacility = function (facilityId) {
          
			var facility = _.find($scope.refs.Facilities, { Id: facilityId });

			if (facility && facility.PayorId) {
				$scope.patient.PayorId = facility.PayorId;
            }
            else {             
                $scope.patient.PayorId = null;
            } 

			$scope.procedures = facility.CptCodes;
			
			$scope.physicians = _.filter($scope.refs.Physicians, function (p) {
				var isAssigned = _.any(p.CallCenterProviderFacilities, function (f) {
					return f.FacilityId === facility.Id;
				});

				return isAssigned;
			});

			$scope.anes = _.filter($scope.refs.Anesthesiologists, function (p) {
				var isAssigned = _.any(p.CallCenterProviderFacilities, function (f) {
					return f.FacilityId === facility.Id;
				});

				return isAssigned;
			});

			$scope.nurses = _.filter($scope.refs.Nurses, function (p) {
				var isAssigned = _.any(p.CallCenterProviderFacilities, function (f) {
					return f.FacilityId === facility.Id;
				});

				return isAssigned;
			});
        };
        
        $scope.sendSms = function (sms) {
            $scope.patient.SendSmsChecked = sms;
        };

        $scope.sendEmail = function (email) {
            $scope.patient.SendEmailChecked = email;
        };

		$scope.save = function () {
			//$scope.patient.PracticeCode = 'abc';
			//$scope.patient.CptCode = 'hip';
            if ($scope.patient.PayorId === null) {
                window.handleResponse($scope,
                    {
                        State: 2,
                        Messages: [{
                            Message: "Please enter a valid Payor Type", max: 4, 'class': 'alert-warning'
                        }]
                    }, 'CCP');
                return;
            } 

			$scope.saving = true;

			if (!$scope.patient.Id) {
				Api.callsitePatients.save($scope.patient, function (data) {
					$scope.patient = data;
					$scope.saving = false;

					if ($scope.isMobileUser) {
						window.xlogoff();
					} else {

						window.handleResponse($scope,
							{
								State: 2,
								Messages: [{Message: "Patient Created.",max: 4,'class': 'alert-success'
								}]
							},'CCP');

						$location.path('/callcenter/patients');
					}
				}, function() {
					$scope.saving = false;
				});
			} else {
                Api.callsitePatients.update($scope.patient, function (data) {

                    $scope.patient = data;
					$scope.saving = false;

					window.handleResponse($scope,
						{
							State: 2,
							Messages: [{
								Message: "Patient Updated.", max: 4, 'class': 'alert-success'
							}]
						}, 'CCP');
				}, function () {
					$scope.saving = false;
				});
			}
		};
	}

	angular.module('app').controller('CallCenterEditPatientsController',
		['$rootScope', '$scope', '$routeParams', '$location', 'Api', callCenterEditPatientsController]);

}())