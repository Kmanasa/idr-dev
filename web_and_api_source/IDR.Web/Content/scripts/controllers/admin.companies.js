﻿(function () {
    idr.controller('AdminFacilitiesController', ['$scope', '$routeParams', '$location', 'Api',
        adminFacilitiesController]);

    function adminFacilitiesController($scope, $routeParams, $location, Api) {

        if (!$routeParams.facilityId || !$routeParams.companyId) {
            return;
        }

        function getFacility() {
            $scope.Facility = Api.facilities.get({ id: $routeParams.facilityId, companyId: $routeParams.companyId });
        }

        $scope.Company = Api.companies.get({ id: $routeParams.companyId });

        if ($routeParams.facilityId == 'new') {
            $scope.Facility = Api.facilities.create({ id: $routeParams.companyId });
        } else {
            getFacility();
        }

        $scope.saveFacility = function (item, form, force) {
            if (form.$dirty || force) {

                Api.facilities.update($scope.Facility, function (data) {
                    data = window.handleResponse($scope, data);
                    $scope.Facility.Id = data.Id;
                    form.$setPristine;
                });
            }
        };

        $scope.getFunc = function () {
            return $scope.saveFacility;
        }
    }
})();
