﻿(function () {

    function editableContentController($scope, $routeParams, $location, $modal, Api) {
        $scope.content = {};
        if ($routeParams.id) {
            
            Api.editableContent.get({ id: $routeParams.id }).$promise.then(function (data) {
                $scope.content = data;
            });
        } else {
            Api.editableContent.save({}).$promise.then(function (data) {
                $scope.content = data;
            });
        }

        $scope.save = function () {
            Api.editableContent.update($scope.content);
        };

        $scope.editorOptions = {
            onDeactivate: function () {
                $scope.save();
            },
            inline: false,
            menubar: 'edit insert format table tools',
            plugins: 'advlist table image link lists charmap colorpicker hr paste textcolor wordcount code idrimagelibrary',
            skin: 'lightgray',
            theme: 'modern',
            fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 36px 38px 40px",
            toolbar: 'image undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | fontselect | fontsizeselect | idrplaceholder',
            height: '400px'
        };

        idr.showImageLibrary = function () {
            var library = $modal.open({
                templateUrl: '/partials/cms/images/modal-image-management.html',
                controller: 'ImagesController',
                size: 'lg'
            });

            return library.result;
        };
    }

    function editableContentListController($scope, Api) {
        
        Api.editableContent.query().$promise.then(function (data) {
            $scope.content = data;
        });
    }

    angular.module('app').controller('EditableContentController', ['$scope', '$routeParams', '$location', '$modal', 'Api', editableContentController]);
    angular.module('app').controller('EditableContentListController', ['$scope', 'Api', editableContentListController]);
}())