﻿
(function() {
    'use strict';

    angular.module('app').controller('navController', [
        '$scope', '$routeParams', '$location', 'Api', function($scope, $routeParams, $location, Api) {
            var id = Math.random().toString();
            $scope.getNav = function() {


                return '/Navigation?id=' + id;
            };
        }
    ]);
})();
