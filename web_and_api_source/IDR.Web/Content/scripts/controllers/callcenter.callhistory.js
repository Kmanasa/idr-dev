﻿(function () {

	function callCenterCallHistoryController($rootScope, $scope, $routeParams, $location, $q, Api) {

		function query() {

			Api.callcenterSchedule.callhistory({ id: $routeParams.id },
				function (data) {
					$scope.summary = data;
					$scope.model = data[0] || {};

					$scope.lastId = data[data.length - 1].Id;

					var rec = _.max(data, function (d) { return d.Satisfaction; });
					$scope.model.Satisfaction = rec.Satisfaction || 0;
				});
		}


		$scope.getClass = function (field) {
			if (field == 1 || field == '1') {
                return 'fa-exclamation-triangle  pain-level pain-yellow';// 'slightly Tol';
			}
            else if (field == 0 || field == '0') {
				return 'fa-check pain-green';// 'tol';
			}

			return 'fa-ban pain-red';// 'intol';
		};

		query();


		$scope.deleteCall = function (callId) {
			Api.callcenterSchedule.remove({ id: callId }, query);
		};

	}

	angular.module('app').controller('CallCenterCallHistoryController',
		['$rootScope', '$scope', '$routeParams', '$location', '$q', 'Api', callCenterCallHistoryController]);
}())