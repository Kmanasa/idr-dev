﻿(function () {

    function physicianPatientController($scope, $routeParams, $location, $q, $modal, Api) {

        var patientId = $routeParams.id;

        function getPatientLibraries() {
            Api.physician.getPatientLibraries({ id: patientId }, function (data) {

                _.each(data, function (li) {
                    li.StartDate = moment.utc(li.StartDate).local();//.format('YYYY-MM-DD HH:mm a');
                    li.StartTime = moment.utc(li.StartTime).local();//.format('YYYY-MM-DD HH:mm a');
                    li.formattedTime = li.StartDate.format('MM/DD/YYYY H:mm a');
                });

                $scope.libraries = data;
            });
        }

        function associatePatientLibrary(model) {
            Api.physician.addPatientLibrary({
                id: patientId,
                patientId: patientId,
                procedureId: model.library.Id,
                facilityId: model.facilityId,
                cptCode: model.term,
                startDate: model.startDate,
                startTime: model.startTime,
                sendEmail: model.SendEmail,
                sendSms: model.SendSms
            }, getPatientLibraries);
        }

        $scope.patient = Api.physician.getPatient({ id: patientId });

        getPatientLibraries();

        $scope.sendEmail = function (lib) {
	        lib.ProcedureId = lib.Id;
            lib.id = lib.NodeLibraryId;
            lib.SendEmail = true;
            lib.SendSms = false;
            Api.physician.updatePatientLibrary(lib, getPatientLibraries);
        };

        $scope.sendSms = function (lib) {
			lib.ProcedureId = lib.Id;
			lib.id = lib.NodeLibraryId;
            lib.SendSms = true;
            lib.SendEmail = false;
            Api.physician.updatePatientLibrary(lib, getPatientLibraries);
        };

        $scope.addProcedure = function () {
            var instance = $modal.open({
                templateUrl: 'SearchPlans.html',
                controller: 'SearchPlansController',
                resolve: {
                    procedure: function () { return null; }
                }
            });

            instance.result.then(function (model) {
                associatePatientLibrary(model);
            });
        };

        $scope.editProcedure = function (lib) {
            
            var instance = $modal.open({
                templateUrl: 'SearchPlans.html',
                controller: 'SearchPlansController',
                resolve: {
                    procedure: function () { return lib; }
                }
            });

            instance.result.then(function (model) {
                Api.physician.updatePatientLibrary({
                    id: lib.NodeLibraryId,
                    procedureId: lib.Id,
                    patientId: patientId,
                    facilityId: model.facilityId,
                    cptCode: model.term,
                    startDate: model.startDate,
                    startTime: model.startTime,
                    accessCode: model.accessCode,
                    sendEmail: model.SendEmail,
                    sendSms: model.SendSms
                }, function () {
                    if (model.print) {

                        var url = '/patients/overview/' + lib.AccessCode;
                        window.open(url, '_blank');
                    }

                    getPatientLibraries();
                });
            });
        };

        $scope.removeProcedure = function (proc) {
            Api.physician.removePlan({ id: proc.Id }, getPatientLibraries);
        };

        $scope.save = function () {
            Api.physician.updatePatient({ id: patientId }, $scope.patient);
        };
    }

    function searchPlansController($rootScope, $scope, $modalInstance, procedure, Api) {
        var code = procedure ? procedure.AccessCode : '';

        $scope.buttonText = "Update";

        if (!procedure) {
            $scope.buttonText = "Add";
        }

        $scope.model = {
            term: '',
            facilities: Api.physician.getFacilities({ proxyId: $rootScope.proxyPhysicianId }),
            startTime: new Date(),
            accessCode: code,
            SendEmail: false,
            SendSms: false,
            print: false
        };

        Api.auth.current(function (data) {
            $scope.model.isDemo = data.IsDemoPhysician;
        });

        $scope.showDetails = false;

        Api.physician.getAllPlans({ id: $rootScope.proxyPhysicianId }, function (data) {
            $scope.model.libraries = data;
        });

        $scope.showAllDetails = function(planId) {
            $scope.model.library = _.find($scope.model.libraries, function(l) {
                return l.Id === planId;
            });
            $scope.showDetails = true;
        };

        if (procedure) {
            $scope.libraryId = procedure.NodeLibraryId;
            $scope.showDetails = true;
            $scope.model.term = procedure.CptCode;
            $scope.model.startDate = procedure.StartDate.format('MM/DD/YYYY');
            $scope.model.startTime = procedure.StartDate;
            $scope.model.facilityId = procedure.FacilityId;
            $scope.model.accessCode = procedure.AccessCode;
            $scope.model.SendSms = procedure.SendSms;
            $scope.model.SendEmail = procedure.SendEmail;

            $scope.model.library = {
                Id: procedure.NodeLibraryId,
                Name: procedure.LibraryName
            };
        }

        $scope.save = function () {

            //TODO: Check for duplicates

            var time = moment($scope.model.startTime);
            $scope.model.startDate = moment($scope.model.startDate)
                .hour(time.hour())
                .minute(time.minute())
                .millisecond(0);

            $modalInstance.close($scope.model);
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    angular.module('app').controller('PhysicianPatientController', ['$scope', '$routeParams', '$location', '$q', '$modal', 'Api', physicianPatientController]);
    angular.module('app').controller('SearchPlansController', ['$rootScope', '$scope', '$modalInstance', 'procedure', 'Api', searchPlansController]);
}())