﻿(function () {

    function callCenterReportingController($rootScope, $scope, $routeParams, $location, $q, $http, Api) {

        $scope.startDate = moment().format('MM/DD/YYYY');
        $scope.endDate = moment().format('MM/DD/YYYY');

        $scope.ranges = [
            'Yesterday',
            'Today',
            'Tomorrow',
            'Month to Date',
            'Year to Date',
            'Inception to Date'
        ];

        selectedFacilities = [];

        Api.auth.current(function (user) {

            Api.callcenterReports.query(function (data) {
                $scope.reports = data;

                if (data && data.length) {
                    $scope.report = data[0];
                }
            });

            $scope.Facilities = [];
            Api.callsiteRefs.query(function (data) {
                $scope.refs = data;
                $scope.Facilities = $scope.refs.Facilities;
                $scope.Facilities.sort(function (a, b) {
                    var nameA = a.Name.toLowerCase(), nameB = b.Name.toLowerCase()
                    if (nameA < nameB) //sort string ascending
                        return -1
                    if (nameA > nameB)
                        return 1
                    return 0
                })
              
            });
        });

        $scope.selectRange = function (rng) {
            var start = moment();
            var end = moment();

            switch (rng) {
                case 'Yesterday':
                    start = start.add('days', -1);
                    end = end.add('days', -1);
                    break;
                case 'Today':
                    break;
                case 'Tomorrow':
                    start = start.add('days', 1);
                    end = end.add('days', 1);
                    break;
                case 'Month to Date':
                    start = start.date(1);
                    break;
                case 'Year to Date':
                    start = start.dayOfYear(1);
                    break;
                case 'Inception to Date':
                    start = start.date(1);
                    start = start.month(0);
                    start = start.year(1900);
                    break;
                case 'All Dates':
                    //start = 01 / 01 / 1900;
                    start = start.date(1);
                    start = start.month(0);
                    start = start.year(1900);
                    end = end.add('days', 1);
                    break;
            }

            $scope.startDate = start.format('MM/DD/YYYY');
            $scope.endDate = end.format('MM/DD/YYYY');
        };

        $scope.runReport = function () {
            $scope.facility = [];
            angular.forEach($scope.selectedFacilities, function (item, index) {
                $scope.facility.push($scope.selectedFacilities[index].Name);
            });

            $scope.facility.Name = $scope.facility.join([separator = ','])

            if (!$scope.report || !$scope.report.ShareUrl) {
                return;
            }
            Api.callcenterReports.reportGenerationAuth(function (data) {
                $scope.auth = data.IsAuthenticatedForGeneratingReports;

                if ($scope.auth === true) {
                    var url = 'https://reporting.smart-dr.com/s/' +
                        $scope.report.ShareUrl +
                        '?date_start=' + moment($scope.startDate).format('MM-DD-YYYY') +
                        '&date_end=' + moment($scope.endDate).format('MM-DD-YYYY') +
                        '&facility_name=' + $scope.facility.Name +
                        '&RP1_Label=' + $scope.report.Rp1Label +
                        '&RP2_Label=' + $scope.report.Rp2Label +
                        '&RP3_Label=' + $scope.report.Rp3Label +
                        '&RP4_Label=' + $scope.report.Rp4Label;
                    var frame = $('#frame');
                 
                    frame.empty();

                    var iframe = $('<iframe frameborder="0" style="width: 100%; height: 800px; border: solid 1px #ccc;" src="' + url + '" ></iframe>');
                    frame.append(iframe);
                }
                else {
                    window.handleResponse($scope,
                        {
                            State: 2,
                            Messages: [{
                                Message: "User is not Authorizied to run reports.",
                                max: 4,
                                'class': 'alert-success'
                            }]
                        },
                        'REPORTS');
                }
            });

        };
    }

    angular.module('app').controller('CallCenterReportingController',
        ['$rootScope', '$scope', '$routeParams', '$location', '$q', '$http', 'Api', callCenterReportingController]);
}())


idr.directive('multiselectDropdown', function () {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        template:
            "<div class='btn-group' ng-class='{open: open}' style='width: 200px;'>" +
            "<button class='btn btn-small' style='width: 160px;'>---Select---</button>" +
            "<button class='btn btn-small dropdown-toggle' ng-click='openDropdown()' style='width: 40px;' ><span class='caret'></span></button>" +
            "<ul class='dropdown-menu' aria-labelledby='dropdownMenu' style='position: relative;'>" +
            "<li style='cursor:pointer;' ><a ng-click='selectAll()'></span> Select All </a></li>" +
            "<li style='cursor:pointer;' ><a ng-click='deselectAll()'> Clear All </a></li>" +
            "<li style='cursor:pointer;' ng-repeat='option in options'><a ng-click='toggleSelectItem(option)'><span ng-class='getClassName(option)' aria-hidden='true'></span> {{option.Name}}</a></li>" +
            "</ul>" +
            "</div>",

        controller: function ($scope) {
            $scope.all = false;
            $scope.openDropdown = function () {

                $scope.open = !$scope.open;

            };

            $scope.selectAll = function () {
                $scope.all = !$scope.all;

                $scope.model = [];

                angular.forEach($scope.options, function (item, index) {

                    $scope.model.push(item);

                });

            };

            $scope.deselectAll = function () {

                $scope.model = [];

            };
            $scope.model = [];
            $scope.toggleSelectItem = function (option) {

                var intIndex = -1;

                angular.forEach($scope.model, function (item, index) {

                    if (item.Id == option.Id) {

                        intIndex = index;

                    }

                });

                if (intIndex >= 0) {

                    $scope.model.splice(intIndex, 1);

                } else {

                    $scope.model.push(option);

                }

            };

            $scope.getClassName = function (option) {

                var varClassName = 'glyphicon glyphicon-remove-circle dashboard-error';

                angular.forEach($scope.model, function (item, index) {

                    if (item.Id == option.Id) {

                        varClassName = 'glyphicon glyphicon-ok-circle dashboard-success';

                    }

                });

                return (varClassName);
            }

        }
    }

});
