﻿(function () {

    function callCenterPatientsController($rootScope, $scope, $controller, $attrs, $routeParams, $modal, $location, $q, $window, Api) {
        $scope.filterType = 0;

        $scope.augmentSearch = function () {
            return {
                filter: $scope.filter,
                filterType: $scope.filterType
            };
        }

        $scope.patientChart = function (id) {

            Api.call.query({ id: id },
                function (data) {
                    $scope.summary = data;
                    $scope.model = data[0] || {};
                    $scope.lastId = data[data.length - 1].Id;
                    var path = '/#/callcenter/call/' + $scope.lastId;
                    $window.open(path, '_self');
                });
        } 


        $scope.sendTextOrPushNotification = function (patient) {
            var Instance = $modal.open({
                templateUrl: 'OnDemandMessage.html',
                controller: 'onDemandMessageController',
                size: 'lg',

                resolve: {
                    patient: function () {
                        return patient;
                    },
                    user: function () {
                        return $scope.user;
                    }
                },
            })
        }
        $scope.deletePatient = function (id) {
            Api.callsitePatients.remove({ id: id }, $scope.query);
        };

        // Use DataListCtrl as the base controller
        angular.extend(this, $controller('DataListCtrl', {
            $rootScope: $rootScope,
            $scope: $scope,
            $location: $location,
            $attrs: $attrs
        }));
    }
    function onDemandMessageController($rootScope, patient, user, $scope, $modalInstance, Api) {


        $scope.user = {
            SMS: 'false',
            Notification: 'false',
            message: ''
        };
        $scope.send = function () {
          
            var messageContent = $scope.user.message;
            if ($scope.user.SMS === 'true' && $scope.user.Notification === 'false' && $scope.user.message != '') {
                var messageType = 0;
                Api.callsitePatients.send({ patientId: patient.PatientId, Phone: patient.Phone, messageType: messageType, messageContent: messageContent });

            }
            else if ($scope.user.SMS === 'false' && $scope.user.Notification === 'true' && $scope.user.message != '') {
                var messageType = 1;
                Api.externalProcedures.get({ patientId: patient.PatientId, Phone: patient.Phone, messageType: messageType, messageContent: messageContent });

            }
            $modalInstance.close($scope.model);
        }

    };

    angular.module('app').controller('CallCenterPatientsController',
        ['$rootScope', '$scope', '$controller', '$attrs', '$routeParams', '$modal', '$location', '$q', '$window', 'Api', callCenterPatientsController]);
    angular.module('app').controller('onDemandMessageController', ['$rootScope', 'patient', 'user', '$scope', '$modalInstance', 'Api', onDemandMessageController]);

}())
