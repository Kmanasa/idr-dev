﻿(function () {

	function callCenterProvidersController($rootScope, $scope, $location, $attrs, $controller, Api) {

		// Use DataListCtrl as the base controller
		angular.extend(this, $controller('DataListCtrl', {
			$rootScope: $rootScope,
			$scope: $scope,
			$location: $location,
			$attrs: $attrs
		}));

		$scope.augmentSearch = function () {
			return {
				filter: $scope.filter,
				filterType: $scope.filterType
			};
		}
	}

	angular.module('app').controller('CallCenterProvidersController',
		['$rootScope', '$scope', '$location', '$attrs', '$controller', 'Api', callCenterProvidersController]);

}())