﻿(function() {
    
    function nodesController($scope, $routeParams, $location, $q,$timeout, $sce, $modal, Api) {

        $scope.libraryType = $routeParams.type;

        $scope.libraryTypes = [
            { key: 'master-template', title: 'Master Template'  },
            { key: 'content-template', title: 'Reusable Template'  }
        ];
       
        $scope.fullLibraryType = $scope.libraryTypes.where(function(item) { return item.key == $scope.libraryType; })[0];

        $scope.predicate = 'Id';
        $scope.reverse = false;
        $scope.order = function (predicate) {
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.predicate = predicate;
        };

        $scope.addNode = function(type) {
            var name = 'New ' + type.Name;
            var nodeLibraryId = getTopParent($scope.node).Id;
            var app = $scope.Nodes.where(function (node) { return node.Id == nodeLibraryId; })[0];

            var orderedNodes = _.sortBy($scope.node.Nodes, function (n) { return n.Sequence; });
            var sequence = _.last(orderedNodes).Sequence+1;

            var newNode = {
                Id: nodeIdx++,
                Name: name,
                Type: type,
                NodeLibraryId: app.Id,
                Sequence: sequence,
                TypeId: type.Id,
                nodeId: $scope.node.UINodeType == 'node_NodeLibrary_TreeItem' ? null : $scope.node.Id,
                Nodes: [],
                parents: []
            };

            Api.nodes.save(newNode, function(data) {
                $scope.node.Nodes.push(data);
                data.Type = $scope.NodeTypes.where(function(item) { return item.Id == data.TypeId; })[0];

                assignParents($scope.node, $scope.node.parent);
                $scope.node.__collapsed = false;
            });
        };

        $scope.copyApp = function(node) {

            window.addMessage({ State: 0, Message: 'Copying Master Template Library...' }, 'COPY');

            Api.nodes.copyApp({ id: node.Id }, function(data) {
                window.addMessage({ State: 1, Message: 'Copy was successful.' }, 'COPY');
                $scope.Nodes.push(data);
                //$scope.node = assignParents(data);
            });
        }

        $scope.removeType = function (node) {
            Api.nodeLibraries.remove({ id: node.Id }).$promise.then(function () {
                $scope.Nodes = _.without($scope.Nodes, node);
            });
        };

        $scope.testDrive = function (id) {
            //var testDriveUrl = 'mobile/drive/' + id;
            var testDriveUrl = '/content/mobile/startup.mobile?id=' + id;
            window.open(testDriveUrl, 'Plan Test Drive', 'width=375,height=627');
        };

        // Get the node libraries
        Api.nodeLibraries.query({ type: $scope.libraryType }, function (data) {
            $scope.Nodes = data;
        });
    }

    angular.module('app').controller('NodesListController', ['$scope', '$routeParams', '$location', '$q', '$timeout', '$sce', '$modal', 'Api', nodesController]);
})();