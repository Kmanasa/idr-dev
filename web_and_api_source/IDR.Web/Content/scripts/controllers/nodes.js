﻿(function() {
    
    function nodesController($scope, $routeParams, $location, $q, $timeout, $sce, $modal, Api) {

        var nodeIdx = -1,
            nodeId = $routeParams.nodeId;

        $scope.cptCodes = [];
        $scope.readonly = true;

        $scope.map = {
            company: false,
            location: false,
            directions: false,
            address1: "",
            address2: "",
            city: "",
            state: "",
            country:"",
            zipCode: ""
        } 

        Api.users.self(function(usr) {
            var roles = usr.Roles;
            var assignedRoles = _.filter(roles, function(role) {
                return role.IsInRole;
            });

            $scope.readonly = _.all(assignedRoles, function (role) {
                return role.RoleName != 'Admin' &&
                    role.RoleName != 'Content Manager' &&
                    role.RoleName != 'Super Admin';
            });

            
        });

        function clone(cls, exclusions) {
            var version = {};
            for (key in cls) {
                if (!exclusions.any(function(i) { return i == key; })) {
                    version[key] = cls[key];
                }
            }
            return version;
        };

        function assignParents(node) {
            node.Nodes.select(function(item) {

                if (item._touched) {
                    return;
                }

                item.parent = node;
                item.parents = $scope.getParents(item);
                item._touched = true;

                assignParents(item, node);
            });

            return node;
        }

        function getTopParent(node) {
            if (node) {
                if (node.parent) {
                    return getTopParent(node.parent);
                } else {
                    return node;
                }
            }
            return null;
        }

        function getNode(node, id) {
            if (node.Id == id) {
                $scope.node = node;
            } else {
                for (var i = 0; i < node.Nodes.length; i++) {
                    var f = node.Nodes[i];
                    getNode(f, id);
                }
            }
        }

        function hasParentWithAttr(node, attr) {
            if (!node.parentNode)
                return false;
            return node.parentNode.hasAttribute(attr) ? true : hasParentWithAttr(node.parentNode, attr);
        }

        function getData(svcName, scopeName, callbackFn) {
            Api[svcName].query(function (data) {
                $scope[scopeName] = data;
                if (callbackFn) {
                    callbackFn(data);
                }
            });
        }

        function ensureNode() {
            var deferred = $q.defer();

            if (nodeId == 'new') {
                Api.nodes.create({ id: nodeId, type: $scope.libraryType }, function (data) {
                 
                    nodeId = data.Id;

                    $scope.node = assignParents(data);

                    deferred.resolve();
                });
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        }

        function setDefaultNodeSequences(nodesToOrder, crnt) {

            var deferred = $q.defer(),
                requestCount = 0;

            var orderedNodes = _.sortBy(nodesToOrder, function (n) { return n.Sequence; });

            var nodesToUpdate = [];

            _.each(orderedNodes, function(n, index) {
                n.idealSequence = index;

                if (n.Sequence != n.idealSequence) {
                    n.Sequence = n.idealSequence;
                    delete n.idealSequence;
                    nodesToUpdate.push(n);
                }
            });
         
            _.each(nodesToUpdate, function (nd) {
                console.log('Reordering ' + nd.Name + ' to ' + nd.Sequence);
                $scope.saveNode(nd, crnt, true, function() {
                    requestCount += 1;

                    if (requestCount == nodesToUpdate.length) {
                        deferred.resolve(orderedNodes);
                    }
                });
            });

            if (!nodesToUpdate.length) {
                deferred.resolve(orderedNodes);
            } 

            return deferred.promise;
        }

        $scope.libraryType = $routeParams.type;

        // Get a list of the available attributes for reusable content
        $scope.availableAttributes = Api.attributes.query();

        tinymce.init({

            font_formats: "Andale Mono=andale mono,times;" +
                "Arial=arial,helvetica,sans-serif;"+
                "Arial Black=arial black,avant garde;"+
                "Book Antiqua=book antiqua,palatino;"+
                "Comic Sans MS=comic sans ms,sans-serif;"+
                "Courier New=courier new,courier;"+
                "Georgia=georgia,palatino;"+
                "Helvetica=helvetica;"+
                "Impact=impact,chicago;"+
                "Symbol=symbol;"+
                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                "Terminal=terminal,monaco;"+
                "Times New Roman=times new roman,times;"+
                "Verdana=verdana,geneva;"
        });

        $scope.libraryTypes = [
            { key: 'master-template', title: 'Master Template' },
            { key: 'content-template', title: 'Reusable Template' }
        ];
        $scope.fullLibraryType = $scope.libraryTypes.where(function(item) {
             return item.key == $scope.libraryType;
        })[0];

        $scope.editorOptions = {
            onDeactivate: function() {
                $scope.saveNode($scope.node, currentNode, true);
            }, 
            inline: false,
            menubar: 'edit insert format table tools',
            plugins: 'advlist table image link lists charmap colorpicker hr paste textcolor wordcount code idrimagelibrary idrplaceholders',
            skin: 'lightgray',
            theme: 'modern',
            fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 36px 38px 40px",
            toolbar: 'image undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | fontselect | fontsizeselect | idrplaceholder',
            height: '400px'

        };

        $scope.videoPlayerTypes = [
            { Id: 'YOUTUBE', Name: 'YouTube.com' }, { Id: 'VIMEO', Name: 'vimeo.com' }
        ];
       
        $scope.goto = function(url) {
            $location.path(url);
        }

        $scope.saveApp = function(node, form) {
            if (form.$dirty) {

                ensureNode().then(function() {
                    var newNode = clone(node, ['Nodes']);

                    newNode.CptCodes = _.map($scope.cptCodes, function (c) {
                        return { code: c };
                    });

                    Api.nodeLibraries.update(newNode, function() {
                        form.$setPristine();
                    });
                    if (node.Nodes) {
                        node.Nodes.forEach(function(f) {
                            f.Key = node.Key;
                        });
                    }
                });
            }
        };

        $scope.saveAppWithCpt = function (node, form) {
            form.$setDirty();
           
            $scope.saveApp(node, form);
        };

        $scope.saveNode = function(node, form, force, callback) {
            if (form.$dirty || force) {
                // strip circs
                node.NodeLibraryId = app.Id;
                var newNode = clone(node, ['parent', 'parents', 'Nodes', 'Type', 'Properties']);

                newNode.Properties = node.Properties;
                Api.nodes.update(newNode, function(data) {
                    if (form.$dirty) {
                        form.$setPristine();
                    }
                    if (callback) {
                        callback(data);
                    }
                });
            }
        }

        $scope.addNode = function(type) {
            var name = 'New ' + type.Name;
            var nodeLibraryId = getTopParent($scope.node).Id;

            var orderedNodes = _.sortBy($scope.node.Nodes, function (n) { return n.Sequence; });

            var sequence = 0;
            if (orderedNodes.length) {
                sequence = _.last(orderedNodes).Sequence + 1;
            }

            var newNode = {
                Id: nodeIdx++,
                Name: name,
                Type: type,
                NodeLibraryId: nodeId,
                Sequence: sequence,
                TypeId: type.Id,
                nodeId: $scope.node.UINodeType == 'node_NodeLibrary_TreeItem' ? null : $scope.node.Id,
                Nodes: [],
                parents: []
            };

            Api.nodes.save(newNode, function(data) {
                $scope.node.Nodes.push(data);
                data.Type = $scope.NodeTypes.where(function(item) { return item.Id == data.TypeId; })[0];

                assignParents($scope.node, $scope.node.parent);
                $scope.node.__collapsed = false;
            });
        };

        $scope.typesFilter = function(item) {
            if (!$scope.node.Type) {
                return item.IsTop;
            }

            var type = $scope.NodeTypes.where(function(typeitem) {
                return typeitem.Id == $scope.node.Type.Id;
            })[0];

            var isTrue = type.AllowedTypes.any(function(x) {
                return x.AllowedTypeId == item.Id;
            });
            return isTrue;
        };

        $scope.copyNode = function(child) {
            Api.nodes.copy({ id: child.Id }, function(data) {
                $scope.node.Nodes.push(data);
                assignParents($scope.node, $scope.node.parent);
            });
        }

        $scope.removeNode = function(child, node) {
            var idx = 0;
            var del = -1;

            node.Nodes.where(function(item) {
                if (item.Id == child.Id) {
                    del = idx;
                }
                idx++;
            });

            if (idx >= 0) {
                Api.nodes.remove({ id: child.Id }, function() {
                    node.Nodes.splice(del, 1);
                });
            }
		};

		$scope.getDisplayText = function (node) {
			if (!node) {
				return '';
			}

			if (node.Title) {
				return node.Title;
			}

			if (node.Name) {
				return node.Name;
			}

			return  '';
		};

	    $scope.setNode = function (node) {
            $scope.node = null;

            $timeout(function() {
                node.isReadonly = false;//(!node.Type.AllowPhysicianEdit || !node.IsPhysicianEditable) && $scope.readonly;
                $scope.node = node;

                if (node.ChildTemplateId != null && node.ChildTemplate == null) {
                    node.ChildTemplate = Api.nodes.get({ id: node.ChildTemplateId });
                }
            }, 200);
           
        };

        $scope.getParents = function(node) {
            var parents = [];

            function addPar(n) {
                if (n && n.parent) {
                    parents.push(n.parent);
                    addPar(n.parent);
                }
            };

            addPar(node);
            return parents.reverse();
        }

        $scope.moveUpValue = function(prop, node, idx) {
            if (idx > 0) {
                var value = prop.Values.splice(idx, 1);
                prop.Values.splice(idx - 1, 0, value[0]);
                var subIdx = 0;
                prop.Values.select(function(i) {
                    i.Sequence = subIdx++;
                });
                $scope.saveNode(node, currentNode, true);
            }
        };

        $scope.moveDownValue = function(prop, node, idx) {
            if (idx < prop.Values.length) {
                var value = prop.Values.splice(idx, 1);
                prop.Values.splice(idx + 1, 0, value[0]);
                var subIdx = 0;
                prop.Values.select(function(i) {
                    i.Sequence = subIdx++;
                });
                $scope.saveNode(node, currentNode, true);
            }
        };

        $scope.moveUp = function(node, parent, index) {
            var updates = setDefaultNodeSequences(parent.Nodes, currentNode);

            updates.then(function (orderedNodes) {
                console.log(orderedNodes);
                //var orderedNodes = _.sortBy(parent.Nodes, function (n) { return n.Sequence; });

                var candidateNodes = _.filter(orderedNodes, function (n) {
                    return n.Sequence < node.Sequence;
                });

                var swapNode = _.last(candidateNodes);

                if (swapNode) {
                    console.log('Moving ' + node.Name + ' from ' + node.Sequence + ' to ' + (node.Sequence - 1));
                    console.log('Moving ' + swapNode.Name + ' from ' + swapNode.Sequence + ' to ' + (swapNode.Sequence + 1));
                    node.Sequence -= 1;
                    swapNode.Sequence += 1;
                    $scope.saveNode(node, currentNode, true);
                    $scope.saveNode(swapNode, currentNode, true);
                }
            });
        };

        $scope.moveDown = function(node, parent, index) {

            var updates = setDefaultNodeSequences(parent.Nodes, currentNode);

            updates.then(function (orderedNodes) {
                console.log(orderedNodes);
                //var orderedNodes = _.sortBy(parent.Nodes, function (n) { return n.Sequence; });

                var candidateNodes = _.filter(orderedNodes, function (n) {
                    return n.Sequence > node.Sequence;
                });

                var swapNode = _.first(candidateNodes);

                if (swapNode) {
                    console.log('Moving ' + node.Name + ' from ' + node.Sequence + ' to ' + (node.Sequence + 1));
                    console.log('Moving ' + swapNode.Name + ' from ' + swapNode.Sequence + ' to ' + (swapNode.Sequence - 1));
                    node.Sequence += 1;
                    swapNode.Sequence -= 1;
                    $scope.saveNode(node, currentNode, true);
                    $scope.saveNode(swapNode, currentNode, true);
                }
            });
        };

        $scope.addAttribute = function(node, attribute) {

            Api.attributes.associate({id: node.Id}, { attributeId: attribute.Id }, function(data) {
                node.Attributes.push(data);
            });
        };

        $scope.dissociateAttribute = function(node, attribute) {
            Api.attributes.dissociate({ id: attribute.Id }, function() {
                var index = node.Attributes.indexOf(attribute);

                if (index > -1) {
                    node.Attributes.splice(index, 1);
                }
            });
        };

        $scope.cleanHtml = function (val) {
            var html = $sce.trustAsHtml(val);
            return html;
        }

        $scope.save = function(prop) {
            window.addMessage({ State: 0, Message: 'Saving content changes....' }, 'SAVE');

            prop.Values.select(function(val) {
                if (val.PartKey == 'LIST') {
                    val.Value = val.items.select(function(item) { return item.Value; }).join('|');
                }
            });

            $scope.saveNode($scope.node, currentNode, true, function(data) {
                window.addMessage({ State: 1, Message: 'Save Succeeded!' }, 'SAVE');

                var newProps = data.Properties.where(function(item) { return item.Id == prop.Id; });

                if (newProps.length > 0) {
                    prop.Values = newProps[0].Values;
                    prop.Values.select(function(val) {
                        val.items = val.Value && val.Value.length > 0 ? (val.Value.split('|')
                            .select(function(str) {
                                return { Value: str };
                            })) : [];
                    });
                }
            });
        }

        $scope.filterAttributes = function(attr) {
            if (!$scope.attributeFilters) {
                $scope.attributeFilters = [];
            }

            // Prevent duplicates
            if (_.any($scope.attributeFilters, function(attrId) { return attrId == attr.Id; })){
                return;
            }

            $scope.attributeFilters.push(attr);

            var idList = _.map($scope.attributeFilters, function (a) { return a.Id; }).join(',');

            $scope.availableTemplates = Api.nodes.getTemplates({ idList: idList });
        };

        $scope.removeFilter = function(flt) {
            $scope.attributeFilters = _.without($scope.attributeFilters, flt);;
        };

        $scope.addTemplateToNode = function(template) {
            $scope.node.ChildTemplateId = template.Id;

            $scope.saveNode($scope.node, currentNode, true, function (data) {
                window.addMessage({ State: 1, Message: 'Save Succeeded!' }, 'SAVE');

                $scope.node.ChildTemplate = { Id: data.Id, Name: data.Name };
                $scope.availableTemplates = [];
                $scope.attributeFilters = [];
            });
        };

        $scope.unassignChildTemplate = function(node) {
            $scope.node.ChildTemplate = null;
            $scope.node.ChildTemplateId = null;
            $scope.availableTemplates = [];
            $scope.attributeFilters = [];

            $scope.saveNode($scope.node, currentNode, true, function (data) {
                window.addMessage({ State: 1, Message: 'Save Succeeded!' }, 'SAVE');
            });
        };

        // Get each node type and the types allowed to be added to it
		Api.nodeTypes.query({ libraryType: $scope.libraryType }, function (data) {
            $scope.NodeTypes = data;

            if (data && data.length) {
                $scope.nodeTypeToAdd = data[0];
            }
        });
      
        getData('attributes', 'attributes');

        idr.showImageLibrary = function() {
            var library = $modal.open({
                templateUrl: '/partials/cms/images/modal-image-management.html',
                controller: 'ImagesController',
                size: 'lg'
            });
            
            return library.result;
        };

        if (!isNaN(nodeId)) {
            Api.nodes.get({ id: nodeId }, function (data) {
                
                $scope.node = assignParents(data);
               
                $scope.PDFFilesForNodeLibrary = $scope.node.PDFFilesForNodeLibrary;
                $scope.AddressForNodeLibrary = $scope.node.AddressForNodeLibrary;
                if (data.CptCodes) {
                    _.each(data.CptCodes, function(c) {
                        $scope.cptCodes.push(c.Code);
                    });
                }

                $scope.rootNode = $scope.node;

                if (idr._returnState) {
                    getNode($scope.node, idr._returnState.nodeId);

                    if ($scope.node.Properties) {
                 
                        $scope.node.Properties.forEach(function (p) {
                            if (p.Values) {
                                p.Values.forEach(function (v) {
                                    if (v.Id == idr._returnState.propValId) {
                                        if (v.Value && v.Value.length > 0) {
                                            v._imageItems = JSON.parse(v.Value);
                                        } else {
                                            v._imageItems = [];
                                        }
                                        v._imageItems.push({ Url: idr._returnState.url });
                                    }
                                });
                            }
                        });
                    }
                    idr._returnState = null;
                }
            });
        } else if (nodeId == 'new') {
            ensureNode().then(function() {
                $scope.rootNode = $scope.node;
            });
        }
            $scope.generateMap = function () {
                $scope.MapUrl = "";
                if ($scope.map.location) {
                    $scope.locationUrl = $scope.map.address1 + " " + $scope.map.address2 + " " + $scope.map.city + " " + $scope.map.state + " " + $scope.map.country +" "+ $scope.map.zipCode;
                    var locationUrl = $scope.locationUrl.split(',').join('%2C');
                    locationUrl = locationUrl.split(' ').join('+');
                  
                    if ($scope.map.directions == true) {
                       
                        window.open("https://www.google.com/maps/dir/?api=1&destination=" + locationUrl);
                        $scope.MapUrl = "https://www.google.com/maps/dir/?api=1&destination=" + locationUrl;
                        
                    }
                    else {
                        window.open("https://www.google.com/maps/search/?api=1&query=" + locationUrl);
                        $scope.MapUrl = "https://www.google.com/maps/search/?api=1&query=" + locationUrl;
                     
                    }
                }
                else {
                    if ($scope.AddressForNodeLibrary.Address1 != null) {
                        $scope.map.address1 = $scope.AddressForNodeLibrary.Address1;
                    }

                    if ($scope.AddressForNodeLibrary.Address2 != null) {
                        $scope.map.address2 = $scope.AddressForNodeLibrary.Address2;
                    }

                    if ($scope.AddressForNodeLibrary.City != null) {
                        $scope.map.city = $scope.AddressForNodeLibrary.City;
                    } 
                    if ($scope.AddressForNodeLibrary.State != null) {
                        $scope.map.state = $scope.AddressForNodeLibrary.State;
                    }

                    if ($scope.AddressForNodeLibrary.Country != null) {
                        $scope.map.country = $scope.AddressForNodeLibrary.Country
                    }

                    if ($scope.AddressForNodeLibrary.PostalCode != null) {
                        $scope.map.zipCode = $scope.AddressForNodeLibrary.PostalCode;
                    } 
                    
                    $scope.locationUrl = $scope.map.address1 + " " + $scope.map.address2 + " " + $scope.map.city + " " + $scope.map.state +
                        " " + $scope.map.country + " " + $scope.map.zipCode;
                    var locationUrl = $scope.locationUrl.split(',').join('%2C');
                    locationUrl = locationUrl.split(' ').join('+');

                    if ($scope.map.directions == true) {
                      
                        window.open("https://www.google.com/maps/dir/?api=1&destination=" + locationUrl);
                        $scope.MapUrl = "https://www.google.com/maps/dir/?api=1&destination=" + locationUrl;
                     
                    }
                    else {
                        window.open("https://www.google.com/maps/search/?api=1&query=" + locationUrl);
                        $scope.MapUrl = "https://www.google.com/maps/search/?api=1&query=" + locationUrl;
                       
                    }
                }
                $scope.MapUrl = $scope.MapUrl.split('+').join('/');
                $scope.node.Properties[0].Value = $scope.MapUrl;
                $scope.saveNode($scope.node, currentNode, true); 
            } 
    }

    angular.module('app').controller('NodesController', ['$scope', '$routeParams', '$location', '$q', '$timeout', '$sce', '$modal', 'Api', nodesController]);

})();