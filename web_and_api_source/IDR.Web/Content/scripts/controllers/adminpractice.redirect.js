﻿(function () {

    function adminPracticeRedirect($rootScope, $scope, $routeParams, $location, Api) {
 
        Api.users.self(function (data) {
            $location.path('/admin/practices/' + data.CompanyId);
        });
    }

    angular.module('app').controller('AdminPracticeRedirect',
        ['$rootScope', '$scope', '$routeParams', '$location', 'Api', adminPracticeRedirect]);
}())