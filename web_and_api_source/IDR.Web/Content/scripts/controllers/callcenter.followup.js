﻿(function () {

	function callCenterFollowupController($rootScope, $scope, $routeParams, $location, $q, Api) {

		var now = moment();
		var dateParams = {
			year: now.get('year'),
			month: $location.search().month || now.month() + 1,
			day: $location.search().day || now.date()
		};

		$scope.date = now.format('MM/DD/YYYY');

		if ($location.search().year && $location.search().month && $location.search().day) {
			$scope.date = moment($location.search().month + '/' + $location.search().day + '/' + $location.search().year).format('MM/DD/YYYY');
		}

		$scope.callType = 'All';

		function query() {

			var search = $location.search();
			if (search && search.year && search.month && search.day) {
				dateParams.year = search.year;
				dateParams.month = search.month;
				dateParams.day = search.day;
			}



			Api.callcenterSchedule.query(dateParams,
				function (data) {
					$scope.schedule = data;

					$scope.dayCounts = [];

					if (data.length) {

						var maxDay = _.max(data, function (d) { return d.Index; }).Index;

						var range = _.range(maxDay);

						_.each(range, function (r) {
							var index = r + 1;
								var count = _.filter(data, function (d) { return d.Index === index; });
								$scope.dayCounts.push({ key: 'Day ' + index, value: count.length });
							});
					}

				});

			Api.callcenterSchedule.aggregates(dateParams,
				function (data) {
					$scope.aggregates = data;
				});
		}

        query();
        $scope.reverseTime = false;
        $scope.reverseName = false;
        $scope.reverseFacility = false;
        $scope.order = function (predicate) {
            if (predicate === 'CallTime') {

                $scope.predicate = predicate;
                $scope.reverseTime = !$scope.reverseTime;
            }
            else if (predicate === 'FirstName') {

                $scope.predicate = predicate;
                $scope.reverseName = !$scope.reverseName;

            }
            else {

                $scope.predicate = predicate;
                $scope.reverseFacility = !$scope.reverseFacility;

            }
        }; 

		$scope.selectCall = function () {
			Api.callcenterSchedule.next(dateParams, function (call) {
				if (call.Id === -1) {
					window.handleResponse($scope,
						{
							State: 2,
							Messages: [{
								Message: "You have a pending call. Please complete the call before selecting another.",
								max: 4,
								'class': 'alert-success'
							}]
						},
						'CALLS');
				}
				else if (!call.Id) {

					window.handleResponse($scope,
						{
							State: 2,
							Messages: [{
								Message: "There are no pending calls available.",
								max: 4,
								'class': 'alert-success'
							}]
						},
						'CALLS');
				} else {
					$location.path('/callcenter/call/' + call.Id);
				}
			});
		};

		$scope.selectSpecificCall = function (id) {
			Api.callcenterSchedule.lock({ id: id, isSuperAdmin: $rootScope.isSuperAdmin }, function (call) {
              
				if (call.Id === -1) {
					window.handleResponse($scope,
						{
							State: 2,
							Messages: [{
								Message: "You have a pending call. Please complete the call before selecting another.",
								max: 4,
								'class': 'alert-success'
							}]
						},
						'CALLS');
				}
				else if (!call.Id) {

					window.handleResponse($scope,
						{
							State: 2,
                            Messages: [{
                                Message: "The selected call is not available- Locked By " + call.FirstName + " " + call.LastName,
								max: 4,
								'class': 'alert-success'
							}]
						},
						'CALLS');
				} else {
					//$location.path('/callcenter/call/' + call.Id + '?');
					window.location = '/#/callcenter/call/' + call.Id;
				}
			});
		};

		$scope.dateChanged = function () {
			var dt = moment($scope.date);
			dateParams = {
				year: dt.get('year'),
				month: dt.month() + 1,
				day: dt.date()
			};

			$location.search(dateParams);
		};

		$scope.filterCallType = function () {
			if ($scope.callType === 'All') {
				_.each($scope.schedule, function (s) {
					s.hidden = false;
				});
			} else if ($scope.callType === 'Mobile') {
				_.each($scope.schedule, function (s) {
					s.hidden = !s.Mobile;
				});
			} else {
				_.each($scope.schedule, function (s) {
					s.hidden = s.Mobile;
				});
			}
		};

		// Watch for query string change
		$scope.$on('$routeUpdate', function () {
			query();

			if ($location.search().year && $location.search().month && $location.search().day) {
				$scope.date = moment($location.search().month + '/' + $location.search().day + '/' + $location.search().year).format('MM/DD/YYYY');
			}
		});
	}

	angular.module('app').controller('CallCenterFollowUpController',
		['$rootScope', '$scope', '$routeParams', '$location', '$q', 'Api', callCenterFollowupController]);
}())