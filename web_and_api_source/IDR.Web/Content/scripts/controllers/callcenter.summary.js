﻿(function () {

    function callCenterSummaryController($rootScope, $scope, $routeParams, $location, $q, Api) {

        var now = moment();
        var sorted;
        var dateParams = {
            year: now.get('year'),
            month: $location.search().month || now.month() + 1,
            day: $location.search().day || now.date()
        };

        $scope.date = now.format('MM/DD/YYYY');
        $scope.sortOrder = '0';

        if ($location.search().year && $location.search().month && $location.search().day) {
            $scope.date = moment($location.search().month + '/' + $location.search().day + '/' + $location.search().year).format('MM/DD/YYYY');
        }

        var activeSummaryChart;

        function createChart(data) {
            $scope.dailyAverages = []; 
           
            if (activeSummaryChart) {
                activeSummaryChart.destroy();
            } 
            for (var i = 1; i <= 4; i++) {
                $scope.dailyAverages.push(data.DailyAverages[i]);
            } 

            // Create chart lables
            var keys = _.map($scope.dailyAverages,
                function (item, key) {
                    return 'Day ' + (key+1);
                });
            // Create the data set
            var ds = _.map($scope.dailyAverages,
                function (item) {
                    return item;
                });
            // Background colors
            var bg = _.map($scope.dailyAverages,
                function (item) {
                    if (!item || item < 4) {
                        return 'green';
                    }

                    if (item >= 4 && item < 7) {
                        return '#ffd700';
                    }

                    return 'red';
                });
            // Render
            activeSummaryChart = new Chart(document.getElementById('averages'), {
                type: 'bar',
                data: {
                    labels: keys,
                    datasets: [
                        {
                            label: "Average Pain Scores",
                            data: ds,
                            fill: false,
                            borderWidth: 1,
                            backgroundColor: bg
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                suggestedMax: 10,
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }

        function resize() {
            let summary = document.getElementById('summaryItems');
            let summaryHeader = document.getElementById('summaryItemsHeader');
            if (!summary.rows[0]) {
                return;
            }
            let cells = summary.rows[0].cells;
            for (var i = 0; i < cells.length; i++) {
                let headerItem = summaryHeader.children[i];
                if (!headerItem) {
                    continue;
                }
                headerItem.style.position = 'absolute';
                headerItem.style.bottom = '5px';
                headerItem.style.left = (cells[i].offsetLeft + 5) + 'px';
            }
        }

        function query() {

            var search = $location.search();
            if (search && search.year && search.month && search.day) {
                dateParams.year = search.year;
                dateParams.month = search.month;
                dateParams.day = search.day;
            }

            Api.callcenterSchedule.summary(dateParams,
                function (data) {
                    $scope.summary = data;


                    $scope.summaryItems = _.map(data, function (s) {
                        var items = _.map(s, function (ss, index) { return s[index] });

                        return _.find(items, function (item) { return item.IsToday });
                    });

                    var scores = _.map($scope.summaryItems, function (i) {
                        return i.PainScoreCurrent;
                    });

                    var avg = _.reduce(scores, function (memo, num) {
                        return memo + num;
                    }, 0) / (scores.length === 0 ? 1 : scores.length);

                    avg = parseFloat(avg).toFixed(1);

                    sorted = $scope.summaryItems;
                    $scope.average = avg;

                    setTimeout(() => { resize(); });

                    window.addEventListener('resize', resize);
                });

            Api.callcenterSchedule.summaryOverview(dateParams, function (data) {

                $scope.overview = data;
                createChart(data);
            });
        }

        query();

        $scope.dateChanged = function () {
            var dt = moment($scope.date);
            dateParams = {
                year: dt.get('year'),
                month: dt.month() + 1,
                day: dt.date()
            };

            $location.search(dateParams);
        };

        $scope.getClass = function (field) {
            if (field == 1 || field == '1') {
                return 'fa-exclamation-triangle  pain-level pain-yellow';// 'slightly Tol';
            }
            else if (field == 0 || field == '0') {
                return 'fa-check pain-green';// 'tol';
            }

            return 'fa-ban pain-red';// 'intol';
        };

        $scope.sort = function (order) {

            if (+order === 1) {
                $scope.summaryItems = _.sortBy($scope.summaryItems, function (s) { return s.PainScoreCurrent; }).reverse();
            } else {
                $scope.summaryItems = sorted;
            }
        }

        // Watch for query string change
        $scope.$on('$routeUpdate', function () {
            query();

            if ($location.search().year && $location.search().month && $location.search().day) {
                $scope.date = moment($location.search().month + '/' + $location.search().day + '/' + $location.search().year).format('MM/DD/YYYY');
            }
        });
    }

    angular.module('app').controller('CallCenterSummaryController',
        ['$rootScope', '$scope', '$routeParams', '$location', '$q', 'Api', callCenterSummaryController]);
}())