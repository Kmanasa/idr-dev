﻿(function (angular) {

    function dataListCtrl($rootScope, $scope, $location, $attrs, Api) {
        
        var service = $attrs.service,
            serviceFn = $attrs.serviceFn || 'query',
            deleteFn = $attrs.deleteFn || 'delete',
            defaultLimit = $attrs.limit || 10,
            defaultFilter = $attrs.filter || null,
            defaultOrder = $attrs.order || 'Id',
            collection = $attrs.collection || 'results',
            searchOptions;

        $scope.scopeId = $scope.$id;
        $scope.pageSizes = [10, 25, 50, 100];

        if (!service) {
            throw new Error('An Api service must be specified for the DataListCtrl (e.g. service="UsersService")');
        }

        //#region Creates an object with the specified paging parameters
        function getSearchOptions() {
            if (!$location.search().limit) {
                var searchOptions = {
                    offset: $scope.offset || 0,
                    limit: $scope.limit || defaultLimit,
                    filter: $scope.filter || defaultFilter || '',
					filterType: $scope.filterType || '',
                    order: $scope.orderBy || defaultOrder || '',
                    ascending: $scope.ascending
                };

                if ($scope.augmentSearch) {
                    searchOptions = angular.extend(searchOptions, $scope.augmentSearch());
                }

                return searchOptions;
            }

            return $location.search();
        }
        //#endregion

        //#region Call the Api service with search parameters
        function query() {
            $scope.loading = true;
            searchOptions = getSearchOptions();
            if ($rootScope.proxyPhysicianId) {
                searchOptions.proxyId = $rootScope.proxyPhysicianId;
            }
            
            // Invoke the query
            Api[service][serviceFn](searchOptions, function (data) {
                $scope[collection] = data.Results;
                $scope.totalRecords = data.TotalRecords;
                $scope.loading = false;
            });
        }
        //#endregion

        $rootScope.$on('proxyChanged', function () {
            query();
        });


        //#region Sets the paging parameters in the query string
        // Set the browser's URL with the search parameters
        $scope.updateResults = function () {
            var updatedCriteria = {
                offset: $scope.offset || 0,
                limit: $scope.limit || defaultLimit,
                filter: $scope.filter || defaultFilter || '',
                order: $scope.orderBy || defaultOrder || '',
                ascending: $scope.ascending
            };

            if ($scope.augmentSearch) {
                updatedCriteria = angular.extend(updatedCriteria, $scope.augmentSearch());
            }

            $location.search(updatedCriteria);
        }
        //#endregion

        $scope.refresh = query;

        // Set scope defaults
        var search = $location.search();
        $scope.limit = parseInt(search.limit) || defaultLimit;
        $scope.offset = parseInt(search.offset) || 0;
        $scope.filter = defaultFilter || '';
        $scope.loading = false;
        $scope.ascending = true;
        $scope.query = query;
        $scope.orderBy = defaultOrder;

        $scope.sortBy = function (col) {
            if (col == $scope.orderBy) {
                $scope.ascending = !$scope.ascending;
            } else {
                // Column changed; default to ascending
                $scope.ascending = true;
            }
            $scope.orderBy = col;
            $scope.$broadcast('sort-changed');
            $scope.updateResults();
        };

        $scope.setPage = function (page) {
            $scope.offset = page;
            $scope.updateResults();
        };

        // Watch for query string change
        $scope.$on('$routeUpdate', function () {
            search = $location.search();
            $scope.limit = parseInt(search.limit);
            $scope.offset = parseInt(search.offset);
            $scope.filter = search.filter || '';

            if (!$scope.limit) {
                $scope.updateResults();
            } else {
                query();
            }
        });

        $scope.delete = function (id) {
            Api[service][deleteFn]({ id: id }, function () {
                query();
            });
        };

        if (!$scope.suppressQuery) {
            query();
        }
    }

    idr.controller('DataListCtrl', ['$rootScope', '$scope', '$location', '$attrs', 'Api', dataListCtrl]);

})(angular);