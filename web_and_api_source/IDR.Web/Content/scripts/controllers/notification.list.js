﻿(function () {

    function notificationsController($scope, $routeParams, $modal, Api) {

        function getPlanNotifications(id) {
            $scope.notifications = Api.nodeLibraries.getNotifications({ id: id });
        }

        //#region remove
        //function saveNotification(notification) {
        //    // Do the adjustments
        //    notification.NodeLibraryId = $routeParams.id;

        //    if (notification.period == 0 && !notification.isNewCopy) {
        //        notification.DaysOffset *= -1;
        //        notification.HoursOffset *= -1;
        //    }

        //    if (notification.Id) {
        //        notification.$update(function () {
        //            getPlanNotifications($routeParams.id);
        //        });
        //    } else {
        //        notification.$save(function () {
        //            getPlanNotifications($routeParams.id);
        //        });
        //    }
        //}

        $scope.planId = $routeParams.id;

        getPlanNotifications($routeParams.id);

        //#region remove
        //$scope.editNotification = function(id) {
        //    var instance = $modal.open({
        //        templateUrl: 'EditNotification.html',
        //        controller: 'EditNotificationController',
        //        resolve: {
        //            notificationId: function () {
        //                return id;
        //            }
        //        }
        //    });

        //    instance.result.then(function (notification) {
        //        saveNotification(notification);
        //    });
        //};

        $scope.removeNotification = function(id) {
            Api.notifications.remove({ id: id }, function() {
                getPlanNotifications($routeParams.id);
            });
        };

        $scope.copyNotification = function (n) {

            Api.notifications.get({ id: n.Id }, function(existing) {

                existing.Id = 0;
                existing.$save(function () {
                    getPlanNotifications($routeParams.id);
                });

            });
           
        };
    }
    //#region remove
    //function editNotificationController($scope, $modalInstance, notificationId, Api) {
    //    $scope.model = {
    //    };
      
    //    $scope.hours = _.range(24);
    //    $scope.minutes = ['00', '15', '30', '45'];

    //    $scope.periods = [
    //        { key: 0, value: 'Before the procedure' },
    //        { key: 1, value: 'After the procedure' }
    //    ];
        
    //    if (notificationId) {
    //        Api.notifications.get({ id: notificationId }, function(data) {
    //            $scope.model = data;
    //            data.period = 1;

    //            if (data.DaysOffset < 0 || data.HoursOffset < 0) {
    //                data.period = 0;
    //            }

    //            data.DaysOffset = Math.abs(data.DaysOffset);

    //            $scope.model.hour = data.Hour;

    //            var minute = data.Minute;

    //            if (minute == 0) {
    //                minute = '00';
    //            } else {
    //                minute = minute.toString();
    //            }
                
    //            $scope.model.minute = minute;

    //        });
    //    } else {
    //        $scope.model = new Api.notifications();
    //        $scope.model.options = [{}];
    //    }

    //    $scope.save = function () {
    //        var time = moment(
    //            ).hour($scope.model.hour)
    //            .minute($scope.model.minute)
    //            .second(0)
    //            .millisecond(0)
    //            .utc();

    //        $scope.model.TimeToSend = time;
    //        $modalInstance.close($scope.model);
    //    };

    //    $scope.cancel = function () {
    //        $modalInstance.dismiss('cancel');
    //    };

    //    $scope.addOption = function() {
    //        $scope.model.options.push({});
    //    };

    //    $scope.removeOption = function(option) {
    //        $scope.model.options = _.without($scope.model.options, option);
    //    };
    //}

    angular.module('app').controller('PlanNotificationsController',
        ['$scope', '$routeParams', '$modal', 'Api', notificationsController]);

    //angular.module('app').controller('EditNotificationController',
    //    ['$scope', '$modalInstance', 'notificationId',  'Api', editNotificationController]);
}())