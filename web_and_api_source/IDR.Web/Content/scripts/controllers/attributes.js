﻿(function () {
    angular.module('app').controller('AttributesController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
            serviceName: 'attributes'
			, controllerName: 'AttributesController'
            , saveFuncName: 'save'
			, idName: 'attributeId'
            , listName: 'Attributes'
			, entityName: 'attribute'
            , listRoute: '/cms/attributes'
        })]);
})();