﻿/// <reference path="../shared/templates.js" />

(function () {
	idr.controller('MetaController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
        	serviceName: 'nodeTypes'
			, controllerName: 'MetaController'
            , listName: 'Types'
            , listRoute: '/#/meta'
            , init: function ($scope, $routeParams, $location, Api) {
                Api.nodeTypes.query(function (data) {
                    $scope._types = data;
                });
            }
            , onExisting: function ($scope, $routeParams, $location, Api) {
                $scope.Entity.AllowedTypes.select(function(type){
                    var mapped = $scope._types.where(function (item) { return item.Id == type.AllowedTypeId; })[0];
                    mapped.Allowed = true;
                });
            }
            , after: function ($scope) {
                $scope.uiKeys = ['CONTENT', 'VIDEOCONTENT', 'CONTENT', 'VIDEOCONTENT', 'SELECT', 'TEXTBOX'];
            }
        })]);
})();