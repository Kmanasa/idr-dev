﻿/// <reference path="../shared/templates.js" />

(function () {

    var afterFn = function($scope, $routeParams, $location, $sce, Api) {
        $scope.saveUserRole = function(role, user) {
            window.addMessage({ State: 0, Message: 'Modifying user role for ' + user.UserName + '...' }, 'ROLE' + user.UserName);
            Api.users.setUserRole(role,function(data) {
                handleResponse($scope, data, 'ROLE' + user.UserName);
            });
        };

        $scope.activate = function(user) {
            window.addMessage({ State: 0, Message: (user.Active ? 'Activating' : 'Deactivating') + ' user...' },
                'ACTIVE' + user.UserName);
            Api.users.update(user,function(data) {
                handleResponse($scope, data, 'ACTIVE' + user.UserName);
            });
        };

        $scope.resetPassword = function(user) {
            user.id = user.Id;
            window.addMessage({ State: 0, Message: 'Sending reset password instructions to ' + user.UserName },
                'RESET' + user.UserName);
            Api.users.resetPassword(user,
                function(data) {
                    window.addMessage({ State: 1 }, 'RESET' + user.UserName);

                });
        };

        var self = $scope;
        $scope.deleteUser = function (user) {
            
            window.addMessage({ State: 0, Message: 'Deleting user... '  },
               'DELETE' + user.UserName);
            Api.users.delete(user,
                function (data) {
                  
                    window.addMessage({ State: 1 }, 'DELETE' + user.UserName);
                    Api.users.query(function(data) {
                        self.Users = data;
                    });

                });
        };
    };

	idr.controller('AdminUsersController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
        	controllerName: 'AdminUsersController'
        	, serviceName: 'users'
            , listName: 'Users'
            , listRoute: '/#/users/list'
			, isPaging: true
			, serviceCalls: [ { serviceName: 'roles', scopeName: 'Roles' } ]
			, after: afterFn
        })]);


    function createUser($scope, $location, Api) {
        $scope.user = new Api.users();
        $scope.roles = Api.roles.getAdmin();
        $scope.busy = false;

        $scope.save = function () {
            
            var assigned = _.filter($scope.roles, function(r) {
                return r.assigned;
            });

            $scope.user.roles = _.map(assigned, function(r) {
                return r.Name;
            });

            $scope.busy = true;
            var promise = $scope.user.$save();

            promise.then(function () {
                    $location.path('/admin/users');
                    $scope.busy = false;
                },
                function(result) {
                    $scope.busy = false;
                    if (result && result.data && result.data.ExceptionMessage) {
                        alert(result.data.ExceptionMessage);
                    }
                });
        };
    }

    idr.controller('AdminUserCreateController', ['$scope', '$location', 'Api', createUser]);
})();
