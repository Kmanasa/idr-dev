﻿/// <reference path="../shared/templates.js" />

(function () {
	angular.module('app').controller('DisciplinesController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
        	serviceName: 'disciplines'
			, controllerName: 'DisciplinesController'
            , saveFuncName: 'save'
			, idName: 'disciplineId'
            , listName: 'Disciplines'
			, entityName: 'discipline'
            , listRoute: '/cms/disciplines'
        })]);
})();