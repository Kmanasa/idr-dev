﻿(function () {


    function callCenterCallDetailController($rootScope, $scope, $routeParams, $location,
        $window, $q, $modal, Api) {

        $scope.posting = false;
        $scope.newComment = '';

        $scope.callType = 1;
        

        $scope.actions = [
            { id: 1, name: 'Call Complete' },
            { id: 2, name: 'Call Complete - Call Back Today' },
            { id: 3, name: 'Patient Discharged' },
            { id: 4, name: 'Patient Discharged - Call Back Tomorrow' }
        ];

        Api.auth.current(function (data) {

            $scope.isMobileUser = _.any(data.Roles,
                function (r) {
                    return r.RoleName === 'Call Site Mobile' && r.IsInRole;
                });

            $scope.isCsa = $scope.isMobileUser ||
                _.any(data.Roles,
                    function (r) {
                        return r.RoleName === 'Call Site Admin' && r.IsInRole;
                    });

            $scope.preventChanges = $scope.isMobileUser || $scope.isCsa;

            Api.callcenterSchedule.get({ id: $routeParams.id },
                function (data) {
                    $scope.patient = data;
                    //console.log($scope.patient);
                    $scope.callType = $scope.patient.CommentTypes[1].Index 
                });
        });

        $scope.evaluateCmt = function (comment) {
            $scope.newComment = comment;
        };

        $scope.setCallType = function (val) {
            $scope.callType = val;
        };

        $scope.postComment = function () {
            $scope.posting = true;
            var typeId = _.find($scope.patient.CommentTypes, function (ct) {
                return ct.Index == +$scope.callType;
            }).Id;

            var newcall = {
                Id: $scope.patient.Id,
                CallCenterProcedureId: $scope.patient.CallCenterProcedureId,
                CallCenterScheduledCallId: $routeParams.id,
                Comment: $scope.newComment,
                CallTypeIndex: +$scope.callType,
                CallCenterCommentTypeId: typeId
            };
            Api.callcenterComments.save(newcall, function (newcmt) {
                $scope.posting = false;
                $scope.patient.Comments.splice(0, 0, newcmt);
                $scope.newComment = '';
                $scope.cmt = '';
                $scope.callType = null;
            },
                function () {
                    $scope.posting = false;
                });
        };

        $scope.getTitle = function (c) {
            if (c.hasTitle) {
                return c.setTitle;
            }

            var title = _.find($scope.patient.CommentTypes,
                function (t) {
                    //if (c.CallCenterCommentTypeId) {
                    return t.Id == c.CallCenterCommentTypeId;
                    //}
                    //return t.Id == c.CallTypeIndex;
                }).Title;
            c.hasTitle = true;
            c.setTitle = title;
            return title;
        };

        $scope.setComment = function (item, model, label, event) {

            $scope.cmt = item;
            $scope.newComment = item;
            $scope.quickComment = null;
        };

        $scope.showAssessment = function () {
            Api.callcenterSchedule.checkForURL({ practiceId: $scope.patient.CompanyId},function (response) {
                $scope.checkForQuestion = response.isThereAddlURL;
            

            var instance = $modal.open({
                templateUrl: 'Assessment.html',
                controller: 'AssessmentController',
                size: 'lg',
                resolve: {
                    callId: function () {
                        return $routeParams.id;
                    },
                    patientId: function () {
                        return $scope.patient.Id;
                    },
                    procedureId: function () {
                        return $scope.patient.CallCenterProcedureId;
                    },
                    commentTypes: function () {
                        return $scope.patient.CommentTypes;
                    },
                    isForm: function () {
                        return $scope.checkForQuestion;
                    }
                }
            });
         
            instance.result.then(function () { //model
                if ($scope.checkForQuestion ) {
                    var instance = $modal.open({
                        templateUrl: 'form.html',
                        controller: 'formController',
                        size: 'lg',
                        resolve: {
                            procedureId: function () {
                                return $scope.patient.CallCenterProcedureId;
                            },
                            patient: function () {
                                return $scope.patient;
                            },
                        }
                    })
                } 
                if (window.history && $window.history.back) {
                    window.history.back();
                } else {
                    $location.path('/callcenter/followup');
                }
                });
            })
        };

        $scope.showCallClosePrompt = function (title, showSatisfaction, showCallbackTime, callback) {
            var instance = $modal.open({
                templateUrl: 'CallClosePrompt.html',
                controller: 'CallClosePromptController',
                size: 'sm',
                resolve: {
                    title: function () { return title },
                    showSatisfaction: function () { return showSatisfaction },
                    showCallbackTime: function () { return showCallbackTime }
                }
            });

            instance.result.then(callback);
        };

        $scope.getClass = function (field) {
            if (field == 1 || field == '1') {
                return 'fa-exclamation-triangle  pain-level pain-yellow';// 'slightly Tol';
            }
            else if (field == 0 || field == '0') {
                return 'fa-check pain-green';// 'tol';
            }

            return 'fa-ban pain-red';// 'intol';
        };

        $scope.cancelCall = function () {
            Api.callcenterSchedule.cancel({ id: $routeParams.id },
                function (data) {
                    //$scope.patient = data;
                    $scope.goBack();
                });
        };

        $scope.callComplete = function () {

            Api.callcenterSchedule.callTomorrow({ id: $routeParams.id }, function (data) {
                $scope.data = data.IsThereACallTomorrow;
               
                if ($scope.data === false) {
                    var instance = $modal.open({
                        templateUrl: 'CallComplete.html',
                        controller: 'CallCompleteController',
                        size: 'medium',
                        resolve: {
                            callId: function () {
                                return $routeParams.id;
                            },
                        }

                    })
                }
                else {
                    Api.callcenterSchedule.callTomorrow({ id: $routeParams.id }, $scope.goBack());
                }
            })
        } 

        $scope.selectAction = function (actionId) {
            var now = moment();
            switch (actionId) {
                case 1: $scope.callComplete(); break;
                case 2: $scope.showCallClosePrompt('Call Complete', false, true, function (model) { Api.callcenterSchedule.callback({ id: $routeParams.id }, { Id: $routeParams.id, Hour: moment(model.time).hour(), Minute: moment(model.time).minute() }) }); break;
                case 3: $scope.showCallClosePrompt('Patient Discharged', true, false, function (model) { Api.callcenterSchedule.discharge({ id: $routeParams.id }, { Id: $routeParams.id, Satisfaction: model.satisfaction, FollowUp: false }) }); break;
                case 4: $scope.showCallClosePrompt('Patient Discharged', true, true, function (model) { Api.callcenterSchedule.discharge({ id: $routeParams.id}, { Id: $routeParams.id, Satisfaction: model.satisfaction, FollowUp: true, FollowUpHour: moment(model.time).hour(), FollowUpMinute: moment(model.time).minute() }) }); break;
            }
        };

        $scope.goBack = function () {
            if (window.history && $window.history.back) {
                window.history.back();
            } else {
                $location.path('/callcenter/followup');
            }
        }

        $rootScope.$on('commentPosted', function (event, newcmt) {

            $scope.patient.Comments.splice(0, 0, newcmt);
            window.handleResponse($scope,
                {
                    State: 2,
                    Messages: [{
                        Message: "Your comment has been posted.",
                        max: 3,
                        'class': 'alert-success'
                    }]
                },
                'CALLS');
        });
    }

    function assessmentController($rootScope, $scope, $modalInstance, callId, patientId, procedureId, commentTypes, isForm, Api) {
        // round up to the nearest hour
        $scope.isForm = isForm;
        Api.callcenterSchedule.get({ id: patientId },
            function (data) {
                $scope.patient = data;
            });
        var m = moment();
        var roundUp = m.minute() || m.second() || m.millisecond() ? m.add(1, 'hour').startOf('hour') : m.startOf('hour');

        $scope.model = {
            PainScoreCurrent: '0',
            Perscription24Hour: '0',
            Otc24Hour: '0',
            Satisfaction: 'true',
            TolerableToilet: 1,
            TolerableConsumption: 1,
            TolerableSleeping: 1,
            TolerableWalking: 1,
            TolerableDressing: 1,
            TolerableStairs: 1,
            TolerablePtOt: 0,
            TolerableReturnToWork: 0,
            CallbackTime: roundUp,
            commentTypes: commentTypes,
            EffectiveUse: false,
            DoseButton: '0'
        };
        $scope.newComment = '';

        $scope.actions = [
            { id: 1, name: 'Call Complete' },
            { id: 2, name: 'Call Back Today' },
            { id: 3, name: 'Discharge Patient' },
            { id: 4, name: 'Discharge Patient - Call Back Tomorrow' },
            { id: 5, name: 'Close Call - New Time Tomorrow' } 
        ];
        $scope.action = '1';

        $scope.evaluateCmt = function (comment) {
            $scope.newComment = comment;
        };

        $scope.postComment = function () {
            $scope.posting = true;

            var newcall = {
                Id: patientId,
                CallCenterProcedureId: procedureId,
                CallCenterScheduledCallId: callId,
                Comment: $scope.newComment,
                CallTypeIndex: +$scope.model.callType,
                CallCenterCommentTypeId: +$scope.model.callType
            };

            var typeId = _.find($scope.model.commentTypes, function (ct) {
                return ct.Index == +$scope.model.callType;
            }).Id;


            newcall.CallCenterCommentTypeId = typeId;

            Api.callcenterComments.save(newcall,
                function (newcmt) {
                    $scope.posting = false;
                    $scope.newComment = '';
                    $scope.cmt = '';
                    $scope.callType = null;
                    newcmt.CallCenterCommentTypeId = typeId;
                    newcmt.Title = _.find($scope.model.commentTypes, function (c) { return c.Index == +$scope.model.callType }).Title;
                    $rootScope.$broadcast('commentPosted', newcmt);
                },
                function () {
                    $scope.posting = false;
                });
        };

        $scope.callBack = function () {

            var time = moment($scope.model.CallbackTime);

            Api.callcenterSchedule.callback({ id: callId, hour: time.hour(), minute: time.minute() }, function () {
                $modalInstance.close();
            });
        };

        $scope.callTomorrow = function () {
            Api.callcenterSchedule.callTomorrow({ id: callId }, function () {
                $modalInstance.close();
            });
        };

        $scope.discharge = function (followUp) {

            var time = moment($scope.model.CallbackTime);

            var params = {
                id: callId,
                satisfaction: $scope.model.Satisfaction,
                followUp: followUp
            };

            if (followUp) {
                params.FollowUpHour = time.hour();
                params.FollowUpMinute = time.minute();
            }

            Api.callcenterSchedule.discharge(params, function () {
                $modalInstance.close();
            });
        };

        function setTriState(value) {
            if (value === "1" || value === 1) {
                return 1;
            } else if (value === "0" || value === 0) {
                return 0;
            }

            return 2;
        }
        $scope.newTimeTomorrow = function () {
          
            var time = moment($scope.model.CallbackTime);
            Api.callcenterSchedule.newTimeTomorrow({ id: callId, hour: time.hour(), minute: time.minute() }, function () {
                $modalInstance.close();
            });
        } 

        $scope.save = function (action) {

            $scope.model.Id = callId;
            $scope.model.TolerableToilet = setTriState($scope.model.TolerableToilet);
            $scope.model.TolerableConsumption = setTriState($scope.model.TolerableConsumption);
            $scope.model.TolerableSleeping = setTriState($scope.model.TolerableSleeping);
            $scope.model.TolerableWalking = setTriState($scope.model.TolerableWalking);
            $scope.model.TolerableDressing = setTriState($scope.model.TolerableDressing);
            $scope.model.TolerableStairs = setTriState($scope.model.TolerableStairs);
            $scope.model.TolerablePtOt = setTriState($scope.model.TolerablePtOt);
            $scope.model.TolerableReturnToWork = setTriState($scope.model.TolerableReturnToWork);


            Api.callcenterSchedule.save($scope.model, function () {
                switch (action) {
                    case 2: $scope.callBack(); break;
                    case 3: $scope.discharge(false); break;
                    case 4: $scope.discharge(true); break;
                    case 5: $scope.newTimeTomorrow(); break; 
                   default: $modalInstance.close(); break;
                }
            });
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    function callClosePromptController($rootScope, $scope, $modalInstance, title, showSatisfaction, showCallbackTime) {
        // round up to the nearest hour
        var m = moment();
        var roundUp = m.minute() || m.second() || m.millisecond() ? m.add(1, 'hour').startOf('hour') : m.startOf('hour');

        $scope.title = title;
        $scope.showSatisfaction = showSatisfaction;
        $scope.showCallbackTime = showCallbackTime;

        $scope.model = {
            satisfaction: '',
            time: roundUp
        };

        $scope.done = function () {
            $modalInstance.close($scope.model);
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    function CallCompleteController($rootScope, $scope, $window, $modalInstance, callId, Api) {

        $scope.model = {
            satisfaction: '',
        } 

        $scope.goBack = function () {
            if (window.history && $window.history.back) {
                window.history.back();
            } else {
                $location.path('/callcenter/followup');
            }
        }
        $scope.newCall = function () {
            Api.callcenterSchedule.scheduleCall({ id: callId }, $modalInstance.dismiss('cancel'),$scope.goBack());
        }

        $scope.endService = function () {
            $scope.showSatisfaction = true;
        }

        $scope.submit = function (satisfaction) {
            $scope.satisfaction = satisfaction; 
       
        }

        $scope.close = function () {
            Api.callcenterSchedule.discharge({ id: callId }, { Id: callId, Satisfaction: $scope.satisfaction, FollowUp: false }, $scope.goBack());
            $modalInstance.dismiss('cancel');
        };
    }

    function formController($rootScope, $scope, $window, $modalInstance, Api, procedureId, patient) {
       
        Api.callcenterSchedule.getAdditionalQuestion({ practiceId: patient.CompanyId }, function (data) {
          
            $scope.URL = data.Addl_URL+'?' + procedureId + '=CallCenterProcedureID&' + data.InfuFacilityId + '=InfuFacilityID';         
            var frame = $('#frame');
            frame.empty();
            var iframe = $('<iframe frameborder="0" style="width: 100%; height: 800px; border: solid 1px #ccc;" src="' + $scope.URL  + '" ></iframe>');
            frame.append(iframe); 

        });
        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
    } 

    angular.module('app').controller('CallCenterCallDetailController',
        ['$rootScope', '$scope', '$routeParams', '$location', '$window', '$q', '$modal', 'Api', callCenterCallDetailController]);

    angular.module('app').controller('AssessmentController',
        ['$rootScope', '$scope', '$modalInstance', 'callId', 'patientId', 'procedureId', 'commentTypes','isForm','Api', assessmentController]);

    angular.module('app').controller('CallClosePromptController',
        ['$rootScope', '$scope', '$modalInstance', 'title', 'showSatisfaction', 'showCallbackTime', callClosePromptController]);

    angular.module('app').controller('CallCompleteController',
        ['$rootScope', '$scope', '$window', '$modalInstance', 'callId', 'Api', CallCompleteController]);  

    angular.module('app').controller('formController',
        ['$rootScope', '$scope', '$window', '$modalInstance', 'Api', 'procedureId', 'patient', formController]);   

}());