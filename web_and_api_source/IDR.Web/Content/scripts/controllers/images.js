﻿(function () {
    angular.module('app').controller('ImagesController', ['$scope', '$routeParams', '$location', '$modalInstance', 'Api', function ($scope, $routeParams, $location, $modalInstance, Api) {

        $scope.paging = { Filter: '', Limit: 10 };
        $scope.image = null;

        $scope.currentImage = {
            EntityName: 'ImageDto',
            EntityId: 0,
            ImageFormType: 'BASIC',
            ImageType: 'Basic',
            ImageTitle: 'Library Image',
            Alias: '',
            Id: 0
        };

        $scope.ok = function () {
            $modalInstance.close($scope.image);
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

        $scope.imageUploaded = function (image) {

            $scope.currentImage = image;
        }

        $scope.updateAlias = function () {
            Api.imageFiles.updateAlias({ id: $scope.currentImage.Id },
		    {
		        id: $scope.currentImage.Id,
		        alias: $scope.currentImage.Alias
		    });
        }

        $scope.loadList = function () {
            $scope.image = null;
            Api.imageFiles.search($scope.paging).$promise.then(function (data) {

                _.each(data, function (img) {
                    img.Url = location.origin + img.Url;
                });

                $scope.images = data.Results;
            });
        }

        $scope.setImage = function (image) {
            _.each($scope.images, function (img) {
                img.selected = false;
            });

            image.selected = true;
            $scope.image = image;
        }

        // Eagerly load the list
        $scope.loadList();
    }]);
})();