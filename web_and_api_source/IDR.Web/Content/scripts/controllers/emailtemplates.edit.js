﻿(function () {

    function emailEdit($scope, $routeParams, Api) {
        $scope.template = {};
      
        Api.emailTemplates.get({ id: $routeParams.id }).$promise.then(function (data) {
            $scope.template = data;
        });
        
        $scope.save = function () {
            Api.emailTemplates.update($scope.template);
        };

        $scope.editorOptions = {
            onDeactivate: function () {
                $scope.save();
            },
            inline: false,
            menubar: 'edit insert format table tools',
            plugins: 'advlist table image link lists charmap colorpicker hr paste textcolor wordcount code',
            skin: 'lightgray',
            theme: 'modern',
            fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 36px 38px 40px",
            toolbar: 'image undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | fontselect | fontsizeselect | idrplaceholder',
            height: '400px'

        };
    }

    angular.module('app').controller('EmailTemplatesEditController', ['$scope', '$routeParams', 'Api',
        emailEdit]);
})();