﻿(function () {

    function notificationEditController($scope, $routeParams, Api) {
        $scope.model = {
            ResponseType: 1,
            Repetitions: 1,
            RepetitionMinutes: 30,
            Repeat: true
        };

        $scope.planId = $routeParams.id;
        $scope.notificationId = parseInt($routeParams.notificationId);

        $scope.hours = _.range(24);
        $scope.minutes = ['00', '15', '30', '45'];
        $scope.responses = [
            { Id: 1, Name: 'Buttons' },
            { Id: 2, Name: 'Single Selection' },
            { Id: 3, Name: 'Multiple Selection' }
        ];

        $scope.periods = [
            { key: 0, value: 'Before the procedure' },
            { key: 1, value: 'After the procedure' }
        ];

        $scope.setModel = function (data) {
            data.period = data.Period;

            $scope.model = data;

            data.DaysOffset = Math.abs(data.DaysOffset);

            $scope.model.hour = data.Hour;

            var minute = data.Minute;

            if (minute == 0) {
                minute = '00';
            } else {
                minute = minute.toString();
            }

            $scope.model.minute = minute;
            if (!$scope.model.options) {
                $scope.model.options = [{ Text: 'Response Message' }];
            }

            $scope.model.ResponseType = data.ResponseType || 1;
            $scope.model.Repetitions = data.Repetitions || 1;
            $scope.model.RepetitionMinutes = data.RepetitionMinutes || 30;
            $scope.model.Repeat = data.Repeat;
        };

        if ($scope.notificationId) {
            Api.notifications.get({ id: $scope.notificationId },function(data) {
                $scope.setModel(data);
            });
        } else {
            $scope.model = new Api.notifications();
            $scope.model.ResponseType = 1;
            $scope.model.Repetitions = 1;
            $scope.model.RepetitionMinutes = 30;
            $scope.model.Repeat = true;
            $scope.model.hour = 12;
            $scope.model.minute = '00';
            $scope.model.period = 0;

        };
        $scope.model.options = [{ Text: 'Response Message' }];


        $scope.save = function () {
            var time = moment()
                .hour($scope.model.hour)
                .minute($scope.model.minute)
                .second(0)
                .millisecond(0)
                .utc();

            $scope.model.TimeToSend = time;

            // Do the adjustments
            var notification = $scope.model;
            notification.NodeLibraryId = $scope.planId;

            if (notification.period == 0) { //&& !notification.isNewCopy
                notification.DaysOffset *= -1;
                notification.HoursOffset *= -1;
            }

            if (notification.Id) {
                notification.$update(function (data) {
                    $scope.setModel(data);
                });
            } else {
                notification.$save(function(data) {
                    $scope.setModel(data);
                });
            }
        };

        $scope.addOption = function () {
            $scope.model.NotificationResponses.push({ Text: 'Response Message' });
            $scope.save();
        };

        $scope.removeOption = function(option) {
            $scope.model.NotificationResponses = _.without($scope.model.NotificationResponses, option);
            $scope.save();
        };
    }

    angular.module('app')
        .controller('NotificationEditController',
            ['$scope', '$routeParams', 'Api', notificationEditController]);
}());