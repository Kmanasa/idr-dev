﻿(function () {

    function physicianPatientsListController($rootScope, $scope, $routeParams, $location, $http, $q, $modal, $controller, $attrs, Api) {

        // Use DataListCtrl as the base controller
        angular.extend(this, $controller('DataListCtrl', {
            $rootScope:$rootScope,
            $scope: $scope,
            $location: $location,
            $attrs: $attrs
        }));

        function associatePatient(patient) {
            patient.proxyId = $rootScope.proxyPhysicianId;

            $http({
                method: 'POST',
                url: '/api/physician/patients/',
                data: patient
            }).then(function(args) {
                $scope.query();

                var code = args.data.Procedure.AccessCode;
                if (patient.print) {
                    var url = '/patients/overview/' + code;
                    window.open(url, '_blank');
                }
            });
        }

        $scope.edit = function (patientId) {
            $location.url('/physician/patients/' + patientId + '/edit');
        }

        $scope.addPatient = function () {
            var instance = $modal.open({
                templateUrl: 'CraetePatients.html',
                controller: 'CraetePatientController'
            });
            
            instance.result.then(function (patient) {
                
                associatePatient(patient);

            });
        };

        $scope.removePatient = function (pat) {
            Api.physician.removePatient({ id: pat.Id }, function () {
                //$scope.search();
                $scope.query();
            });
        };
    }

    function createPatientController($rootScope, $scope, $modalInstance, Api) {

        $scope.model = {
            term: '',
            facilities: Api.physician.getFacilities({ proxyId: $rootScope.proxyPhysicianId }),
            startTime: new Date()
        };

        Api.auth.current(function (data) {
            $scope.model.isDemo = data.IsDemoPhysician;
        });

        $scope.showDetails = false;

        Api.physician.getAllPlans({ id: $rootScope.proxyPhysicianId }, function (data) {
            $scope.model.libraries = data;
        });


        $scope.showAllDetails = function (planId) {
            $scope.model.library = _.find($scope.model.libraries, function (l) {
                return l.Id == planId;
            });
            $scope.showDetails = true;
        };

        $scope.searchMrn = function() {
          
            Api.physician.getPatientForMrn({
                mrn: $scope.model.MedicalRecordNumber,
                proxyId: $rootScope.proxyPhysicianId || 0
            }, function (data) {
                
                if (data && data.Id) {
                    $scope.model.MedicalRecordNumber = data.MedicalRecordNumber;
                    $scope.model.firstName = data.FirstName;
                    $scope.model.MiddleInitial = data.MiddleInitial;
                    $scope.model.lastName = data.LastName;
                    $scope.model.email = data.Email;
                    $scope.model.age = data.Age;
                    $scope.model.phone = data.Phone;
                }
            });
        };

        $scope.save = function () {
            $scope.model.procedure = {
                procedureId: $scope.model.library.Id,
                cptCode: $scope.model.term,
                facilityId: $scope.model.facilityId,
                startDate: $scope.model.startDate,
                startTime: $scope.model.startTime,
                accessCode: $scope.model.accessCode,
                sendSms: $scope.model.SendSms,
                sendEmail: $scope.model.SendEmail
            };

            $modalInstance.close($scope.model);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    angular.module('app').controller('PhysicianPatientsListController', ['$rootScope', '$scope', '$routeParams', '$location', '$http', '$q', '$modal', '$controller', '$attrs', 'Api', physicianPatientsListController]);
    angular.module('app').controller('CraetePatientController', ['$rootScope', '$scope', '$modalInstance', 'Api', createPatientController]);
}())