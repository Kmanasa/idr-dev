﻿/// <reference path="../shared/templates.js" />

(function () {
	var scope;
	function assignParents(node, parent) {
		node.Nodes.select(function (item) {
			if (item._touched) return;
			if (item.IsSupplemented) {
				var lib = scope.Libraries.where(function (i) {
					return i.Id == item.SupplementalLibraryId;
				})[0];

				item._supplementalLib = lib;
			}
			item.parent = node;
			item._touched = true;
			assignParents(item, node);
		});
		return node;
	}


	function loadLibs($scope, $routeParams, $location, Api, data) {
		scope = $scope;
		data.select(assignParents);
	}



	angular.module('app').controller('TemplatesController', ['$scope', '$routeParams', '$location', '$sce', 'Api'
        , idrTemplates.template_listEditController({
        	serviceName: 'templates'
			, controllerName: 'TemplatesController'
            , saveFuncName: 'saveTemplate'
            , listName: 'Templates'
			, entityName: 'template'
            , listRoute: '/cms/recoverytemplates'
			, onExisting: function ($scope, $routeParams, $location, Api, data) {
				$scope.template = data;
				var lib = $scope.Libraries.where(function (item) {
					return item.Id == data.NodeLibrary.Id;
				})
				$scope.node = lib[0];
			}
			, serviceCalls: [{ serviceName: 'nodes', scopeName: 'Libraries', handler: loadLibs }]

        })]);
})();