﻿(function () {

    function emailList($scope, Api) {

        Api.emailTemplates.query(function(data) {
            $scope.templates = data;
        });
       
    }

    angular.module('app').controller('EmailTemplatesListController', ['$scope', 'Api', emailList]);
})();