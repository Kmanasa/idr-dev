﻿(function () {

    function procedureDashboardController($scope, $routeParams, $sce, Api) {

        var id = $routeParams.id;

        function setGauge() {
            $('#gauge').remove();
            var metricsElm = $('#metrics');
            var gaugeElm = $('<canvas id="gauge"></canvas>');
            gaugeElm.width(metricsElm.width());
            $('#metrics').prepend(gaugeElm);

            $scope.sent = 0;
            $scope.awaiting = 0;
            $scope.completed = 0;
            $scope.percent = 0;

            if (!$scope || !$scope.data || !$scope.data.DashboardNotifications) {
                return;
            }

            var sent = _.filter($scope.data.DashboardNotifications, function (n) {
                return n.Sent;
            }).length;
            var completed = _.filter($scope.data.DashboardNotifications, function (n) {
                return n.Sent && n.Response && n.Response.length;
            }).length;

            var percent = parseInt(Math.floor((completed / sent) * 100)) || 0;

            var gauge = new Gauge(document.getElementById('gauge'));
            var opts = {
                angle: 0, // The span of the gauge arc
                lineWidth: 0.25, // The line thickness
                radiusScale: 1, // Relative radius
                pointer: {
                    length: 0.6, // // Relative to gauge radius
                    strokeWidth: 0.025, // The thickness
                    color: '#000000' // Fill color
                },
                staticZones: [
                    { strokeStyle: "#F03E3E", min: 0, max: 15 },  // Red
                    { strokeStyle: "#FFDD00", min: 16, max: 45 }, // Yellow
                    { strokeStyle: "#30B32D", min: 46, max: 100 } // Green
                ],

                highDpiSupport: true
            };
            gauge.setOptions(opts);
            gauge.maxValue = 100;
            gauge.setMinValue(0);
            gauge.set(percent);
            $scope.sent = sent;
            $scope.awaiting = sent - completed;
            $scope.completed = completed;
            $scope.percent = Math.max(1, percent); // Never display lower than 1 since the chart is 0 based
        }

        Api.procedures.getDashboard({ id: id }, function (data) {

            data.notes = [];

            _.each(data.DashboardNotifications, function (d) {
                if (!d.Sent) {
                    return;
                }
                var date = moment(d.SendTime).local().format('MM/DD/YYYY hh:mm a');
                var responseDate = moment(d.DateResponded).local();

                var response = '';

                if (responseDate && responseDate.year() === 1) {
                    response = '**No Response**';
                } else {
                    response = responseDate.format('MM/DD/YYYY hh:mm a') + ' Response: ' + d.Response;
                }
               
                var msg = date + ' Message: ' + d.Message + '<br/><br/>' + response;
                if (!d.Response && d.Attempts > 0) {
                    msg += ' after ' + d.Attempts + ' attempt(s)';
                } else {
                    msg += '<br/>';
                }

                var note = $sce.trustAsHtml(msg);
                data.notes.push(note);
            });

            $scope.data = data;
            $scope.data.start = moment(data.StartDate).local().format('MM/DD/YYYY hh:mm a');
            setGauge();
        });

        $scope.copy = function () {
            var targetId = "_hiddenCopyText_";
            var target;
            var content = '';

            var procHeader = '';

            _.each($('.procHeader'), function (n) {
                procHeader += (n.textContent || '') + '\n';
            });

            _.each($('.note'), function (n) {
                content += (n.textContent || '') + '\n';
            });

            target = document.getElementById(targetId);
            if (!target) {
                target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = procHeader + '\n\n' + content + '\n\n***Disclaimer***\n\nThis is a Visit Summary extracted from the IDR Mobile chart.\nIt is not a copy of an existing EMR  progress note.';
            target.focus();
            target.setSelectionRange(0, target.value.length);

            try {
                document.execCommand("copy");
            } catch (e) {
            }

            target.textContent = "";
        }

        jQuery(window).on('resize', setGauge);
    }

    angular.module('app').controller('ProcedureDashboardController',
    ['$scope', '$routeParams', '$sce', 'Api', procedureDashboardController]);
}())