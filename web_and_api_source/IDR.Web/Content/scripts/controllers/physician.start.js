﻿(function() {
	
    function physicianStartController($rootScope, $scope, $routeParams, $location, $q, $modal, Api) {
        
        $scope.pid = $rootScope.proxyPhysicianId;

        $scope.selectPhysician = function (p) {
  
            $rootScope.proxyChanged(p, true);
        }

        $rootScope.$on('proxyChanged', function() {
            $scope.pid = $rootScope.proxyPhysicianId;
        });
	}


	angular.module('app').controller('PhysicianStartController', ['$rootScope', '$scope', physicianStartController]);
}())