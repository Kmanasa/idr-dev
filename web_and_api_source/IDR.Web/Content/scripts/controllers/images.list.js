﻿(function() {
	
    function imagesListController($rootScope, $scope, $routeParams, $location, $controller, $attrs, Api) {

        // Use DataListCtrl as the base controller
        angular.extend(this, $controller('DataListCtrl', {
            $rootScope:$rootScope,
            $scope: $scope,
            $location: $location,
            $attrs: $attrs
        }));
	}

    angular.module('app').controller('ImagesListController',
         ['$rootScope', '$scope', '$routeParams', '$location', '$controller', '$attrs', 'Api', imagesListController]);
}())