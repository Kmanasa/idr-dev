﻿(function () {

    function adminCompanyListController($rootScope, $scope, $routeParams, $location, $controller, $attrs, Api) {

        // Use DataListCtrl as the base controller
        angular.extend(this, $controller('DataListCtrl', {
            $rootScope:$rootScope,
            $scope: $scope,
            $location: $location,
            $attrs: $attrs
        }));

        $scope.removeCompany = function (company) {
            Api.companies.remove({ id: company.Id }, $scope.refresh);
        };

        $scope.copyCompany = function (company) {
            Api.copyCompany.copy({ id: company.Id }, $scope.refresh);
        };
    }

    angular.module('app').controller('AdminCompanyListController',
        ['$rootScope', '$scope', '$routeParams', '$location', '$controller', '$attrs', 'Api', adminCompanyListController]);
}())