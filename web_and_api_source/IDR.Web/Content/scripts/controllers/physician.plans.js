﻿(function() {
	
    function physicianPlansListController($rootScope, $scope, $routeParams, $location, $q, $modal, Api) {

        function getPhysicianLibraries() {
            if (!$rootScope.proxyPhysicianId) {
                return;
            }

	        Api.physician.getPlans({ proxyId:$rootScope.proxyPhysicianId },function(data) {
	            $scope.libraries = data.Results;
	        });
	    }

	    function associateLibrary(library) {

	        Api.physician.addPlan({
	            id: library.Id, 
	            proxyId: $rootScope.proxyPhysicianId
	        }, library, getPhysicianLibraries);
	    }

	    $rootScope.$on('proxyChanged', function () {
	        getPhysicianLibraries();
	    });

	    // Get plans on page load
	    getPhysicianLibraries();

	    $scope.addPlan = function() {
	        var instance = $modal.open({
	            templateUrl: 'SearchLibraries.html',
	            controller: 'SearchLibrariesController'
	        });

	        instance.result.then(function (library) {
	            window.addMessage({ State: 1, Message: 'Please wait.  Importing a new plan may take several minutes.' }, 'ACTIVE');

	            associateLibrary(library);
	        });
	    };

	    $scope.getCptCodes = function (lib) {
	       
	        var codes = _.map(lib.CptCodes, function(cpt) {
	            return cpt.Code;
	        });

	        return codes.join(', ');
	    };

	    $scope.removeType = function (lib) {

	        Api.nodeLibraries.remove({ id: lib.Id }).$promise.then(function () {
	            $scope.libraries = _.without($scope.libraries, lib);
	        });
	    };

	    $scope.testDrive = function (id) {
	        var testDriveUrl = '/content/mobile/startup.mobile?id=' + id;
	        window.open(testDriveUrl, 'Plan Test Drive', 'width=375,height=627');
	    };
	}

	function searchLibrariesController($scope, $modalInstance, Api) {
	    $scope.model = {
	        term: ''
	    };

	    $scope.search = function () {
	        Api.nodeLibraries.search({ term: $scope.model.term }, function(data) {
	            $scope.libraries = data.Results;
	        });
	    };

	    $scope.getCptCodes = function (lib) {

	        var codes = _.map(lib.CptCodes, function (cpt) {
	            return cpt.Code;
	        });

	        return codes.join(', ');
	    };

	    $scope.select = function(library) {
	        $modalInstance.close(library);
	    };

	    $scope.cancel = function() {
	        $modalInstance.dismiss('cancel');
	    };
	}

	angular.module('app').controller('PhysicianPlansListController', ['$rootScope', '$scope', '$routeParams', '$location', '$q', '$modal', 'Api', physicianPlansListController]);
	angular.module('app').controller('SearchLibrariesController', ['$scope', '$modalInstance', 'Api', searchLibrariesController]);
}())