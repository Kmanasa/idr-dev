﻿(function() {
	
    function imageEditController($scope, $routeParams, $q, $location, $controller, $attrs, $window, Api) {


        $scope.isNew = false;

        $scope.image = {
            EntityId: $routeParams.id,
            Id: $routeParams.id,
        };

        if ($routeParams.id) {
            $scope.imageFile = Api.images.get({ id: $routeParams.id });
        } else {
            $scope.isNew = true;
            $scope.imageFile = new Api.imageFiles();
        }

        $scope.saveAlias = function () {
            Api.imageFiles.updateAlias({ id: $scope.imageFile.Id },
		    {
		        id: $scope.imageFile.Id,
		        alias: $scope.imageFile.Alias
		    });
        };

        $scope.delete = function() {
            Api.imageFiles.remove({ id: $routeParams.id }, function() {
                $location.path('/images/list');
            });
        };

        $scope.imageUploaded = function (img) {
            if ($scope.isNew) {
                $scope.$apply(function() {
                    $location.path('/images/edit/' + img.Id);
                });
            } else {
                $window.location.reload();
            }
        };
	}

    angular.module('app').controller('ImageEditController',
        ['$scope', '$routeParams', '$q', '$location', '$controller', '$attrs', '$window', 'Api', imageEditController]);
}())