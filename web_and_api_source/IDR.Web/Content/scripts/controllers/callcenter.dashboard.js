﻿(function () {

    function callCenterDashboardController($rootScope, $scope, $routeParams, $location, $q, Api) {

        $rootScope.$on('proxyChanged', function () {
            getDashboard();
        });

       
    }

	angular.module('app').controller('CallCenterDashboardController',
		['$rootScope', '$scope', '$routeParams', '$location', '$q', 'Api', callCenterDashboardController]);
}())