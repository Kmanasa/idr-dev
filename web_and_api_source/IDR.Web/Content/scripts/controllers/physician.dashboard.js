﻿(function () {

    function physicianDashboardController($rootScope, $scope, $routeParams, $location, $q, Api) {

        $scope.range = 0;
        $scope.required = 0;

        function getDashboard() {
            if (!$rootScope.proxyPhysicianId) {
                return;
            }

            Api.physician.getDashboard({ id: $rootScope.proxyPhysicianId }, function (data) {

                _.each(data, function (d) {
                    d.PatientPhone = $rootScope.formatPhone(d.PatientPhone);

                    _.each(d.DashboardNotifications, function(n) {
                        n.isInOpFilter = true;
                    });
                });

                $scope.data = data;
            });
        }

        if ($rootScope.proxyPhysicianId) {
            getDashboard();
        }

        $rootScope.$on('proxyChanged', function () {
            getDashboard();
        });

        $scope.filterChanged = function () {
            
            //Reset
            _.each($scope.data, function(d) {
                d.hide = false;

                _.each(d.DashboardNotifications, function (n) {
                    n.isInOpFilter = true;
                });
            });

            var date = moment($scope.date);

            _.each($scope.data, function (d) {
                // Filter by date
                var dt = moment(d.StartDate);

                if ($scope.date && date.isValid()) {
                    var show = date.day() === dt.day() &&
                        date.month() === dt.month() &&
                        date.year() === dt.year();

                    d.hide = !show;
                   
                } else {
                    d.hide = false;
                }

                if (d.hide) {
                    return;
                }
                
                // Filter by range
                if ($scope.range == 0) {
                    d.hide = false;

                    _.each(d.DashboardNotifications, function (n) {
                        n.isInOpFilter = true;
                    });
                } else if ($scope.range == 1) {

                    _.each(d.DashboardNotifications, function (n) {
                        n.isInOpFilter = n.IsPreOp;
                    });

                    var preOpOptions = _.filter(d.DashboardNotifications, function(n) { return n.IsPreOp; });
                    // Hide if no notifications are pre op
                    d.hide = preOpOptions.length == 0;
                } else if ($scope.range == 2) {

                    _.each(d.DashboardNotifications, function (n) {
                        n.isInOpFilter = !n.IsPreOp;
                    });

                    var postOpOptions = _.filter(d.DashboardNotifications, function(n) { return !n.IsPreOp; });

                    // Hide if no notifications are post op
                    d.hide = postOpOptions.length == 0;
                }

                if (d.hide) {
                    return;
                }

                // Required filter
                if ($scope.required == 0) {
                    d.hide = false;
                } else {
                    var requireds = _.filter(d.DashboardNotifications, function (n) {
                        return n.Required && n.isInOpFilter;
                    });
                    d.hide = requireds.length == 0;
                }
            });

        };

        $scope.rowHasOps = function(item) {
            if ($scope.range == 0) {
                return true;
            } else if ($scope.range == 1) {
                // Hide if no notifications are pre op
                return _.any(item.DashboardNotifications, function (n) {
                    return n.IsPreOp;
                });

            } else if ($scope.range == 2) {
                return _.all(item.DashboardNotifications, function (n) {
                    return !n.IsPreOp;
                });
            }

            return false;
        };
    }

    angular.module('app').controller('PhysicianDashboardController',
    ['$rootScope', '$scope', '$routeParams', '$location', '$q', 'Api', physicianDashboardController]);
}())