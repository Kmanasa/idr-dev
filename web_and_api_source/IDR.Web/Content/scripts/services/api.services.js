﻿(function () {
	idr.service('Api', ['$resource', api]);

    function api($resource) {

        var adminPhysicians = $resource('/api/adminphysicians/:practiceId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , deactivate: { method: 'POST', isArray: false, url: '/api/adminphysicians/deactivate/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/adminphysicians/new/:id' }
            , invite: { method: 'GET', isArray: false, url: '/api/adminphysicians/invite/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/adminphysicians/:id/:practiceId' }
            , copy: { method: 'GET', isArray: false, url: '/api/adminphysicians/:id/copy' }
        });

        var adminPhysiciansAssistants = $resource('/api/adminphysiciansassistants/:practiceId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , deactivate: { method: 'POST', isArray: false, url: '/api/adminphysiciansassistants/deactivate/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/adminphysiciansassistants/new/:id' }
            , invite: { method: 'GET', isArray: false, url: '/api/adminphysiciansassistants/invite/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/adminphysiciansassistants/:id/:practiceId' }
            , copy: { method: 'GET', isArray: false, url: '/api/adminphysiciansassistants/:id/copy' }
        });

        var adminSchedulers = $resource('/api/adminschedulers/:practiceId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , deactivate: { method: 'POST', isArray: false, url: '/api/adminschedulers/deactivate/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/adminschedulers/new/:id' }
            , invite: { method: 'GET', isArray: false, url: '/api/adminschedulers/invite/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/adminschedulers/:id/:practiceId' }
            , copy: { method: 'GET', isArray: false, url: '/api/adminschedulers/:id/copy' }
        });

        var callsiteAdmins = $resource('/api/calladmins/:practiceId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , deactivate: { method: 'POST', isArray: false, url: '/api/calladmins/deactivate/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/calladmins/new/:id' }
            , invite: { method: 'GET', isArray: false, url: '/api/calladmins/invite/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/calladmins/:id/:practiceId' }
            , copy: { method: 'GET', isArray: false, url: '/api/calladmins/:id/copy' }
        });

        var callsiteMobileUsers = $resource('/api/callmobileusers/:practiceId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , deactivate: { method: 'POST', isArray: false, url: '/api/callmobileusers/deactivate/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/callmobileusers/new/:id' }
            , invite: { method: 'GET', isArray: false, url: '/api/callmobileusers/invite/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/callmobileusers/:id/:practiceId' }
            , copy: { method: 'GET', isArray: false, url: '/api/callmobileusers/:id/copy' }
        });



        var callsitePatients = $resource('/api/callcenterpatients/:id', { id: '@id' }, {
            query: { isArray: false }
            , create: { method: 'POST', isArray: false, url: '/api/callcenterpatients' }
            , update: { method: 'PUT', isArray: false, url: '/api/callcenterpatients' }
            , mobile: { method: 'POST', isArray: false, url: 'api/external/procedures/registercc' }
            , remove: { method: 'DELETE', isArray: false }
            , send: { method: 'GET', isArray: false, url: '/api/callcenterpatients/SendTextMessage' }
            , getmanagepatients: { method: 'GET', isArray: false, url: 'api/callcenterpatients/managepatients' }
            //, notification: { method: 'GET', isArray: false, url: '/api/external/procedures/SendPushNotification' }
        });

        var call = $resource('/api/:url/:id', { url: 'callcenterschedule/callhistory/', id: '@id' }, {
            query: { isArray: true }

        });

        var externalProcedures = $resource('/api/external/procedures/SendPushNotification', {
        });


        var callsiteProviders = $resource('api/callcenterproviders/:id', { id: '@id' }, {
            query: { isArray: false }
            , create: { method: 'POST', isArray: false, url: '/api/callcenterproviders' }
            , update: { method: 'PUT', isArray: false, url: '/api/callcenterproviders' }
            , remove: { method: 'DELETE', isArray: false }

        });

        var callsiteRefs = $resource('/api/callcenterrefs', { id: '@id' },
            {
                query: { isArray: false }
            });

        var callcenterReports = $resource('/api/callcenterreports/:id', { id: '@id' },
            {
                getFacilities: { method: 'GET', isArray: true, url: '/api/callcenterreports/facilities' },
                reportGenerationAuth: { method: 'GET', isArray: false, url: 'api/callcenterreports/AuthenticateUserForReports' }
            });

        var callcenterSchedule = $resource('/api/callcenterschedule', { id: '@id' },
            {
                query: { isArray: true },
                next: { method: 'GET', url: 'api/callcenterschedule/next', isArray: false },
                lock: { method: 'GET', url: 'api/callcenterschedule/lock', isArray: false },
                aggregates: { method: 'GET', url: 'api/callcenterschedule/aggregates', isArray: false },
                cancel: { method: 'GET', url: 'api/callcenterschedule/cancel', isArray: false },
                callback: { method: 'POST', url: 'api/callcenterschedule/:id/callback', isArray: false },
                callTomorrow: { method: 'GET', url: 'api/callcenterschedule/:id/calltomorrow', isArray: false },
                discharge: { method: 'POST', url: 'api/callcenterschedule/:id/discharge', isArray: false },
                summary: { method: 'GET', url: 'api/callcenterschedule/summary', isArray: true },
                summaryOverview: { method: 'GET', url: 'api/callcenterschedule/summaryOverview', isArray: false },
                callhistory: { method: 'GET', url: 'api/callcenterschedule/callhistory/:id', isArray: true },
                scheduleCall: { method: 'POST', url: 'api/callcenterschedule/:id/scheduleacalltomorrow', isArray: false },
                newTimeTomorrow: { method: 'POST', url: 'api/callcenterschedule/:id/scheduleanewcalltime', isArray: false },
                getAdditionalQuestion: { method: 'GET', url: 'api/callcenterschedule/checkforadditionalquestions', isArray: false },
                checkForURL: { method: 'GET', url: 'api/callcenterschedule/checkforaddURL', isArray: false }
            });

        var callcenterComments = $resource('/api/callcentercomments', { id: '@id' },
            {
            });


        var adminPracticeAdmin = $resource('/api/adminpracticeadmins/:practiceId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , deactivate: { method: 'POST', isArray: false, url: '/api/adminpracticeadmins/deactivate/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/adminpracticeadmins/new/:id' }
            , invite: { method: 'GET', isArray: false, url: '/api/adminpracticeadmins/invite/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/adminpracticeadmins/:id' }
            , copy: { method: 'GET', isArray: false, url: '/api/adminpracticeadmins/:id/copy' }
        });

        var attributes = $resource('/api/attributes', { id: '@id', attributeId: '@attributeId' }, {
            typeahead: { method: 'GET', url: '/api/attributes/typeahead', isArray: true },
            update: { method: 'PUT', isArray: false },
            remove: { method: 'DELETE', isArray: false },
            create: { method: 'GET', isArray: false, url: '/api/attributes/new' },
            associate: { method: 'POST', url: 'api/attributes/associate' },
            dissociate: { method: 'DELETE', url: 'api/attributes/dissociate/:id' },

        });

        var auth = $resource('/api/auth/current', { id: '@Id' }, {
            current: { method: 'GET', isArray: false, url: '/api/auth/current' }
        });

        var callCenters = $resource('/api/callcenters', { id: '@Id' }, {
        });

        var companies = $resource('/api/companies', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , query: { method: 'GET', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , create: { method: 'GET', isArray: false, url: '/api/companies/new' }
            , copy: { method: 'GET', isArray: false, url: '/api/companies/:id/copy' }
            , search: { method: 'GET', isArray: false, url: '/api/companies' }
        });

        var disciplines = $resource('/api/disciplines', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , create: { method: 'GET', isArray: false, url: '/api/disciplines/new' }
            , copy: { method: 'GET', isArray: false, url: '/api/disciplines/:id/copy' }
        });

        var editableContent = $resource('/api/editablecontent/:id', { id: '@Id' }, {
            update: { method: 'PUT', isArray: false },
            create: { method: 'GET', isArray: false, url: '/api/editablecontent/new' },
            remove: { method: 'DELETE', isArray: false }
        });

        var emailTemplates = $resource('/api/emailTemplates/:id', { id: '@Id' }, {
            update: { method: 'PUT', isArray: false }
        });

        var facilities = $resource('/api/facilities/:companyId', { id: '@id' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false, url: '/api/facilities/:id' }
            , create: { method: 'GET', isArray: false, url: '/api/facilities/new/:id' }
            , get: { method: 'GET', isArray: false, url: '/api/facilities/:id/:companyId' }
            , copy: { method: 'GET', isArray: false, url: '/api/facilities/:id/copy' }
        });

        var imageFiles = $resource('/api/imagefiles/:id', { id: '@Id', entityId: '@EntityId' }, {
            update: { method: 'PUT', isArray: false }
            , remove: { method: 'DELETE', isArray: false }
            , search: { method: 'GET', isArray: false, url: '/api/imagefiles' }
            , updateAlias: { method: 'POST', isArray: false, url: '/api/imagefiles/files/:id/alias' }
            , associatePhysicianImage: { method: 'GET', isArray: false, url: '/api/imagefiles/files/associatephysician' }
        });

        var pdfFiles = $resource('/api/pdffiles/:id', { id: '@Id' }, {
            GetPdfFiles: { method: 'GET', isArray: true, url: '/api/pdffiles/files/GetAllFiles' },
            Delete: { method: 'DELETE', url: '/api/pdffiles' },
            Alias: { method: 'POST', url: '/api/pdffiles/files/:id/alias' } 


        });

		var images = $resource('/api/images/:id', { id: '@id' }, {
			update: { method: 'PUT', isArray: false }
			, remove: { method: 'DELETE', isArray: false }
			, add: { method: 'GET', isArray: false, url: '/api/images/new/:id' }
		});

		var nodeLibraries = $resource('/api/nodelibraries', { id: '@Id' }, {
			update: { method: 'PUT', isArray: false },
			copy: { method: 'GET', url: '/api/nodelibraries/copy/:id', isArray: false },
			search: { method: 'GET', isArray: false, url: '/api/nodelibraries/search' },
			getNotifications: { method: 'GET', url: '/api/nodelibraries/:id/notifications', isArray: true }

		});

		var nodeTypes = $resource('/api/nodetypes', { id: '@id' }, {
			query: { method: 'GET', isArray: true, url: '/api/nodeTypes/:libraryType' }
		});

		var nodes = $resource('/api/nodes', { id: '@Id' }, {
			update: { method: 'PUT', isArray: false }
			, copy: { method: 'GET', isArray: false, url: '/api/nodes/copy/:id' }
			, get: { method: 'GET', isArray: false, url: '/api/nodes/:id' }
			, query: { method: 'GET', isArray: true, url: '/api/nodes/library/:type' }
			, create: { method: 'GET', isArray: false, url: '/api/nodes/new/:id/:type' }
			, copyApp: { method: 'GET', isArray: false, url: '/api/nodes/app/copy/:id' }
			, noMasters: { method: 'GET', isArray: true, url: '/api/nodes/library/nomaster' }
            , getTemplates: { method: 'GET', isArray: true, url: '/api/nodes/templates' }
            , delete: { method: 'DELETE', isArray: false, url: '/api/nodes/deleteplan' }
               
		});

		var notifications = $resource('/api/notifications', { id: '@Id' },
			{
				save: { method: 'POST' },
				update: { method: 'PUT', isArray: false },
				remove: { method: 'DELETE', isArray: false }
			}
		);

		var patients = $resource('/api/patients', { id: '@Id' }, {

		});

		var procedures = $resource('/api/proceduredetails', { id: '@Id' }, {
			getDashboard: { method: 'GET', url: '/api/proceduredetails/:id/dashboard', isArray: false }
		});

		var physician = $resource('/api/physician', { id: '@id', procedureId: '@ProcedureId' }, {
			update: { method: 'PUT', isArray: false },
			getPlans: { method: 'GET', url: '/api/physician/plans', isArray: false },
			getAllPlans: { method: 'GET', url: '/api/physician/plans/:id/all', isArray: true },
			getPlatForCpt: { method: 'GET', url: '/api/physician/plans/cpt/:id' },
			addPlan: { method: 'POST', url: '/api/physician/plans/add/:id' },
			removePlan: { method: 'DELETE', url: '/api/physician/plans/:id' },
			getPatients: { method: 'GET', url: '/api/physician/patients', isArray: false },
			addPatient: { method: 'POST', url: '/api/physician/patients/' },
			getPatient: { method: 'GET', url: '/api/physician/patients/:id' },
			removePatient: { method: 'DELETE', url: '/api/physician/patients/:id' },
			addPatientLibrary: { method: 'POST', url: '/api/physician/patients/:id/libraries' },
			updatePatientLibrary: { method: 'PUT', url: '/api/physician/patients/:id/libraries' },
			getPatientLibraries: { method: 'GET', url: '/api/physician/patients/:id/libraries', isArray: true },
			updatePatient: { method: 'PUT', url: '/api/physician/patients/:id/' },
			getFacilities: { method: 'GET', url: '/api/physician/facilities', isArray: true },
			getPatientForMrn: { method: 'GET', url: '/api/physician/mrn/:mrn', isArray: false },
			getDashboard: { method: 'GET', url: '/api/physician/:id/dashboard', isArray: true }
		});

		var plans = $resource('/api/recoveryplans', { id: '@Id' }, {
			create: { method: 'GET', isArray: false, url: '/api/recoveryplans/new/:id' }
		});

		var practice = $resource('/api/practice', { id: '@id' }, {
			getUsers: { method: 'GET', isArray: false, url: '/api/practice/:id/users' }
			, getPhysicians: { method: 'GET', isArray: false, url: '/api/practice/:id/physicians' }
		});

		var practices = $resource('/api/practices', { id: '@id' }, {
			update: { method: 'PUT', isArray: false }
			, remove: { method: 'DELETE', isArray: false }
			, query: { method: 'GET', isArray: false, url: '/api/practices' }
			, create: { method: 'GET', isArray: false, url: '/api/practices/new' }
            , copy: { method: 'GET', isArray: false, url: '/api/practices/:id/copy' }
            , getFacilities: { method: 'GET', isArray: true, url: '/api/practices/facilitygroup' }
            , updateghostUser: { method: 'PUT', isArray: false, url: '/api/practices/ghostpractice/:practiceId' }
            , getPractice: { method: 'GET', isArray: false, url: '/api/practices/getPractice' }
		});

		var roles = $resource('/api/roles', { id: '@id' }, {
			getAdmin: { method: 'GET', isArray: true, url: 'api/roles/admin' }
		});

		var snippets = $resource('/api/snippets');

		var templates = $resource('/api/templates', { id: '@Id' }, {
			create: { method: 'GET', isArray: false, url: '/api/templates/new/:id' }
			, get: { method: 'GET', isArray: false, url: '/api/templates/:id' }
			, update: { method: 'PUT', isArray: false }
			, all: { method: 'GET', url: '/api/templates/all', isArray: true }
			, copy: { method: 'GET', url: '/api/templates/:id/copy', isArray: false }
		});

		var treatmentBindables = $resource('/api/treatmentbindables', { id: '@Id' }, {
			query: { method: 'GET', isArray: false }
		});

		var users = $resource('/api/users', { id: '@id' }, {
			update: { method: 'PUT', isArray: false }
			, query: { method: 'GET', isArray: false, url: '/api/users' }
			, self: { method: 'GET', isArray: false, url: 'api/users/self' }
			, setUserRole: { method: 'PUT', isArray: false, url: '/api/users/userrole' }
			, resetPassword: { method: 'PUT', isArray: false, url: '/api/users/resetpassword/:id' }
			, 'delete': { method: 'DELETE', isArray: false, url: '/api/users/resetpassword/:id' }
		});

		return {
			adminPhysiciansAssistants: adminPhysiciansAssistants,
			adminPhysicians: adminPhysicians,
			adminPracticeAdmin: adminPracticeAdmin,
			attributes: attributes,
			auth: auth,
			companies: companies,
			disciplines: disciplines,
			emailTemplates: emailTemplates,
			facilities: facilities,
            imageFiles: imageFiles,
            pdfFiles: pdfFiles,
			images: images,
			editableContent: editableContent,
			nodes: nodes,
			nodeTypes: nodeTypes,
			nodeLibraries: nodeLibraries,
			notifications: notifications,
			patients: patients,
			procedures: procedures,
			physician: physician,
			plans: plans,
			practice: practice,
			practices: practices,
			roles: roles,
			snippets: snippets,
			templates: templates,
			treatmentBindables: treatmentBindables,
			users: users,
			adminSchedulers: adminSchedulers,

			callsiteAdmins: callsiteAdmins,
			callsiteMobileUsers: callsiteMobileUsers,
			callCenters: callCenters,
            callsitePatients: callsitePatients,
            call: call,
            externalProcedures: externalProcedures,
			callsiteRefs: callsiteRefs,
			callsiteProviders: callsiteProviders,
			callcenterSchedule: callcenterSchedule,
			callcenterComments: callcenterComments,
			callcenterReports: callcenterReports
		};
	}
})();
