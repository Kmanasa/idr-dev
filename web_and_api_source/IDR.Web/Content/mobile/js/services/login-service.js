(function(){
	
	
	function loginSvc($http, $q, apiUrl){
	
		function setCredentials() {
			
			var token = localStorage.getItem('token');
			    
			if(token){
				$http.defaults.headers.common.Authorization = 'Bearer ' + token;	
			}
		}
	
		var login = function(username, password){
	
			// Create a promise that will be satisfied when the login requests is finished.
			var deferred = $q.defer(),
	
				credentials = {
					username: username,
					password: password,
					grant_type: 'password'
				},
		
				authUrl = apiUrl + 'token';

			// post the credentials to the server and handle the returned token
			$http({
				url: authUrl,
				method: 'POST',
				data: $.param(credentials),
				headers: { "Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8" }
			}).then(function(response){
				localStorage.setItem('token', response.data.access_token);
				localStorage.setItem('token_expires', response.data['.expires']);
				setCredentials();
					
				// Finalize the request
		    	deferred.resolve(response.data);
			}, function(errResponse){
		  		// Reject the request
		   		deferred.reject(status);
			});
	
			return deferred.promise;
		};
	
		return {
			login: login
		}	
	}
	
	app.factory('LoginService', ['$http', '$q', 'apiUrl', loginSvc]);
	
}());