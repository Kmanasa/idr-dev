
(function(){
	
	var _push;

	function notificationSvc(DeviceService){

		function init(accessCode){

			if(!PushNotification || !PushNotification.init){
				return;
			}


 			_push = PushNotification.init({ 

 				"android": {"senderID": "683316021784"},

         		"ios": {"alert": "true", "badge": "true" } 
         	});
			
			
			_push.on('registration', function(data) {
		        // data.registrationId
		        console.log(data);
		        //alert(data.registrationId);

		        var deviceType = 1;

		        if(device.platform == 'Android'){
		        	deviceType = 2;
		        }

		        // Register this device for this procedure.  This ends 
		        // up as a device description an Azure notification hub.
		        var registration = new DeviceService();
				registration.handle= data.registrationId;
	        	registration.deviceType= deviceType;
	        	registration.ProcedureCode= accessCode;
	        	console.log(registration);
		        registration.$save();
		    });

		    _push.on('notification', function(data) {
		        // data.message,
		        // data.title,
		        // data.count,
		        // data.sound,
		        // data.image,
		        // data.additionalData

		        // Nav to inbox?

		        console.log(data);
		    });

		    _push.on('error', function(e) {
		        // e.message
		        console.log(e.message);
		    });
		}

		function unregister(){
			if(_push && _push.unregister){
				_push.unregister(function(){}, function(){});
			}
		}
	
		return { 
			init: init,
			unregister: unregister
		};
	}
	
	app.factory('NotificationService', notificationSvc);
	
}());
