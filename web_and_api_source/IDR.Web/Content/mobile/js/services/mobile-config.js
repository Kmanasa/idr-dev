﻿(function () {


    function mobileConfigService($resource, apiUrl) {

        var url = apiUrl + 'api/external/mobileconfig/';

        return $resource(url, { id: '@id' }, {
        });
    }

    app.factory('MobileConfigService', ['$resource', 'apiUrl', mobileConfigService]);

}());