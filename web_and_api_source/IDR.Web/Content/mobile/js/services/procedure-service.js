(function(){
	
	
	function procedureSvc($resource, apiUrl){

		var url = apiUrl + 'api/external/procedures/';

		return $resource(url, { id: '@id' }, {
		    getTestDrive: { method: 'GET', url: '/api/external/procedures/testdrive/:id', isArray: false }
		});
	}
	
	app.factory('ProcedureService', ['$resource', 'apiUrl', procedureSvc]);
	
}());