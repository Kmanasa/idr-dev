(function(){
	
	var fieldsToReplace = {

		'FacilityName': { value: /{{facility.Name}}/ig},
		'FacilityStreetAddress1':{ value: /{{facility.StreetAddress1}}/ig },
		'FacilityStreetAddress2':{ value: /{{facility.StreetAddress2}}/ig },
		'FacilityStreetAddressCity':{ value: /{{facility.StreetCity}}/ig },
		'FacilityStreetAddressState':{ value: /{{facility.StreetState}}/ig },
		'FacilityStreetAddressPostalCode':{ value: /{{facility.StreetPostal}}/ig },
		'FacilityManagerEmail':{ value: /{{facility.ManagerEmail}}/ig },
		'FacilityManagerContactPhone':{ value: /{{facility.ManagerContactPhone}}/ig, isPhone: true },
		'FacilityPatientInformationEmail':{ value: /{{facility.PatientInfoEmail}}/ig },
		'FacilityPatientInformationPhone':{ value: /{{facility.PatientInfoPhone}}/ig, isPhone: true  },
		
		'PracticeName': { value: /{{practice.Name}}/ig },
		'PracticeContac:tPhone':{ value: /{{practice.ContactPhone}}/ig, isPhone: true  },
		'PracticeContactEmail':{ value: /{{practice.ContactEmail}}/ig },
		'PracticeSchedulingContactName':{ value: /{{practice.SchedulingName}}/ig },
		'PracticeSchedulingContactPhone':{ value: /{{practice.SchedulingPhone}}/ig, isPhone: true  },
		'PracticeSchedulingContactEmail':{ value: /{{practice.SchedulingEmail}}/ig },
		'PracticeAddress1': { value: /{{practice.StreetAddress1}}/ig },
		'PracticeAddress2':{ value: /{{practice.StreetAddress2}}/ig },
		'PracticeCity':{ value: /{{practice.StreetCity}}/ig },
		'PracticeState':{ value: /{{practice.Streetstate}}/ig },
		'PracticePostalCode':{ value: /{{practice.StreetPostal}}/ig },
		'PracticeOnCallServicePhone':{ value: /{{physician.OnCallPhone}}/ig, isPhone: true  },
		'PracticeOnCallServiceEmail':{ value: /{{physician.OnCallEmail}}/ig }, 
		'PracticeImageRef':{ value: /{{practice.PracticeImage}}/ig, isImage: true },
		
		'PhysicianTitle':{ value: /{{physician.Title}}/ig },
		'PhysicianFirstName':{ value: /{{physician.FirstName}}/ig },
		'PhysicianLastName': { value: /{{physician.LastName}}/ig },
		'PhysicianDirectContactPhone':{ value: /{{physician.DirectContactPhone}}/ig, isPhone: true  },
		'PhysicianDirectContactEmail':{ value: /{{physician.DirectContactEmail}}/ig },
		'PhysicianSchedulingContactName':{ value: /{{physician.SchedulingContactName}}/ig },
		'PhysicianSchedulingContactPhone': { value: /{{physician.SchedulingContactPhone}}/ig, isPhone: true },
		'PhysicianSchedulingContactExt':{ value: /{{physician.SchedulingContactExt}}/ig },
		'PhysicianSchedulingContactEmail':{ value: /{{physician.SchedulingContactEmail}}/ig }, 
		'PhysicianImageRef':{ value: /{{physician.PhysicianImage}}/ig, isImage: true }

	};
	app.constant('fieldReplacements', fieldsToReplace);
	
}());