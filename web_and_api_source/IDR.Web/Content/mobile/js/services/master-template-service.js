(function(){
	
	
	function masterTemplateSvc($resource, apiUrl){

		var url = apiUrl + 'api/external/mastertemplates/';

		return $resource(url, { id: '@id' }, {
        	
        });
	}
	
	app.factory('MasterTemplateService', ['$resource', 'apiUrl', masterTemplateSvc]);
	
}());