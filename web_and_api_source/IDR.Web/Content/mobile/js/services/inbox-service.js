(function(){
	
	
	function inboxService($resource, apiUrl){

		var url = apiUrl + 'api/external/procedures/:procedureId/inbox/:id';

		return $resource(url, { id: '@id', procedureId: '@procedureId' }, { 
			'update': { method: 'PUT' }
		});
	}
	
	app.factory('InboxService', ['$resource', 'apiUrl', inboxService]);
	
}());