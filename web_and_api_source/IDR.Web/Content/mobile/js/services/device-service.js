(function(){
	
	
	function deviceService($resource, apiUrl){

		var url = apiUrl + 'api/external/devices/';

		return $resource(url, { id: '@id' }, { });
	}
	
	app.factory('DeviceService', ['$resource', 'apiUrl', deviceService]);
	
}());