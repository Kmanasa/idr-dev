(function(){

	app.controller('WelcomeLetterCtrl', welcomeLetterCtrl);
	
	function welcomeLetterCtrl($scope, $sce, apiUrl){ 
		
		var page = app.rootNav.getCurrentPage(),
			opt = page.options;

		$scope.model = opt.currentNode;
		$scope.index = -1;

		function setPage(){
			$scope.model = opt.currentNode;
			$scope.model.Nodes[$scope.index].visible = true;
			$scope.title = $scope.model.Nodes[$scope.index].Title;
			$scope.currentNode = $scope.model.Nodes[$scope.index];
			// bad idea...
			jQuery('.page__content').scrollTop(0);
		}

		$scope.nextPage = function(){

			_.each($scope.model.Nodes, function(n){
				n.visible = false;
			});

			$scope.index++;

			if($scope.index == $scope.model.Nodes.length){
				// Welcome letter is done.  Back to the regular content.
				app.rootNav.popPage();
				return;
			}

			setPage();
		}


		$scope.previousPage = function(){
			_.each($scope.model.Nodes, function(n){
				n.visible = false;
			});

			$scope.index--;

			if($scope.index == -1){
				// Back to the main menu
				var pages = app.rootNav.getPages();
				// Destroy the parent page before popping the current page
				pages[1].destroy();
				// Pops to the main menu
				app.rootNav.popPage();
				return;
			}

			setPage();
		};

		$scope.backText = function(){
			return ($scope.index < 1) ? 'Home' : 'Back';
		};

		$scope.nextPage();
	}
}());