(function(){

	app.controller('TemplateCtrl', templateCtrl);
	
	function templateCtrl(MasterTemplateService, $scope, $sce, $rootScope, $timeout, apiUrl){ 
		
		var page = app.rootNav.getCurrentPage();

		$scope.loading = true;
		$scope.step1 = 1;
		$scope.step2 = $scope.step3 = 0;

		$timeout(function(){
			
			$scope.step1 = 2;
			$scope.step2 = 1;
					
		}, 2000);

		$timeout(function(){
			$scope.step2 = 2;
			$scope.step3 = 1;
		}, 3500);

		$timeout(function(){
			$scope.step3 = 2;
		}, 5000);
		

		// Get the ID of the content that was selected.  The first time a library
		// is selected, it's via $rootScope.libraryId.  Subsequent selections
		// are via the Onse page object
		if($rootScope.libraryId || page.options.templateId){
			var id = $rootScope.libraryId || page.options.templateId;

			// Clear root scope so it never evaluates to true again
			$rootScope.libraryId = null;
			$scope.isTopLevel = true;		
			$scope.template = MasterTemplateService.get( { id: id }, showInitialNode);
			
		}else if(page.options.currentNode){
			
			$scope.currentNode = page.options.currentNode;
			$scope.loading = false;
		}

		if(page.options.isTopLevel){
			$scope.isTopLevel = true;	
		}

		// Set the page's content to the selected item
		$scope.setCurrentCotent = function (node) {

			console.log(node);
			if (node.Type.TemplateId == 7) {
				app.rootNav.pushPage('video.html', {
					currentNode: node
				});
			} else {
				// Navigate to the newly selected node
				app.rootNav.pushPage('content.html', {
					currentNode: node
				});
			}
		};

		$scope.isLoading = function(step){
			return $scope[step] === 1;
		}

		$scope.isLoaded = function(step){
			return $scope[step] === 2;
		}

		function showInitialNode (data){
			$scope.step1 = $scope.step2 = $scope.step3 = 0;
			console.log(data);

			$scope.loading = false;

			// Find the welcome page
			var welcomeNode = _.find($scope.template.Nodes, function(n){
				return n.TypeId == 2; 
			});

			// Set a variable to all content excluding the welcome letter
			$scope.content = _.without($scope.template.Nodes, welcomeNode);

			// Navigate to the welcome-specific pager if a welcome letter exists
			if(welcomeNode){
				app.rootNav.pushPage('welcome-letter.html', { 
					currentNode: welcomeNode,
					animation: 'none'
				});
			}

			// Prepare the first node even though a welcome page will likely be displayed
			$scope.currentNode = $rootScope.rootNode = $scope.content[0];
		}
	}

}());