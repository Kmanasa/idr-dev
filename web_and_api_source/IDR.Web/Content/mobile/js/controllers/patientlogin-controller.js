(function(){

	function patientLoginController(LoginService, ProcedureService, NotificationService, $scope, $rootScope) {
		
		$scope.loading = false;
	    $scope.accessCode = _.last(location.search.split('='));
	    
	    function setDisabledState() {
	        $scope.disabled = !$scope.accessCode || $scope.accessCode.length == 0 || $scope.loading;
	    }

	    setDisabledState();
		
		// Prevent swite access to the menu
		menu.setSwipeable(false);

		$scope.getProcedure = function(){

			$scope.loading = true;
		    setDisabledState();

			var token = $scope.accessCode;

			// Get the procedure details
			ProcedureService.getTestDrive({ id: token }, loadSuccess, loadFailure);
		}

	    $scope.getProcedure();
	
		function loadSuccess(data){
			// Procedure loaded successfully
			console.log(data);

			// Make sure a plan was returned.
			if(!data.AccessCode){
				loadFailure();
				return;
			}

			$rootScope.accessCode = data.AccessCode;
			//$rootScope.$broadcast('procedureLoaded');

			// Register for push notifications
			//NotificationService.init(data.AccessCode);

			$rootScope.placeholders = data;

			$scope.loading = false;
		    setDisabledState();

			// Calculate durations
			$rootScope.daysOffset = 0;

			if(data.StartDate){
				$rootScope.placeholders.daysOffset = moment().diff(moment(data.StartDate), 'days');
			} 
			
			$rootScope.libraryId = data.LibraryId;

			menu.setMainPage('template.html');
		} 
		
		function loadFailure(){
			console.log('login failed');

			$scope.loading = false;
			
			ons.notification.alert({message: 'We were unable to locate your recovery plan.  Please try again.'});
		}
	}

	app.controller('PatientLoginCtrl', patientLoginController);

}());