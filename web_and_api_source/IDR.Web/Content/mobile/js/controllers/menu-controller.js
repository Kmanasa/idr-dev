(function(){

	
    function menuController(LoginService, InboxService, NotificationService, MobileConfigService, $scope, $rootScope, apiUrl) {
	    
		$scope.model = $scope;

		$scope.inboxCount = 0;

		MobileConfigService.get(function(mobileConfig){
			$scope.menuItems = _.filter(mobileConfig.EditableContent, function(m){
				return m.InMobileMenu;
			});
		});


		$scope.home = function(){
			
			menu.close();
			app.rootNav.resetToPage('content.html', { currentNode: $rootScope.rootNode, isTopLevel: true });
		};

		$scope.logout = function(){

			menu.close();
			if(!localStorage.getItem('saveCode') == true){
				localStorage.removeItem('accessCode');
			}
			
			NotificationService.unregister();
			app.rootNav.resetToPage('patient-login.html');
		}
        $scope.showInbox = function(){
			menu.close();
			app.rootNav.resetToPage('inbox.html');
		};

		$scope.showMenuItem = function(mi){
			menu.close();
			app.rootNav.resetToPage('local.html', {title: mi.Title, content: mi.Content });
		};
		
	}

	app.controller('MenuCtrl', menuController);

}());