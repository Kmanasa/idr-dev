﻿(function () {

    app.controller('LocalCtrl', localCtrl);

    function localCtrl($scope, $sce) {

        var page = app.rootNav.getCurrentPage(),
			opt = page.options;

	    $scope.title = opt.title;
        $scope.content = opt.content;
    }
}());