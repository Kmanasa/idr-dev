(function(){

	app.controller('RemoteCtrl', remoteCtrl);
	
	function remoteCtrl($scope, $sce){ 
		
		var page = app.rootNav.getCurrentPage();

		$scope.remoteUrl = $sce.trustAsResourceUrl(page.options.remoteUrl);

	}

}());