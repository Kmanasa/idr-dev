(function(){

	
	function inboxItemController($scope, $rootScope, InboxService){
		var page = app.rootNav.getCurrentPage();
		var item = page.options.item;
		$scope.item = item;

		if(!item.Read){
			item.Read = true;
			$scope.item.$update({id: $scope.item.Id, procedureId: $rootScope.accessCode});
		}
	}

	app.controller('InboxItemCtrl', inboxItemController);

}());