(function(){

	function masterTemplateCtrl(MasterTemplateService, $scope){ 
		
		$scope.templates = MasterTemplateService.query();
		
		$scope.selectTemplate = function(template){
			app.rootNav.pushPage('template.html', { templateId: template.Id, animation: 'slide' });
		};
	}

	app.controller('MasterTemplatesCtrl', masterTemplateCtrl);
}());