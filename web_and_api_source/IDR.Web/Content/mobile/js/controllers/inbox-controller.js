(function(){

	
	function inboxController(InboxService, $scope, $rootScope, apiUrl){
		
		InboxService.query({ procedureId: $rootScope.accessCode }, function(data){
			$scope.inbox = data;
		});

		$scope.selectInboxItem = function(item){
			
			app.rootNav.pushPage('inbox-item.html', { item: item, animation: 'slide' });

			$rootScope.inboxCount = _.filter($scope.inbox, function(i){
				return !i.Read;
			}).length;
		};
	}

	app.controller('InboxCtrl', inboxController);

}());