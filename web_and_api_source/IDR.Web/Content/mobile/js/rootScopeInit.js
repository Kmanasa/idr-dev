var rootScopeInit = function($rootScope, $sce, apiUrl, fieldReplacements, $http) {

	var tkn = localStorage.getItem('token');
	if (tkn) {
		$http.defaults.headers.common.Authorization = 'Bearer ' + tkn;
	}

    function setPlaceholders(html) {

        html = html.replace(/{{physician.Name}}/ig, $rootScope.placeholders.PhysicianFirstName +
			' ' + $rootScope.placeholders.PhysicianLastName);

        html = html.replace(/{{procedure.Name}}/ig, $rootScope.placeholders.LibraryName);

        _.each(_.keys(fieldReplacements), function (key) {

            if ($rootScope.placeholders[key]) {
                if (fieldReplacements[key].isPhone) {
                    var phoneLink = '<a href="tel:' + $rootScope.placeholders[key] + '" target="_system">' + $rootScope.placeholders[key] + '</a>';
                    html = html.replace(fieldReplacements[key].value, phoneLink);
                }
                else if (fieldReplacements[key].isImage) {
                    var image = '<img src="' + apiUrl + 'api/imagefiles/' + $rootScope.placeholders[key] + '" />';
                    html = html.replace(fieldReplacements[key].value, image);
                }
                else {
                    html = html.replace(fieldReplacements[key].value, $rootScope.placeholders[key]);
                }
            }
        });

        return html;
    }

    // Mark the HTML content as safe
    $rootScope.sanitize = function (val) {
        // Replace relative image URLs with fully qualified

        var html = setPlaceholders(val);

        var urlRegEx = /(src=")(api\/)([-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])(")/ig;
        html = html.replace(urlRegEx, '$1' + apiUrl + 'api/' + '$3$4');

        var localImageRegEx = /(src=")(images\/)([-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])(")/ig;
        html = html.replace(localImageRegEx, '$1' + apiUrl + 'images/' + '$3$4');

        // Fix links for child browser window
        var hrefRegEx = /<a([^>]+)>(.+?)<\/a>/ig
        html = html.replace(hrefRegEx, '<customlink $1 >$2</customlink>');

        return $sce.trustAsHtml(html);
    };

	$rootScope.sanitizeUrl = function(val){
		return $sce.trustAsResourceUrl(val);
	};

	// Find the correct video player type (YouTube or Vimeo)
	$rootScope.getPlayerType = function(properties){
		var playerType = _.find(properties, function(p){
			return p.Title == 'Video Player Type';
		});

		if(playerType){
			return playerType.Value;
		}

		return '';
	};

	// Gets the full URL for a Video from a node property list
	$rootScope.getVideoUrl = function(properties){
		
		var playerUrl = _.find(properties, function(p){
			return p.Title == 'Video Url';
		});

		return playerUrl ? playerUrl.Value : '';
	};

	// Convert relative image paths to a full URL
	$rootScope.resolveUrl = function(path){
		return apiUrl.substring(0, apiUrl.length - 1) + path;
	};

	// Gets the image align attribute for inline content images
	$rootScope.getImageAlignment = function(style){
		if(style == 'pull-right'){
			return 'right';
		}

		return 'left';
	};

	$rootScope.getFormHtml = function(properties){
		
		// Get the property that contains the URL
		var prop = _.find(properties, function(p){
			return p.Title == 'Form Url';
		});

		var url = prop.Value;

		//Substitute values
		url += "?1=" + $rootScope.placeholders.PatientIdentifier;// + '&2=' + $rootScope.placeholders.daysOffset;

		// Return a formatted iframe witht the URL
		return '<iframe src="'+url+'" style="left: 0px;border: none;margin: 0;padding: 0;overflow: scroll;z-index: 999999;width: 100%;height: 100%;"></iframe>';
	};

	$rootScope.getMenuPadding = function(properties){
		// Get the property that contains the URL
		var prop = _.find(properties, function(p){
			return p.Title == 'Menu Padding';
		});

		return prop && prop.Value || 10;
	};

};