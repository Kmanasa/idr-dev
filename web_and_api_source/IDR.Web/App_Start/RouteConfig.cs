﻿using System.Web.Mvc;
using System.Web.Routing;

namespace IDR.Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.RouteExistingFiles = false;
			routes.IgnoreRoute("Views/{file}.html");
			routes.IgnoreRoute("bower_components/*");

            routes.MapRoute(
                name: "MobileHtml",
                url: "content/mobile/startup.mobile",
                defaults: new { controller = "Mobile", action = "File", id = UrlParameter.Optional }
            );

            routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);

            routes.MapRoute(
                name: "Impersonate",
                url: "Account/Impersonate",
                defaults: new { controller = "Account", action = "Impersonate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
				name: "ResetPassword",
				url: "Account/ForgotPassword",
				defaults: new { controller = "Account", action = "ForgotPassword", id = UrlParameter.Optional }
			);

			routes.MapRoute(
				name: "Convert",
				url: "Convert/",
				defaults: new { controller = "Convert", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
