﻿using IDR.Web.App_Start;
using System.Configuration;
using System.Data.Entity.Migrations;


[assembly: WebActivator.PreApplicationStartMethod(typeof(DatabaseMigrator), "Start")]


namespace IDR.Web.App_Start
{
    public class DatabaseMigrator
    {
        public static void Start()
        {
			bool migrate;
			var parsed = bool.TryParse(ConfigurationManager.AppSettings["MigrateDatabase"], out migrate);

			if (!parsed || !migrate)
			{
				return;
			}

			var config = new IDR.Migrations.Configuration();
			var migrator = new DbMigrator(config);
			migrator.Update();


		}
    }
}