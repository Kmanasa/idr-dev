﻿using System.Web.Http;

namespace IDR.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ExternalApi",
                routeTemplate: "api/external/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

	        //config.Routes.MapHttpRoute(
		       // name: "CallAdmins",
		       // routeTemplate: "api/calladmins/{id}",
		       // defaults: new { controller= "AdminCallSiteAdminsController", id = RouteParameter.Optional }
	        //);

			config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
