﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IDR.Web.Startup))]
namespace IDR.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
