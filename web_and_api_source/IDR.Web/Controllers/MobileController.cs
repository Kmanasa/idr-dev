﻿using System.Web.Mvc;

namespace IDR.Web.Controllers
{
    public class MobileController : Controller
    {
        [Authorize]
        public ActionResult Drive(int id)
        {
            return View();
        }
        
        public ActionResult File()
        {
            return View("Drive");
        }
    }
}
