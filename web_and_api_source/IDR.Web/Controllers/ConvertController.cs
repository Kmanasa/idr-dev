﻿using System;
using System.Web.Mvc;
using IDR.Services;

namespace IDR.Web.Controllers
{
	public class ConvertController : Controller
	{
		private readonly ProcedureService _procedureService;

		public ConvertController(ProcedureService procedureService)
		{
			_procedureService = procedureService;
		}

		public ActionResult Index(string id)
		{
			var path = Server.MapPath($"~/App_Data/{id}");

			return File(path, "application/pdf");
		}

		public ActionResult Done(string id, string url, string source, string accessCode)
		{
			var path = Server.MapPath($"~/App_Data/");

			try
			{
				_procedureService.NotifyAoB(url, accessCode);

				System.IO.File.Delete($"{path}{source}");
			}
			catch (Exception ex)
			{
				System.IO.File.WriteAllText($"{path}_error_done_{DateTime.UtcNow.Ticks}.txt", id);

				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
			}

			return Content("ok");
		}
	}
}