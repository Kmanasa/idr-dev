﻿using System.Web.Mvc;

namespace IDR.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
	        if (Request.IsAuthenticated)
	        {


		        return View();
	        }

	        return RedirectToAction("Login", "Account");
        }
    }
}