﻿using System.Web.Mvc;

namespace IDR.Web.Controllers
{
    public class NavigationController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}