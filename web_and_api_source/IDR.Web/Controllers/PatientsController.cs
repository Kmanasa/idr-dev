﻿using System.Threading.Tasks;
using System.Web.Mvc;
using IDR.Domain;
using IDR.Domain.Portal.Dto;
using IDR.Services;

namespace IDR.Web.Controllers
{
	[Authorize]
    public class PatientsController : Controller
    {
        private readonly ProcedureService _procedureService;
        private readonly PhysiciansService _physiciansService;

        public PatientsController(ProcedureService procedureService, PhysiciansService physiciansService)
        {
            _procedureService = procedureService;
            _physiciansService = physiciansService;
        }

	    public async Task<ActionResult> Overview(string id)
        {
            var procedure = await _procedureService.GetProcedureRefs(id);
	        var patient = await _physiciansService.GetPatient(procedure.PatientId);

            return View(new PatientOverviewDto
            {
                Procedure = procedure,
                Patient = patient
            });
        }

        public async Task<ActionResult> Schedule(string id)
        {
            var notifications = await _procedureService.GetNotifications(id);

            return View(notifications);
        }
    }

    public class PatientOverviewDto
    {
        public ProcedureRefDto Procedure { get; set; }
        public PatientDto Patient { get; set; }
    }
}
