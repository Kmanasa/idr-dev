﻿using System.Collections.Generic;
using IDR.Data;
using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Domain.CallCenter;
using IDR.Domain.Companies.Dto;
using IDR.Infrastructure;
using IDR.Services.Models.CallCenter;
using IDR.Web.App;

namespace IDR.Web.Api
{
	[Authorize(Roles = "Admin,Super Admin,Practice Admin")]
	[NotMobile]
	[BearerRequired]
	public class CallCenterController : ApiController
    {
		private readonly ApplicationDbContext _context;
	    private readonly CallCenterService _callCenterService;

		public CallCenterController(ApplicationDbContext context, CallCenterService callCenterService)
		{
			_context = context;

		    _callCenterService = callCenterService;
		}

		[Route("api/callcenters")]
		[HttpGet]
		public async Task<List<CallCenterDto>> Get()
		{
			var model = await _callCenterService.GetCallCenters();
			return model;
		}
    }
}
