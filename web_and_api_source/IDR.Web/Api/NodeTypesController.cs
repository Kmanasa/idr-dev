﻿using IDR.Data;
using IDR.Domain.Cms.Dto;
using IDR.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;

namespace IDR.Web.Api
{
	[RoutePrefix("api/nodeTypes")]
	[Authorize]
	[NotMobile]
	[BearerRequired]
    public class NodeTypesController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
	    private readonly NodeLibraryService _nodeLibraryService;

		public NodeTypesController(ApplicationDbContext context, IAuditService auditService,
			NodeLibraryService nodeLibraryService)
		{
			_context = context;
			_auditService = auditService;
			_nodeLibraryService = nodeLibraryService;
		}

		[Route("{libraryType}")]
        public async Task<List<NodeTypeDto>> Get(string libraryType)
        {
			var aps = await _nodeLibraryService.GetMetaData(libraryType);

            return aps;
        }

		public async Task<NodeTypeDto> Get(int id)
		{
			var aps = await _nodeLibraryService.GetNodeType(id);

			return aps;
		}
		
        [HttpDelete]
		public async Task<NodeLibraryDto> Remove(int id)
		{
			//var aps = new DeleteNodeLibraryCommand(_context, id, _auditService);
			var aps =await _nodeLibraryService.DeleteNodeLibrary(id);

			return aps;
		}
	}
}
