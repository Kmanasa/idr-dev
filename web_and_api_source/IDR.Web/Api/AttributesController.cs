﻿using System.Net;
using System.Net.Http;
using IDR.Domain.Cms.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Services;

namespace IDR.Web.Api
{
	[RoutePrefix("api/attributes")]
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class AttributesController : ApiController
    {
	    private readonly NodeLibraryService _nodeLibraryService;

        public AttributesController(NodeLibraryService nodeLibraryService)
		{
            _nodeLibraryService = nodeLibraryService;
		}

	    public async Task<List<AttributeDto>> Get()
	    {
	        return await _nodeLibraryService.GetAttributes();
	    }

        public async Task<AttributeDto> GetAttribute(int id)
        {
	        return await _nodeLibraryService.GetAttribute(id);
        }

        public async Task<AttributeDto> Put([FromBody] AttributeDto attr)
        {
            return await _nodeLibraryService.UpdateAttribute(attr);
        }

        [Route("associate")]
	    public async Task<NodeLibraryAttributeDto> Associate(int id, int attributeId)
        {
            return await _nodeLibraryService.AssociateAttribute(id, attributeId);
        }

        [Route("dissociate/{id}")]
	    [HttpDelete]
	    public async Task<HttpResponseMessage> Dissociate(int id)
        {
            await _nodeLibraryService.DissociateAttribute(id);
            return Request.CreateResponse(HttpStatusCode.OK);
	    }

        [Route("new")]
        [HttpGet]
        public async Task<AttributeDto> New()
        {
            return await _nodeLibraryService.AddAttribute();
        }

        [HttpDelete]
        public async void Delete(int id)
        {
            await _nodeLibraryService.DeleteAttribute(id);
        }
    }
}
