﻿
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using IDR.Data;
using IDR.Domain.Membership.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Web.Http;
using IDR.web.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Web.Mailers;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;

namespace IDR.Web.Api
{
	[RoutePrefix("api/users")]
	[Authorize(Roles = "Admin,Practice Admin, Call Center Admin, Call Site Admin, Admin,Super Admin")]
	[BearerRequired]
	public class UsersController : BaseApiController
	{
		private readonly ApplicationDbContext _context;
		private readonly IAuditService _auditService;
		private readonly ApplicationUserManager _userManager;
		private readonly IResetPasswordMailer _mailer;
		private readonly UsersService _usersService;

		public UsersController(ApplicationDbContext context,
			IAuditService auditService,
			IResetPasswordMailer mailer,
			UsersService usersService)
		{
			_userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
			
			// Not used, but required to initialize so the provider works properly
			var provider = new DpapiDataProtectionProvider("ASP.NET Identity");

			_userManager.UserTokenProvider = new EmailTokenProvider<ApplicationUser, string>();
			_context = context;
			_auditService = auditService;
			_mailer = mailer;
			_usersService = usersService;
		}

		public async Task<PagedQueryResults<UserDto>> Get([FromUri]PagingParameters paging)
		{
			var users = await _usersService.QueryUsers(paging ?? new PagingParameters());

			return users;
		}

		public UserDto Get(string id)
		{
			//var query = new GetUserQuery(_context, id);
			var user = _usersService.GetUser(id);

			return user;
		}

		[HttpGet]
		[Route("self")]
		public UserDto Self()
		{
			//var query = new GetUserQuery(_context, CurrentUserId);
			var user = _usersService.GetUser(CurrentUserId);

			return user;
		}

		[HttpPost]
		public async Task<UserDto> Post(NewUserModel model)
		{
			var user = new ApplicationUser
			{
				UserName = model.UserName,
				Email = model.UserName,
				Firstname = model.FirstName,
				Lastname = model.LastName,
				Active = true,
				PhoneNumber = model.Phone,
				Bilingual = model.Bilingual
			};
			var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
			var result = await userManager.CreateAsync(user, model.Password);

			if (result.Succeeded)
			{
				userManager.AddToRoles(user.Id, model.Roles);
				return new UserDto();
			}

			throw new Exception(result.Errors.First());
		}

		[HttpPut]
		public async Task<OperationResponse<UserDto>> Put(UserDto user)
		{
			var model = await _usersService.UpdateUser(user);

			return model;
		}

		[Route("userrole")]
		[HttpPut]
		public async Task<OperationResponse<UserRoleDto>> Put(UserRoleDto userRole)
		{
			var role = await _usersService.UpdateUserRole(userRole);

			return role;
		}

		[Route("resetpassword/{id}")]
		[HttpPut]
		public async Task<OperationResponse<UserDto>> Resetpassword(string id)
		{
			var user = _userManager.FindById(id);
			var token = _userManager.GeneratePasswordResetToken(user.Id);

			var url = $"{Request.RequestUri.Scheme}://{Request.RequestUri.Host}/Account/ResetPassword?userId={user.Id}&code={token}";
			_mailer.Send(new ResetPasswordRequest
			{
				Email = user.Email,
				LastName = user.Lastname,
				FirstName = user.Firstname,
				Url = url
			});

			var response = new OperationResponse<UserDto>();

			response.Messages.Add(new ResponseMessage
			{
				Message = "Password reset request sent",
				State = ResponseState.Succeeded
			});

			return response;
		}

		[HttpDelete]
		public HttpResponseMessage Delete(string id)
		{
			var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_context));
			var user = userManager.FindById(id);
			userManager.Delete(user);

			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}

	public class NewUserModel
	{
		public string UserName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Password { get; set; }
		public string Phone { get; set; }
		public bool Bilingual { get; set; }
		public string[] Roles { get; set; }
	}
}
