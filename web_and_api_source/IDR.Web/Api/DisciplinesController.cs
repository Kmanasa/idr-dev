﻿using IDR.Data;
//using IDR.Domain.Disciplines.Commands;
using IDR.Domain.Disciplines.Dto;
//using IDR.Domain.Disciplines.Queries;
using IDR.Services;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;

namespace IDR.Web.Api
{
	[RoutePrefix("api/disciplines")]
	[Authorize]
	[NotMobile]
	[BearerRequired]
	public class DisciplinesController : ApiController
	{
		private readonly DisciplinesService _disciplinesService;

		public DisciplinesController(DisciplinesService disciplinesService)
		{
			_disciplinesService = disciplinesService;
		}

		public async Task<List<DisciplineDto>> Get()
		{
			var disciplines = await _disciplinesService.Query();

			return disciplines;
		}

		public async Task<DisciplineDto> Get(int id)
		{
			var discipline = await _disciplinesService.GetDiscipline(id);
			return discipline;
		}

		public async Task<DisciplineDto> Put([FromBody] DisciplineDto disc)
		{
			var returnCat = await _disciplinesService.UpdateDiscipline(disc);

			return returnCat;
		}

		[Route("new")]
		[HttpGet]
		public async Task<DisciplineDto> New()
		{
			var discipline = await _disciplinesService.AddDiscipline();

			return discipline;
		}

		[Route("{id}/copy")]
		[HttpGet]
		public async Task<DisciplineDto> Copy(int id)
		{
			var returnTemplate = await _disciplinesService.CopyDiscipline(id);

			return returnTemplate;
		}

		[HttpDelete]
		public async Task<HttpResponseMessage> Delete(int id)
		{
			await _disciplinesService.DeleteDiscipline(id);
			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}
}
