﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IDR.Data;
using IDR.Domain.Images.Dto;
using IDR.Services;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http.Headers;
using System.IO;
using IDR.Infrastructure;
using IDR.Services.Models.PDFs;
using System.Text.RegularExpressions;
using Spire.Pdf;

namespace IDR.Web.Api
{
    [RoutePrefix("api/pdffiles")]
    public class PDFFilesController : ApiController
    {
        private readonly ApplicationDbContext _context;
        private readonly PDFService _pdfService;
        private readonly IAuditService _auditService;

        public PDFFilesController(ApplicationDbContext context, IAuditService auditService, PDFService pdfservice)
        {
            _context = context;
            _auditService = auditService;
            _pdfService = pdfservice;
        }

        //public async Task<PagedQueryResults<PDFFileDto>> Get([FromUri]PagingParameters paging, int companyId)
        //{
        //    return await _pdfService.SearchPDFs(paging, companyId);
        //}

        [Route("files/GetAllFiles")]
        [HttpGet]
        public async Task<List<PDFFileDto>> GetAllFiles(int companyId)
        {
            return await _pdfService.SearchPDFs(companyId);
        }

        [Route("files/readPDF")]
        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            //var file = await new GetImageForFileQuery(_context, id).Execute();
            var file = await _pdfService.GetPDFForFile(id);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file.PDF.ToArray())
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
            {
                FileName = file.FileName
            };

            var fileExt = new FileInfo(file.FileName).Extension;
            string contentType = "application/pdf";

            result.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);

            return result;
        }

        [Route("files/new")]
        [HttpPost]
        [Authorize]
        public async Task<bool> AddNew(int companyId)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            // Read each file into memory
            var provider = new MultipartMemoryStreamProvider();
            provider = await Request.Content.ReadAsMultipartAsync(provider);

            // Execute an upload task for every file
            var uploadTasks = provider.Contents.Select(c => UploadNew(c, companyId)).ToList();

            // Wait for all upload tasks to complete before responding
            var fileResults = (await Task.WhenAll(uploadTasks)).ToList();

            return fileResults.FirstOrDefault();
        }

        private async Task<bool> UploadNew(HttpContent file, int companyId)
        {

            // Read in the file from the context stream
            var fileStream = await file.ReadAsStreamAsync();

            var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);

            byte[] fileBytes;
            using (var streamReader = new MemoryStream())
            {
                fileStream.CopyTo(streamReader);
                fileBytes = streamReader.ToArray();
            }

            double fileSize = (fileBytes.Length / 1024) + 1;
            PdfDocument document = new PdfDocument();
            document.LoadFromBytes(fileBytes);
            int numberOfPages = document.Pages.Count;

            var response = await _pdfService.CreatePDFToFile(fileBytes, fileName, companyId, fileSize, numberOfPages);

            return response;
        }

        [Route("files/{id}/alias")]
        [Authorize]
        [HttpPost]
        public async Task<HttpResponseMessage> PostAlias([FromBody] PDFAlias model)
        {
            await _pdfService.UpdatePDFAlias(model.Id, model.Alias);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("files/{id}")]
        [HttpPost]
        [Authorize]
        public async Task<bool> Post(int id)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            // Read each file into memory
            var provider = new MultipartMemoryStreamProvider();
            provider = await Request.Content.ReadAsMultipartAsync(provider);

            // Execute an upload task for every file
            var uploadTasks = provider.Contents.Select(c => Upload(c, id)).ToList();

            // Wait for all upload tasks to complete before responding
            var fileResults = (await Task.WhenAll(uploadTasks)).ToList();

            return fileResults.FirstOrDefault();
        }

        private async Task<bool> Upload(HttpContent file, int id)
        {
            // Read in the file from the context stream
            var fileStream = await file.ReadAsStreamAsync();


            var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);


            byte[] fileBytes;
            using (var streamReader = new MemoryStream())
            {
                fileStream.CopyTo(streamReader);
                fileBytes = streamReader.ToArray();
            }

            double fileSize = (fileBytes.Length / 1024) + 1;
            PdfDocument document = new PdfDocument();
            document.LoadFromBytes(fileBytes);
            int numberOfPages = document.Pages.Count;

            var response = await _pdfService.UpdatePDFToFile(id, fileBytes, fileName, fileSize, numberOfPages);

            return response;
        }

        [HttpDelete]
        [Authorize]
        public async Task<HttpResponseMessage> Delete(int id)
        {
            await _pdfService.RemovePDF(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }

    public class PDFAlias
    {
        public int Id { get; set; }
        public string Alias { get; set; }
    }
}
