﻿using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Services;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;

namespace IDR.Web.Api
{
	[RoutePrefix("api/Facilities")]
	[Authorize(Roles = "Admin,Super Admin,Physician,Physicians Assistant,Practice Admin")]
	[NotMobile]
	[BearerRequired]
	public class FacilitiesController : ApiController
	{
		private readonly CompanyService _companyService;

		public FacilitiesController(CompanyService companyService)
		{
			_companyService = companyService;
		}

		[Route("{companyId}")]
		[Authorize]
		public async Task<List<FacilityDto>> Get(int companyId)
		{
			var facilities = await _companyService.GetFacilities(companyId);
			return  facilities;
		}

		[Route("{id}/{companyId}")]
		[Authorize]
		public async Task<FacilityDto> Get(int companyId, int id)
		{
			var facility = await _companyService.GetFacility(id);
			return facility;
		}

		public async Task<FacilityDto> Put([FromBody] FacilityDto disc)
		{
			var returnCat = await _companyService.UpdateFacility(disc);
			return returnCat;
		}

		[Route("new/{companyId}")]
		[HttpGet]
		public async Task<FacilityDto> New(int companyId)
		{
			var cat = await _companyService.AddFacility(companyId);

			return cat;
		}

		[Route("{id}/copy")]
		[HttpGet]
		public async Task<FacilityDto> Copy(int id)
		{
			var returnTemplate = await _companyService.CopyFacility(id);

			return returnTemplate;
		}

		[Route("{id}")]
		[HttpDelete]
		public async Task<HttpResponseMessage> Delete(int id)
		{
			await _companyService.DeleteFacility(id);
			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}
}
