﻿using System.Collections.Generic;
using IDR.Services;
using System.Web.Http;
using IDR.Web.App;
using IDR.Data;

namespace IDR.Web.Api
{
    [RoutePrefix("api/editablecontent")]
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class EditableContentController : ApiController
    {
        private readonly EditableContentService _editableContentService;

        public EditableContentController(EditableContentService editableContentService)
        {
            _editableContentService = editableContentService;
        }

        public List<EditableContent> Get()
        {
            return _editableContentService.GetContent();
        }

        public EditableContent Get(int id)
        {
            return _editableContentService.GetContent(id);
        }

        [HttpPost]
        public EditableContent Post(EditableContent model)
        {
            return _editableContentService.CreateContent(model);
        }

        [HttpPut]
        public EditableContent Put(EditableContent model)
        {
            return _editableContentService.UpdateContent(model);
        }

        //[HttpDelete]
        //public async Task<HttpResponseMessage> Delete(int id)
        //{
        //    await _notificationService.DeleteNotification(id);

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}
    }
}
