﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Dashboard;
using IDR.Domain.External.Dto;
using IDR.Domain.Portal.Dto;
using IDR.Services;
using IDR.Infrastructure;

namespace IDR.Web.Api
{
    [RoutePrefix("api/physician")]
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class PhysicianController : BaseApiController
    {
        private readonly PhysiciansService _physiciansService;
        private readonly NodeLibraryService _nodeLibraryService;
        private readonly ProcedureService _procedureService;
        private readonly DashboardService _dashboardService;

        public PhysicianController(PhysiciansService physiciansService,
            NodeLibraryService nodeLibraryService, ProcedureService procedureService, DashboardService dashboardService)
        {
            _physiciansService = physiciansService;
            _nodeLibraryService = nodeLibraryService;
            _procedureService = procedureService;
            _dashboardService = dashboardService;
        }

        [HttpGet]
        [Route("plans")]
        public async Task<PagedQueryResults<MasterTemplateExternalDto>> GetRecoveryPlans([FromUri]PagingParameters paging)
        {
            if (paging == null)
            {
                paging = new PagingParameters();
            }
            return await _physiciansService.GetPhysicianPlans(CurrentUserId, paging);
        }

        [HttpGet]
        [Route("plans/{id}/all")]
        public async Task<List<MasterTemplateExternalDto>> GetAllRecoveryPlans(string id)
        {
            return await _physiciansService.GetAllPhysicianPlans(id);
        }

        [HttpGet]
        [Route("plans/cpt/{id}")]
        public async Task<MasterTemplateExternalDto> GetPlanForCpt(string id, int? proxyId)
        {
            var plan = await _physiciansService.GetPlanForCptCode(CurrentUserId, id, proxyId);

            if (plan != null)
            {
                return plan;
            }

            return new MasterTemplateExternalDto();
        }

        [HttpPost]
        [Route("plans/add/{id}")]
        public async Task<HttpResponseMessage> AddLibrary(int id, [FromBody]MasterTemplateExternalDto lib, [FromUri]int? proxyId = null)
        {

            await _nodeLibraryService.CopyLibrary(lib.Id, CurrentUserId, proxyId);
            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpDelete]
        [Route("plans/{id}")]
        public async Task RemoveProcedure(int id)
        {
            await _physiciansService.RemoveProcedure(id);
        }

        [HttpGet]
        [Route("patients")]
        public async Task<PagedQueryResults<PatientDto>> GetPatients([FromUri]PagingParameters paging)
        {
            if (paging == null)
            {
                paging = new PagingParameters();
            }

            return await _physiciansService.GetPhysicianPatients(CurrentUserId, paging);
        }

        [HttpGet]
        [Route("patients/{id}")]
        public async Task<PatientDto> GetPatient(int id)
        {
            return await _physiciansService.GetPhysicianPatient(CurrentUserId, id);
        }

        [HttpPost]
        [Route("patients")]
        public async Task<PatientDto> CreatePatient(PatientDto model)
        {
            return await _physiciansService.CreatePhysicianPatient(CurrentUserId, model);
        }

        [HttpPut]
        [Route("patients/{id}")]
        public async Task UpdatePatient(int id, PatientDto model)
        {
            await _physiciansService.UpdatePhysicianPatient(model);
        }

        [HttpDelete]
        [Route("patients/{id}")]
        public async Task RemovePatient(int id)
        {
            await _physiciansService.RemovePhysicianPatient(id);
        }

        [HttpGet]
        [Route("patients/{id}/libraries")]
        public async Task<List<PatientLibraryDto>> GetPatientLibrary(int id)
        {
            var libraries = await _physiciansService.GetPatientLibraries(id);
            return libraries;
        }

        [HttpPost]
        [Route("patients/{id}/libraries")]
        public async Task<PatientProcedureDto> AddPatientLibrary(PatientProcedureDto model)
        {
            var procedure = await _procedureService.AddPatientProcedure(CurrentUserId, model);
            return procedure;
        }


        [HttpPut]
        [Route("patients/{id}/libraries")]
        public async Task<HttpResponseMessage> UpdatePatientLibrary(int id, [FromBody]PatientProcedureDto model)
        {
            try
            {
                await _procedureService.UpdatePatientProcedure(CurrentUserId, model);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("facilities")]
        public async Task<List<FacilityDto>> GetFacilities([FromUri]int? proxyId)
        {
            return await _physiciansService.GetFacilities(CurrentUserId, proxyId);
        }

        [HttpGet]
        [Route("mrn/{mrn}")]
        public async Task<PatientDto> GetPatientByMrn(string mrn, int? proxyId = null)
        {
            return await _physiciansService.GetPatientByMrn(mrn, CurrentUserId, proxyId);
        }

        [HttpGet]
        [Route("{id}/dashboard")]
        public async Task<List<DashboardItem>> GetDashboard(int id)//, [FromUri]DateTime? date
        {
            return await _dashboardService.GetDashboard(CurrentUserId, id);//date
        }
    }
}
