﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Configuration;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;

namespace IDR.Web.Api
{
    [RoutePrefix("api/mailtest")]
    public class MailTestController : ApiController
    {
        public HttpResponseMessage Get()
        {
            try
            {
                var configuration = new HttpConfiguration();
                Request = new System.Net.Http.HttpRequestMessage();
                Request.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
                SmtpSection smtpSection = (SmtpSection)ConfigurationManager.GetSection("mailSettings/smtp_2");
                //var client = new SmtpClient("smtp.sendgrid.net");
                var client = new SmtpClient(smtpSection.Network.Host, smtpSection.Network.Port);
                // var mail = new MailMessage("no-reply@infusystem.com", "sean.rudd@infusystem.com");
                client.EnableSsl = smtpSection.Network.EnableSsl;
                client.UseDefaultCredentials = smtpSection.Network.DefaultCredentials;
                client.DeliveryMethod = smtpSection.DeliveryMethod;
                client.Credentials = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                var mail = new MailMessage(smtpSection.From, "hardik.asnani@betsol.com");

                var now = DateTime.UtcNow.ToLongTimeString();
                mail.IsBodyHtml = true;
                mail.Subject = now;
                mail.Body = "BETSOL DEV team is currently testing the mail service - " + now + " UTC";

                client.Send(mail);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, ex.StackTrace);
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.OK, "Mail Sent Succesfullly");
        }
    }
}
