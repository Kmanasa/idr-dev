﻿using System.Collections.Generic;
using System.Configuration;
using IDR.Data;
using IDR.Domain.Cms.Dto;
using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Domain.External.Dto;
using IDR.Domain.Notifications;
using IDR.Infrastructure;

namespace IDR.Web.Api
{
	[RoutePrefix("api/nodelibraries")]
	[Authorize]
	[BearerRequired]
    public class NodeLibrariesController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly NodeLibraryService _nodeLibraryService;
	    private readonly NotificationService _notificationService;
	    private static readonly int _demoLibraryId;

        static NodeLibrariesController()
        {
            _demoLibraryId = int.Parse(ConfigurationManager.AppSettings["DemoLibraryId"]);
        }

        public NodeLibrariesController(NodeLibraryService nodeLibraryService, ApplicationDbContext context, NotificationService notificationService)
        {
            _nodeLibraryService = nodeLibraryService;
            _context = context;
            _notificationService = notificationService;
        } 
        
        public async Task<List<MasterTemplateExternalDto>> Get(string type)
        {
	        var libraries = await _nodeLibraryService.QueryMasterTemplates(type);

            if (libraries != null)
            {
                foreach (var lib in libraries)
                {
                    lib.IsDemoPlan = lib.Id == _demoLibraryId;
                }
            }

            return libraries;
        }

        public async Task<NodeLibraryDto> Put(int id, [FromBody] NodeLibraryDto app)
        {
	        var returnForm = await _nodeLibraryService.UpdateNodeLibrary(app);

			return returnForm;
		}

        public async Task<NodeLibraryDto> Delete(int id)
        {
	        var returnForm = await _nodeLibraryService.DeleteNodeLibrary(id);
			return returnForm;
        }

        [HttpGet]
        [Route("search")]
        public async Task<PagedQueryResults<MasterTemplateExternalDto>> Search([FromUri]string term, [FromUri]PagingParameters paging)
        {
            if (paging == null)
            {
                paging = new PagingParameters();
            }

            return await _nodeLibraryService.SearchNodeLibraries(term, paging);
        }

	    [HttpGet]
	    [Route("{id}/notifications")]
	    public async Task<List<NotificationDto>> GetNotifications(int id)
	    {
	        return await _notificationService.GetPlanNotifications(id);
	    }
    }
}
