﻿using System.Net;
using System.Net.Http;
using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;

namespace IDR.Web.Api
{
    [RoutePrefix("api/companies")]
	[Authorize]//(Roles = "Admin,Super Admin")]
	[NotMobile]
	[BearerRequired]
    public class CompaniesController : ApiController
	{
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
        private readonly CompanyService _companyService;

        public CompaniesController(CompanyService companyService, ApplicationDbContext context, IAuditService auditService)
        {
            _context = context;
            _auditService = auditService;
            _companyService = companyService;
        }

        [Authorize]
        public async Task<PagedQueryResults<CompanySearchDto>> Get([FromUri]PagingParameters paging)
        {
            return await _companyService.Search(paging);
        }

        [Authorize]
        public async Task<CompanyDto> Get(int id)
        {
            return await _companyService.GetCompany(id);
        }

        public async Task<CompanyDto> Put([FromBody] CompanyDto model)
        {
            return await _companyService.UpdateCompany(model);
        }

        [Route("new")]
        [HttpGet]
        public async Task<CompanyDto> New()
        {
            return await _companyService.AddCompany();
        }

        [Route("{id}/copy")]
        [HttpGet]
        public async Task<CompanyDto> Copy(int id)
        {
            return await _companyService.CopyCopmany(id);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            _companyService.DeleteCompany(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
