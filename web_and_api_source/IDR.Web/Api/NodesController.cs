﻿using System.Linq;
using IDR.Data;
using IDR.Domain.Cms.Dto;
using IDR.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Services.Models.PDFs;

namespace IDR.Web.Api
{
	[RoutePrefix("api/nodes")]
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class NodesController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
	    private readonly NodeLibraryService _nodeLibraryService;
        private readonly PDFService _pdfService;

        public NodesController(NodeLibraryService nodeLibraryService, ApplicationDbContext context, IAuditService auditService, PDFService pdfService)
		{
			_context = context;
			_auditService = auditService;
            _nodeLibraryService = nodeLibraryService;
            _pdfService = pdfService;
		}

		[Route("copy/{id}")]
		[HttpGet]
		public async Task<NodeDto> Copy(int id)
		{
		    return await _nodeLibraryService.CopyNode(id);
		}
        
        [Route("app/copy/{id}")]
        [HttpGet]
        public async Task<NodeLibraryDto> CopyLibrary(int id)
        {
            return await _nodeLibraryService.CopyLibrary(id);
        }

		[Route("{id}")]
		public async Task<NodeLibraryDto> Get(int id)
		{
		    return await _nodeLibraryService.GetNodeLibrary(id, loadDisciplines:true);
        }

        [Route("templates")]
        [HttpGet]
        public async Task<List<NodeLibraryDto>> GetTemplates(string idList)
        {
            // TODO: refacror
            var idArray = idList.Split(',').Select(int.Parse).ToArray();
	        var aps = await _nodeLibraryService.GetNodeLibraryTemplates(idArray);

	        return aps;
        }
        
        [Route("new/{id}/{type}")]
        [HttpGet]
        public async Task<NodeLibraryDto> New(string id, string type)
        {
            return await _nodeLibraryService.CreateNodeLibrary(type);
        }

		[HttpGet]
		[Route("library/{type}")]
		public async Task<List<NodeLibraryDto>> Get(string type)
		{
		    return await _nodeLibraryService.GetNodeLibraries(type);
		}

		public async Task<NodeDto> Post([FromBody] Node node)
		{
		    return await _nodeLibraryService.AddNode(node);
		}
        
        public async Task<NodeDto> Put(int id, [FromBody]Node node)
        {
            return await _nodeLibraryService.UpdateNode(node);
		}

		[HttpDelete]
        public async Task Delete(int id)
		{
		    await _nodeLibraryService.DeleteNode(id);
		}

        [Route("deleteplan")]
        [HttpDelete]
        public async Task<bool> DeletePlan(int id)
        {
            return await _nodeLibraryService.DeletePlan(id);
        }

    }
}
