﻿using IDR.Data;
//using IDR.Domain.Images.Commands;
using IDR.Domain.Images.Dto;
//using IDR.Domain.Images.Queries;
using IDR.Services;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http.Headers;
using System.IO;
using IDR.Infrastructure;


namespace IDR.Web.Api
{
	[RoutePrefix("api/imagefiles")]
    public class ImageFilesController : ApiController
    {
        private readonly ApplicationDbContext _context;
	    private readonly ImageService _imageService;
		private readonly IAuditService _auditService;
		private int _id;

		public ImageFilesController( ApplicationDbContext context, IAuditService auditService, ImageService imageService)
		{
			_context = context;
			_auditService = auditService;
		    _imageService = imageService;
		}

		public async Task<PagedQueryResults<ImageFileDto>> Get([FromUri]PagingParameters paging)
		{
		    return await _imageService.SearchImages(paging);
		}

        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            //var file = await new GetImageForFileQuery(_context, id).Execute();
	        var file = await _imageService.GetImageForFile(id);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file.Image.ToArray())
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
            {
                FileName = file.FileName
            };

            var fileExt = new FileInfo(file.FileName).Extension;
            string contentType;

            switch (fileExt.ToLower())
            {
                default:
                    contentType = "image/octet-stream";
                    break;

                case ".jpg":
                case ".jpeg":
                    contentType = "image/jpeg";
                    break;

                case ".bmp":
                    contentType = "image/bmp";
                    break;

                case ".gif":
                    contentType = "image/gif";
                    break;

                case ".png":
                    contentType = "image/png";
                    break;
            }

            result.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);

            return result;
        }

        [Route("files/new")]
		[HttpPost]
		[Authorize]
		public async Task<ImageFileDto> AddNew()
		{
			if (!Request.Content.IsMimeMultipartContent())
			{
				throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
			}

			// Read each file into memory
			var provider = new MultipartMemoryStreamProvider();
			provider = await Request.Content.ReadAsMultipartAsync(provider);

			// Execute an upload task for every file
			var uploadTasks = provider.Contents.Select(UploadNew).ToList();

			// Wait for all upload tasks to complete before responding
			var fileResults = (await Task.WhenAll(uploadTasks)).ToList();

			return fileResults.FirstOrDefault();
		}

		[Route("files/{id}")]
		[HttpPost]
		[Authorize]
		public async Task<string> Post(int id)
		{
			if (!Request.Content.IsMimeMultipartContent())
			{
				throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
			}

			// Read each file into memory
			var provider = new MultipartMemoryStreamProvider();
			provider = await Request.Content.ReadAsMultipartAsync(provider);

			// Execute an upload task for every file
			var uploadTasks = provider.Contents.Select(c => Upload(c, id)).ToList();

			// Wait for all upload tasks to complete before responding
			var fileResults = (await Task.WhenAll(uploadTasks)).ToList();

			return fileResults.FirstOrDefault();
        }

	    [HttpDelete]
	    [Authorize]
	    public async Task<HttpResponseMessage> Delete(int id)
	    {
	        await _imageService.RemoveImage(id);
	        return Request.CreateResponse(HttpStatusCode.OK);
	    }

	    [Route("files/{id}/alias")]
        [Authorize]
        [HttpPost]
        public async Task<HttpResponseMessage> PostAlias([FromBody] ImageAlias model)
	    {
		    await _imageService.UpdateImageAlias(model.Id, model.Alias);
			
			return Request.CreateResponse(HttpStatusCode.OK);
	    }

        [Route("files/associatephysician")]
        [HttpGet]
        [Authorize]
        public async Task<HttpResponseMessage> AssociatePhysician([FromUri]int id, [FromUri]int entityId)
        {
            await _imageService.AssociatePhysicianImage(id, entityId);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private async Task<string> Upload(HttpContent file, int id)
        {
            // Read in the file from the context stream
            var fileStream = await file.ReadAsStreamAsync();


            var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);


            byte[] fileBytes;
            using (var streamReader = new MemoryStream())
            {
                fileStream.CopyTo(streamReader);
                fileBytes = streamReader.ToArray();
            }

            //await new UpdateImageToFileCommand(_context, _id, result, fileName, _auditService).Execute();
	        await _imageService.UpdateImageToFile(id, fileBytes, fileName);

            return fileName;
        }

        private async Task<ImageFileDto> UploadNew(HttpContent file)
        {
            // Read in the file from the context stream
            var fileStream = await file.ReadAsStreamAsync();

            var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);

            byte[] fileBytes;
            using (var streamReader = new MemoryStream())
            {
                fileStream.CopyTo(streamReader);
                fileBytes = streamReader.ToArray();
            }

	        var response = await _imageService.CreateImageToFile(fileBytes, fileName);

            return response;
        }
    }

    public class ImageAlias
    {
        public int Id { get; set; }
        public string Alias { get; set; }
    }
}
