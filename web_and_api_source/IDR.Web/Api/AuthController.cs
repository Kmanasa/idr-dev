﻿using IDR.Data;
using IDR.Domain.Membership.Dto;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Services;
using IDR.Web.App;

namespace IDR.Web.Api
{
	[RoutePrefix("api/auth")]
	[Authorize]
	[NotMobile]
	[BearerRequired]
    public class AuthController : ApiController
	{
		private readonly ApplicationDbContext _context;
		private readonly UsersService _usersService;

		public AuthController(ApplicationDbContext context, UsersService usersService)
		{
			_context = context;
			_usersService = usersService;
		}

        //[Route("current")]
        //public async Task<UserDto> Get()
        //{
	       // return await _usersService.GetUser(User.Identity.Name);
			
        //}
		[Route("current")]
		public  UserDto Get()
		{
			return  _usersService.GetUser(User.Identity.Name);

		}

		// POST: api/Auth
		public void Post([FromBody]string username, string password)
        {
        }

        // PUT: api/Auth/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Auth/5
        public void Delete(int id)
        {
        }
    }
}
