﻿using System;
using IDR.Data;
using IDR.Domain.Portal.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Web.Mailers;

namespace IDR.Web.Api
{
	[RoutePrefix("api/adminphysicians")]
	[Authorize(Roles="Admin,Super Admin,Practice Admin,Scheduler")]
	[NotMobile]
	[BearerRequired]
    public class AdminPhysiciansController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
	    private readonly PhysiciansService _physiciansService;
        private readonly IInvitationMailer _invitationMailer;

        public AdminPhysiciansController(ApplicationDbContext context,
            IAuditService auditService, 
            PhysiciansService physiciansService,
            IInvitationMailer invitationMailer)
		{
			_context = context;
            _auditService = auditService;
            _physiciansService = physiciansService;
            _invitationMailer = invitationMailer;
		}

		// GET: api/physicians
		[Route("{practiceId}")]
		[HttpGet]
		public async Task<IEnumerable<PhysicianDto>> Get(int practiceId)
        {
		    var physicians = await _physiciansService.GetPhysiciansForPractice(practiceId);
		    return physicians;
        }

		[HttpGet]
		[Route("{id}/{practiceId}")]
        public async Task<PhysicianDto> Get(int practiceId, int id)
        {
		    var physician = await _physiciansService.GetPhysician(id);
			return physician;
        }

		[HttpGet]
		[Route("new/{practiceId}")]
		public async Task<PhysicianDto> New(int practiceId)
		{
		    var physician = await _physiciansService.AddPhysician(practiceId);
		    return physician;
		}

		[HttpPut]
        public async Task<PhysicianDto> Put([FromBody] PhysicianDto physician)
        {
		    var model = await _physiciansService.UpdatePhysician(physician);
		    return model;
        }

		[Route("invite/{id}")]
		[HttpGet]
		public async Task<OperationResponse<int>> Invite(int id)
		{
			return await InvitePhysician(id);
		}

		[HttpDelete]
		public async Task<OperationResponse<int>> Delete(int id)
		{
			return await _context.Delete<Physician>(_auditService, id);
		}

        [Route("deactivate/{id}")]
        [HttpPost]
        public HttpResponseMessage Deactivate(int id)
        {
            _context.Deactivate<Physician>(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private async Task<OperationResponse<int>> InvitePhysician(int id)
        {
            var response = new OperationResponse<int>();
            try
            {
                var physician = await _context.Physicians.FirstOrDefaultAsync(x => x.Id == id);

                _invitationMailer.Send(physician);

                response.Succeed($"An invitiation has been emailed to {physician.ProvisionalEmail}");
            }
            catch (Exception ex)
            {
                response.Exception(ex);
            }
            return response;
        }
    }
}
