﻿using System.Web.Http;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Services;

namespace IDR.Web.Api.CallCenter
{
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class CallCenterRefsController : BaseApiController
    {
	    private readonly CallCenterService _callCenterService;

        public CallCenterRefsController(
			CallCenterService callCenterService
			)
        {
	        _callCenterService = callCenterService;
        }

        [HttpGet]
        [Route("api/callcenterrefs")]
        public async Task<CallCenterRefDto> Get()
        {
	        return await _callCenterService.GetCallCenterRefs(CurrentUserId);
        }
	}
}
