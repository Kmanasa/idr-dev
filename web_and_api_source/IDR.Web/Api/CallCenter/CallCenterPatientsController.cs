﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Infrastructure;
using IDR.Web.App;
using IDR.Services;
using IDR.Services.Models.CallCenter;

namespace IDR.Web.Api.CallCenter
{
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class CallCenterPatientsController : BaseApiController
    {
	    private readonly CallCenterService _callCenterService;

        public CallCenterPatientsController(
			CallCenterService callCenterService
			)
        {
	        _callCenterService = callCenterService;
        }

        [HttpGet]
        [Route("api/callcenterpatients")]
        public async Task<PagedQueryResults<CallCenterPatientDto>> GetPatients([FromUri]PagingParameters paging)
        {
			var filter =  new CallCenterPatientSearchModel();
	        filter.Filter = GetQueryString(Request,"filter");

	        try
	        {
		        var typeVal = GetQueryString(Request, "filterType");
		        int.TryParse(typeVal, out var filterType);

		        filter.FilterType = (PatientFilterType)filterType;
	        }
			catch { }

	        filter.Paging = paging;
	        return await _callCenterService.GetPatients(CurrentUserId, filter);
        }

	    [HttpGet]
	    [Route("api/callcenterpatients/{id}")]
	    public async Task<CallCenterPatientDto> GetPatient(int id)
	    {
		    return await _callCenterService.GetPatient(id);
	    }

		[HttpPost]
		[Route("api/callcenterpatients")]
		public async Task<CallCenterPatientDto> CreatePatient(CallCenterPatientDto patient)
		{
			return await _callCenterService.CreatePatient(patient);
		}

	    [HttpPut]
	    [Route("api/callcenterpatients")]
	    public async Task<CallCenterPatientDto> UpdatePatient(CallCenterPatientDto patient)
	    {
			return await _callCenterService.UpdatePatient(patient);
		}

		[HttpDelete]
		[Route("api/callcenterpatients/{id}")]
		public async Task<bool> DeletePatient(int id)
		{
			return await _callCenterService.DeletePatient(CurrentUserId, id);
		}

		public static string GetQueryString(HttpRequestMessage request, string key)
	    {
		    var queryStrings = request.GetQueryNameValuePairs();
		    if (queryStrings == null)
		    {
			    return null;
		    }

		    var match = queryStrings.FirstOrDefault(kv => string.Compare(kv.Key, key, true) == 0);
		    return string.IsNullOrEmpty(match.Value) ? null : match.Value;
	    }

        [HttpGet]
        [Route("api/callcenterpatients/SendTextMessage")]
        public async Task<bool> SendTextMessage(int patientId, string phone, int messageType, string messageContent)
        {
            return await _callCenterService.SendTextMessage(patientId, phone, messageType, messageContent);
        }

        [Route("api/callcenterpatients/managepatients")]
        public async Task<PagedQueryResults<CallCenterPatientDto>> GetManagePatients([FromUri]PagingParameters paging)
        {
            var filter = new CallCenterPatientSearchModel();
            filter.Filter = GetQueryString(Request, "filter");

            try
            {
                var typeVal = GetQueryString(Request, "filterType");
                int.TryParse(typeVal, out var filterType);

                filter.FilterType = (PatientFilterType)filterType;
            }
            catch { }

            filter.Paging = paging;
            return await _callCenterService.GetPatients(CurrentUserId, filter, true);
        }
    }
}
