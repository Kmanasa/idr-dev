﻿using System.Collections.Generic;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Services;
using IDR.Infrastructure;
using IDR.Services.Models.CallCenter;

namespace IDR.Web.Api.CallCenter
{
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class CallCenterProvidersController : BaseApiController
    {
	    private readonly CallCenterService _callCenterService;

        public CallCenterProvidersController(CallCenterService callCenterService)
        {
	        _callCenterService = callCenterService;
        }

        [HttpGet]
        [Route("api/callcenterproviders")]
        public async Task<PagedQueryResults<CallCenterProviderDto>> Get([FromUri]PagingParameters paging)
        {
            if (paging == null)
            {
                paging = new PagingParameters();
            }

	        return await _callCenterService.GetProviders(paging);
        }

	    [HttpGet]
	    [Route("api/callcenterproviders/{id}")]
	    public async Task<CallCenterProviderDto> GetProvider(int id)
	    {
		    return await _callCenterService.GetProvider(id);
	    }

		[HttpPost]
	    [Route("api/callcenterproviders")]
	    public async Task<CallCenterProviderDto> CreateProvidert(CallCenterProviderDto provider)
	    {
			return await _callCenterService.CreateProvider(provider, CurrentUserId);
		}

	    [HttpPut]
	    [Route("api/callcenterproviders")]
	    public async Task<CallCenterProviderDto> UpdatePatient(CallCenterProviderDto provider)
	    {
		    return await _callCenterService.UpdateProvider(provider, CurrentUserId);
	    }

	    [HttpDelete]
	    [Route("api/callcenterproviders/{id}")]
	    public async Task<bool> DeleteProvider(int id)
	    {
		    return await _callCenterService.DeleteProvider(id);
	    }

		[HttpGet]
	    [Route("api/callcenterproviders/facilities")]
	    public async Task<List<CallCenterFacilityTypeaheadDto>> GetFacilities([FromUri]string filter)
	    {
		    return await _callCenterService.GetFilteredFacilities(filter);
	    }
	}
}
