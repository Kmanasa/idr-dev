﻿using System.Threading.Tasks;
using System.Web.Http;
using IDR.Services;
using IDR.Services.Models.CallCenter;
using IDR.Web.App;

namespace IDR.Web.Api.CallCenter
{
	[Authorize]
	[NotMobile]
	[BearerRequired]
	public class CallCenterCommentsController : BaseApiController
	{
		private readonly CallCenterService _callCenterService;

		public CallCenterCommentsController(CallCenterService callCenterService
		)
		{
			_callCenterService = callCenterService;
		}

		public async Task<CallCenterCommentDto> Post(CallCenterCommentDto model)
		{
			return await _callCenterService.CreateComment(model);
		}
	}
}