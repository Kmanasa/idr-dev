﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Domain.CallCenter;
using IDR.Services;
using IDR.Services.Models.CallCenter;
using IDR.Web.App;

namespace IDR.Web.Api.CallCenter
{
	[Authorize]
	[NotMobile]
	[BearerRequired]
	public class CallCenterScheduleController : BaseApiController
	{
		private readonly CallCenterService _callCenterService;

		public CallCenterScheduleController(CallCenterService callCenterService
		)
		{
			_callCenterService = callCenterService;
		}

		public async Task<List<CallCenterScheduleDto>> Get(int year, int month, int day)
		{
			return await _callCenterService.GetSchecule(year, month, day);
		}

		public async Task<CallCenterCallDetailDto> Get(int id)
		{
			return await _callCenterService.GetCall(id, CurrentUserId);
		}

		[HttpDelete]
		[Route("api/callcenterschedule/{id}")]
		public async Task<bool> Delete(int id)
		{
			return await _callCenterService.DeleteCall(id);
		}

		[HttpGet]
		[Route("api/callcenterschedule/aggregates")]
		public async Task<CallCenterAggregates> Aggregates(int year, int month, int day)
		{
			return await _callCenterService.GetAggregates(CurrentUserId, year, month, day);
		}

		[HttpGet]
		[Route("api/callcenterschedule/next")]
		public async Task<NextCallId> Next(int year, int month, int day)
		{
			return await _callCenterService.GetNextCall(CurrentUserId, year, month, day);
		}

		[HttpGet]
		[Route("api/callcenterschedule/lock")]
		public async Task<NextCallId> Lock(int id, bool isSuperAdmin)
		{
			return await _callCenterService.LockCall(CurrentUserId, id, isSuperAdmin);
		}

		[HttpGet]
		[Route("api/callcenterschedule/cancel")]
		public async Task<bool> Cancel(int id)
		{
			return await _callCenterService.CancelCall(id);
		}

		[HttpPost]
		public async Task<bool> Post(AssociatedCallDto model)
		{
			return await _callCenterService.SaveAssessment(model);
		}


		[HttpPost]
		[Route("api/callcenterschedule/{id}/callback")]
		public async Task<bool> Callback(CallbackModel model)
		{
			return await _callCenterService.CallBack(model.Id, model.Hour, model.Minute);
		}

		[HttpGet]
		[Route("api/callcenterschedule/{id}/calltomorrow")]
		public async Task<CallTomorrow> CallTomorrow(int id)
		{
			return await _callCenterService.CallTomorrow(id);
		}

		[HttpPost]
		[Route("api/callcenterschedule/{id}/discharge")]
		public async Task<bool> Discharge(DischargeModel model)
		{
			return await _callCenterService.Discharge(model.Id, model.Satisfaction, model.FollowUp, model.FollowUpHour, model.FollowUpMinute);
		}

		[HttpGet]
		[Route("api/callcenterschedule/summary")]
		public async Task<List<IGrouping<string, SummaryDto>>> Summary(int year, int month, int day)
		{
			return await _callCenterService.GetSummary(CurrentUserId, year, month, day);
		}

		[HttpGet]
		[Route("api/callcenterschedule/summaryoverview")]
		public async Task<SummaryOverviewDto> SummaryOverview(int year, int month, int day)
		{
			return await _callCenterService.GetSummaryOverview(CurrentUserId, year, month, day);
		}

		[HttpGet]
		[Route("api/callcenterschedule/CallHistory/{id}")]
		public async Task<List<SummaryDto>> CallHistory(int id)
		{
			return await _callCenterService.GetCallHistory(id);
		}

        [HttpPost]
        [Route("api/callcenterschedule/{id}/scheduleacalltomorrow")]
        public async Task ScheduleACallTomorrow(int id)
        {
            await _callCenterService.ScheduleACallTomorrow(id);
        }

        [HttpPost]
        [Route("api/callcenterschedule/{id}/scheduleanewcalltime")]
        public async Task<bool> ScheduleANewCallTime(CallbackModel model)
        {
            return await _callCenterService.ScheduleANewTimeTomorrow(model.Id, model.Hour, model.Minute);
        }
        [HttpGet]
        [Route("api/callcenterschedule/checkforadditionalquestions")]
        public async Task<PracticeDto> CheckForAdditionalQuestions(int practiceId)
        {
            return await _callCenterService.CheckForAdditionalQuestions(practiceId);
        }

        [HttpGet]
        [Route("api/callcenterschedule/checkforaddURL")]
        public async Task<Additional_URL> CheckForAddURL(int practiceId)
        {
            return await _callCenterService.CheckForAddURL(practiceId);
        }
    }

	public class CallbackModel
	{
		public int Id { get; set; }
		public int Hour { get; set; }
		public int Minute { get; set; }
	}

	public class DischargeModel
	{
		public int Id { get; set; }
		public int Satisfaction { get; set; }
		public bool FollowUp { get; set; }
        public int FollowUpHour { get; set; }
        public int FollowUpMinute { get; set; }
	}
}