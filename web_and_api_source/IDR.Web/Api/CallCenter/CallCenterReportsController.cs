﻿using System.Collections.Generic;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Data;
using IDR.Web.App;
using IDR.Services;
using IDR.Services.Models.CallCenter;

namespace IDR.Web.Api.CallCenter
{
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class CallCenterReportsController : BaseApiController
    {
	    private readonly CallCenterService _callCenterService;

        public CallCenterReportsController(CallCenterService callCenterService)
        {
	        _callCenterService = callCenterService;
        }

        [HttpGet]
        [Route("api/callcenterreports")]
        public async Task<List<Report>> Get()
        {
	        return await _callCenterService.GetReports();
        }

        [Route("api/callcenterreports/AuthenticateUserForReports")]
        [HttpGet]
        public async Task<CallCenterReportDto> AuthenticateForCallCenterReports()
        {
            return await _callCenterService.AuthenticateForCallCenterReports();
        }

        //[HttpGet]
        //[Route("api/callcenterreports/facilities")]
        //public async Task<List<Report>> GetFacilities()
        //{
        // return await _callCenterService.GetFacilities(CurrentUserId);
        //}
    }
}
