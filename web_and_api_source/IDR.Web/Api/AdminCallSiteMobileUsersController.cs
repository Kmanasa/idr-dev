﻿using System;
using IDR.Data;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Web.Mailers;

namespace IDR.Web.Api
{
	//[RoutePrefix("api/calladmins")]
	[Authorize(Roles = "Admin,Super Admin,Practice Admin,Scheduler,")]
	[NotMobile]
	[BearerRequired]
    public class AdminCallSiteMobileUsersController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
        private readonly IInvitationMailer _invitationMailer;
        private readonly CallSiteMobileUserService _service;

        public AdminCallSiteMobileUsersController(
            
            ApplicationDbContext context,
            IAuditService auditService,
            IInvitationMailer mailer,
            CallSiteMobileUserService service)
		{
			_context = context;
            _auditService = auditService;
            _invitationMailer = mailer;
		    _service = service;
		}
        
		[HttpGet]
		[Route("api/callmobileusers/{id}")]
		public async Task<CallSiteMobileUserDto> Get(int id)
		{
		    return await _service.GetUser(id);
        }

		[HttpGet]
		[Route("api/callmobileusers/new")]
		public async Task<CallSiteMobileUserDto> New(int practiceId)
		{
		    return await _service.Create(practiceId);
		}

		[HttpPut]
		[Route("api/callmobileusers/")]
		public async Task<CallSiteMobileUserDto> Put([FromBody] CallSiteMobileUserDto admin)
        {
		    return await _service.Update(admin);
		}

		[Route("api/callmobileusers/invite/{id}")]
		[HttpGet]
		public async Task<OperationResponse<int>> Invite(int id)
		{
		    return await InviteUser(id);
		}

		[HttpDelete]
		public async Task<OperationResponse<int>> Delete(int id)
		{
			return await _context.Delete<Scheduler>(_auditService, id);
		}

        [Route("api/callmobileusers/deactivate/{id}")]
        [HttpPost]
        public HttpResponseMessage Deactivate(int id)
        {
            _context.Deactivate<Scheduler>(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public object BaseUserHelper { get; set; }

        private async Task<OperationResponse<int>> InviteUser(int id)
        {
            var response = new OperationResponse<int>();
            try
            {
                var admin = await _context.CallSiteMobileUsers.FirstOrDefaultAsync(x => x.Id == id);

                _invitationMailer.Send(admin);

                response.Succeed($"An invitiation has been emailed to {admin.ProvisionalEmail}");
            }
            catch (Exception ex)
            {
                response.Exception(ex);
            }
            return response;
        }
    }
}
