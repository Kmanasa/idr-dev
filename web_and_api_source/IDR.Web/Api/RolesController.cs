﻿using IDR.Domain.Membership;
using System.Collections.Generic;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Data;
using IDR.Services;

namespace IDR.Web.Api
{
	[RoutePrefix("api/roles")]
    [Authorize(Roles="Admin,Super Admin,Practice Admin")]
	[NotMobile]
	[BearerRequired]
    public class RolesController : ApiController
	{
		private readonly ApplicationDbContext _context;
		private readonly UsersService _usersService;

		public RolesController(ApplicationDbContext context, UsersService usersService)
		{
			_context = context;
			_usersService = usersService;
		}

        public async Task<List<RoleDto>> Get()
        {
			var roles = await _usersService.GetRoles(User.Identity.GetUserId());

	        return roles;
        }

        [HttpGet]
        [Route("admin")]
        public async Task<List<RoleDto>> GetAdmin()
        {
	        var roles = await _usersService.GetRoles(User.Identity.GetUserId(), true);

	        return roles;
        }
    }
}
