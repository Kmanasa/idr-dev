﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Data;
using IDR.Domain;
using IDR.Services;

namespace IDR.Web.Api.External
{
	[RoutePrefix("api/external/procedures")]
	//[Authorize]
	//[BearerRequired]
	public class ProceduresController : ApiController
	{
		private readonly ProcedureService _procedureService;

		private static readonly string DemoToken;
		private static readonly int DemoLibraryId;

		static ProceduresController()
		{
			DemoToken = ConfigurationManager.AppSettings["DemoAccessToken"];
			DemoLibraryId = int.Parse(ConfigurationManager.AppSettings["DemoLibraryId"]);
		}

		public ProceduresController(ProcedureService procedureService)
		{
			_procedureService = procedureService;
		}

		public async Task<ProcedureRefDto> Get(string id)
		{
			if (id.ToLower() != DemoToken.ToLower())
			{
				var model = await _procedureService.GetProcedureRefs(id);
				return model;
			}

			return _procedureService.GetMockProcedureRefs(id, DemoLibraryId);
		}

		[Route("api/external/byid/{id}")]
		public Procedure GetById([FromUri]int id)
		{
			var model = _procedureService.GetProcedureById(id);
			return model;
		}

		[Route("testdrive/{id}")]
		[HttpGet]
		public async Task<ProcedureRefDto> GetTestDrive(int id)
		{
			return _procedureService.GetMockProcedureRefs(id.ToString(), id);
		}

		[Route("logdevice")]
		[HttpPost]
		[AllowAnonymous]
		public bool LogDevice(LogDeviceModel model)
		{
			try
			{
				_procedureService.LogDevice(model);
			}
			catch { }
			return true;
		}

		[Route("{id}/inbox")]
		public async Task<List<InboxItemDto>> GetInbox(string id)
		{
			var inbox = await _procedureService.GetInboxItems(id);
			return inbox;
		}

		[Route("logtimerstart")]
		[HttpPost]
		[AllowAnonymous]
		public string LogTimerStart(LogTimerModel model)
		{
			try
			{
				_procedureService.LogTimerStart(model);
				return "ok";
			}
			catch (Exception ex)
			{
				//var a = ex;
				return "fault";// ex.Message + " " + ex.StackTrace;
			}
		}

		[Route("logtimer")]
		[HttpPost]
		[AllowAnonymous]
		public string LogTimerEnd(LogTimerModel model)
		{
			try
			{
				_procedureService.LogTimer(model);
				return "ok";
			}
			catch (Exception ex)
			{
				return "fault";// ex.Message + " " + ex.StackTrace;
			}
		}

        [HttpGet]
        [Route("SendPushNotification")]
        public async Task<bool> SendPushNotification(int patientId, string phone, int messageType, string messageContent)
        {
            return await _procedureService.SendPushNotification(patientId, phone, messageType, messageContent);
        }
    }
}
