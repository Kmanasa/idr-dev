﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Data;
using IDR.Domain.CallCenter;
using IDR.Services;
using IDR.Services.Models.CallCenter;

namespace IDR.Web.Api.External
{
	[RoutePrefix("api/external")]
	//[Authorize]
	public class MobileConfigController : ApiController
	{
		private readonly EditableContentService _editableContentService;
		private readonly CallCenterService _callCenterService;

		public MobileConfigController(EditableContentService editableContentService,
			CallCenterService callCenterService)
		{
			_editableContentService = editableContentService;
			_callCenterService = callCenterService;
		}

		public async Task<MobileConfig> Get()
		{
			var model = new MobileConfig
			{
				EditableContent = _editableContentService.GetContent(),
				ProcedureCodes = await _callCenterService.GetProcedureCptCodes()
			};

			return model;
		}
	}


	public class MobileConfig
	{
		public MobileConfig()
		{
			EditableContent = new List<EditableContent>();
			ProcedureCodes = new List<CallCenterFacilityCpt>();
		}

		public List<EditableContent> EditableContent { get; set; }
		public List<CallCenterFacilityCpt> ProcedureCodes { get; set; }
	}
}