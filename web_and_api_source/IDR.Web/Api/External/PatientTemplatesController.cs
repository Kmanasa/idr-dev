﻿using IDR.Domain.Cms.Dto;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Services;

namespace IDR.Web.Api.External
{
	[RoutePrefix("api/external/patienttemplates")]
	[Authorize]
	[BearerRequired]
	public class PatientTemplatesController : ApiController
    {
	    private readonly NodeLibraryService _nodeLibraryService;

        public PatientTemplatesController(NodeLibraryService nodeLibraryService)
        {
            _nodeLibraryService = nodeLibraryService;
        }

        public async Task<NodeLibraryDto> Get(string id)
        {
            return await _nodeLibraryService.GetPatientNodeLibrary(id);
        }
    }
}
