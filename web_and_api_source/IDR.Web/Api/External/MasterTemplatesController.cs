﻿using System;
using System.Collections.Generic;
using IDR.Domain.Cms.Dto;
using IDR.Domain.External.Dto;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Services;

namespace IDR.Web.Api.External
{
	[RoutePrefix("api/external/mastertemplates")]
	//[Authorize]
    public class MasterTemplatesController : ApiController
    {
	    private readonly NodeLibraryService _nodeLibraryService;

        public MasterTemplatesController(NodeLibraryService nodeLibraryService)
        {
            _nodeLibraryService = nodeLibraryService;
        }

	    public async Task<List<MasterTemplateExternalDto>> Get()
	    {
	        return await _nodeLibraryService.GetNodeLibrariesExternal();
	    }

        [AllowAnonymous]
        public async Task<NodeLibraryDto> Get(int id)
        {
            try
            {
                var library = await _nodeLibraryService.GetNodeLibrary(id, true);

                foreach (var node in library.Nodes)
                {
                    FixLocalImageUrls(node);
                }

                return library;
            }
            catch(Exception ex)
            {
                return new NodeLibraryDto
                {
                    Description = ex.Message + ex.StackTrace
                };
            }
        }

	    private void FixLocalImageUrls(NodeDto node)
	    {
            if(node == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(node.HtmlContent))
	        {
	            var fullUrl = "src=\"https://" + Request.RequestUri.Host + "/api/imagefiles/";

	            node.HtmlContent = node.HtmlContent.Replace("src=\"api/imagefiles/", fullUrl);
	            node.HtmlContent = node.HtmlContent.Replace("src=\"/api/imagefiles/", fullUrl);
	        }

	        if (node.Nodes != null)
	        {
	            foreach (var child in node.Nodes)
	            {
	                FixLocalImageUrls(child);
	            }
	        }
	    }
    }
}
