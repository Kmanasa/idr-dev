﻿using System.Threading.Tasks;
using System.Web.Http;
using IDR.Services;
using IDR.Services.Models.CallCenter;

namespace IDR.Web.Api.External
{
	[RoutePrefix("api/external/register")]
	public class RegisterController : ApiController
	{
		private readonly CallCenterService _callCenterService;

		public RegisterController(CallCenterService callCenterService)
		{
			_callCenterService = callCenterService;
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<CallCenterPatientDto> Register(CallCenterPatientDto model)
		{
			model.Mobile = true;
			return await _callCenterService.RegisterMobileProcedure(model);
		}
	}
}
