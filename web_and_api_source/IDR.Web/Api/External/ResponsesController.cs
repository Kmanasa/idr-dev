﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Data;
using IDR.Services;

namespace IDR.Web.Api.External
{
    [RoutePrefix("api/external/responses")]
    [Authorize]
	[BearerRequired]
    public class ResponsesController : ApiController
    {
        private readonly ProcedureService _procedureService;

        public ResponsesController(ProcedureService procedureService)
        {
            _procedureService = procedureService;
        }

        public async Task<HttpResponseMessage> Post(ProcedureNotificationOption model)
        {
            await _procedureService.UpdateProcedureNotification(model);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
