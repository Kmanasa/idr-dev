﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Domain;
using IDR.Services;

namespace IDR.Web.Api.External
{
    [RoutePrefix("api/external/devices")]
    //[Authorize]
    public class DevicesController : ApiController
    {
        private readonly DeviceService _deviceService;

        public DevicesController(DeviceService deviceService)
        {
            _deviceService = deviceService;
        }
      
        public async Task<HttpResponseMessage> Post(DeviceDto device)
        {
            try
            {
                await _deviceService.RegisterDevice(device);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "!!"+ex.Message);
            }
        }
    }
}
