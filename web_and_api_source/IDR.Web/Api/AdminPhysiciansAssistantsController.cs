﻿using System;
using IDR.Data;
using IDR.Domain.Portal.Dto;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http; 
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Web.Mailers;

namespace IDR.Web.Api
{ 
	[RoutePrefix("api/adminphysiciansassistants")]
	[Authorize(Roles = "Admin,Super Admin,Practice Admin,Scheduler")]
	[NotMobile]
	[BearerRequired]
	public class AdminPhysiciansAssistantsController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
	    private readonly PhysiciansService _physiciansService;
        private readonly IInvitationMailer _invitationMailer;

        public AdminPhysiciansAssistantsController(ApplicationDbContext context, IAuditService auditService, PhysiciansService physiciansService, IInvitationMailer invitationMailer)
		{
			_context = context;
            _auditService = auditService;
		    _physiciansService = physiciansService;
		    _invitationMailer = invitationMailer;
		}

		[Route("{practiceId}")]
		[HttpGet]
		public async Task<IEnumerable<PhysicianDto>> Get(int practiceId)
        {
		    var physicians = await _physiciansService.GetPhysiciansForPractice(practiceId);
		    return physicians;
        }

		[HttpGet]
		[Route("{id}/{practiceId}")]
		public async Task<PhysiciansAssistantDto> Get(int practiceId, int id)
        {
		    var assistant = await _physiciansService.GetPhysicianAssistant(id);
		    return assistant;
        }

		[HttpGet]
		[Route("new/{practiceId}")]
		public async Task<PhysiciansAssistantDto> New(int practiceId)
		{
			var pa = await _physiciansService.AddPhysiciansAssistant(practiceId);

			return pa;
		}

		[HttpPut]
		public async Task<PhysiciansAssistantDto> Put([FromBody] PhysiciansAssistantDto assistant)
        {
			var pa = await _physiciansService.UpdatePhysiciansAssistant(assistant);

	        return pa;
        }

		[Route("invite/{id}")]
		[HttpGet]
		public async Task<OperationResponse<int>> Invite(int id)
		{
            return await InvitePA(id);
        }

		[HttpDelete]
		public async Task<OperationResponse<int>> Delete(int id)
		{
			return await _context.Delete<PhysiciansAssistant>(_auditService, id);
		}

		[Route("deactivate/{id}")]
        [HttpPost]
        public HttpResponseMessage Deactivate(int id)
        {
            _context.Deactivate<PhysiciansAssistant>(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private async Task<OperationResponse<int>> InvitePA(int id)
        {
            var response = new OperationResponse<int>();
            try
            {
                var physician = await _context.PhysiciansAssistants.FirstOrDefaultAsync(x => x.Id == id);

                _invitationMailer.Send(physician);

                response.Succeed($"An invitiation has been emailed to {physician.ProvisionalEmail}");
            }
            catch (Exception ex)
            {
                response.Exception(ex);
            }
            return response;
        }
    }
}
