﻿using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Infrastructure;
using IDR.Domain.Images.Dto;


namespace IDR.Web.Api
{
	[RoutePrefix("api/images")]
	[Authorize]
    public class ImagesController : ApiController
    {
	    private readonly ImageService _imageService;

		public ImagesController(ImageService imageService)
		{
		    _imageService = imageService;
		}

	    public async Task<PagedQueryResults<ImageFileDto>> Get([FromUri]PagingParameters paging)
	    {
	        return await _imageService.SearchImages(paging);
	    }

	    public async Task<ImageFileDto> Get(int id)
	    {
	        return await _imageService.GetImage(id);
	    }
    }
}
