﻿using System;
using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Net.Http;
using IDR.Domain.CallCenter;
using IDR.Web.App;

namespace IDR.Web.Api
{
	[RoutePrefix("api/practice")]
	[Authorize(Roles = "Admin,Super Admin,Practice Admin,Call Center Admin")]
	[NotMobile]
	[BearerRequired]
	public class PracticeController : ApiController
    {
		private readonly ApplicationDbContext _context;
	    private readonly PracticeService _practiceService;

		public PracticeController(ApplicationDbContext context, PracticeService practiceService)
		{
			_context = context;

		    _practiceService = practiceService;
		}

        [Authorize(Roles = "Admin,Super Admin,Practice Admin")]
        public async Task<CompanyDto> Get(int id)
        {
            var model = await _practiceService.GetPractice(User.Identity.GetUserId(), id);
            return model;
        }

        [HttpPut]
        public async Task<PracticeDto> Put([FromBody] PracticeDto model)
        {
            var company = await _practiceService.UpdatePractice(model);
            return company;
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            _practiceService.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("{id}/users")]
		public async Task<PagedQueryResults<BaseUserDto>> Get([FromUri]FilterByTypePagedResults paging, int id)
		{
			try
			{
				var practiceUsers = await _practiceService.GetPracticeUsers(id, paging ?? new PagingParameters());
				return practiceUsers;

			}
			catch (Exception ex)
			{
				var a = ex.Message;
				throw ex;
			}

		}

		[Route("{id}/physicians")]
		public async Task<PagedQueryResults<BaseUserDto>> GetPhysicians(int id)
		{
			var paging = new FilterByTypePagedResults
			{
				LoadPracticeAdmins = false,
				LoadPatients = false,
				LoadPhysicanAssistants = false,
				LoadSchedulers = false,
				Limit = 1000
			};

			var practiceUsers = await _practiceService.GetPracticeUsers(id, paging);

			return practiceUsers;
		}

        [Route("new")]
        [HttpGet]
        public async Task<CompanyDto> New()
        {
            var company = await _practiceService.Create();
            return company;
        }

		[Route("{id}/copy")]
		[HttpGet]
        public async Task<CompanyDto> Copy(int id)
		{
		    var company = await _practiceService.Copy(id);
			return company;
		}
    }
}
