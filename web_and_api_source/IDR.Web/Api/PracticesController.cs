﻿using IDR.Data;
using IDR.Domain.Companies.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Net;
using IDR.Domain.CallCenter;
using IDR.Web.App;
using System.Collections.Generic;

namespace IDR.Web.Api
{
    [RoutePrefix("api/practices")]
    [Authorize(Roles = "Admin,Super Admin,Practice Admin,Call Center Admin, Call Site Admin")]
    [NotMobile]
    [BearerRequired]
    public class PracticesController : ApiController
    {
        private readonly ApplicationDbContext _context;
        private readonly PracticeService _practiceService;

        public PracticesController(ApplicationDbContext context, PracticeService practiceService)
        {
            _context = context;

            _practiceService = practiceService;
        }

        public async Task<PagedQueryResults<CompanyDto>> Get([FromUri]PagingParameters paging)
        {
            var practices = await _practiceService.GetPractices(paging ?? new PagingParameters());

            return practices;
        }

        public async Task<PracticeDto> Get(int id)
        {
            var model = await _practiceService.GetPractice(User.Identity.GetUserId(), id);
            return model;
        }

        public async Task<CompanyDto> Put([FromBody] PracticeDto model)
        {
            var company = await _practiceService.UpdatePractice(model);
            return company;
        }

        [Route("new")]
        [HttpGet]
        public async Task<CompanyDto> New()
        {
            var company = await _practiceService.Create();
            return company;
        }

        [Route("{id}/copy")]
        [HttpGet]
        public async Task<CompanyDto> Copy(int id)
        {
            var company = await _practiceService.Copy(id);
            return company;
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            _practiceService.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("facilitygroup")]
        [HttpGet]
        public async Task<List<CompanyDto>> GetPracticesForFacilityGroup()
        {
            return await _practiceService.GetPracticesForFacilityGroup(User.Identity.GetUserId());
        }

        [Route("ghostpractice/{practiceId}")]
        [HttpPut]
        public async Task<bool> UpdatePractice(int practiceId)
        {
            return await _practiceService.UpdatePractice(User.Identity.GetUserId(), practiceId);
        }

        [Route("getPractice")]
        [HttpGet]
        public async Task<CompanyDto> GetPractice()
        {
            return await _practiceService.GetPractice(User.Identity.GetUserId());
        }
    }
}
