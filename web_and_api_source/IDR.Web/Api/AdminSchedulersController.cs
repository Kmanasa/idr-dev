﻿using System;
using IDR.Data;
using IDR.Domain.PracticeUsers.Dto;
using IDR.Infrastructure;
using IDR.Services;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using IDR.Web.App;
using IDR.Web.Mailers;

namespace IDR.Web.Api
{
	[RoutePrefix("api/adminschedulers")]
	[Authorize(Roles = "Admin,Super Admin,Practice Admin,Scheduler")]
	[NotMobile]
	[BearerRequired]
    public class AdminSchedulersController : ApiController
    {
		private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
        private readonly IInvitationMailer _invitationMailer;
        private readonly SchedulersService _service;

        public AdminSchedulersController(
            
            ApplicationDbContext context,
            IAuditService auditService,
            IInvitationMailer mailer, SchedulersService service)
		{
			_context = context;
            _auditService = auditService;
            _invitationMailer = mailer;
		    _service = service;
		}
        
		[HttpGet]
		public async Task<PracticeAdminDto> Get(int id)
		{
		    return await _service.GetScheduler(id);
        }

		[HttpGet]
		[Route("new")]
		public async Task<SchedulerDto> New(int practiceId)
		{
		    return await _service.Create(practiceId);
		}

		[HttpPut]
		public async Task<SchedulerDto> Put([FromBody] SchedulerDto admin)
        {
		    return await _service.Update(admin);
		}

		[Route("invite/{id}")]
		[HttpGet]
		public async Task<OperationResponse<int>> Invite(int id)
		{
		    return await InviteAdmin(id);
		}

		[HttpDelete]
		public async Task<OperationResponse<int>> Delete(int id)
		{
			return await _context.Delete<Scheduler>(_auditService, id);
		}

        [Route("deactivate/{id}")]
        [HttpPost]
        public HttpResponseMessage Deactivate(int id)
        {
            _context.Deactivate<Scheduler>(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public object BaseUserHelper { get; set; }

        private async Task<OperationResponse<int>> InviteAdmin(int id)
        {
            var response = new OperationResponse<int>();
            try
            {
                var admin = await _context.Schedulers.FirstOrDefaultAsync(x => x.Id == id);

                _invitationMailer.Send(admin);

                response.Succeed($"An invitiation has been emailed to {admin.ProvisionalEmail}");
            }
            catch (Exception ex)
            {
                response.Exception(ex);
            }
            return response;
        }
    }
}
