﻿using System.Net;
using System.Net.Http;
using IDR.Services;
using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Domain.Notifications;

namespace IDR.Web.Api
{
    [RoutePrefix("api/notifications")]
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class NotificationsController : ApiController
    {
        private readonly NotificationService _notificationService;

        public NotificationsController(NotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        public async Task<NotificationDto> Get(int id)
        {
            return await _notificationService.GetNotification(id);
        }

        [HttpPost]
        public async Task<NotificationDto> Post(NotificationDto notification)
        {
            var id = await _notificationService.CreateNotification(notification);
            return await _notificationService.GetNotification(id);
        }

        [HttpPut]
        public async Task<NotificationDto> Put( NotificationDto notification)
        {
            await _notificationService.UpdateNotification(notification);

            var response = await _notificationService.GetNotification(notification.Id);

            return response;
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> Delete(int id)
        {
            await _notificationService.DeleteNotification(id);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
