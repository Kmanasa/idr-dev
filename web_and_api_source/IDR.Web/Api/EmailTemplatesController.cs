﻿using System.Collections.Generic;
using IDR.Services;
using System.Web.Http;
using IDR.Web.App;
using IDR.Data;
namespace IDR.Web.Api
{
    [RoutePrefix("api/emailtemplates")]
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class EmailTemplatesController : ApiController
    {
        private readonly EmailTemplatesService _emailTemplatesService;

        public EmailTemplatesController(EmailTemplatesService emailTemplatesService)
        {
            _emailTemplatesService = emailTemplatesService;
        }

        public List<EmailTemplate> Get()
        {
            return _emailTemplatesService.GetTemplates();
        }

        public EmailTemplate Get(string id)
        {
            return _emailTemplatesService.GetTemplate(id);
        }

        [HttpPut]
        public EmailTemplate Put(EmailTemplate model)
        {
            return _emailTemplatesService.UpdateTemplate(model);
        }
    }
}
