﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web;
using RestSharp;

namespace IDR.Web.Api
{
	[RoutePrefix("api/parse")]
    public class ParseController : ApiController
	{
		public async Task<HttpResponseMessage> Post()
		{
			var path = HttpContext.Current.Server.MapPath("~/App_Data");
			var provider = new MultipartFormDataStreamProvider(path);

			try
			{
				await Request.Content.ReadAsMultipartAsync(provider);

				//var to = provider.FormData.GetValues("to").SingleOrDefault();
				var accessCode = provider.FormData.GetValues("subject").SingleOrDefault();
				var fileData = provider.FileData.FirstOrDefault();

				var fileInfo = new FileInfo(fileData.LocalFileName);
				var fileName = fileInfo.Name;
				ConvertToTiff(fileName, accessCode);

			}
			catch (Exception ex)
			{
				// Log to elmah
				File.WriteAllText($"{path}_error_{DateTime.UtcNow.Ticks}.txt", ex.Message);
			}

			return Request.CreateResponse(HttpStatusCode.OK);
		}

		private void ConvertToTiff(string fileName, string accessCode)
		{
			var host = ConfigurationManager.AppSettings["Host"];
			var key = ConfigurationManager.AppSettings["CloudConvertKey"];

			var client = new RestClient("https://api.cloudconvert.com/convert");
			var request = new RestRequest(Method.GET);
			request.AddQueryParameter("apikey", key);
			request.AddQueryParameter("inputformat", "pdf");
			request.AddQueryParameter("outputformat", "tiff");
			request.AddQueryParameter("input", "download");
			request.AddQueryParameter("file", $"{host}/convert?id={fileName}");
			request.AddQueryParameter("filename", "aob.pdf");
			request.AddQueryParameter("callback", $"{host}/convert/done?source={fileName}&AccessCode={accessCode}");
			request.AddQueryParameter("wait", "true");
			request.AddQueryParameter("download", "true");
			// Save = true is required to be able to download the output
			request.AddQueryParameter("save", "true");

			client.Execute(request);
		}
	}
}
