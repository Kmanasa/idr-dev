﻿using System.Threading.Tasks;
using System.Web.Http;
using IDR.Web.App;
using IDR.Data;
using IDR.Domain.Dashboard;
using IDR.Services;

namespace IDR.Web.Api
{
    [Authorize]
	[NotMobile]
	[BearerRequired]
    public class ProcedureDetailsController : ApiController
    {
        private readonly ProcedureService _procedureService;
        private readonly DashboardService _dashboardService;

        public ProcedureDetailsController(ProcedureService procedureService, DashboardService dashboardService)
        {
            _procedureService = procedureService;
            _dashboardService = dashboardService;
        }

        public Procedure Get(int id)
        {
            return _procedureService.GetProcedureById(id);
        }

        [HttpGet]
        [Route("api/proceduredetails/{id}/dashboard")]
        public async Task<DashboardItem> GetDashboard(int id)
        {
            return await _dashboardService.GetProcedureDashboard(id);
        }
    }
}
