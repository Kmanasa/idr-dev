﻿using IDR.Data;
using IDR.Domain.Addresses.Dtos;
using IDR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IDR.Web.Api
{
    // [RoutePrefix("api/googlemaps")]
    public class GoogleMapsController
    {
        private readonly ApplicationDbContext _context;
        private readonly IAuditService _auditService;
        private readonly GoogleMapService _googleMapService;

        public GoogleMapsController(ApplicationDbContext context, IAuditService auditService, GoogleMapService googlemapservice)
        {
            _context = context;
            _auditService = auditService;
            _googleMapService = googlemapservice;
        }


        //[HttpGet]
        //public async Task<List<AddressDto>> GetAddressforNodeLibrary(int nodelibraryId)
        //{
        //    return await _googleMapService.GetAddressforNodeLibrary(nodelibraryId);
        //}

    }
}