using IDR.Domain.Membership.Dto;
using Mvc.Mailer;
using System.Configuration;
using System.Linq;
using IDR.Data;

namespace IDR.Web.Mailers
{ 
    public class ResetPassword : MailerBase, IResetPasswordMailer
    {
        private ApplicationDbContext _context;// = new ApplicationDbContext();

		public ResetPassword(ApplicationDbContext context)
		{
			_context = context;
			MasterName="_Layout";
		}
		
		public void Send(ResetPasswordRequest model)
		{
            var host = ConfigurationManager.AppSettings["host"];
            
            var template = _context.EmailTemplates.First(t => t.Alias == "resetpassword").Content;

		    template = template.Replace("{{FirstName}}", model.FirstName)
		        .Replace("{{LastName}}", model.LastName)
		        .Replace("{{PasswordUrl}}", model.Url)
		        .Replace("{{Host}}", host);

		    ViewBag.Template = template;

			var emailLink = Populate(x => {
                x.Subject = @"Information to Reset Your Password";
                x.ViewName = "EmailLink";
			    x.To.Add(model.Email);
			});

            emailLink.Send();
		}
	}
}