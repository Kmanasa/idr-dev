
using IDR.Domain.Membership.Dto;

namespace IDR.Web.Mailers
{ 
    public interface IResetPasswordMailer
    {
        void Send(ResetPasswordRequest model);
    }
}