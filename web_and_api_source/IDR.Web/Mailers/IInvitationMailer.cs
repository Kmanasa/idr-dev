using IDR.Data;

namespace IDR.Web.Mailers
{
    public interface IInvitationMailer
    {
        void Send(Physician physician);
        void Send(PhysiciansAssistant assistant);
        void Send(PracticeAdmin admin);
        void Send(Scheduler scheduler);
        void Send(CallSiteAdmin admin);
        void Send(CallSiteMobileUser admin);
	}
}