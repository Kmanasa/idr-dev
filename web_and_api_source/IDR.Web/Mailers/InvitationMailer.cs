using System;
using Mvc.Mailer;
using System.Configuration;
using System.Linq;
using IDR.Data;

namespace IDR.Web.Mailers
{
    public class InvitationMailer : MailerBase, IInvitationMailer
    {
        private readonly ApplicationDbContext _context;

        public InvitationMailer(ApplicationDbContext context)
        {
	        _context = context;
            MasterName = "_Layout";
        }

        public void Send(string template, string email, string firstName, string lastName,
            string practiceName, string key, string viewName)
        {
            var host = ConfigurationManager.AppSettings["Host"];

            template = template.Replace("{{FirstName}}", firstName)
                .Replace("{{Host}}", host)
                .Replace("{{PracticeName}}", practiceName)
                .Replace("{{ProvisionalKey}}", key);

            ViewBag.Template = template;

            var invitataion = Populate(x =>
            {
                x.Subject = @"You have been invited to begin using IDR";
                x.ViewName = viewName;
                x.To.Add(email);
            });

            invitataion.Send();
        }

        public void Send(Physician physician)
        {
            var template = "";

            if (physician.CompanyId.HasValue)
            {
                var company = _context.Companies.FirstOrDefault(c => c.Id == physician.CompanyId.Value);

                if (company != null)
                {
                    template = company.PhysicianEmailTemplate;
                }
            }

            if (string.IsNullOrWhiteSpace(template))
            {
                template = _context.EmailTemplates.First(t => t.Alias == "physicianinvitation").Content;
            }

            var key = (physician.ProvisionalKey ?? Guid.Empty).ToString();
            Send(template, physician.ProvisionalEmail, physician.FirstName, physician.LastName,
                physician.Company?.Name, key, "InvitePhysician");
        }

        public void Send(PhysiciansAssistant assistant)
        {
            var template = "";

            if (assistant.CompanyId.HasValue)
            {
                var company = _context.Companies.FirstOrDefault(c => c.Id == assistant.CompanyId.Value);

                if (company != null)
                {
                    template = company.PhysiciansAssistantEmailTemplate;
                }
            }

            if (string.IsNullOrWhiteSpace(template))
            {
                template = _context.EmailTemplates.First(t => t.Alias == "painvitation").Content;
            }

            var key = (assistant.ProvisionalKey ?? Guid.Empty).ToString();
            Send(template, assistant.ProvisionalEmail, assistant.FirstName, assistant.LastName,
                assistant.Company?.Name, key, "InviteAssistant");
        }

        public void Send(PracticeAdmin admin)
        {
            var template = "";

            if (admin.CompanyId.HasValue)
            {
                var company = _context.Companies.FirstOrDefault(c => c.Id == admin.CompanyId.Value);

                if (company != null)
                {
                    template = company.PracticeAdminEmailTemplate;
                }
            }

            if (string.IsNullOrWhiteSpace(template))
            {
                template = _context.EmailTemplates.First(t => t.Alias == "admininvitation").Content;
            }

	        if (string.IsNullOrWhiteSpace(template))
	        {
		        template = _context.EmailTemplates.First(t => t.Alias == "calladmininvitation").Content;
	        }

			var key = (admin.ProvisionalKey ?? Guid.Empty).ToString();
            Send(template, admin.ProvisionalEmail, admin.FirstName, admin.LastName,
                admin.Company?.Name, key, "InviteAdmin");
        }

        public void Send(Scheduler admin)
        {
            var template = "";

            if (admin.CompanyId.HasValue)
            {
                var company = _context.Companies.FirstOrDefault(c => c.Id == admin.CompanyId.Value);

                if (company != null)
                {
                    template = company.PracticeAdminEmailTemplate;
                }
            }

            if (string.IsNullOrWhiteSpace(template))
            {
                template = _context.EmailTemplates.First(t => t.Alias == "admininvitation").Content;
            }
			
	        if (string.IsNullOrWhiteSpace(template))
	        {
		        template = _context.EmailTemplates.First(t => t.Alias == "calladmininvitation").Content;
	        }

			var key = (admin.ProvisionalKey ?? Guid.Empty).ToString();
            Send(template, admin.ProvisionalEmail, admin.FirstName, admin.LastName,
                admin.Company?.Name, key, "InviteAdmin");
        }

	    public void Send(CallSiteAdmin admin)
	    {
		    var template = "";

		    if (admin.CompanyId.HasValue)
		    {
			    var company = _context.Companies.FirstOrDefault(c => c.Id == admin.CompanyId.Value);

			    if (company != null)
			    {
				    template = company.CallSiteAdminEmailTemplate;
			    }
		    }

		    if (string.IsNullOrWhiteSpace(template))
		    {
			    template = _context.EmailTemplates.First(t => t.Alias == "calladmininvitation").Content;
		    }

		    var key = (admin.ProvisionalKey ?? Guid.Empty).ToString();
		    Send(template, admin.ProvisionalEmail, admin.FirstName, admin.LastName,
			    admin.Company?.Name, key, "InviteAdmin");
	    }

	    public void Send(CallSiteMobileUser admin)
	    {
		    var template = "";

		    if (admin.CompanyId.HasValue)
		    {
			    var company = _context.Companies.FirstOrDefault(c => c.Id == admin.CompanyId.Value);

			    if (company != null)
			    {
				    template = company.CallSiteMobileUserEmailTemplate;
			    }
		    }

		    if (string.IsNullOrWhiteSpace(template))
		    {
			    template = _context.EmailTemplates.First(t => t.Alias == "callamobileinvitation").Content;
		    }

		    var key = (admin.ProvisionalKey ?? Guid.Empty).ToString();
		    Send(template, admin.ProvisionalEmail, admin.FirstName, admin.LastName,
			    admin.Company?.Name, key, "InviteMobileUser");
	    }
	}
}