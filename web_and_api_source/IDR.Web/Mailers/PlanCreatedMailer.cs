using System.Configuration;
using System.Linq;
using IDR.Data;
using Mvc.Mailer;
using IDR.Domain;

namespace IDR.Web.Mailers
{
    public interface IPlanCreatedMailer
    {
        void Mail(ProcedureCreatedRequest message);
    }

    public class PlanCreatedMailer : MailerBase, IPlanCreatedMailer
	{
        private readonly ApplicationDbContext _context;

        public PlanCreatedMailer(ApplicationDbContext context)
        {
	        _context = context;
			MasterName = "_Layout";
		}
		
		public void Mail(ProcedureCreatedRequest model)
		{
            var host = ConfigurationManager.AppSettings["host"];

            var template = _context.EmailTemplates.First(t => t.Alias == "patientinvitation").Content;

            template = template.Replace("{{FirstName}}", model.FirstName)
                .Replace("{{AccessCode}}", model.AccessCode)
                .Replace("{{PracticeName}}", model.PracticeName)
		        .Replace("{{Host}}", host);

            ViewBag.Template = template;

            var email = Populate(x => {
                x.Subject = @"Welcome to IDR for Smart Dr.";
                x.ViewName = "PlanCreated";
			    x.To.Add(model.Email);
			});
            
            email.Send();
		}
	}
}