﻿Imports System.Net
Imports System.IO
Imports SmartDr.NeoSecure.ClassLibrary

Public Class EasyHttp

	Private _oCookieContainer As New System.Net.CookieContainer

	Public Enum HTTPMethod As Short
		HTTP_GET = 0
		HTTP_POST = 1
	End Enum

	Public Property Cookies() As System.Net.CookieContainer
		Get
			Return _oCookieContainer
		End Get
		Friend Set(ByVal Value As System.Net.CookieContainer)
			_oCookieContainer = Value
		End Set
	End Property

	Public Sub ClearCookies()
		_oCookieContainer = New System.Net.CookieContainer
	End Sub

	Public Function Send(ByVal URL As String,
		Optional ByVal PostData As String = "",
		Optional ByVal Method As HTTPMethod = HTTPMethod.HTTP_GET,
		Optional ByVal ContentType As String = "",
		Optional ByVal Credentials As System.Net.ICredentials = Nothing)

		Dim Request As HttpWebRequest = WebRequest.Create(URL)
		Dim Response As HttpWebResponse
		Dim SW As StreamWriter
		Dim SR As StreamReader
		Dim ResponseData As String

        ' Prepare Request Object
        Request.CookieContainer = _oCookieContainer
		Request.Method = Method.ToString().Substring(5)
		Request.Credentials = Credentials

        ' Set form/post content-type if necessary
        If (Method = HTTPMethod.HTTP_POST AndAlso PostData <> "" AndAlso ContentType = "") Then
			ContentType = "application/x-www-form-urlencoded"
		End If

        ' Set Content-Type
        If (ContentType <> "") Then
			Request.ContentType = ContentType
			Request.ContentLength = PostData.Length
		End If

        ' Send Request, If Request
        If (Method = HTTPMethod.HTTP_POST) Then
			Try
				SW = New StreamWriter(Request.GetRequestStream())
				SW.Write(PostData)
			Catch Ex As Exception
				Throw Ex
			Finally
				SW.Close()
			End Try
		End If

        ' Receive Response
        Try
			Response = Request.GetResponse()
			SR = New StreamReader(Response.GetResponseStream())
			ResponseData = SR.ReadToEnd()
		Catch Wex As System.Net.WebException
			SR = New StreamReader(Wex.Response.GetResponseStream())
			ResponseData = SR.ReadToEnd()
			Throw New Exception(ResponseData)
		Finally
			SR.Close()
		End Try

		Return ResponseData
	End Function

	Public Function SendSecure(ByVal fromAddress As String, ByVal toAddress As String, ByVal messageSubject As String, ByVal messageBody As String, ByVal attachmentPath As String)

		Dim distributor As New Distributor
		distributor.AddAttachment(attachmentPath)
		distributor.Send(fromAddress, toAddress, messageSubject, messageBody)

		Return True

	End Function
End Class




