﻿Imports System.Xml
Imports System.Web
Imports System.Net
Imports System.Text
Imports MiCo.MiForms.GenericDialogs

Public Class Connector

    '*** Constructor
    Public Sub New(Optional ByVal sServerUrl As String = "", Optional ByVal sUsername As String = "", Optional ByVal sPassword As String = "")

        'check the server url and set it
        If (sServerUrl = "") Then sServerUrl = GetBentbitConnectorUrl()
        _sServerUrl = sServerUrl

        'Check the username and password and set it
        If (Not sUsername = "") Then _sServerUsername = sUsername
        If (Not sPassword = "") Then _sServerPassword = sPassword

    End Sub

    '*** Public Variables

    '*** Private Variables
    Private _sServerUrl As String = ""
    Private _sServerUsername As String = ""
    Private _sServerPassword As String = ""
    Private _sServerSession As String = ""
    Private _sRegistryHive As String = "SYSTEM"
    Private _sRegistrySection As String = "Preferences/Mi-Co/"
    Private _oHttp As New EasyHttp

    '*** Properties
    Public Property url() As String
        Get
            Return _sServerUrl
        End Get
        Set(ByVal Value As String)
            _sServerUrl = Value
        End Set
    End Property

    Public Property username() As String
        Get
            Return _sServerUsername
        End Get
        Set(ByVal Value As String)
            _sServerUsername = Value
        End Set
    End Property

    Public Property password() As String
        Get
            Return _sServerPassword
        End Get
        Set(ByVal Value As String)
            _sServerPassword = Value
        End Set
    End Property

    Public Property session() As String
        Get
            Return _sServerSession
        End Get
        Set(ByVal Value As String)
        End Set
    End Property

    Public Property RegistryHive() As String
        Get
            Return _sRegistryHive
        End Get
        Set(ByVal Value As String)
            _sRegistryHive = Value
        End Set
    End Property

    Public Property RegistrySection() As String
        Get
            Return _sRegistrySection
        End Get
        Set(ByVal Value As String)
            _sRegistrySection = Value
        End Set
    End Property

    '*** Public Methods
    Public Function Login(Optional ByVal sUsername As String = "", Optional ByVal sPassword As String = "") As Boolean

        Dim rtnVal As Boolean = False

        Try

            'Check the username and password and set it
            If (Not sUsername = "") Then _sServerUsername = sUsername
            If (Not sPassword = "") Then _sServerPassword = sPassword

            'Prepare the post data
            Dim oPostData As New Dictionary(Of String, String)
            oPostData.Add("_COMMAND", "user.login")

            'load the data
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNode As XmlNode

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST, "", New System.Net.NetworkCredential(_sServerUsername, _sServerPassword)))

            oXmlNode = oXmlDoc.SelectSingleNode("//Body/data")

            rtnVal = Boolean.Parse(oXmlNode.ChildNodes(0).InnerText)
            _sServerSession = oXmlNode.ChildNodes(2).InnerText

        Catch ex As Exception

            rtnVal = False

        End Try

        Login = rtnVal

    End Function

    Public Function Logout() As Boolean

        Dim rtnVal As Boolean = False

        Try

            'Prepare the post data
            Dim oPostData As New Dictionary(Of String, String)
            oPostData.Add("_COMMAND", "user.logout")

            'load the data
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNode As XmlNode

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))

            oXmlNode = oXmlDoc.SelectSingleNode("//Body")

            rtnVal = Boolean.Parse(oXmlNode.ChildNodes(0).InnerText)

        Catch ex As Exception

            rtnVal = False

        End Try

        'clear the cookies
        _oHttp.ClearCookies()

        Logout = rtnVal

    End Function

    Public Function Execute(ByVal oPostData As Dictionary(Of String, String)) As XmlDocument

        Dim rtnVal As XmlDocument = Nothing

        Try

            'load the data
            Dim oXmlDoc As XmlDocument = New XmlDocument

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))

            rtnVal = oXmlDoc

        Catch ex As Exception

            rtnVal = Nothing

        End Try

        Execute = rtnVal

    End Function

    Public Function RegSet(ByRef oInputValue As Object, Optional ByVal sFieldValue As Object = "", Optional ByVal sRegistrySection As String = "") As Boolean

        Dim rtnVal As Boolean = False
        Dim sFieldName As String = ""

        Try
            'set field
            If (TypeOf oInputValue Is String) Then
                sFieldName = oInputValue
            Else
                sFieldName = oInputValue.Name

                If (sFieldValue = "") Then sFieldValue = oInputValue.Value

            End If

            'load the data
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList

            'set params
            Dim oPostData As New Dictionary(Of String, String)
            oPostData.Add("_COMMAND", "registry.key.set")
            oPostData.Add("hive", _sRegistryHive)
            oPostData.Add("section", _sRegistrySection + sRegistrySection)
            oPostData.Add("key", sFieldName)
            oPostData.Add("value", sFieldValue)

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data")

            If (oXmlNodeList.Item(0).InnerText.ToLower = "true") Then rtnVal = True

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception
            rtnVal = False
        End Try

        RegSet = rtnVal

    End Function

    Public Function RegGet(ByRef oInputValue As Object, Optional ByVal sRegistrySection As String = "") As String

        Dim rtnVal As String = ""
        Dim sFieldName As String = ""

        Try
            'set field
            If (TypeOf oInputValue Is String) Then
                sFieldName = oInputValue
            Else
                sFieldName = oInputValue.Name
            End If

            'load the data
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList

            'set params
            Dim oPostData As New Dictionary(Of String, String)
            oPostData.Add("_COMMAND", "registry.key.get")
            oPostData.Add("hive", _sRegistryHive)
            oPostData.Add("section", _sRegistrySection + sRegistrySection)
            oPostData.Add("key", sFieldName)

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data")

            rtnVal = oXmlNodeList.Item(0).InnerText

            'set object
            If (Not TypeOf oInputValue Is String) Then
                Call SetCheckbox(oInputValue, rtnVal)
            End If

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception
            rtnVal = ""
        End Try

        RegGet = rtnVal

    End Function

    Public Sub RegLoadForm(ByRef oForm As Object, Optional ByVal sRegistrySection As String = "")

        Try

            'Populate the form
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList
            Dim iCounter As Integer
            Dim oDataTable As New DataTable

            'set params
            Dim oPostData As New Dictionary(Of String, String)
            oPostData.Add("_COMMAND", "registry.query")
            oPostData.Add("hive", _sRegistryHive)
            oPostData.Add("section", _sRegistrySection + sRegistrySection)

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/data")

            'if no data was selected then fall back for compatibility
            If ((oXmlNodeList.Count - 1) < 0) Then oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/row")

            'Load new data into form
            For iCounter = 0 To oXmlNodeList.Count - 1

                Try

                    Dim oChild As XmlNode : oChild = oXmlNodeList.Item(iCounter)
                    Dim sName : sName = oChild.SelectSingleNode("key").InnerText 'oXmlDoc.DocumentElement.SelectNodes("//Body/data/data[" & (iCounter + 1) & "]/key").Item(0).InnerText '
                    Dim sValue : sValue = oChild.SelectSingleNode("value").InnerText 'oXmlDoc.DocumentElement.SelectNodes("//Body/data/data[" & (iCounter + 1) & "]/value").Item(0).InnerText '

                    'MsgBox(_sRegistrySection + sRegistrySection + " = (" & sName & " : " & sValue & ")" & vbCrLf & oChild.InnerXml)

                    If (Not sName = "") Then SetCheckbox(oForm.GetFormControlByName(sName), sValue)

                Catch ex As Exception
                End Try

            Next

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception
        End Try

    End Sub

    Public Function RegSaveForm(ByRef oForm As Object, Optional ByVal sRegistrySection As String = "") As Boolean

        Dim rtnVal As Boolean = False

        Try

            'Populate the form
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList
            Dim oDataTable As New DataTable

            'set params
            Dim oPostData As New Dictionary(Of String, String)
            oPostData.Add("_COMMAND", "registry.save")
            oPostData.Add("hive", _sRegistryHive)
            oPostData.Add("section", _sRegistrySection + sRegistrySection)

            For Each oFormPage As Object In oForm.Pages
                For Each oControl As Object In oFormPage.Controls
                    Try
                        oPostData.Add(CStr(oControl.Name), CStr(oControl.Value))
                    Catch ex As Exception
                    End Try
                Next
            Next

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data")

            rtnVal = CBool(oXmlNodeList.Item(0).InnerText)

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception

            rtnVal = False

        End Try

        RegSaveForm = rtnVal

    End Function

    Public Sub SetCheckbox(ByRef oCheckbox As Object, ByVal sValue As String)

        Try

            If IsNumeric(sValue) Then

                oCheckbox.Value = sValue
                oCheckbox.NumericValue = CDbl(sValue)

            Else

                Dim sItem As String = ""
                Dim nItem As Integer = 0

                oCheckbox.Value = sValue

                Try
                    For nItem = 0 To oCheckbox.Boxes.Count
                        For Each sItem In sValue.Split(",")

                            Try
                                If IsNumeric(sItem) Then
                                    oCheckbox.ParentForm.GetFormControlByName(oCheckbox.Name + "_" + nItem.ToString.PadLeft(3).Replace(" ", "0")).Value = sItem
                                    oCheckbox.ParentForm.GetFormControlByName(oCheckbox.Name + "_" + nItem.ToString.PadLeft(3).Replace(" ", "0")).NumericValue = CDbl(sItem)
                                Else
                                    oCheckbox.ParentForm.GetFormControlByName(oCheckbox.Name + "_" + nItem.ToString.PadLeft(3).Replace(" ", "0")).Value = sItem
                                End If
                            Catch ex As Exception
                            End Try

                        Next
                    Next
                Catch ex As Exception
                End Try

            End If

        Catch ex As Exception
        End Try

    End Sub

    Public Sub LoadPicklist(ByVal oPicklist As Object, ByVal sCommand As String, Optional ByVal oPostData As Dictionary(Of String, String) = Nothing, Optional ByVal sDefaultItem As String = "")

        Try

            'Populate the picklist
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList
            Dim iCounter As Integer
            Dim oSelected As Object = Nothing

            'load the data
            Try
                oPostData.Remove("_COMMAND")
                oPostData.Add("_COMMAND", sCommand)
            Catch ex As Exception
                oPostData = New Dictionary(Of String, String)
                oPostData.Add("_COMMAND", sCommand)
            End Try

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/data")

            'if no data was selected then fall back for compatibility
            If ((oXmlNodeList.Count - 1) <= -1) Then oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/row")

            'Update the picklist
            oPicklist.BeginUpdate()

            'Clear existing data from the picklist
            oPicklist.ClearItems()

            'Load new data into picklist
            For iCounter = 0 To oXmlNodeList.Count - 1

                Dim oTupleList As New System.Collections.ArrayList

                For Each oChild As XmlNode In oXmlNodeList.Item(iCounter).ChildNodes
                    oTupleList.Add(oChild.InnerText)
                    'Try
                    'If (oChild.InnerText.ToLower().Contains(sDefaultItem.ToLower()) = True) Then oSelected = oTupleList.Item(oXmlNodeList.Count - 1)
                    'Catch ex As Exception
                    'End Try
                Next

                'Find the first item
                If ((oXmlNodeList.Count - 1) = 0) Then oSelected = oTupleList.Item(0)

                oPicklist.AddTupleItems(oTupleList)
                oTupleList.Clear()

            Next

            'End the picklist update
            oPicklist.EndUpdate()

            'Select the appropriate item
            If (Not sDefaultItem = "") Then oPicklist.Value = sDefaultItem

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception
        End Try

    End Sub

    Public Sub LoadGrid(ByVal oGrid As Object, ByVal sCommand As String, Optional ByVal oPostData As Dictionary(Of String, String) = Nothing, Optional ByVal sDefaultItem As String = "")

        Try

            'clear value
            oGrid.Value = New DataTable

            'Populate the grid
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList
            Dim iCounter As Integer
            Dim nSelected As Integer = 0
            Dim oDataTable As New DataTable

            'load the data
            Try
                oPostData.Remove("_COMMAND")
                oPostData.Add("_COMMAND", sCommand)
            Catch ex As Exception
                oPostData = New Dictionary(Of String, String)
                oPostData.Add("_COMMAND", sCommand)
            End Try

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/data")

            'if no data was selected then fall back for compatibility
            If ((oXmlNodeList.Count - 1) <= -1) Then oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/row")

            'Create the DataTable columns
            For Each oChild As XmlNode In oXmlNodeList.Item(0).ChildNodes

                Try

                    Dim oDataColumn As DataColumn = New DataColumn(oChild.Name)

                    If (Left(oChild.Name, 1) = "_") Then
                        oDataColumn.ColumnMapping = MappingType.Hidden
                    End If

                    oDataTable.Columns.Add(oDataColumn)

                Catch ex As Exception
                End Try

            Next

            'Load new data into picklist
            For iCounter = 0 To oXmlNodeList.Count - 1

                Try

                    Dim oDataRow As DataRow = oDataTable.NewRow

                    For Each oChild As XmlNode In oXmlNodeList.Item(iCounter).ChildNodes

                        Try
                            oDataRow(oChild.Name) = oChild.InnerText
                            'If (oChild.InnerText.ToLower().Contains(sDefaultItem.ToLower()) = True) Then nSelected = (oXmlNodeList.Count - 1)
                        Catch ex As Exception
                        End Try

                    Next

                    oDataTable.Rows.Add(oDataRow)

                Catch ex As Exception
                End Try

            Next

            'set the data
            oGrid.Value = oDataTable

            'set the default item
            'oGrid.SelectedRowIndex = nSelected

            'hide the grid columns ()
            For Each oChild As XmlNode In oXmlNodeList.Item(0).ChildNodes

                Try

                    If (Left(oChild.Name, 1) = "_") Then

                        Dim oGridColumnProperty = oGrid.Properties.GetColumnProperty(oChild.Name)
                        oGridColumnProperty.Visible = False
                        oGrid.Properties.SetColumnProperty(oChild.Name, oGridColumnProperty)

                    End If

                Catch ex As Exception
                End Try

            Next

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception
        End Try

    End Sub

    Public Function LoadDialogListBox(ByVal sCommand As String, Optional ByVal oPostData As Dictionary(Of String, String) = Nothing, Optional ByVal sDefaultItem As String = "", Optional ByVal sField As String = "_id", Optional ByVal sTitle As String = "Please make a selection") As Dictionary(Of String, String)

        Dim rtnVal As Dictionary(Of String, String) : rtnVal = New Dictionary(Of String, String)

        Try

            'Populate the grid
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNodeList As XmlNodeList
            Dim iCounter As Integer
            Dim nSelected As Integer = 0

            Dim oDialog As New MiCo.MiForms.GenericDialogs.DlgListBox
            oDialog.Text = sTitle
            oDialog.Width = 550
            oDialog.Height = 450
            oDialog.Font = New System.Drawing.Font("Courier", 12)

            'load the data
            Try
                oPostData.Remove("_COMMAND")
                oPostData.Add("_COMMAND", sCommand)
            Catch ex As Exception
                oPostData = New Dictionary(Of String, String)
                oPostData.Add("_COMMAND", sCommand)
            End Try

            'parse the data
            oXmlDoc.LoadXml(_oHttp.Send(_sServerUrl, GetPostData(oPostData), EasyHttp.HTTPMethod.HTTP_POST))
            oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/data")

            'if no data was selected then fall back for compatibility
            If ((oXmlNodeList.Count - 1) <= -1) Then oXmlNodeList = oXmlDoc.DocumentElement.SelectNodes("//Body/data/row")

            'Load new data into picklist
            oDialog.CtlListBox.BeginUpdate()

            For iCounter = 0 To oXmlNodeList.Count - 1

                Try

                    Dim sDataRow As String : sDataRow = ""

                    For Each oChild As XmlNode In oXmlNodeList.Item(iCounter).ChildNodes

                        Try
                            If (oChild.Name = sField) Then
                                sDataRow = oChild.InnerText
                            End If
                        Catch ex As Exception
                        End Try

                    Next

                    oDialog.CtlListBox.Items.Add(sDataRow)

                Catch ex As Exception
                End Try

            Next

            oDialog.CtlListBox.EndUpdate()

            'if there is only one item in list default
            If (oDialog.CtlListBox.Items.Count = 0) Then
            ElseIf (oDialog.CtlListBox.Items.Count = 1) Then
                oDialog.CtlListBox.SelectedIndex = oDialog.CtlListBox.Items.Count - 1
            Else
                'show dialog
                oDialog.ShowDialog()
            End If

            'set the data
            If (oDialog.CtlListBox.Items.Count > 0) Then
                For Each oChild As XmlNode In oXmlNodeList.Item(oDialog.CtlListBox.SelectedIndex).ChildNodes

                    Try

                        rtnVal.Add(oChild.Name, oChild.InnerText)

                    Catch ex As Exception
                    End Try

                Next
            End If

            'set the default item
            'oGrid.SelectedRowIndex = nSelected

            'Clean up
            oXmlNodeList = Nothing
            oXmlDoc = Nothing

        Catch ex As Exception
        End Try

        LoadDialogListBox = rtnVal

    End Function

    '*** Private Methods
    Private Function GetBentbitConnectorUrl()

        Dim rtnVal As String
        Dim sPathMicoPrefs As String
        Dim sMicoHostName As String
        Dim bMicoUseHttps As Boolean

        Try
            'get the path to the mico prefs config file
            sPathMicoPrefs = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Mi-Co\Mi-Forms\prefs.xml"

            Dim oReader As XmlTextReader = New XmlTextReader(sPathMicoPrefs)
            Dim oXmlDoc As XmlDocument = New XmlDocument
            Dim oXmlNode As XmlNode

            'load the data
            oXmlDoc.Load(oReader)

            'load the hostname
            oXmlNode = oXmlDoc.SelectSingleNode("/prefs/pref[@name='Network_HostName']")
            sMicoHostName = (oXmlNode.Attributes("value").Value.ToString)

            'load if https
            oXmlNode = oXmlDoc.SelectSingleNode("/prefs/pref[@name='Network_UseHTTPS']")
            bMicoUseHttps = Boolean.Parse(oXmlNode.Attributes("value").Value)

            'build the path
            If (bMicoUseHttps = True) Then
                rtnVal = "https://" & sMicoHostName & "/bentbit-connector/"
            Else
                rtnVal = "http://" & sMicoHostName & "/bentbit-connector/"
            End If
        Catch ex As Exception
            rtnVal = ""
        End Try

        GetBentbitConnectorUrl = rtnVal
    End Function

    Private Function GetPostData(ByVal oData As Dictionary(Of String, String))

        Dim rtnVal As String = ""

        Try

            For Each oItem In oData
                rtnVal = rtnVal & System.Web.HttpUtility.UrlEncode(oItem.Key) & "=" & System.Web.HttpUtility.UrlEncode(oItem.Value) & "&"
            Next

            rtnVal = rtnVal.Substring(0, rtnVal.Length - 1)

        Catch ex As Exception

            rtnVal = ""

        End Try

        GetPostData = rtnVal

    End Function

End Class
