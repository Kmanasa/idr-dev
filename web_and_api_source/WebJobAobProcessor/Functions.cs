﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using IDR.Data;
using IDR.Messaging;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Deserializers;

namespace WebJobAobProcessor
{
	public class Functions
	{
		// This function will get triggered/executed when a new message is written 
		// on an Azure Queue called queue.
		public static void ProcessQueueMessage([QueueTrigger("aob")] AobNotification queue, TextWriter log)
		{
			try
			{
				log.WriteLine(queue.ToString());
				// Download the converted TIFF file
				var tiffClient = new RestClient("https:" + queue.Url);
				var tiffRequest = new RestRequest(Method.GET);
				var content = tiffClient.Get(tiffRequest).Content;

				dynamic json = JObject.Parse(content);

				var fileUrl = $"https:{json.output.url}";

				tiffClient = new RestClient(fileUrl);
				var file = tiffClient.DownloadData(new RestRequest());

                var infuApiTest = ConfigurationManager.AppSettings["InfuApiTestUrl"];
                var authClient = new RestClient($"{infuApiTest}/api/v1/auth/ExternalService");

                //var infuApiProd = ConfigurationManager.AppSettings["InfuApiProdUrl"];
                //var authClient = new RestClient($"{infuApiProd}/api/v1/auth/ExternalService");

                var authRequest = new RestRequest(Method.POST);
				//authRequest.AddHeader("Cache-Control", "no-cache");
				authRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded");
				//authRequest.AddHeader("Accept", "application/json;"); //;odata=verbose
				authRequest.AddParameter("application/x-www-form-urlencoded",
					"Username=mobile%40infusystem.com&Password=Infu%24yst3m&ApplicationName=InfuMobile",
					ParameterType.RequestBody);
				var authResponse = authClient.Execute<InfuAuthResponse>(authRequest);

				var deserializer = new JsonDeserializer();
				var auth = deserializer.Deserialize<InfuAuthResponse>(authResponse);

				log.Write(authResponse.Content);

				// Post the AoB file
				var aobClient = new RestClient($"{infuApiTest}/odata/PatientConsent/Default.AddMobileConsent%28%29");
                //var aobClient = new RestClient($"{infuApiProd}/odata/PatientConsent/Default.AddMobileConsent%28%29");

                var aobRequest = new RestRequest(Method.POST);
				//aobRequest.AddHeader("Cache-Control", "no-cache");
				aobRequest.AddHeader("Accept", "application/json;odata=verbose");
				aobRequest.AddHeader("Content-Type", "application/json;odata=verbose");
				aobRequest.AddHeader("X-Infusystem-Authentication", auth.token);
                aobRequest.AddHeader("OData-MaxVersion", "4.0");

                var aob = GetAoB(queue.AccessCode);
				aob.pumpReceivedDate = DateTime.Now.ToString("yyyy-MM-dd");
				aob.signatureDate = DateTime.Now.ToString("yyyy-MM-dd");
				aob.documentBase64Image = Convert.ToBase64String(file);

				var message = JsonConvert.SerializeObject(aob);

				aobRequest.AddParameter("application/json", message, ParameterType.RequestBody);
				var aobResponse = aobClient.Execute(aobRequest);

				log.Write(aobResponse.Content);
			}
			catch (Exception ex)
			{
				log.Write(ex.Message);
			}
		}

		private static InfuAoB GetAoB(string accessCode)
		{
			var _db = new ApplicationDbContext("IdrDataContext");
			var proc = _db.Procedures.First(p => p.AccessCode == accessCode);
			var ccProc = _db.CallCenterProcedures.First(c => c.ProcedureId == proc.Id);
			var patient = _db.CallCenterPatients.First(p => p.CallCenterProcedureId == ccProc.Id);
			var facility = _db.CallCenterPracticeReferences.First(f => f.PracticeId == ccProc.PracticeFacilityId);
			var patientId = patient.InfuPatientId;
			var facilityId = facility.InfuFacilityId;

			ccProc.DeliveredAoB = true;
			ccProc.DeliveredAoBDate = DateTime.UtcNow;
			_db.SaveChanges();

			return new InfuAoB
			{
				facilityId = facilityId,
				patientId = patientId
			};
		}
	}

	public class InfuAuthResponse
	{
		public string userId { get; set; }
		public string token { get; set; }
	}

	public class InfuAoB
	{
		public string patientId { get; set; }
		public string facilityId { get; set; }
		public string pumpReceivedDate { get; set; }
		public string signatureDate { get; set; }
		public string documentBase64Image { get; set; }
	}
}
