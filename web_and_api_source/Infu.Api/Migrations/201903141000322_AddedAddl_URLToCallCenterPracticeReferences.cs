namespace Infu.Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedAddl_URLToCallCenterPracticeReferences : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterPracticeReferences", "Addl_URL", c => c.String(maxLength: 100));
        }

        public override void Down()
        {
            DropColumn("dbo.CallCenterPracticeReferences", "Addl_URL");
        }
    }
}