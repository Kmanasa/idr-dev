﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Infu.Api.AuthStartup))]
namespace Infu.Api
{
    public partial class AuthStartup
	{
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
