﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Infu.Api
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class InfuApplicationUserManager : UserManager<InfuApplicationUser>
    {
        public InfuApplicationUserManager(IUserStore<InfuApplicationUser> store) : base(store)
        {
        }

        public static InfuApplicationUserManager Create(IdentityFactoryOptions<InfuApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new InfuApplicationUserManager(
				new UserStore<InfuApplicationUser>(context.Get<InfuApplicationDbContext>()));

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<InfuApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<InfuApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<InfuApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
          
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<InfuApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<InfuApplicationUser, string>
    {
        public ApplicationSignInManager(InfuApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
			
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(InfuApplicationUser user)
        {
            var ident = user.GenerateUserIdentityAsync((InfuApplicationUserManager)UserManager);
	        return ident;
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
	        var db = context.Get<InfuApplicationDbContext>();
	        var cn = db.Database.Connection.ConnectionString;

            var sim = new ApplicationSignInManager(context.GetUserManager<InfuApplicationUserManager>(), 
				context.Authentication);
	        return sim;
        }
    }
}
