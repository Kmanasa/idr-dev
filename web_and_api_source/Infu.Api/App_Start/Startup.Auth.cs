﻿using System;

using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace Infu.Api
{
	public partial class AuthStartup
	{
		public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

		public static string PublicClientId { get; private set; }

		// For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
		public void ConfigureAuth(IAppBuilder app)
		{
			// Configure the db context, user manager and signin manager to use a single instance per request
			app.CreatePerOwinContext(() =>
			{
				var appDb = new InfuApplicationDbContext();//.Create(System.Web.HttpContext.Current.Request.Url.Host);
				return appDb;
			});
			app.CreatePerOwinContext<InfuApplicationUserManager>(InfuApplicationUserManager.Create);
			app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

			// Enable the application to use a cookie to store information for the signed in user
			app.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				LoginPath = new PathString("/Account/Login"),
			});
			app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

			// Configure the application for OAuth based flow
			PublicClientId = "self";
			OAuthOptions = new OAuthAuthorizationServerOptions
			{
				TokenEndpointPath = new PathString("/token"),
				Provider = new ApplicationOAuthProvider(PublicClientId),
				AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
				AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
				AllowInsecureHttp = true,

				//AccessTokenFormat = new JwtFormat
				//(
				//	issuerCredentialProvider: new SymmetricKeyIssuerSecurityTokenProvider
				//	(
				//		issuer: "http://idr.smart-dr.com/trust/",
				//		base64Key: "iGROpg/Q+8oFSihQKJ6+D6Vsu+GOyfEC4qcGKzY4qNQ="
				//	)
				//)
			};

			// Enables the application to remember the second login verification factor such as phone or email.
			// Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
			// This is similar to the RememberMe option when you log in.
			app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

			app.UseOAuthAuthorizationServer(OAuthOptions);
			app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
			//app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

			//// Configure Web API to use only bearer token authentication.
			//var config = GlobalConfiguration.Configuration;
			//config.SuppressDefaultHostAuthentication();
			//config.Filters.Add(new HostAuthenticationFilter(OAuthBearerOptions.AuthenticationType));

			//app.UseWebApi(config);
		}
	}
}