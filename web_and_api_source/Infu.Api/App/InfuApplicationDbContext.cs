﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Infu.Api
{
	public class InfuApplicationDbContext : IdentityDbContext<InfuApplicationUser>
	{
		public InfuApplicationDbContext() : base(
			"InfuDataContext",
			throwIfV1Schema: false)
		{
		}

		public InfuApplicationDbContext(string connectionString) : base(connectionString, throwIfV1Schema: false)
		{
			//var cs = connectionString;
		}

		//public static ApplicationDbContext Create(string host)
		//{
		//	var cs = Tenancy.GetConnectionForHost(host);
		//	return new ApplicationDbContext(cs);
		//}

	}
}