﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Dapper;
using Elmah.Contrib.WebApi;
using IDR.Data;
using IDR.Data.CallCenter;
using IDR.Domain;
using IDR.Domain.CallCenter;
using IDR.Domain.Cms.Dto;
using IDR.Domain.Portal.Dto;
using IDR.Services;
using IDR.Services.Models.CallCenter;
using IDR.web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;

namespace Infu.Api.App
{
    public class PatientService
    {
        private readonly string _cs;
        private SqlConnection _cn;
        private readonly PracticeService _practiceService;
        private readonly PhysiciansService _physiciansService;
        private readonly CallCenterService _callCenterService;
        private readonly NodeLibraryService _libraryService;
        private readonly InfuProcedureMessageProvider _messageService;
        private readonly NotificationService _notificationService;
        private readonly PDFService _pdfService;
        private readonly GoogleMapService _googleMapService;


        public PatientService()
        {
            _cs = ConfigurationManager.ConnectionStrings["IdrDataContext"].ConnectionString;

            var ctx = new ApplicationDbContext();
            var audit = new AuditService();
            var dt = new DateService();
            var usrMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(ctx));
            _messageService = new InfuProcedureMessageProvider();

            _notificationService = new NotificationService(ctx, audit);
            var procSvc = new ProcedureService(ctx, dt, audit, _messageService, _notificationService);
            var physSvc = new PhysiciansService(ctx, audit, procSvc, usrMgr, _messageService);
            _practiceService = new PracticeService(ctx, audit, dt, physSvc);
            _callCenterService = new CallCenterService(ctx, physSvc, dt, audit, _messageService);
            _physiciansService = new PhysiciansService(ctx, audit, procSvc, usrMgr, _messageService);
            _libraryService = new NodeLibraryService(ctx, audit, _pdfService, _googleMapService);
        }

        #region Create Patient
        public async Task<Patient> Create(Patient patient)
        {
            try
            {
                _cn = new SqlConnection(_cs);
                _cn.Open();
                var facilityId = await AssociateFacility(patient);
                await UpdateCompany(facilityId, patient);

                var physicianId = await AssociatePhysician(patient, facilityId);
                var newlyCreatedPatient = await AddPatient(patient, facilityId, physicianId);
                SendAccessCodeToInfuSystem(newlyCreatedPatient, patient);

                _cn.Close();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new Exception("Server Error: Unable to complete request.");
            }

            return patient;
        }

        private async Task<int> AssociateFacility(Patient patient)
        {
            int practiceId;

            var practiceRef = await _cn.QueryFirstOrDefaultAsync(
                "Select * from CallCenterPracticeReferences where InfuFacilityId = @FacilityId;",
                new
                {
                    patient.FacilityId
                });

            if (practiceRef == null)
            {
                var practice = await CreateCompany(patient);
                practiceId = practice.Id;
            }
            else
            {
                practiceId = practiceRef.PracticeId;
            }

            return practiceId;
        }

        private async Task<PracticeDto> CreateCompany(Patient patient)
        {
            var defaultPayorId = await _cn.ExecuteScalarAsync<int>("SELECT TOP 1 Id FROM CallCenterPayors");
            var callCenterId = await _cn.ExecuteScalarAsync<int>("SELECT TOP 1 Id FROM CallCenters");

            var practice = await _practiceService.Create(true);
            var updated = Mapper.Map<PracticeDto>(practice);

            updated.Name = patient.FacilityName;
            updated.StreetAddress.Address1 = patient.FacilityAddressLine1 ?? "";
            updated.StreetAddress.City = patient.FacilityCity ?? "";
            updated.StreetAddress.State = patient.FacilityState ?? "";
            updated.StreetAddress.PostalCode = patient.FacilityZip ?? "";
            updated.PayorId = defaultPayorId;
            updated.CallCenterId = callCenterId;

            updated.StartCalls = 1;
            updated.EndCalls = 3;
            updated.SatisfactionInitial = 7;
            updated.SatisfactionFinal = 14;

            updated.InfuFacilityId = patient.FacilityId;
            updated.PracticeCode = patient.FacilityId;

            await _practiceService.UpdatePractice(updated);

            return updated;
        }

        private async Task<int> AssociatePhysician(Patient patient, int practiceId)
        {
            var existingPhysicians = _cn.Query<PhysicianDto>("SELECT * FROM Physicians WHERE CompanyId = @practiceId", new { practiceId }).ToList();
            int id;

            if (!existingPhysicians.Any())
            {
                // Create the default physician
                var physician = await _physiciansService.AddPhysician(practiceId);

                id = physician.Id;
            }
            else
            {
                id = existingPhysicians.First().Id;
            }

            await AssociateDefaultPlan(patient, id);

            return id;
        }

        private async Task AssociateDefaultPlan(Patient patient, int physicianId)
        {
            var libraryId = await GetLibraryId("content-template", patient.PlanCpt, physicianId);

            // No plan exists; create it.
            if (libraryId == 0)
            {
                var masterId = await GetLibraryId("master-template", patient.PlanCpt);

                if (masterId > 0)
                {
                    var library = await _libraryService.CopyLibrary(masterId, proxyId: physicianId, isContent: true);
                }
            }
        }

        private async Task<CallCenterPatientDto> AddPatient(Patient patient, int facilityId, int physicianId)
        {
            var cptCode = await GetInfuCpt(patient, facilityId, physicianId);

            if (cptCode == null)
            {
                var masterId = await GetLibraryId("master-template", patient.PlanCpt);
                var library = await _libraryService.CopyLibrary(masterId, proxyId: physicianId, isContent: true);
                cptCode = await GetInfuCpt(patient, facilityId, physicianId);
            }

            var pt = new CallCenterPatientDto
            {
                CptCode = cptCode.Code,
                Age = patient.Age,
                CptId = cptCode.Id,
                FacilityId = facilityId,
                FirstName = patient.FirstName,
                Gender = (PatientGender)patient.Gender,
                LastName = patient.LastName,
                Mobile = true,
                Phone = patient.Phone,
                SpanishSpeaking = patient.IsSpanishSpeaking,
                StartTime = patient.TreatmentStartDate,
                StartDate = patient.TreatmentStartDate,
                PayorId = cptCode.PayorId,
                IsDualCath = patient.IsDualCath,
                PhysicianId = physicianId,
                PumpSerial = patient.PumpSerial,
                PumpSerial2 = patient.PumpSerial2,

                InfuPatientId = patient.HDMSID,

                InfuVolume = patient.Volume,
                InfuRate = patient.Rate,
                InfuCck = patient.CCK,
                InfuTreatmentPlan = patient.TreatmentPlan,
                InfuPumpModel = patient.PumpModel,
                InfuHaveConsent = patient.HaveConsent,
                InfuHomeDisconnect = patient.HomeDisconnect,
                InfuImSafetyFeatures = patient.IMSafetyFeatures
            };

            //var proc =
            CallCenterPatientDto newCallCenterPatient = await _callCenterService.CreatePatient(pt, true, true, true);

            if (newCallCenterPatient != null)
                SendAppInstructions(newCallCenterPatient, newCallCenterPatient.AccessCode, newCallCenterPatient.PracticeName, false, true);

            return newCallCenterPatient;
        }

        private void SendAppInstructions(CallCenterPatientDto patient, string accessCode, string practiceName, bool sendEmail, bool sendSms)
        {
            try
            {
                var request = new ProcedureCreatedRequest
                {
                    FirstName = patient.FirstName ?? "",
                    PhoneNumber = patient.Phone ?? "",
                    AccessCode = accessCode ?? "",
                    PracticeName = practiceName ?? "",
                    Recipients = new List<string> { patient.Email ?? "" }
                };

                _messageService.SendProcedureCreated(request, sendEmail, sendSms);
            }
            catch (Exception ex)
            {
                //
            }
        }

        private async Task<CptLookup> GetInfuCpt(Patient patient, int facilityId, int physicianId)
        {
            var sql = @"
SELECT TOP 1
	cpt.Id,
	cpt.Code,
	cpt.NodeLibraryId,
	nl.Name,
	nl.PhysicianId,
	ph.CompanyId,
	co.PayorId
FROM CptCodes cpt
INNER JOIN NodeLibraries nl ON nl.Id = cpt.NodeLibraryId 
INNER JOIN  Physicians ph ON ph.Id = nl.PhysicianId
INNER JOIN Companies co ON co.Id= ph.CompanyId
WHERE cpt.Code like CONCAT('%',@PlanCpt,'%')
AND cpt.DeletedOn IS NULL
AND ph.Id = @physicianId
AND co.Id = @facilityId
order by id desc";

            var cptCode = await _cn.QueryFirstOrDefaultAsync<CptLookup>(sql, new
            {
                physicianId,
                facilityId,
                patient.PlanCpt
            });

            return cptCode;
        }

        private async Task<int> GetLibraryId(string type, string cptCode, int? physicianId = null)
        {
            var sql = @"SELECT top 1 nl.Id FROM NodeLibraries nl
			INNER JOIN CptCodes cpt on cpt.NodeLibraryId = nl.Id
			WHERE LibraryType = @type
			AND cpt.Code = @cptCode 
			
			AND nl.DeletedOn IS NULL
			AND cpt.DeletedOn IS NULL
";

            if (physicianId.HasValue)
            {
                sql += " AND nl.PhysicianId = @physicianId";
            }

            var libraryId = await _cn.QueryFirstOrDefaultAsync<int>(sql,
                new
                {
                    type,
                    cptCode,
                    physicianId
                });

            return libraryId;
        }

        #endregion

        #region Update Patient
        public async Task<Patient> Update(Patient patient)
        {
            try
            {
                _cn = new SqlConnection(_cs);
                _cn.Open();
                var facilityId = await UpdateFacility(patient);

                var physicianId = await AssociatePhysician(patient, facilityId);
                var newlyUpdatedPatient = await UpdateProcedure(patient, facilityId, physicianId);
                SendAccessCodeToInfuSystem(newlyUpdatedPatient, patient, patient.ResendAC);

                _cn.Close();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new Exception("Server Error: Unable to complete request.");
            }

            return patient;
        }

        private async Task<int> UpdateFacility(Patient patient)
        {
            var id = await AssociateFacility(patient);

            var practice = await _practiceService.GetPractice(null, id);

            practice.Name = patient.FacilityName;
            practice.StreetAddress.Address1 = patient.FacilityAddressLine1 ?? "";
            practice.StreetAddress.City = patient.FacilityCity ?? "";
            practice.StreetAddress.State = patient.FacilityState ?? "";
            practice.StreetAddress.PostalCode = patient.FacilityZip ?? "";

            await _practiceService.UpdatePractice(practice);

            return practice.Id;
        }

        private async Task<int> UpdateCompany(int id, Patient patient)
        {
            var practice = await _practiceService.GetPractice(null, id);

            practice.Name = patient.FacilityName;
            practice.StreetAddress.Address1 = patient.FacilityAddressLine1 ?? "";
            practice.StreetAddress.City = patient.FacilityCity ?? "";
            practice.StreetAddress.State = patient.FacilityState ?? "";
            practice.StreetAddress.PostalCode = patient.FacilityZip ?? "";

            await _practiceService.UpdatePractice(practice);

            return practice.Id;
        }

        private async Task<CallCenterPatientDto> UpdateProcedure(Patient patient, int facilityId, int physicianId)
        {
            var cptCode = await GetInfuCpt(patient, facilityId, physicianId);

            var sql = @"SELECT TOP 1 cpat.Id FROM 
				CallCenterPatients cpat
				WHERE cpat.InfuPatientId = @InfuPatientId";

            var id = await _cn.QueryFirstOrDefaultAsync<int>(sql, new
            {
                InfuPatientId = patient.HDMSID
            });

            var pt = await _callCenterService.GetPatient(id);

            pt.CptCode = cptCode.Code;
            pt.Age = patient.Age;
            //pt.CptId = cptCode.Id;
            pt.FacilityId = facilityId;
            pt.FirstName = patient.FirstName;
            pt.Gender = (PatientGender)patient.Gender;
            pt.LastName = patient.LastName;
            pt.Phone = patient.Phone;
            pt.SpanishSpeaking = patient.IsSpanishSpeaking;
            pt.StartTime = patient.TreatmentStartDate;
            pt.StartDate = patient.TreatmentStartDate;
            pt.IsDualCath = patient.IsDualCath;
            pt.PhysicianId = physicianId;
            pt.PumpSerial = patient.PumpSerial;
            pt.PumpSerial2 = patient.PumpSerial2;
            pt.InfuPatientId = patient.HDMSID;
            pt.InfuVolume = patient.Volume;
            pt.InfuRate = patient.Rate;
            pt.InfuCck = patient.CCK;
            pt.InfuTreatmentPlan = patient.TreatmentPlan;
            pt.InfuPumpModel = patient.PumpModel;
            pt.InfuHaveConsent = patient.HaveConsent;
            pt.InfuHomeDisconnect = patient.HomeDisconnect;
            pt.InfuImSafetyFeatures = patient.IMSafetyFeatures;

            CallCenterPatientDto updatedCallCenterPatient = await _callCenterService.UpdatePatient(pt, true);

            if (updatedCallCenterPatient != null && patient.ResendAC == true && patient.ResendAC != null)
                SendAppInstructions(updatedCallCenterPatient, updatedCallCenterPatient.AccessCode, updatedCallCenterPatient.PracticeName, false, true);

            return pt;
        }

        public void SendAccessCodeToInfuSystem(CallCenterPatientDto newlyCreatedOrUpdatedPatient, Patient patientFromInfuSystem, bool? resendAccessCode = true)
        {
            if (resendAccessCode == true && resendAccessCode != null)
            {
                try
                {
                    var infuApiTest = ConfigurationManager.AppSettings["InfuApiTestUrl"];
                    var authClient = new RestClient($"{infuApiTest}/api/v1/auth/ExternalService");

                   // var infuApiProd = ConfigurationManager.AppSettings["InfuApiProdUrl"];
                   // var authClient = new RestClient($"{infuApiProd}/api/v1/auth/ExternalService");

                    var authRequest = new RestRequest(Method.POST);
                    authRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    authRequest.AddParameter("application/x-www-form-urlencoded",
                        "Username=mobile%40infusystem.com&Password=Infu%24yst3m&ApplicationName=InfuMobile",
                        ParameterType.RequestBody);
                    var authResponse = authClient.Execute<InfuAuthResponse>(authRequest);

                    var deserializer = new JsonDeserializer();
                    var auth = deserializer.Deserialize<InfuAuthResponse>(authResponse);

                    var aobAccessCodeClient = new RestClient($"{infuApiTest}/odata/Mobile/Default.UpdateAccessCode%28%29");
                    //var aobAccessCodeClient = new RestClient($"{infuApiProd}/odata/Mobile/Default.UpdateAccessCode%28%29");

                    var aobAccessCodeRequest = new RestRequest(Method.POST);
                    // aobAccessCodeRequest.AddHeader("Cache-Control", "no-cache");
                    aobAccessCodeRequest.AddHeader("Accept", "application/json;odata=verbose");
                    aobAccessCodeRequest.AddHeader("Content-Type", "application/json;odata=verbose");
                    aobAccessCodeRequest.AddHeader("X-Infusystem-Authentication", auth.token);
                    aobAccessCodeRequest.AddHeader("OData-MaxVersion", "4.0");

                    InfuAoBReturnAccessCode aobAccessCode = new InfuAoBReturnAccessCode();
                    aobAccessCode.patientID = patientFromInfuSystem.HDMSID;
                    aobAccessCode.facilityID = patientFromInfuSystem.FacilityId;
                    aobAccessCode.accessCode = newlyCreatedOrUpdatedPatient.AccessCode;

                    var message = JsonConvert.SerializeObject(aobAccessCode);

                    aobAccessCodeRequest.AddParameter("application/json", message, ParameterType.RequestBody);
                    var aobAccessCodeResponse = aobAccessCodeClient.Execute(aobAccessCodeRequest);
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    throw new Exception("Server Error: Unable to complete request while returning AccessCode to the InfuSystem.");
                }
            }
        }
        #endregion
    }

    public class AuditService : IAuditService
    {
        public void Audit(IAuditEntity entity)
        {
        }
    }

    public class CptLookup
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int NodeLibraryId { get; set; }
        public string Name { get; set; }
        public string CtssCode { get; set; }
        public int PhysicianId { get; set; }
        public int CompanyId { get; set; }
        public int PayorId { get; set; }
    }

    public class InfuAuthResponse
    {
        public string userId { get; set; }
        public string token { get; set; }
    }

    public class InfuAoBReturnAccessCode
    {
        public string patientID { get; set; }
        public string facilityID { get; set; }
        public string accessCode { get; set; }
    }
}