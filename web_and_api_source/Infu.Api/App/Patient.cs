﻿using System;

namespace Infu.Api.App
{
	public class Patient
	{
		// Facility
		public string FacilityId { get; set; }
		public string FacilityName { get; set; }
		public string FacilityAddressLine1 { get; set; }
		public string FacilityCity { get; set; }
		public string FacilityState { get; set; }
		public string FacilityZip { get; set; }

		// Patient
		public bool CCK { get; set; }
		public string HDMSID { get; set; } // Unique InfuSystem patient identifier
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int Gender { get; set; } // Male = 0 Female = 1, Non-Binary = 2
		public int Language { get; set; } // English = 1, Spanish = 2
		public int Age { get; set; }
		public string Phone { get; set; }

		public bool IsSpanishSpeaking => Language == 2;

		// Treatment
		public string PumpModel { get; set; }
		public DateTime TreatmentStartDate { get; set; }
		public string TreatmentPlan { get; set; }
		public string PumpSerial { get; set; }
		public string PumpSerial2 { get; set; }
		public int Volume { get; set; }
		public decimal Rate { get; set; }
		public bool IsDualCath { get; set; }
		public bool IMSafetyFeatures { get; set; } // Maps to 'Active'

		public string HomeDisconnect { get; set; } // A, S, N
		public bool HaveConsent { get; set; }

		public string PlanCpt
		{
			get
			{
				var model = (PumpModel ?? "").Replace(" ", "");
				var lang = IsSpanishSpeaking ? "ES" : "EN";

                return $"{model}_{lang}";
			}
		}

        public bool? ResendAC { get; set; }
    }
}