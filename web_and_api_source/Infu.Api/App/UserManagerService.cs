﻿using System.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Infu.Api
{
	public class InfuUserManagerService //: IInfuUserManagerService
	{
		private readonly UserManager<InfuApplicationUser> _userManager;

		public InfuUserManagerService()
		{
			var context = new InfuApplicationDbContext(ConfigurationManager.ConnectionStrings["InfuDataContext"].ConnectionString);
			_userManager = new UserManager<InfuApplicationUser>(new UserStore<InfuApplicationUser>(context));
			_userManager.UserValidator = new UserValidator<InfuApplicationUser>(_userManager)
			{
				AllowOnlyAlphanumericUserNames = false
			};
		}

		public IdentityResult Create(InfuApplicationUser user)
		{
			return _userManager.Create(user);
		}

		public IdentityResult Create(InfuApplicationUser user, string password)
		{
			var result = _userManager.Create(user, password);
			return result;
		}

		public InfuApplicationUser FindById(string userId)
		{
			return _userManager.FindById(userId);
		}
	}
}