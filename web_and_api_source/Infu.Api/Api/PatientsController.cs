﻿using System.Web.Http;
using System.Threading.Tasks;
using Infu.Api.App;

namespace Infu.Api
{
	[BearerRequired]
	public class PatientsController : ApiController
	{
		//public PatientsController()
		//{
			
		//}

		public async Task<Patient> Post(Patient patient)
		{
			return await new PatientService().Create(patient);
		}

		public async Task<Patient> Put(Patient patient)
		{
			return await new PatientService().Update(patient);
		}
	}
}
