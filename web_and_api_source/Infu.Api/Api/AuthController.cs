﻿using System.Web.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Infu.Api
{
	[RoutePrefix("api/auth")]
	//[BearerRequired]
	public class AuthController : ApiController
	{
		private readonly InfuUserManagerService _userService;

		public AuthController(InfuUserManagerService userService)
		{
			_userService = userService;
		}

		public async Task<object> Get()
		{
			_userService.Create(new InfuApplicationUser
			{
				Active = true,
				Email = "api@infusystem.com",
				UserName = "api@infusystem.com"
			}, "infu$yst3m");
			//return await _usersService.GetUser(User.Identity.Name);
			return null;
		}
	}
}
