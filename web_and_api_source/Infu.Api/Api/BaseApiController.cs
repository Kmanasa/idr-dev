using System.Web.Http;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity;

namespace Infu.Api
{
    public class BaseApiController : ApiController
    {
        public string CurrentUserId { get; set; }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            try
            {
                CurrentUserId = User.Identity.GetUserId();
            }
            catch
            {
                CurrentUserId = string.Empty;
            }

            base.Initialize(controllerContext);
        }
    }
}