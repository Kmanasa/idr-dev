﻿namespace IDR.Messaging
{
	public class AobNotification
	{
		public string Url { get; set; }
		public string AccessCode { get; set; }

		public override string ToString()
		{
			return $"{Url}, {AccessCode}";
		}
	}
}