﻿
namespace IDR.Messaging
{
    public class ProcedurePushNotification
    {
        public int ProcedureId { get; set; }
        public string ProcedureCode { get; set; }
        public string Message { get; set; }
	    public string Connection { get; set; }
    }
}
