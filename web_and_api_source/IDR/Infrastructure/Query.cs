﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Mappers;
using IDR.Data;
//using IDR.Domain.PracticeUsers.Queries;

namespace IDR.Infrastructure
{
	public abstract class Query<T>
	{
		protected ApplicationDbContext Context;

		protected Query(ApplicationDbContext context)
		{
			Context = context;
		}

		public virtual T Execute()
		{
			throw new NotImplementedException();
		}

		public MappingEngine CreateMapper<TP>() where TP : Profile, new()
		{
			var store = new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
			store.AssertConfigurationIsValid();
			var engine = new MappingEngine(store);
			store.AddProfile<TP>();

			return engine;
		}
	}

	public abstract class AsyncQuery<T>
	{
		protected ApplicationDbContext Context;

		protected AsyncQuery(ApplicationDbContext context)
		{
			Context = context;
		}

		public PagingParameters Paging { get; set; }

		public AsyncQuery<T> SetPaging(PagingParameters paging)
		{
		    if (paging == null)
		    {
		        paging = new PagingParameters();
		    }

			Paging = paging;

			return this;
		}

		public abstract Task<T> Execute();

		public MappingEngine CreateMapper<TP>() where TP : Profile, new()
		{
			var store = new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
			store.AssertConfigurationIsValid();
			var engine = new MappingEngine(store);
			store.AddProfile<TP>();

			return engine;
		}
	}
}
