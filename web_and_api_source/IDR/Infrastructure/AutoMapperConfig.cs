﻿using AutoMapper;
using IDR.Data;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Cms.Dto;
using IDR.Domain.Companies.Dto;
using IDR.Domain.Contacts.Dtos;
using IDR.Domain.Disciplines.Dto;
using IDR.Domain.Images.Dto;
using IDR.Domain.Membership;
using IDR.Domain.Membership.Dto;
using IDR.Domain.Portal.Dto;
using IDR.Domain.PracticeUsers.Dto;
using IDR.web.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using IDR.Data.CallCenter;
using IDR.Domain;
using IDR.Domain.CallCenter;
using IDR.Domain.External.Dto;
using IDR.Domain.Notifications;

namespace IDR.Infrastructure
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            #region external
            Mapper.CreateMap<NodeLibrary, MasterTemplateExternalDto>();
            Mapper.CreateMap<ProcedureNotification, InboxItemDto>()
                .ForMember(i => i.Title, o => o.MapFrom(pn => pn.Notification.Title))
                .ForMember(i => i.Message, o => o.MapFrom(pn => pn.Notification.Message))
                .ForMember(i => i.Required, o => o.MapFrom(pn => pn.Notification.Required))
                .ForMember(i => i.TriggerText, o => o.MapFrom(pn => pn.Notification.TriggerText))
                ;
            #endregion


            Mapper.CreateMap<ApplicationUser, UserDto>()
                .ForMember(d => d.Roles, o => o.Ignore());

            Mapper.CreateMap<NodeLibrary, NodeLibraryDto>();

            Mapper.CreateMap<NodeType, NodeTypeDto>();

            Mapper.CreateMap<NodeLibraryAttribute, NodeLibraryAttributeDto>()
                .ForMember(a => a.Name, o => o.MapFrom(at => at.Attribute.Name));

            Mapper.CreateMap<AllowedType, AllowedTypeDto>();

            Mapper.CreateMap<PropertyType, PropertyTypeDto>();

            Mapper.CreateMap<PropertyType, PropertyTypeDto>();

            Mapper.CreateMap<Node, NodeDto>();

            Mapper.CreateMap<CptCode, CptCodeDto>();

            Mapper.CreateMap<Data.Attribute, AttributeDto>();

            Mapper.CreateMap<Property, PropertyDto>();

            Mapper.CreateMap<Image, ImageDto>();

            Mapper.CreateMap<Contact, ContactDto>();

			Mapper.CreateMap<ContactDto, Contact>();

            Mapper.CreateMap<Address, AddressDto>();

            Mapper.CreateMap<AddressDto, Address>();

            Mapper.CreateMap<ImageFileDto, ImageFile>();

            Mapper.CreateMap<ImageFile, ImageFileDto>();

            Mapper.CreateMap<Company, CompanyDto>()
                .ForMember(x => x.Facilities, o => o.Ignore())
                .ForMember(x => x.RemoteFacilities, o => o.Ignore());

	        Mapper.CreateMap<Company, PracticeDto>()
		        .ForMember(x => x.Facilities, o => o.Ignore())
		        .ForMember(x => x.RemoteFacilities, o => o.Ignore());

	        Mapper.CreateMap<CompanyDto, PracticeDto>();

			Mapper.CreateMap<Company, CompanySearchDto>();

            Mapper.CreateMap<Facility, FacilityDto>();
			
            Mapper.CreateMap<RemoteFacility, FacilityDto>()
                .ForMember(rf => rf.Name, o => o.MapFrom(rf => rf.Facility.Name))
                .ForMember(rf => rf.Id, o => o.MapFrom(rf => rf.Facility.Id));

            Mapper.CreateMap<Procedure, PatientLibraryDto>()
                .ForMember(p => p.LibraryCtss, o => o.MapFrom(s => s.NodeLibrary.CtssCode))
                .ForMember(p => p.LibraryName, o => o.MapFrom(s => s.NodeLibrary.Name));

            Mapper.CreateMap<PhysiciansAssistant, PhysiciansAssistantDto>()
                .ForMember(d => d.ProvisionalKey, d => d.MapFrom(o => o.ProvisionalKey.HasValue
                    ? o.ProvisionalKey.Value
                    : Guid.Empty));

            Mapper.CreateMap<Physician, BaseUserDto>();

            Mapper.CreateMap<PhysiciansAssistant, BaseUserDto>();

            Mapper.CreateMap<PracticeAdmin, BaseUserDto>();
            Mapper.CreateMap<SiteAdmin, BaseUserDto>();

            Mapper.CreateMap<Scheduler, BaseUserDto>();

            Mapper.CreateMap<Physician, PhysicianDto>()
                .ForMember(d => d.Patients, d => d.Ignore());

            Mapper.CreateMap<PracticeAdmin, PracticeAdminDto>().ReverseMap();
            Mapper.CreateMap<SiteAdmin, SiteAdminDto>().ReverseMap();
            Mapper.CreateMap<Scheduler, SchedulerDto>().ReverseMap();

			Mapper.CreateMap<Physician, PhysicianDto>();

            Mapper.CreateMap<Procedure, PatientProcedureDto>();

            Mapper.CreateMap<Patient, PatientDto>();

            Mapper.CreateMap<PatientDto, Patient>();

            Mapper.CreateMap<PatientProcedureDto, Procedure>();

            Mapper.CreateMap<PatientProcedureDto, Patient>();

            Mapper.CreateMap<Discipline, DisciplineDto>()
                .ForMember(d => d.Items, d => d.Ignore());

            Mapper.CreateMap<DisciplineEntityItem, DisciplineEntityItemDto>();

            Mapper.CreateMap<DisciplineEntityItem, DisciplineDto>()
                .ForMember(d => d.Id, d => d.MapFrom(s => s.DisciplineId))
                .ForMember(d => d.Name, d => d.MapFrom(s => s.Discipline.Name))
                .ForMember(d => d.Description, d => d.MapFrom(s => s.Discipline.Description));

            Mapper.CreateMap<IdentityRole, RoleDto>();

            //Mapper.CreateMap<TreatmentBindable, TreatmentBindableDto>();

            Mapper.CreateMap<Notification, NotificationDto>();

            Mapper.CreateMap<NotificationResponse, NotificationResponseDto>();



	        Mapper.CreateMap<CallCenterProvider, CallCenterProviderDto>()
				;
            Mapper.CreateMap<CallCenter, CallCenterDto>();
	        Mapper.CreateMap<Facility, CallCenterFacilityDto>();
	        Mapper.CreateMap<Company, CallCenterFacilityDto>();
	        Mapper.CreateMap<CallCenterProviderFacility, CallCenterProviderFacilityDto>();
	        Mapper.CreateMap<Company, CallCenterFacilityTypeaheadDto>();
		}
	}
}
