﻿using System.Collections.Generic;

namespace IDR.Infrastructure
{
	public class PagedQueryResults<T>
	{
		public PagedQueryResults()
		{
			// Parameterless constructor required for XML serialization 
		}

		public PagedQueryResults(IList<T> results, int totalRecords)
		{
			Results = results;
			TotalRecords = totalRecords;
		}

		public IList<T> Results { get; set; }
		public int TotalRecords { get; set; }
	}
}
