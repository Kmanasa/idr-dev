﻿using IDR.Domain.ImageFiles;

namespace IDR.Domain.Images.Dto
{
	public class ImageDto : IImagable
	{
		public int Id { get; set; }

		[ImageFileInfo(Title = "Library Image", FormType = Constants.ImageFormTypes.Basic)]
		public ImageFileDto Image { get; set; }
	}
}
