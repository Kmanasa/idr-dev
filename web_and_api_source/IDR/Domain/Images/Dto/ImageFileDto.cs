﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IDR.Domain.Images.Dto
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ImageFileInfoAttribute : Attribute
	{
		public string Title { get; set; }

		public string FormType { get; set; }

		public bool IsPublic { get; set; }
	}

	public class ImageFileDto
	{
		public string FileName { get; set; }

		public string EntityName { get; set; }
        
        [StringLength(500)]
        public string Alias { get; set; }

		public int EntityId { get; set; }

		public string ImageType { get; set; }

		public string ImageFormType { get; set; }

		public string ImageTitle { get; set; }

		public int Id { get; set; }
        
        public DateTime? ModifiedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

		public string Url
		{
			get
			{
				return string.Format("/api/imagefiles/{0}", Id);
			}
		}
	}
}
