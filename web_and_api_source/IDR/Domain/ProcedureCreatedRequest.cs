﻿using IDR.Infrastructure;

namespace IDR.Domain
{
    public class ProcedureCreatedRequest : MailRequest
    {
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string AccessCode { get; set; }
        public string PracticeName { get; set; }
    }
}
