﻿using System;

namespace IDR.Domain
{
    public class ProcedureRefDto
    {
        public int LibraryId { get; set; }
        public int PatientId { get; set; }
        public int FacilityId { get; set; }
        public string PatientIdentifier { get; set; }

        public DateTime? StartDate { get; set; }
        public int DurationInDays { get; set; }

        public string LibraryName { get; set; }
        public string CptCode { get; set; }
        public string AccessCode { get; set; }

        public string FacilityName { get; set; }
        public string FacilityStreetAddress1 { get; set; }
        public string FacilityStreetAddress2 { get; set; }
        public string FacilityStreetAddressCity { get; set; }
        public string FacilityStreetAddressState { get; set; }
        public string FacilityStreetAddressPostalCode { get; set; }
        public string FacilityManagerEmail { get; set; }
        public string FacilityManagerContactPhone { get; set; }
        public string FacilityPatientInformationEmail { get; set; }
        public string FacilityPatientInformationPhone { get; set; }

        public string PracticeName { get; set; }
        public string PracticeContactPhone { get; set; }
        public string PracticeContactEmail { get; set; }
        public string PracticeSchedulingContactName { get; set; }
        public string PracticeSchedulingContactPhone { get; set; }
        public string PracticeSchedulingContactEmail { get; set; }
        public string PracticeAddress1 { get; set; }
        public string PracticeAddress2 { get; set; }
        public string PracticeCity { get; set; }
        public string PracticeState { get; set; }
        public string PracticePostalCode { get; set; }
        public string PracticeOnCallServicePhone { get; set; }
        public string PracticeOnCallServiceEmail { get; set; }
        public string PracticeImageRef { get; set; }


        public string PhysicianTitle { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string PhysicianDirectContactPhone { get; set; }
        public string PhysicianDirectContactEmail { get; set; }
        public string PhysicianSchedulingContactName { get; set; }
        public string PhysicianSchedulingContactPhone { get; set; }
        public string PhysicianSchedulingContactExt { get; set; }
        public string PhysicianSchedulingContactEmail { get; set; }
        public string PhysicianImageRef { get; set; }
        public int? PhysicianCompanyId { get; set; }
        public string PhysicianId { get; set; }

    }
}
