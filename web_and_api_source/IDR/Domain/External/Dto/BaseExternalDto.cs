﻿using System;

namespace IDR.Domain.External.Dto
{
    public class BaseExternalDto
    {
        public int Id { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int? CopyOfId { get; set; }
        public bool IsNew { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}