﻿namespace IDR.Domain.Disciplines.Dto
{
	public class DisciplineEntityItemDto
	{
		public int DisplineId { get; set; }

		public string DisciplineName { get; set; }

		public int EntityId { get; set; }

		public string EntityName { get; set; }

		public int? CopyOfId { get; set; }
	}
}
