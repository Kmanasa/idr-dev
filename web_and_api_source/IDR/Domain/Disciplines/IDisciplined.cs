﻿using IDR.Domain.Disciplines.Dto;

namespace IDR.Domain.Disciplines
{
	public interface IDisciplined
	{
		int Id { get; set; }

		DisciplineDto Discipline { get; set; }
	}
}
