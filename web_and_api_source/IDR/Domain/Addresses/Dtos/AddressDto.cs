﻿using System.Linq;

namespace IDR.Domain.Addresses.Dtos
{
    public class AddressDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }
		public int EntityId { get; set; }
		public string EntityName { get; set; }
		public string AddressType { get; set; }
		public string AddressFormType { get; set; }
		public string AddressTitle { get; set; }

		public string DisplayAddress
		{
			get
			{
				var items = new [] { Address1, Address2, City, State, PostalCode, Country };

				return string.Join(", ", items.Where(x => !string.IsNullOrEmpty(x)));
			}
		}
	}
}
