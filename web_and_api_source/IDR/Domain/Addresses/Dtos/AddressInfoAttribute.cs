﻿using System;

namespace IDR.Domain.Addresses.Dtos
{
    [AttributeUsage(AttributeTargets.Property)]
    public class AddressInfoAttribute : Attribute
    {
        public string Title { get; set; }

        public string FormType { get; set; }
    }
}