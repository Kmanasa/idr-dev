﻿using System;
using IDR.Domain.Addresses;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Contacts;
using IDR.Domain.Contacts.Dtos;
using IDR.Domain.Images;
using IDR.Domain.Images.Dto;

namespace IDR.Domain.Companies.Dto
{
	public class FacilityDto : IContactable, IAddressable, IImagable
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public int CompanyId { get; set; }

		public CompanyDto Company { get; set; }

		[ImageFileInfo(Title="Logo", FormType=Constants.ImageFormTypes.Logo)]
		public ImageFileDto Logo { get; set; }

		[ContactInfo(Title = "Facility Manager Contact", FormType = Constants.ContactFormTypes.Person)]
		public ContactDto FacilityContact { get; set; }

		[ContactInfo(Title = "Patient Information", FormType = Constants.ContactFormTypes.Simple)]
		public ContactDto PatientContact { get; set; }

		[AddressInfo(Title = "Mailing Address", FormType = Constants.AddressFormTypes.Service)]
		public AddressDto MailingAddress { get; set; }

		[AddressInfo(Title = "Street Address", FormType = Constants.AddressFormTypes.Service)]
		public AddressDto StreetAddress { get; set; }

        public DateTime? DeletedOn { get; set; }

        public bool IsNew { get; set; }
    }
}
