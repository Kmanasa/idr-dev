﻿using System;
using IDR.Data;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Contacts.Dtos;

namespace IDR.Domain.Companies.Dto
{
    public class CompanySearchDto
    {
        public int Id { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsNew { get; set; }
        public CompanyType CompanyType { get; set; }
        public string Name { get; set; }
        public string Primary { get; set; }
        public string Sales { get; set; }
        public string Mailing { get; set; }
        public string Corporate { get; set; }

        public AddressDto BillingAddress { get; set; }
        public AddressDto CorporateAddress { get; set; }
        public AddressDto MailingAddress { get; set; }
        public AddressDto StreetAddress { get; set; }

        public ContactDto PrimaryContact { get; set; }
        public ContactDto SalesContact { get; set; }
        public ContactDto BillingContact { get; set; }
        public ContactDto SchedulingContact { get; set; }
    }
}
