﻿using System;
using IDR.Data;
using IDR.Domain.Addresses;
using IDR.Domain.Addresses.Dtos;
using IDR.Domain.Contacts;
using IDR.Domain.Contacts.Dtos;
using System.Collections.Generic;
using IDR.Domain.Images;
using IDR.Domain.Images.Dto;
using IDR.Domain.Portal.Dto;

namespace IDR.Domain.Companies.Dto
{
    public class CompanyDto : IContactable, IAddressable, IImagable
    {
        public CompanyDto()
        {
            Facilities = new List<FacilityDto>();
            RemoteFacilities = new List<FacilityDto>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }
        public bool IsPracticeAdmin { get; set; }

        public string PatientEmailTemplate { get; set; }
        public string PhysicianEmailTemplate { get; set; }
        public string PhysiciansAssistantEmailTemplate { get; set; }
        public string PracticeAdminEmailTemplate { get; set; }

        public int MaxNamedUsers { get; set; }
        public int MaxPhysicians { get; set; }
        public int MaxFacilities { get; set; }
        public int MaxPlans { get; set; }

        public decimal CostPerUser { get; set; }
        public decimal CostPerPhysician { get; set; }
        public decimal CostPerFacility { get; set; }
        public decimal CostPerPlan { get; set; }
        public decimal Discount { get; set; }
		public int PayorId { get; set; }

		public decimal UserCost => MaxNamedUsers * CostPerUser;
        public decimal PhysicianCost => MaxPhysicians * CostPerPhysician;
        public decimal FacilityCost => MaxFacilities * CostPerFacility;
        public decimal PlanCost => MaxPlans * CostPerPlan;
        public decimal TotalCost => PlanCost + UserCost + PhysicianCost + FacilityCost - Discount;

        public DateTime LicenseExpiration { get; set; }

        [ImageFileInfo(Title = "Logo", FormType = Constants.ImageFormTypes.Logo)]
        public ImageFileDto Logo { get; set; }

        [ContactInfo(Title = "Primary Contact", FormType = Constants.ContactFormTypes.Person)]
        public ContactDto PrimaryContact { get; set; }

        [ContactInfo(Title = "Sales Contact", FormType = Constants.ContactFormTypes.Person)]
        public ContactDto SalesContact { get; set; }

        [ContactInfo(Title = "Billing Contact", FormType = Constants.ContactFormTypes.Person)]
        public ContactDto BillingContact { get; set; }

        [ContactInfo(Title = "Scheduling Contact", FormType = Constants.ContactFormTypes.Service)]
        public ContactDto SchedulingContact { get; set; }

        [AddressInfo(Title = "Mailing Address", FormType = Constants.AddressFormTypes.Service)]
        public AddressDto MailingAddress { get; set; }

        [AddressInfo(Title = "Billing Address", FormType = Constants.AddressFormTypes.Service)]
        public AddressDto BillingAddress { get; set; }

        [AddressInfo(Title = "Street Address", FormType = Constants.AddressFormTypes.Service)]
        public AddressDto StreetAddress { get; set; }

        [AddressInfo(Title = "Physical Address", FormType = Constants.AddressFormTypes.Service)]
        public AddressDto CorporateAddress { get; set; }

        public string TimeZone { get; set; }

        public List<PhysicianDto> Physicians { get; set; }

        public List<FacilityDto> Facilities { get; set; }

        public List<FacilityDto> RemoteFacilities { get; set; }
    }
}
