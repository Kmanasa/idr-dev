﻿using System;

namespace IDR.Domain.Contacts.Dtos
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ContactInfoAttribute : Attribute
	{
		public string Title { get; set; }

		public string FormType { get; set; }
	}

	public class ContactDto
	{
		public int Id { get; set; }

		public int EntityId { get; set; }

		public string EntityName { get; set; }

		public string ServiceName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public string OfficePhone { get; set; }

		public string OfficeExt { get; set; }

		public string CellPhone { get; set; }

		public string PersonalPhone { get; set; }

		public string ContactType { get; set; }

		public string ContactFormType { get; set; }

		public string ContactTitle { get; set; }

		public bool IsDirty { get; set; }
	}
}
