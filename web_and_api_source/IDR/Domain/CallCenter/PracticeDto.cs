﻿using IDR.Domain.Companies.Dto;

namespace IDR.Domain.CallCenter
{
	public class PracticeDto : CompanyDto
	{
		public int CallCenterId { get; set; }
		public int StartCalls { get; set; }
		public int EndCalls { get; set; }
		public int SatisfactionInitial { get; set; }
		public int SatisfactionFinal { get; set; }
		public string CallCenterNotes { get; set; }
	}
}