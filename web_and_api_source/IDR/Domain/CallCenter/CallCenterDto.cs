﻿namespace IDR.Domain.CallCenter
{
	public class CallCenterDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}