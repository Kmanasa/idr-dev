﻿using System.Collections.Generic;

namespace IDR.Domain.CallCenter
{
	public class CallCenterFacilityDto
	{
		public CallCenterFacilityDto()
		{
			CptCodes = new List<CallCenterFacilityCpt>();
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public bool IsNew { get; set; }
		public int PayorId { get; set; }

		public List<CallCenterFacilityCpt> CptCodes { get; set; }
	}

	public class CallCenterFacilityCpt
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public int NodeLibraryId { get; set; }
	}
}