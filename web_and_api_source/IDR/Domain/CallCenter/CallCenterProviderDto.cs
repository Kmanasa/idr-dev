﻿using System.Collections.Generic;
using IDR.Data.CallCenter;

namespace IDR.Domain.CallCenter
{
	public class CallCenterProviderDto
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Title { get; set; }
		public string Suffix { get; set; }

		public ProviderType ProviderType { get; set; }
		public List<CallCenterProviderFacilityDto> CallCenterProviderFacilities { get; set; }
	}

	public class CallCenterProviderFacilityDto
	{
		public int Id { get; set; }
		public int FacilityId { get; set; }
		public string Name { get; set; }
	}
}