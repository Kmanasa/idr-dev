﻿using System.Collections.Generic;

namespace IDR.Domain.Membership.Dto
{
	public class UserDto
	{
		public UserDto()
		{
            Physicians = new List<object>();
			Roles = new List<UserRoleDto>();
		}

		public string Id { get; set; }

        public int CompanyId { get; set; }

		public string UserName { get; set; }

		public string Firstname { get; set; }

		public string Lastname { get; set; }

		public string PhoneNumber { get; set; }

		public List<UserRoleDto> Roles { get; set; }

        public bool IsDemoPhysician { get; set; }

        public bool IsAdmin { get; set; }

		public bool Active { get; set; }
		public bool Bilingual { get; set; }

		public List<object> Physicians { get; set; }
	}
}
