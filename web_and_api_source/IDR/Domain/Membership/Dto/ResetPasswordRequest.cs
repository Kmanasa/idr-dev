﻿using IDR.Domain.Portal.Dto;
using IDR.Infrastructure;

namespace IDR.Domain.Membership.Dto
{
	public class ResetPasswordRequest : MailRequest
	{
		public string Email { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Url { get; set; }

		public string Host { get; set; }
	}
}
