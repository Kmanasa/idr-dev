﻿namespace IDR.Domain.Membership
{
	public class RoleDto
	{
		public string Id { get; set; }

		public string Name { get; set; }

	}
}
