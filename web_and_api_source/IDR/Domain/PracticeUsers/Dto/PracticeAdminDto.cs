﻿using IDR.Domain.Contacts;
using IDR.Domain.Contacts.Dtos;

namespace IDR.Domain.PracticeUsers.Dto
{
    public class PracticeAdminDto : BaseUserDto, IContactable
    {
		public PracticeAdminDto()
        {
			UserTypeTitle = "Practice Admin";

			UserType = "practiceadmin";
        }

		[ContactInfo(FormType = Constants.ContactFormTypes.Simple, Title = "Direct Contact")]
		public ContactDto DirectContact { get; set; }
	}
}
