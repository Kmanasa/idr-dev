﻿using IDR.Domain.Companies.Dto;
using IDR.Domain.Mailings;
using IDR.Domain.Mailings.Dtos;
using System;
using System.Collections.Generic;

namespace IDR.Domain.PracticeUsers.Dto
{
	public class BaseUserDto : IMailing
	{
		public int Id { get; set; }

        public DateTime? DeletedOn { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string ProvisionalEmail { get; set; }

		public string UserTypeTitle { get; set; }

		public string UserType { get; set; }

		public string UserName { get; set; }

		public string UserId { get; set; }

		public bool IsProvisional { get; set; }

		public Guid? ProvisionalKey { get; set; }

		public bool InvitationSent { get; set; }

		public List<MailingDto> Mailings { get; set; }

        public int? CompanyId { get; set; }

        public CompanyDto Practice { get; set; }

		public int? PracticeId { get; set; }

		public string Email
		{
			get { return ProvisionalEmail;  }
		}
	}
}
