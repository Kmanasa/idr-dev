﻿using IDR.Domain.Contacts;

namespace IDR.Domain.PracticeUsers.Dto
{
	public class SiteAdminDto : BaseUserDto, IContactable
	{
		public SiteAdminDto()
		{
			UserTypeTitle = "Site Admin";

			UserType = "calladmin";
		}
	}
}