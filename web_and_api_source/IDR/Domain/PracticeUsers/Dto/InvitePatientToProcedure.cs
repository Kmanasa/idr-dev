﻿using IDR.Domain.Portal.Dto;
using IDR.Infrastructure;

namespace IDR.Domain.PracticeUsers.Dto
{
	public class InvitePatientToProcedureRequest : MailRequest
	{
		public PatientProcedureDto PatientProcedure { get; set; }

		public PhysicianDto Physician { get; set; }

        public string PracticeName { get; set; }

		public string Host { get; set; }
	}
}
