﻿namespace IDR.Domain.Cms.Dto
{
	public class PropertyTypeDto
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public string UIKey { get; set; }

		public bool IsRequired { get; set; }

		public string DomainName { get; set; }

		public string General { get; set; }
	}
}
