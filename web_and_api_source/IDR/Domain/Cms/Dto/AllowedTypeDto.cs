﻿namespace IDR.Domain.Cms.Dto
{
	public class AllowedTypeDto 
	{
		public int Id { get; set; }

		public int AllowedTypeId { get; set; }

		public string AllowedTypeName { get; set; }

		public int NodeTypeId { get; set; }
	}
}
