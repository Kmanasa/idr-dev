﻿
namespace IDR.Domain.Cms.Dto
{
    public class NodeLibraryAttributeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NodeLibraryId { get; set; }
        public int AttributeId { get; set; }
    }
}
