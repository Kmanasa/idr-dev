﻿namespace IDR.Domain.Cms.Dto
{
	public class TreatmentBindableDto
	{
		public int Id { get; set; }

		public string PathName { get; set; }

		public string Category { get; set; }

		public bool IsImage { get; set; }

	}
}
