﻿namespace IDR.Domain.Cms.Dto
{
	public interface IMappableForm
	{
		string UINodeType { get; set; }
	}
}
