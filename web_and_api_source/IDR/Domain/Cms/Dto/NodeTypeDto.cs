﻿using System.Collections.Generic;

namespace IDR.Domain.Cms.Dto
{
	public class NodeTypeDto
	{
		public NodeTypeDto()
		{
			Properties = new List<PropertyTypeDto>();
		}
		public int Id { get; set; }

		public string Name { get; set; }

		public string UIKey { get; set; }

        public int TemplateId { get; set; }

		public string Description { get; set; }

		public List<PropertyTypeDto> Properties { get; set; }

		public List<AllowedTypeDto> AllowedTypes { get; set; }

		public bool IsTop { get; set; }

		public bool HasContentProperties { get; set; }

		public bool AllowPhysicianEdit { get; set; }

		public bool IsConfigurable { get; set; }

	}
}
