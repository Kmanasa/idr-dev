﻿using System;
using System.Collections.Generic;

namespace IDR.Domain.Cms.Dto
{
	public class NodeDto : IMappableForm
	{
		public NodeDto()
		{
			UINodeType = Constants.FORM_NODE_TEMPLATE;
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public NodeTypeDto Type { get; set; }
        public string HtmlContent { get; set; }
		public int TypeId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		//public string Key { get; set; }
        public string MenuIconStyle { get; set; }
        public int? ChildTemplateId { get; set; }
		public List<PropertyDto> Properties { get; set; }
		public List<NodeDto> Nodes { get; set; }
		public int? NodeId { get; set; }
		public int? NodeLibraryId { get; set; }
		public string SubMenuText { get; set; }
		public string UINodeType { get; set; }
		public DateTime? DeletedOn { get; set; }
		public bool IsPhysicianEditable { get; set; }
		public int? OverrideSourceId { get; set; }
		public int Sequence { get; set; }
        public bool IsNew { get; set; }

	    public int TemplateId
	    {
	        get
	        {
	            if (Type == null)
	            {
	                return 0;
                }

	            return Type.TemplateId;
	        }
	    }
	}

    public class CptCodeDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int NodeLibraryId { get; set; }
    }
}
