﻿
using System;

namespace IDR.Domain.Cms.Dto
{
    public class AttributeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsNew { get; set; }
    }
}
