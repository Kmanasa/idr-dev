using System;

namespace IDR.Domain.Portal.Dto
{
    public class PatientProcedureDto 
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int PhysicianId { get; set; }
        public int ProcedureId { get; set; }
        public int FacilityId { get; set; }
        public string CptCode { get; set; }
        public string AccessCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime StartTime { get; set; }
        public int UtcOffset { get; set; }
        public bool SendEmail { get; set; }
        public bool SendSms { get; set; }
    }
}