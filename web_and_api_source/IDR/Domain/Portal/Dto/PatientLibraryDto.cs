using System;

namespace IDR.Domain.Portal.Dto
{
    public class PatientLibraryDto
    {
        public int Id { get; set; }
        public int NodeLibraryId { get; set; }
        public int PatientId { get; set; }
        public string LibraryName { get; set; }
        public string LibraryCtss { get; set; }
        public int FacilityId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string AccessCode { get; set; }
        public string CptCode { get; set; }
        public bool SendEmail { get; set; }
        public bool SendSms { get; set; }
    }
}