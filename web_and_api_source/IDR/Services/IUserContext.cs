﻿
using Microsoft.AspNet.Identity;

namespace IDR.Services
{
    public interface IUserContext
    {
        string GetCurrentUserName();
    }

    public class UserContext : IUserContext
    {
        public string GetCurrentUserName()
        {
            var principal = System.Threading.Thread.CurrentPrincipal;

            return principal.Identity.GetUserName();
        }
    }
}
