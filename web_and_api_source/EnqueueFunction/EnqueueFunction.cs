using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using IDR.Data;
using IDR.Messaging;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;


namespace EnqueueFunction
{
	public static class EnqueueFunction
	{
		private static bool run = false;

		[FunctionName("EnqueueFunction")]
		public static void Run([TimerTrigger("*/10 * * * * *")]TimerInfo timer, TraceWriter log)
		{
			if (run)
			{
				return;
			}

			run = true;
			var start = DateTime.UtcNow;
			log.Info($"IDR event queue service started at {DateTime.UtcNow}");

			new NotificationQueueService(log).Enqueue();

			var elapsed = DateTime.UtcNow - start;
			log.Info($"IDR event queue service completed at {elapsed.TotalMilliseconds}");
		}


	}

	public class NotificationQueueService
	{
		//private ApplicationDbContext _context;
		private SqlConnection _cn;
		private string _currentCs;
		private readonly CloudQueueClient _queueClient;
		private readonly TraceWriter _log;

		public NotificationQueueService(TraceWriter log)
		{
			_log = log;
			//_context = new ApplicationDbContext("IdrDataContext");

			//var storageString = CloudConfigurationManager.GetSetting("StorageConnectionString");
			var storageString = GetEnvironmentVariable("StorageConnectionString");
			var storageAccount = CloudStorageAccount.Parse(storageString);

			// Create the queue client
			_queueClient = storageAccount.CreateCloudQueueClient();
		}

		public static string GetEnvironmentVariable(string name)
		{
			return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
		}

		public void Enqueue()
		{
			var queue = _queueClient.GetQueueReference("procedurenotifications");
			var task = queue.CreateIfNotExistsAsync();
			task.Wait();

			var hosts = GetEnvironmentVariable("TenantHosts").Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

			var cn = new SqlConnection(GetEnvironmentVariable("IdrTenantsConnectionString"));
			cn.Open();
			var connections = cn.Query<string>("select distinct ConnectionString from TenantConnections where Host in @hosts", new { hosts }).ToList();
			cn.Close();

			foreach (var cs in connections)
			{
				_currentCs = cs;
				//_context = new ApplicationDbContext(cs);
				_cn = new SqlConnection(cs);
				EneueNewMessages(queue);
			}
		}

		public void EneueNewMessages(CloudQueue queue)
		{
			var now = DateTime.UtcNow;

			_log.Info($"Running EnqueueNewMessages at {now} UTC");

			try
			{
				// Get unsent notifications 
				//var notifications = _context.ProcedureNotifications
				//	.Where(n =>
				//		// Not sent, ready for 1st send
				//		(n.Sent == false && n.SendTime < now) ||
				//		// Sent, not responded
				//		(n.Notification.Required && n.Response == null && n.Attempts < 3 && n.NextAttemptTime < now))
				//	.OrderBy(n => n.SendTime)
				//	.ToList();

				var notifications = _cn.Query(@"select *, p.AccessCode as ProcedureAccessCode  from ProcedureNotifications pn
inner join Notifications n on n.Id = pn.NotificationId
inner join Procedures p on p.Id = pn.ProcedureId
Where (pn.Sent = 0 and pn.SendTime < GETDATE())
or
(n.Required = 1 and pn.Response is null and pn.Attempts < 3 and pn.NextAttemptTime < GETDATE())
order by
SendTime");

				//_log.Info($"Total new notifications found: {notifications.Count}");

				foreach (var n in notifications)
				{
					
					var note = new ProcedurePushNotification
					{
						ProcedureId = n.ProcedureId,
						ProcedureCode = n.ProcedureAccessCode,
						Message = n.Message,
						Connection = _currentCs
					};

					var message = new CloudQueueMessage(JsonConvert.SerializeObject(note));
					var addTask = queue.AddMessageAsync(message);
					addTask.Wait();

					_cn.Execute("@Update ProcedureNotifications set Sent = 1, Attempts = @attempts, NextAttemptTime = @next  where id = @id", new
					{
						Id = n.Id,
						attempts = n.Attempts++,
						next = n.SendTime.AddMinutes(n.RepetitionMinutes)
					});
					// Flag the notification as sent
					//n.Sent = true;
					//var notifConfig = n.Notification;

					//// Flag the next attempt
					//n.Attempts++;
					//n.NextAttemptTime = n.SendTime.AddMinutes(notifConfig.RepetitionMinutes);

					//_context.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				_log.Error($"NotificationQueueService exception at {DateTime.UtcNow}", ex);
			}
		}

		public void EnqeueRetryMessages(CloudQueue queue)
		{
			var now = DateTime.UtcNow;

			_log.Info($"Running EneueRetryMessages at {now} UTC");

			try
			{
				// Get unsent notifications 
				//var notifications = (from pn in _context.ProcedureNotifications
				//					 join n in _context.Notifications on pn.NotificationId equals n.Id
				//					 where pn.Sent && pn.NextAttemptTime != null &&
				//						pn.NextAttemptTime < now &&
				//						pn.Attempts < n.Repetitions &&
				//						pn.DateResponded == null
				//					 orderby pn.SendTime
				//					 select pn)
				//	.ToList();

				var notifications = _cn.Query(@"select pn.*, p.AccessCode, n.Message from ProcedureNotifications pn
inner join Notifications n on pn.NotificationId = n.Id
inner join Procedures p on p.Id = pn.ProcedureId
where pn.Sent = 1 and 
pn.NextAttemptTime is not null and
pn.NextAttemptTime < getdate() and
pn.Attempts < n.Repetitions and
pn.DateResponded is null
order by pn.SendTime");

				//_log.Info($"Total notifications found: {notifications.Count}");

				foreach (var n in notifications)
				{
					//_log.Info($"Sending notification {n.NotificationId} for code {n.Procedure.AccessCode} with send time {n.SendTime.ToLongTimeString()} at UTC {now.ToLongTimeString()}");

					var notif = n.Notification;

					var note = new ProcedurePushNotification
					{
						ProcedureId = n.ProcedureId,
						ProcedureCode = n.AccessCode,
						Message = n.Message
					};

					var message = new CloudQueueMessage(JsonConvert.SerializeObject(note));
					var addTask = queue.AddMessageAsync(message);
					addTask.Wait();

					_cn.Execute("@Update ProcedureNotifications set Sent = 1, Attempts = @attempts where id = @id", new
					{
						id = n.Id,
						attempts = n.Attempts++,
					});

					// Flag the notification as sent
					//n.Sent = true;
					//n.Attempts++;

					//_context.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				_log.Error($"NotificationQueueService exception at {DateTime.UtcNow}", ex);
			}
		}
	}
}
