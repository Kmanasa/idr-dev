using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using IDR.Messaging;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using System.Json;


namespace NotificationsFunction
{
    public static class NotificationsFunction
    {
	    public static string GetEnvironmentVariable(string name)
	    {
		    return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
	    }

		[FunctionName("NotificationsFunction")]
        public static void Run([QueueTrigger("procedurenotifications")]string queueItem, TraceWriter log)
		{
			//var message = JsonConvert.DeserializeObject<ProcedurePushNotification>(queueItem);
			var message = JsonValue.Parse(queueItem);

			if (string.IsNullOrWhiteSpace(message["Connection"]))
			{
				log.Info($"Invalid DB connection: {message["Connection"]}");
				return;
			}

			// Connect using the supplied database
			//var context = new ApplicationDbContext(message.Connection);
			var cn = new SqlConnection(message["Connection"]);

			var procedure = cn.QueryFirstOrDefault("SELECT * From Procedures where id = @id", 
				new { id = message["ProcedureId"].ToString() });
			//      //var procedure = context.Procedures.FirstOrDefault(p => p.Id == message.ProcedureId);

			if (procedure != null)
			{
				try
				{
					// Find the notification hub with registrations for the user's devices
					var endpoint = GetEnvironmentVariable("NotificationHubEndpoint");// CloudConfigurationManager.GetSetting("NotificationHubEndpoint");
					var hubName = GetEnvironmentVariable("HubName");// CloudConfigurationManager.GetSetting("HubName");

					var hub = NotificationHubClient.CreateClientFromConnectionString(endpoint, hubName);

					// Send Apple and Google push notifications
					var note = new ProcedurePushNotification
					{
						ProcedureId = int.Parse(message["ProcedureId"]),
						Message = message["Message"],
						ProcedureCode = message["ProcedureCode"]
					};

					var appleTask = new AppleNotification(hub, note).Send();
					var googleTask = new GoogleNotification(hub, note).Send();

					Task.WaitAll(appleTask, googleTask);

					var appleStatus = appleTask.Status;
					var googleStatus = googleTask.Status;
				}
				catch (Exception ex)
				{
					log.Error($"ProcessQueueMessage exception at {DateTime.UtcNow}", ex);
				}
			}
			
		}
	}
}
