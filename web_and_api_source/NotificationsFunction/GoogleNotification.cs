using System;
using System.Threading.Tasks;
using IDR.Messaging;
using Microsoft.Azure.NotificationHubs;

namespace NotificationsFunction
{
    public class GoogleNotification : PushNotification
    {
        public GoogleNotification(NotificationHubClient hub, ProcedurePushNotification notification):base(hub,notification)
        {
        }

        public override async Task Send()
        {
            var max = Math.Min(Notification.Message.Length, 140);
            var notif = Notification.Message.Substring(0, max);

            if (notif.Length == 140)
            {
                notif += "...";
            }

            var msg = "{\"data\":{\"message\":\"" + notif + "\"}}";
            await Hub.SendGcmNativeNotificationAsync(msg, new[] { Notification.ProcedureCode });
        }
    }
}