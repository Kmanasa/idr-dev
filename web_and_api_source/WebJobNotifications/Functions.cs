﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IDR.Data;
using IDR.Messaging;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.NotificationHubs;

namespace WebJobNotifications
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage([QueueTrigger("procedurenotifications")] ProcedurePushNotification message, TextWriter log)
        {
	        if (string.IsNullOrWhiteSpace(message.Connection))
	        {
		        log.WriteLine($"Invalid DB connection: {message.Connection}");
		        return;
	        }

			// Connect using the supplied database
			var context = new ApplicationDbContext(message.Connection);
            log.WriteLine($"DB connection: {message.Connection}");

            var procedure = context.Procedures.FirstOrDefault(p => p.Id == message.ProcedureId);

            if (procedure != null)
            {
                log.WriteLine($"Sending push notifications for procedure {message.ProcedureId} with code {message.ProcedureCode}");
                try
                {
                    // Find the notification hub with registrations for the user's devices
                    var endpoint = CloudConfigurationManager.GetSetting("NotificationHubEndpoint");
                    var hubName = CloudConfigurationManager.GetSetting("HubName");
                    var hub = NotificationHubClient.CreateClientFromConnectionString(endpoint, hubName);
                    
                    // Send Apple and Google push notifications
                    var appleTask = new AppleNotification(hub, message).Send();
                    var googleTask = new GoogleNotification(hub, message).Send();

                    Task.WaitAll(appleTask, googleTask);

                    var appleStatus = appleTask.Status;
                    var googleStatus = googleTask.Status;
                }
                catch (Exception ex)
                {
                    log.WriteLine("ProcessQueueMessage exception at {0}", DateTime.UtcNow);
                    log.WriteLine("===");
                    log.Write(ex.Message);
                }
            }
            else
            {
                log.WriteLine("Unable to locate procedure {0}", message.ProcedureId);
            }
        }
    }
}
