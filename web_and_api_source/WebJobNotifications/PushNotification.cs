﻿using System.Threading.Tasks;
using IDR.Messaging;
using Microsoft.Azure.NotificationHubs;

namespace WebJobNotifications
{
    public abstract class PushNotification
    {
        protected NotificationHubClient Hub { get; set; }
        protected ProcedurePushNotification Notification { get; set; }

        protected PushNotification(NotificationHubClient hub, ProcedurePushNotification notification)
        {
            Hub = hub;
            Notification = notification;
        }

        public abstract Task Send();
    }
}