﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class Report
	{
		public int Id { get; set; }

		[StringLength(50)]
		public string Description { get; set; }

		[StringLength(20)]
		public string ShareUrl { get; set; }

		[StringLength(2)]
		public string GlobalFlag { get; set; }

		[StringLength(25)]
		public string Rp1Label { get; set; }

		[StringLength(25)]
		public string Rp2Label { get; set; }

		[StringLength(25)]
		public string Rp3Label { get; set; }

		[StringLength(25)]
		public string Rp4Label { get; set; }
	}
}