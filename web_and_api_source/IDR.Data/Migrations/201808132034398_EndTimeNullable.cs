namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EndTimeNullable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedureTimers", "InitialVolume", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CallCenterProcedureTimers", "RecordedVolume", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CallCenterProcedureTimers", "NotificationSent", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterProcedureTimers", "End", c => c.DateTime());
            DropColumn("dbo.CallCenterProcedureTimers", "Volume");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallCenterProcedureTimers", "Volume", c => c.Int(nullable: false));
            AlterColumn("dbo.CallCenterProcedureTimers", "End", c => c.DateTime(nullable: false));
            DropColumn("dbo.CallCenterProcedureTimers", "NotificationSent");
            DropColumn("dbo.CallCenterProcedureTimers", "RecordedVolume");
            DropColumn("dbo.CallCenterProcedureTimers", "InitialVolume");
        }
    }
}
