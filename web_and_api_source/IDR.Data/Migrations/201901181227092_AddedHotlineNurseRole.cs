namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedHotlineNurseRole : DbMigration
    {
        public override void Up()
        {
            #region New Role

            var hotlineNurseId = "AA8CE308-A318-44D9-9E4F-05B7D7B04D17";

            var query = string.Format(@"
if not exists(select top 1 Id from AspNetRoles where [Name] = 'Hotline Nurse')
begin
                INSERT INTO [AspNetRoles] ([Id],[Name]) VALUES('{0}','Hotline Nurse')
end

", hotlineNurseId);

            Sql(query);

            #endregion
        }

        public override void Down()
        {
        }
    }
}
