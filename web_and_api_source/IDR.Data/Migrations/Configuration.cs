namespace IDR.Migrations
{
    using IDR.Data;
    using IDR.web.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        private ApplicationDbContext _context;
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            _context = context;

            bool seedMetaData;
            bool.TryParse(ConfigurationManager.AppSettings["SeedMetaData"], out seedMetaData);
            if (seedMetaData) SeedMetaData();

            return;

            bool seedUserRoles;
            bool.TryParse(ConfigurationManager.AppSettings["SeedUserRoles"], out seedUserRoles);
            if (seedUserRoles) SeedUserRoles();

            bool seedUsers;
            bool.TryParse(ConfigurationManager.AppSettings["SeedUsers"], out seedUsers);
            if (seedUsers) SeedUsers();

            bool seedDisciplines;
            bool.TryParse(ConfigurationManager.AppSettings["SeedDisciplines"], out seedDisciplines);
            if (seedDisciplines) SeedDisciplines();

            bool seedTreatmentBindables;
            bool.TryParse(ConfigurationManager.AppSettings["SeedTreatmentBindables"], out seedTreatmentBindables);
            if (seedTreatmentBindables) SeedTreatmentBindables();

            SeedEmailTemplates();

        }

        private void SeedEmailTemplates()
        {
            var template = _context.EmailTemplates.FirstOrDefault(t => t.Alias == "resetpassword");
            if (template == null)
            {
                template = new EmailTemplate
                {
                    Title="Reset Password",
                    Alias = "resetpassword",
                    Content = GetResetPasswordTemplate()
                };
                _context.EmailTemplates.Add(template);
                _context.SaveChanges();
            }

            template = _context.EmailTemplates.FirstOrDefault(t => t.Alias == "patientinvitation");
            if (template == null)
            {
                template = new EmailTemplate
                {
                    Title= "Patient Invitation",
                    Alias = "patientinvitation",
                    Content = GetPatientInviteTemplate()
                };
                _context.EmailTemplates.Add(template);
                _context.SaveChanges();
            }

            template = _context.EmailTemplates.FirstOrDefault(t => t.Alias == "physicianinvitation");
            if (template == null)
            {
                template = new EmailTemplate
                {
                    Title = "Physician Invitation",
                    Alias = "physicianinvitation",
                    Content = GetPhysicianInviteTemplate()
                };
                _context.EmailTemplates.Add(template);
                _context.SaveChanges();
            }
            template = _context.EmailTemplates.FirstOrDefault(t => t.Alias == "painvitation");
            if (template == null)
            {
                template = new EmailTemplate
                {
                    Title = "PA Invitation",
                    Alias = "painvitation",
                    Content = GetPAInviteTemplate()
                };
                _context.EmailTemplates.Add(template);
                _context.SaveChanges();
            }

            template = _context.EmailTemplates.FirstOrDefault(t => t.Alias == "admininvitation");
            if (template == null)
            {
                template = new EmailTemplate
                {
                    Title = "Admin Invitation",
                    Alias = "admininvitation",
                    Content = GetAdminInviteTemplate()
                };
                _context.EmailTemplates.Add(template);
                _context.SaveChanges();
            }
        }

        private string GetResetPasswordTemplate()
        {
            return @"
<label>
	Dear {{FirstName}},
</label>

<p>
	Than you for contacting us.  Follow the link below to reset your password.
</p>

<p>
	<a href=""{{PasswordUrl}}"">{{PasswordUrl}}</a>
</p>

<p>
	Sincerely,
	<br />
	The IDR User Access Team
</p>";
        }

        private string GetPhysicianInviteTemplate()
        {
            return @"

<label>
	Dear {{FirstName}},
</label>

<p>
	We are excited to notify you that you have been invited to begin using the IDR system by TOMR2 on behalf of {{PracticeName}}.
</p>

<p>
	Your first step in using the IDR system and providing great service to your patients is to sign up.  To do so, you click (OR copy and use your browser) the following link to set up your account.
</p>

<p>
	<a href=""{{Host}}Account/RegisterPracticeUser?provisionalKey={{ProvisionalKey}}"">{{Host}}Account/RegisterPracticeUser?provisionalKey={{ProvisionalKey}}</a>
</p>
<p>
    Warmest Regards,
<br />
    The IDR Team
</p>";
        }

        private string GetPAInviteTemplate()
        {
            return @"

<label>
	Dear {{FirstName}},
</label>

<p>
	We are excited to notify you that you have been invited to begin using the IDR system by TOMR2 on behalf of {{PracticeName}}.
</p>

<p>
	Your first step in using the IDR system and providing great service to your patients is to sign up.  To do so, you click (OR copy and use your browser) the following link to set up your account.
</p>

<p>
	<a href=""{{Host}}Account/RegisterPracticeUser?provisionalKey={{ProvisionalKey}}"">{{Host}}Account/RegisterPracticeUser?provisionalKey={{ProvisionalKey}}</a>
</p>
<p>
    Warmest Regards,
<br />
    The IDR Team
</p>";
        }

        private string GetAdminInviteTemplate()
        {
            return @"

<label>
	Dear {{FirstName}},
</label>

<p>
	We are excited to notify you that you have been invited to begin using the IDR system by TOMR2 on behalf of {{PracticeName}}.
</p>

<p>
	Your first step in using the IDR system and providing great service to your patients is to sign up.  To do so, you click (OR copy and use your browser) the following link to set up your account.
</p>

<p>
	<a href=""{{Host}}Account/RegisterPracticeUser?provisionalKey={{ProvisionalKey}}"">{{Host}}Account/RegisterPracticeUser?provisionalKey={{ProvisionalKey}}</a>
</p>
<p>
    Warmest Regards,
<br />
    The IDR Team
</p>";
        }

        private string GetPatientInviteTemplate()
        {
            return @"
<label>
    Dear {{FirstName}},
</label>


<p>
    Recently your physician at {{PracticeName}} has enrolled you in their new patient education
    program called IDR. IDR is a texting acronym for &quot;I Don�t Remember&quot; and is designed to place
    as much information as possible for your upcoming procedure into an easy to use &quot;app&quot; for
    smartphones and tablets. This app, IDR is available from both the iTunes and Google Play Stores as a FREE
    download and we encourage you to download IDR as soon as possible.
</p>
<p>
    {{PracticeName}} has assembled pertinent information they feel will be useful to you, your
    family and care givers as you prepare for your upcoming procedure.
</p>

<p>
    Over the course of the next few weeks, you will receive text reminders and notifications of key
    milestones and events in your preparation process. These notices, along with the information
    contained in your personal procedure plan should help you answer many of the questions you may
    have about getting ready for your procedure and will help you achieve the best possible outcome.
</p>
<p>
    Your preparation/recovery plan has been created and is ready for access on your mobile device. Use
    the following links to download the app. Once downloaded, enter your personalized access code below
    to enroll your smartphone or tablet in our notification process.
</p>

<p>
    Apple https://goo.gl/clW7Tl
</p>
<p>
    Google https://goo.gl/Rgi1s2
</p>
<p>
    To access your personalized procedure plan,
</p>
<p>Use access code {{AccessCode}}</p>

<p>
    The IDR User Access Team
</p>";
        }

        private void SeedTreatmentBindables()
        {
            Func<string, TreatmentBindable> upsertF = (name) =>
            {
                var list = name.Split(':');
                name = list[0];
                var category = list.Count() > 1 ? list[1] : string.Empty;
                var image = list.Count() > 2 ? list[2] : string.Empty;
                bool isImage = false;
                bool.TryParse(image, out isImage);
                var bindable = _context.TreatmentBindables.FirstOrDefault(x => x.PathName == name);

                if (bindable == null)
                {
                    bindable = new TreatmentBindable { PathName = name, IsImage = isImage, Category = category };
                    _context.Entry(bindable).State = EntityState.Added;
                }
                else
                {
                    bindable.IsImage = isImage;
                    bindable.Category = category;
                }
                _context.SaveChanges();

                return bindable;
            };

            "Physcian.FullName:Physician;Physician.FirstName:Physician;Physician.ProfileImage:Physician:true;Practice.Name:Practice;Practice.Contact:Phone:Practice".Split(';').Select(upsertF).ToList();
        }

        private void SeedUsers()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
            userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };
            
            AddUser(userManager, "hardik.asnani@betsol.com", "password", "Hardik", "Asnani", "Admin", "Super Admin");
            AddUser(userManager, "jbhibben@trackingoutcomes.com", "password", "John", "Hibben", "Admin", "Content Manager");
            AddUser(userManager, "dr@idr.com", "password", "Doc", "Lastname", "Physician");
            AddUser(userManager, "practiceadmin@idr.com", "password", "PracAdmin", "Lastname", "Practice Admin");
            AddUser(userManager, "physassist@idr.com", "password", "Phys", "Lastname", "Physicians Assistant");
            AddUser(userManager, "content@idr.com", "password", "Content", "Lastname", "Content Manager");
        }

        private void AddUser(UserManager<ApplicationUser> userManager,
            string email, string password, string first, string last, params string[] roles)
        {
            try
            {
                var existingUser = _context.Users.FirstOrDefault(u => u.UserName == email);

                if (existingUser == null)
                {
                    var user = new ApplicationUser
                    {
                        Firstname = first,
                        Lastname = last,
                        UserName = email,
                        Email = email,
                        EmailConfirmed = true,
                        Id = Guid.NewGuid().ToString(),
                        Active = true,
                    };
                    userManager.Create(user, password);
                    _context.SaveChanges();

                    existingUser = _context.Users.FirstOrDefault(u => u.UserName == email);
                }


                foreach (var role in roles)
                {
                    if (!userManager.IsInRole(existingUser.Id, role))
                    {
                        userManager.AddToRole(existingUser.Id, role);
                    }

                    if (role == "Physician")
                    {
                        var practice = SeedPractice("Unassigned");
                        var physician = _context.Physicians.FirstOrDefault(x => x.UserId == existingUser.Id);
                        if (physician == null)
                        {
                            physician = new Physician
                            {
                                FirstName = existingUser.Firstname,
                                LastName = existingUser.Lastname,
                                Title = "Md",
                                UserId = existingUser.Id,
                                CompanyId = practice.Id
                            };
                            AuditSeedEntity(physician);
                            _context.Physicians.Add(physician);
                            _context.SaveChanges();
                        }
                        else if (!physician.CompanyId.HasValue)
                        {
                            physician.CompanyId = practice.Id;
                            _context.SaveChanges();
                        }
                    }
                }

               
            }
            catch { }
        }

        private Company SeedPractice(string p)
        {
            var practice = _context.Companies.FirstOrDefault(x => x.Name == p);

            if (practice == null)
            {
                practice = new Company
                {
                    Name = p,
                    CompanyType = CompanyType.Practice
                };

                practice.Audit("System");

                practice = _context.Companies.Add(practice);
            }

            return practice;
        }

        private void SeedMetaData()
        {
            // Form Types
            var welcomeLetter = AddType(IDR.Constants.NodeLibraryTypes.MasterTemplate, "Welcome Letter", "A Physician welcome letter, usually contains valuable notes, dos and don'ts, etc.  The child pages contain the content in order of occurance", "dash", true, false, false);
            var welcomeLetterPage = AddType(IDR.Constants.NodeLibraryTypes.MasterTemplate, "Welcome Letter Page", "", "dash", false, true, false);
            var contentNav = AddType(string.Empty, "Content", "A Navigation Menu, but with preceding content.  Each child menu item may be of different types, but will appear in this menu as an item.", "contentNav", true, true, false);
            var videoLib = AddType(string.Empty, "Video Library", "A library of video content as child nodes", "videoLibP", true, true, false);
            var video = AddType(string.Empty, "Video Content", "A specialized content that displays a video", "video", false, false, false);
            var contentNavContact = AddType(string.Empty, "Contact", "A specialized content that displays dialable phone contact", "contentNavContent", false, false, false);
            var feedback = AddType(IDR.Constants.NodeLibraryTypes.MasterTemplate, "Feedback", "A placeholder that points to a form site form.", "feedback", false, false, false);
            var reusable = AddType(IDR.Constants.NodeLibraryTypes.MasterTemplate, "Reusable Template", "Inserted reusable content template", "reuse", true, false, false);
            var menuDivider = AddType(string.Empty, "Menu Divider", "A Navigation divider", "menuDivider", false, true, false);
            var pdfFiles = AddType(IDR.Constants.NodeLibraryTypes.MasterTemplate, "PDF Files", "PDF Files associated with this Node Library", "pdfFiles", false, false, false);
            var googleMaps = AddType(IDR.Constants.NodeLibraryTypes.MasterTemplate, "Google Maps", "Specialized Content that displays a Map", "googleMaps", false, false, false);

            #region Form Type Assigments
            AllowType(welcomeLetter, welcomeLetterPage);
            AllowType(welcomeLetter, video);
            AllowType(contentNav, contentNav);
            AllowType(contentNav, contentNavContact);
            AllowType(contentNav, videoLib);
            AllowType(contentNav, feedback);
            AllowType(contentNav, reusable);
            AllowType(contentNav, menuDivider);
            AllowType(contentNav, pdfFiles);
            AllowType(contentNav, googleMaps);
            AllowType(videoLib, video);
            #endregion

            #region properties
            AddProp(video, "Video Url", "Url to streaming video content", "URL", true);
            AddPropDom(video, "Video Player Type", "", "SELECT", "videoPlayerTypes", string.Empty, true);
            AddProp(feedback, "Form Url", "Usually this is the url to the form site application.  You may use bind tokens to inject application variables.", "URL", true);
            AddProp(feedback, "Formsite Id", "The FORMSITE form id.", "TEXTBOX", true);
            AddProp(menuDivider, "Menu Padding", "Menu Padding", "TEXTBOX", false);
            AddProp(pdfFiles, "PDF Files", "PDF Files associated with this Node Library", "PDFFILE", false);
            AddProp(googleMaps, "Google Maps", "Google Map of the selected location", "MAP", false);
            #endregion
        }

        private void SeedDisciplines()
        {
            MakeDiscipline("Internal Medicine", "Internal Medicine");
            MakeDiscipline("Gastroenterology", "Gastroenterology");
            MakeDiscipline("General Surgery", "General Surgery");
        }

        private Discipline MakeDiscipline(string name, string desc)
        {
            var discipline = _context.Disciplines.FirstOrDefault(x => x.Name == name);

            if (discipline == null)
            {
                discipline = new Discipline { Name = name, Description = desc };
                discipline.Audit("Seed");
                discipline = _context.Disciplines.Add(discipline);
            }

            discipline.Description = desc;

            _context.SaveChanges();

            return discipline;
        }

        private void SeedUserRoles()
        {
            const string roles = "Admin,Super Admin,User,Physician,Content Manager,Practice Admin,Physicians Assistant,Call Site Admin,Call Site Mobile";

            roles.Split(',')
                .Where(role => !_context.Roles.Any(r => r.Name == role)).ToList()
                .ForEach(role => _context.Roles.AddOrUpdate(new IdentityRole { Name = role }));
        }

        private void AuditSeedEntity(AuditEntity instance)
        {
            instance.Audit("Automated Seeding");
        }

        private NodeType AddType(string libraryType, string name, string desc, string key,
            bool isTop, bool allowEdit, bool isPhysConfig)
        {
            if (_context.NodeTypes.Any(ft => ft.Name == name))
            {
                var type = _context.NodeTypes.First(x => x.Name == name);

                type.Description = desc;
                type.LibraryType = libraryType;
                type.IsTop = isTop;
                type.UIKey = key;
                type.AllowPhysicianEdit = allowEdit;
                type.IsConfigurable = isPhysConfig;
                _context.SaveChanges();

                return type;
            }
            var formType = new NodeType { Name = name, Description = desc, UIKey = key, IsTop = isTop, AllowPhysicianEdit = allowEdit, LibraryType = libraryType, IsConfigurable = isPhysConfig };
            AuditSeedEntity(formType);
            _context.NodeTypes.Add(formType);
            _context.SaveChanges();

            return formType;

        }

        private void AllowType(NodeType baseType, NodeType childType)
        {
            if (!_context.AllowedTypes.Any(x => x.AllowedTypeId == childType.Id && x.NodeTypeId == baseType.Id))
            {
                var type = new AllowedType { AllowedTypeId = childType.Id, NodeTypeId = baseType.Id, };
                AuditSeedEntity(type);
                _context.AllowedTypes.Add(type);
                _context.SaveChanges();
            }
        }

        private void AddPropDom(NodeType formType, string name, string desc, string key, string domain, string general, bool isRequired)
        {
            if (formType.Properties.Any(pt => pt.Name == name))
            {
                var propType = formType.Properties.FirstOrDefault(x => x.Name == name);
                if (propType != null)
                {
                    propType.Description = desc;
                    propType.UIKey = key;
                    propType.IsRequired = isRequired;
                    propType.DomainName = domain;
                    propType.General = general;

                    AuditSeedEntity(propType);
                    _context.SaveChanges();
                }
            }
            else
            {
                var propType = new PropertyType
                {
                    Name = name,
                    Description = desc,
                    UIKey = key,
                    IsRequired = isRequired,
                    DomainName = domain,
                    General = general
                };
                AuditSeedEntity(propType);
                formType.Properties.Add(propType);
                _context.SaveChanges();
            }
        }

        private void AddProp(NodeType formType, string name, string desc, string key, bool isRequired)
        {
            var propType = formType.Properties.FirstOrDefault(x => x.Name == name);
            if (propType != null)
            {
                propType.Description = desc;
                propType.UIKey = key;
                propType.IsRequired = isRequired;

                _context.SaveChanges();
                return;
            }
            propType = new PropertyType { Name = name, Description = desc, UIKey = key, IsRequired = isRequired };
            AuditSeedEntity(propType);
            formType.Properties.Add(propType);
            _context.SaveChanges();
        }
    }
}
