namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredColumnTypeInCallCenterScheduledCallsTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableToilet", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableConsumption", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableSleeping", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableWalking", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableDressing", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableStairs", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerablePtOt", c => c.Int());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableReturnToWork", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableReturnToWork", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerablePtOt", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableStairs", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableDressing", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableWalking", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableSleeping", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableConsumption", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableToilet", c => c.Boolean());
        }
    }
}
