namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RevisedAssessments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterScheduledCalls", "Perscription24Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableToilet", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableConsumption", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableSleeping", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableWalking", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableDressing", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableStairs", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerablePtOt", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TolerableReturnToWork", c => c.Boolean(nullable: false));
            DropColumn("dbo.CallCenterScheduledCalls", "SleepQuality");
            DropColumn("dbo.CallCenterScheduledCalls", "SleepRating");
            DropColumn("dbo.CallCenterScheduledCalls", "TimesAwake");
            DropColumn("dbo.CallCenterScheduledCalls", "DoseButton");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidShort12Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidShort24Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidLong12Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidLong24Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "Otc12Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "ReserviorLevel");
            DropColumn("dbo.CallCenterScheduledCalls", "TimePainChanged");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreManagable");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreWorst");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreAverage");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterNoIssues");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterRedness");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterTenderness");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterSwelling");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterClearFluid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterClearFluid", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterSwelling", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterTenderness", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterRedness", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterNoIssues", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreAverage", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreWorst", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreManagable", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TimePainChanged", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "ReserviorLevel", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CallCenterScheduledCalls", "Otc12Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidLong24Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidLong12Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidShort24Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidShort12Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "DoseButton", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TimesAwake", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "SleepRating", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "SleepQuality", c => c.Int(nullable: false));
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableReturnToWork");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerablePtOt");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableStairs");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableDressing");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableWalking");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableSleeping");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableConsumption");
            DropColumn("dbo.CallCenterScheduledCalls", "TolerableToilet");
            DropColumn("dbo.CallCenterScheduledCalls", "Perscription24Hour");
        }
    }
}
