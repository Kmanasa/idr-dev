namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeCommentColumnVarchar : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CallCenterComments", "Comment", c => c.String(maxLength: 8000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CallCenterComments", "Comment", c => c.String());
        }
    }
}
