namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CallCenterCommentTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallCenterCommentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        Title = c.String(maxLength: 50),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.CallCenterComments", "CallCenterCommentTypeId", c => c.Int());
            CreateIndex("dbo.CallCenterComments", "CallCenterCommentTypeId");
            AddForeignKey("dbo.CallCenterComments", "CallCenterCommentTypeId", "dbo.CallCenterCommentTypes", "Id");
            DropColumn("dbo.CallCenterComments", "CallType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallCenterComments", "CallType", c => c.Int(nullable: false));
            DropForeignKey("dbo.CallCenterComments", "CallCenterCommentTypeId", "dbo.CallCenterCommentTypes");
            DropIndex("dbo.CallCenterComments", new[] { "CallCenterCommentTypeId" });
            DropColumn("dbo.CallCenterComments", "CallCenterCommentTypeId");
            DropTable("dbo.CallCenterCommentTypes");
        }
    }
}
