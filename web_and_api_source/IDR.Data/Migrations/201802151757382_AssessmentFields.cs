namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssessmentFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterScheduledCalls", "Satisfaction", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "SleepQuality", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "SleepRating", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "TimesAwake", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "DoseButton", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidShort12Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidShort24Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidLong12Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "OpioidLong24Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "Otc12Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "Otc24Hour", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "ReserviorLevel", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CallCenterScheduledCalls", "TimePainChanged", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreManagable", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreCurrent", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreWorst", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainScoreAverage", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterNoIssues", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterRedness", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterTenderness", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterSwelling", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CatheterClearFluid", c => c.Boolean(nullable: false));
            DropColumn("dbo.CallCenterScheduledCalls", "PainLevel");
            DropColumn("dbo.CallCenterScheduledCalls", "Hotline");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallCenterScheduledCalls", "Hotline", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "PainLevel", c => c.Int(nullable: false));
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterClearFluid");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterSwelling");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterTenderness");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterRedness");
            DropColumn("dbo.CallCenterScheduledCalls", "CatheterNoIssues");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreAverage");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreWorst");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreCurrent");
            DropColumn("dbo.CallCenterScheduledCalls", "PainScoreManagable");
            DropColumn("dbo.CallCenterScheduledCalls", "TimePainChanged");
            DropColumn("dbo.CallCenterScheduledCalls", "ReserviorLevel");
            DropColumn("dbo.CallCenterScheduledCalls", "Otc24Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "Otc12Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidLong24Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidLong12Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidShort24Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "OpioidShort12Hour");
            DropColumn("dbo.CallCenterScheduledCalls", "DoseButton");
            DropColumn("dbo.CallCenterScheduledCalls", "TimesAwake");
            DropColumn("dbo.CallCenterScheduledCalls", "SleepRating");
            DropColumn("dbo.CallCenterScheduledCalls", "SleepQuality");
            DropColumn("dbo.CallCenterScheduledCalls", "Satisfaction");
        }
    }
}
