namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Outpatient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "Outpatient", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableToilet", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableConsumption", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableSleeping", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableWalking", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableDressing", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableStairs", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerablePtOt", c => c.Boolean());
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableReturnToWork", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableReturnToWork", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerablePtOt", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableStairs", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableDressing", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableWalking", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableSleeping", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableConsumption", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CallCenterScheduledCalls", "TolerableToilet", c => c.Boolean(nullable: false));
            DropColumn("dbo.CallCenterProcedures", "Outpatient");
        }
    }
}
