namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CallCenterComment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallCenterComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CallCenterProcedureId = c.Int(nullable: false),
                        CallCenterScheduledCallId = c.Int(nullable: false),
                        Comment = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CallCenterProcedures", t => t.CallCenterProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.CallCenterScheduledCalls", t => t.CallCenterScheduledCallId, cascadeDelete: false)
                .Index(t => t.CallCenterProcedureId)
                .Index(t => t.CallCenterScheduledCallId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CallCenterComments", "CallCenterScheduledCallId", "dbo.CallCenterScheduledCalls");
            DropForeignKey("dbo.CallCenterComments", "CallCenterProcedureId", "dbo.CallCenterProcedures");
            DropIndex("dbo.CallCenterComments", new[] { "CallCenterScheduledCallId" });
            DropIndex("dbo.CallCenterComments", new[] { "CallCenterProcedureId" });
            DropTable("dbo.CallCenterComments");
        }
    }
}
