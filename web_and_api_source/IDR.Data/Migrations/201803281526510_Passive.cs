namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Passive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterPracticeReferences", "Passive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterPracticeReferences", "Passive");
        }
    }
}
