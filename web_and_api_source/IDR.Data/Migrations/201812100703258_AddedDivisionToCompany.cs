namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDivisionToCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "Division", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Division");
        }
    }
}
