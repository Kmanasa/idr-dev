namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CCV : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedureTimers", "CCV", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterProcedureTimers", "CCV");
        }
    }
}
