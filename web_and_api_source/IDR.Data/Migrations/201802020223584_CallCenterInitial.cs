namespace IDR.Migrations
{
	using System;
	using System.Data.Entity.Migrations;

	public partial class CallCenterInitial : DbMigration
	{
		public override void Up()
		{
			#region New Roles

			var callCenterAdminId = "BE189CB6-DF1E-48B6-AAB4-1CA4FB18CDA1";
			var monitorId = "BE45FABA-B7F9-4416-8A2C-F9B32B65453A";
			var siteAdminId = "03EF8188-F034-4C86-99E7-4352FC39B08B";

			var query = string.Format(@"
if not exists(select top 1 Id from AspNetRoles where [Name] = 'Call Center Admin')
begin
	INSERT INTO [AspNetRoles] ([Id],[Name]) VALUES('{0}','Call Center Admin')
end

if not exists(select top 1 Id from AspNetRoles where [Name] = 'Monitor')
begin
	INSERT INTO [AspNetRoles] ([Id],[Name]) VALUES('{1}','Monitor')
end

if not exists(select top 1 Id from AspNetRoles where [Name] = 'Call Site Admin')
begin
	INSERT INTO [AspNetRoles] ([Id],[Name]) VALUES('{2}','Call Site Admin')
end

", callCenterAdminId, monitorId, siteAdminId);

			Sql(query);

			#endregion

			#region Schema
			CreateTable(
				"dbo.CallCenterPatients",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					CallCenterProcedureId = c.Int(nullable: false),
					PatientId = c.Int(nullable: false),
					Gender = c.Int(nullable: false),
					AltContact = c.String(maxLength: 256),
					AltPhone = c.String(maxLength: 256),
					Mobile = c.Boolean(nullable: false),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.CallCenterProcedures", t => t.CallCenterProcedureId, cascadeDelete: true)
				.Index(t => t.CallCenterProcedureId);

			CreateTable(
				"dbo.CallCenterProcedures",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					CallCenterId = c.Int(nullable: false),
					ProcedureId = c.Int(nullable: false),
					CallCenterPayorId = c.Int(),
					PhysicianId = c.Int(),
					AnesthesiologistId = c.Int(),
					NurseId = c.Int(),
					PumpSerial = c.String(maxLength: 256),
					IsDualCath = c.Boolean(nullable: false),
					PumpSerial2 = c.String(maxLength: 256),
					Cancelled = c.Boolean(nullable: false),
					Completed = c.Boolean(nullable: false),
					NoMonitoring = c.Boolean(nullable: false),
					DoNotDisturb = c.Boolean(nullable: false),
					InitialContactDate = c.DateTime(nullable: false),
					DiscontinueDate = c.DateTime(nullable: false),
					CptId = c.Int(nullable: false),
					PracticeFacilityId = c.Int(nullable: false),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.CallCenterProviders", t => t.AnesthesiologistId)
				.ForeignKey("dbo.CallCenters", t => t.CallCenterId, cascadeDelete: true)
				.ForeignKey("dbo.CallCenterPayors", t => t.CallCenterPayorId)
				.ForeignKey("dbo.CallCenterProviders", t => t.NurseId)
				.ForeignKey("dbo.CallCenterProviders", t => t.PhysicianId)
				.Index(t => t.CallCenterId)
				.Index(t => t.CallCenterPayorId)
				.Index(t => t.PhysicianId)
				.Index(t => t.AnesthesiologistId)
				.Index(t => t.NurseId);

			CreateTable(
				"dbo.CallCenterProviders",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					FirstName = c.String(maxLength: 100),
					LastName = c.String(maxLength: 100),
					Title = c.String(maxLength: 20),
					Suffix = c.String(maxLength: 40),
					ProviderType = c.Int(nullable: false),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"dbo.CallCenterProviderFacilities",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					CallCenterProviderId = c.Int(nullable: false),
					FacilityId = c.Int(nullable: false),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.CallCenterProviders", t => t.CallCenterProviderId, cascadeDelete: true)
				.Index(t => t.CallCenterProviderId);

			CreateTable(
				"dbo.CallCenters",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					Name = c.String(maxLength: 100),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"dbo.CallCenterPayors",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					Title = c.String(maxLength: 100),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"dbo.CallCenterScheduledCalls",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					CallCenterProcedureId = c.Int(nullable: false),
					CallStatus = c.Int(nullable: false),
					CallDay = c.Int(nullable: false),
					CallMonth = c.Int(nullable: false),
					CallYear = c.Int(nullable: false),
					Index = c.Int(nullable: false),
					Assigned = c.Boolean(nullable: false),
					AssignedUserId = c.String(maxLength: 256),
					PainLevel = c.Int(nullable: false),
					Hotline = c.Boolean(nullable: false),
					CallTime = c.DateTime(nullable: false),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.CallCenterProcedures", t => t.CallCenterProcedureId, cascadeDelete: true)
				.Index(t => t.CallCenterProcedureId);

			CreateTable(
				"dbo.CallCenterPracticeReferences",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					CallCenterId = c.Int(nullable: false),
					PracticeId = c.Int(nullable: false),
					StartCalls = c.Int(nullable: false),
					EndCalls = c.Int(nullable: false),
					SatisfactionInitial = c.Int(nullable: false),
					SatisfactionFinal = c.Int(nullable: false),
					Notes = c.String(),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.CallCenters", t => t.CallCenterId, cascadeDelete: true)
				.Index(t => t.CallCenterId);

			CreateTable(
				"dbo.CallSiteAdmins",
				c => new
				{
					Id = c.Int(nullable: false, identity: true),
					CompanyId = c.Int(),
					ProvisionalEmail = c.String(maxLength: 255),
					UserId = c.String(maxLength: 255),
					IsProvisional = c.Boolean(nullable: false),
					ProvisionalKey = c.Guid(),
					InvitationSent = c.Boolean(nullable: false),
					FirstName = c.String(maxLength: 255),
					LastName = c.String(maxLength: 255),
					CreatedOn = c.DateTime(),
					CreatedBy = c.String(maxLength: 128),
					ModifiedOn = c.DateTime(),
					ModifiedBy = c.String(maxLength: 128),
					DeletedOn = c.DateTime(),
					CopyOfId = c.Int(),
					IsNew = c.Boolean(nullable: false),
					User_Id = c.String(maxLength: 128),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.Companies", t => t.CompanyId)
				.ForeignKey("dbo.AspNetUsers", t => t.User_Id)
				.Index(t => t.CompanyId)
				.Index(t => t.User_Id);

			AddColumn("dbo.Companies", "CallSiteAdminEmailTemplate", c => c.String());
			AddColumn("dbo.Companies", "PayorId", c => c.Int(nullable: false));
			AddColumn("dbo.AspNetUsers", "Bilingual", c => c.Boolean(nullable: false));

			#endregion

			#region Default Call Center

			var defaults = @"
INSERT INTO [CallCenters] ([Name], IsNew) VALUES('Default Call Center', 0)
INSERT INTO [CallCenterPayors] ([Title], IsNew) VALUES('Default Payor', 0)
";
			Sql(defaults);
			#endregion
		}

		public override void Down()
		{
			DropForeignKey("dbo.CallSiteAdmins", "User_Id", "dbo.AspNetUsers");
			DropForeignKey("dbo.CallSiteAdmins", "CompanyId", "dbo.Companies");
			DropForeignKey("dbo.CallCenterPracticeReferences", "CallCenterId", "dbo.CallCenters");
			DropForeignKey("dbo.CallCenterPatients", "CallCenterProcedureId", "dbo.CallCenterProcedures");
			DropForeignKey("dbo.CallCenterProcedures", "PhysicianId", "dbo.CallCenterProviders");
			DropForeignKey("dbo.CallCenterProcedures", "NurseId", "dbo.CallCenterProviders");
			DropForeignKey("dbo.CallCenterScheduledCalls", "CallCenterProcedureId", "dbo.CallCenterProcedures");
			DropForeignKey("dbo.CallCenterProcedures", "CallCenterPayorId", "dbo.CallCenterPayors");
			DropForeignKey("dbo.CallCenterProcedures", "CallCenterId", "dbo.CallCenters");
			DropForeignKey("dbo.CallCenterProcedures", "AnesthesiologistId", "dbo.CallCenterProviders");
			DropForeignKey("dbo.CallCenterProviderFacilities", "CallCenterProviderId", "dbo.CallCenterProviders");
			DropIndex("dbo.CallSiteAdmins", new[] { "User_Id" });
			DropIndex("dbo.CallSiteAdmins", new[] { "CompanyId" });
			DropIndex("dbo.CallCenterPracticeReferences", new[] { "CallCenterId" });
			DropIndex("dbo.CallCenterScheduledCalls", new[] { "CallCenterProcedureId" });
			DropIndex("dbo.CallCenterProviderFacilities", new[] { "CallCenterProviderId" });
			DropIndex("dbo.CallCenterProcedures", new[] { "NurseId" });
			DropIndex("dbo.CallCenterProcedures", new[] { "AnesthesiologistId" });
			DropIndex("dbo.CallCenterProcedures", new[] { "PhysicianId" });
			DropIndex("dbo.CallCenterProcedures", new[] { "CallCenterPayorId" });
			DropIndex("dbo.CallCenterProcedures", new[] { "CallCenterId" });
			DropIndex("dbo.CallCenterPatients", new[] { "CallCenterProcedureId" });
			DropColumn("dbo.AspNetUsers", "Bilingual");
			DropColumn("dbo.Companies", "PayorId");
			DropColumn("dbo.Companies", "CallSiteAdminEmailTemplate");
			DropTable("dbo.CallSiteAdmins");
			DropTable("dbo.CallCenterPracticeReferences");
			DropTable("dbo.CallCenterScheduledCalls");
			DropTable("dbo.CallCenterPayors");
			DropTable("dbo.CallCenters");
			DropTable("dbo.CallCenterProviderFacilities");
			DropTable("dbo.CallCenterProviders");
			DropTable("dbo.CallCenterProcedures");
			DropTable("dbo.CallCenterPatients");
		}
	}
}
