namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveSatisfaction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "Satisfaction", c => c.Int(nullable: false));
            DropColumn("dbo.CallCenterScheduledCalls", "Satisfaction");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallCenterScheduledCalls", "Satisfaction", c => c.Int(nullable: false));
            DropColumn("dbo.CallCenterProcedures", "Satisfaction");
        }
    }
}
