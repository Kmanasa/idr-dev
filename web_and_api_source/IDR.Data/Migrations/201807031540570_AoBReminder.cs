namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AoBReminder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "RemindedAoB", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterProcedures", "RemindedAoB");
        }
    }
}
