namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadePainScoreCurrentNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CallCenterScheduledCalls", "PainScoreCurrent", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CallCenterScheduledCalls", "PainScoreCurrent", c => c.Int(nullable: false));
        }
    }
}
