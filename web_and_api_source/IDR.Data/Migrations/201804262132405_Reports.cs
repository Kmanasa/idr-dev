namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reports : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                        ShareUrl = c.String(maxLength: 20),
                        GlobalFlag = c.String(maxLength: 2),
                        Rp1Label = c.String(maxLength: 25),
                        Rp2Label = c.String(maxLength: 25),
                        Rp3Label = c.String(maxLength: 25),
                        Rp4Label = c.String(maxLength: 25),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Reports");
        }
    }
}
