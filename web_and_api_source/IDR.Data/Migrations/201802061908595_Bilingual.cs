namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Bilingual : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "SpanishSpeaking", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterProcedures", "SpanishSpeaking");
        }
    }
}
