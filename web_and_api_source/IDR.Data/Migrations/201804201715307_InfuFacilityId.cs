namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InfuFacilityId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterPracticeReferences", "InfuFacilityId", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterPracticeReferences", "InfuFacilityId");
        }
    }
}
