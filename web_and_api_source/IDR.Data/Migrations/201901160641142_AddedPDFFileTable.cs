namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPDFFileTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PDFFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        PDF = c.Binary(fixedLength: false),
                        CompanyId = c.Int(nullable: false),
                        Alias = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PDFFiles");
        }
    }
}
