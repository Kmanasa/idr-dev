namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAddl_URLToCallCenterPracticeReferencesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterPracticeReferences", "ADDL_URL", c => c.String(maxLength:100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterPracticeReferences", "ADDL_URL");
        }
    }
}
