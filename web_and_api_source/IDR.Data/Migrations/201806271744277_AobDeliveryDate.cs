namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AobDeliveryDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "DeliveredAoBDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterProcedures", "DeliveredAoBDate");
        }
    }
}
