namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DefaultCommentTypes : DbMigration
    {
        public override void Up()
        {
			Sql("if not exists (select top 1 * from CallCenterCommentTypes where Title = 'Assessment') insert into CallCenterCommentTypes([Index], Title, IsNew) values(1, 'Assessment', 0)");
			Sql("if not exists (select top 1 * from CallCenterCommentTypes where Title = 'Hotline Note') insert into CallCenterCommentTypes([Index], Title, IsNew) values(2, 'Hotline Note', 0)");
			Sql("if not exists (select top 1 * from CallCenterCommentTypes where Title = 'Patient Reported') insert into CallCenterCommentTypes([Index], Title, IsNew) values(3, 'Patient Reported', 0)");
			Sql("if not exists (select top 1 * from CallCenterCommentTypes where Title = 'General') insert into CallCenterCommentTypes([Index], Title, IsNew) values(4, 'General', 0)");
			Sql("if not exists (select top 1 * from CallCenterCommentTypes where Title = 'Unable to Reach Patient') insert into CallCenterCommentTypes([Index], Title, IsNew) values(5, 'Unable to Reach Patient', 0)");
			Sql("if not exists (select top 1 * from CallCenterCommentTypes where Title = 'Call Closed') insert into CallCenterCommentTypes([Index], Title, IsNew) values(6, 'Call Closed', 0)");
		}
        
        public override void Down()
        {
        }
    }
}
