namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDivisionToFacility : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Facilities", "Division", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Facilities", "Division");
        }
    }
}
