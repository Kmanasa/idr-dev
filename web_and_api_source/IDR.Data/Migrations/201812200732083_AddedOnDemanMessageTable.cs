namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOnDemanMessageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OnDemandMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PatientId = c.Int(nullable: false),
                        Phone = c.String(),
                        AccessCode = c.String(),
                        MessageType = c.String(),
                        MessageContent = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.OnDemandMessages");
        }
    }
}
