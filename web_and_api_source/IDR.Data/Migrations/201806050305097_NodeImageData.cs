namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NodeImageData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "ImageData", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "ImageData");
        }
    }
}
