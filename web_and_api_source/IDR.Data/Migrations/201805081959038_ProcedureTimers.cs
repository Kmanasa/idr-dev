namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureTimers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallCenterProcedureTimers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CallCenterProcedureId = c.Int(nullable: false),
                        ProcedureId = c.Int(nullable: false),
                        AccessCode = c.String(),
                        Rate = c.Double(nullable: false),
                        Volume = c.Int(nullable: false),
                        Safe = c.Boolean(nullable: false),
                        Duration = c.Int(nullable: false),
                        Unit = c.String(),
                        End = c.DateTime(nullable: false),
                        Start = c.DateTime(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CallCenterProcedures", t => t.CallCenterProcedureId, cascadeDelete: true)
                .Index(t => t.CallCenterProcedureId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CallCenterProcedureTimers", "CallCenterProcedureId", "dbo.CallCenterProcedures");
            DropIndex("dbo.CallCenterProcedureTimers", new[] { "CallCenterProcedureId" });
            DropTable("dbo.CallCenterProcedureTimers");
        }
    }
}
