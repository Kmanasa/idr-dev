namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CallSiteMobileEmailTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "CallSiteMobileUserEmailTemplate", c => c.String());

			Sql("UPDATE Companies SET CallSiteAdminEmailTemplate = PracticeAdminEmailTemplate WHERE CallSiteAdminEmailTemplate IS NULL");
			Sql("UPDATE Companies SET CallSiteMobileUserEmailTemplate = CallSiteAdminEmailTemplate WHERE CallSiteMobileUserEmailTemplate IS NULL");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "CallSiteMobileUserEmailTemplate");
        }
    }
}
