namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEffectiveUseAndDoseButtonToCallCenterScheduledCalls : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterScheduledCalls", "DoseButton", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "EffectiveUse", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterScheduledCalls", "EffectiveUse");
            DropColumn("dbo.CallCenterScheduledCalls", "DoseButton");
        }
    }
}
