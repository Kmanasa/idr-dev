namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFileSizeAndPageCountToPDFFilesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PDFFiles", "FileSize", c => c.Double(nullable: false));
            AddColumn("dbo.PDFFiles", "PageCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PDFFiles", "PageCount");
            DropColumn("dbo.PDFFiles", "FileSize");
        }
    }
}
