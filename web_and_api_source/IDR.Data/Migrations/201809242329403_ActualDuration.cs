namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualDuration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedureTimers", "ActualDuration", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterProcedureTimers", "ActualDuration");
        }
    }
}
