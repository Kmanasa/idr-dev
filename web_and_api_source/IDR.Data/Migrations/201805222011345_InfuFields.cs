namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InfuFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "InfuHaveConsent", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterProcedures", "InfuImSafetyFeatures", c => c.Boolean(nullable: false));
            DropColumn("dbo.CallCenterProcedures", "InfuHaveCosent");
            DropColumn("dbo.CallCenterProcedures", "InfuPhysicianNpi");
            DropColumn("dbo.CallCenterProcedures", "InfuPhysicianName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallCenterProcedures", "InfuPhysicianName", c => c.String(maxLength: 256));
            AddColumn("dbo.CallCenterProcedures", "InfuPhysicianNpi", c => c.String(maxLength: 256));
            AddColumn("dbo.CallCenterProcedures", "InfuHaveCosent", c => c.Boolean(nullable: false));
            DropColumn("dbo.CallCenterProcedures", "InfuImSafetyFeatures");
            DropColumn("dbo.CallCenterProcedures", "InfuHaveConsent");
        }
    }
}
