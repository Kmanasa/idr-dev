namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeliveredAoB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "DeliveredAoB", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterProcedures", "DeliveredAoB");
        }
    }
}
