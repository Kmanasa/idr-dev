namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InfuApiFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterProcedures", "InfuPumpModel", c => c.String(maxLength: 256));
            AddColumn("dbo.CallCenterProcedures", "InfuCck", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterProcedures", "InfuHaveCosent", c => c.Boolean(nullable: false));
            AddColumn("dbo.CallCenterProcedures", "InfuTreatmentPlan", c => c.String(maxLength: 256));
            AddColumn("dbo.CallCenterProcedures", "InfuRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CallCenterProcedures", "InfuVolume", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterProcedures", "InfuPhysicianNpi", c => c.String(maxLength: 256));
            AddColumn("dbo.CallCenterProcedures", "InfuPhysicianName", c => c.String(maxLength: 256));
            AddColumn("dbo.CallCenterPatients", "InfuPatientId", c => c.String(maxLength: 20));
            AddColumn("dbo.CallCenterPracticeReferences", "InfuFacilityGroupName", c => c.String(maxLength: 200));
            AddColumn("dbo.CallCenterPracticeReferences", "InfuImSafetyFeatures", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterPracticeReferences", "InfuImSafetyFeatures");
            DropColumn("dbo.CallCenterPracticeReferences", "InfuFacilityGroupName");
            DropColumn("dbo.CallCenterPatients", "InfuPatientId");
            DropColumn("dbo.CallCenterProcedures", "InfuPhysicianName");
            DropColumn("dbo.CallCenterProcedures", "InfuPhysicianNpi");
            DropColumn("dbo.CallCenterProcedures", "InfuVolume");
            DropColumn("dbo.CallCenterProcedures", "InfuRate");
            DropColumn("dbo.CallCenterProcedures", "InfuTreatmentPlan");
            DropColumn("dbo.CallCenterProcedures", "InfuHaveCosent");
            DropColumn("dbo.CallCenterProcedures", "InfuCck");
            DropColumn("dbo.CallCenterProcedures", "InfuPumpModel");
        }
    }
}
