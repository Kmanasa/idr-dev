namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CallType : DbMigration
    {
        public override void Up()
        {
	        var ccpaId = "53A94775-9C34-4BDE-B401-B3A6F141C278";

	        var query = string.Format(@"
if not exists(select top 1 Id from AspNetRoles where [Name] = 'Call Site Mobile')
begin
	INSERT INTO [AspNetRoles] ([Id],[Name]) VALUES('{0}','Call Site Mobile')
end
", ccpaId);

	        Sql(query);

			CreateTable(
                "dbo.CallSiteMobileUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(),
                        ProvisionalEmail = c.String(maxLength: 255),
                        UserId = c.String(maxLength: 128),
                        IsProvisional = c.Boolean(nullable: false),
                        ProvisionalKey = c.Guid(),
                        InvitationSent = c.Boolean(nullable: false),
                        FirstName = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.CompanyId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.CallCenterComments", "CallType", c => c.Int(nullable: false));
            AddColumn("dbo.CallCenterScheduledCalls", "CallType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CallSiteMobileUsers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CallSiteMobileUsers", "CompanyId", "dbo.Companies");
            DropIndex("dbo.CallSiteMobileUsers", new[] { "UserId" });
            DropIndex("dbo.CallSiteMobileUsers", new[] { "CompanyId" });
            DropColumn("dbo.CallCenterScheduledCalls", "CallType");
            DropColumn("dbo.CallCenterComments", "CallType");
            DropTable("dbo.CallSiteMobileUsers");
        }
    }
}
