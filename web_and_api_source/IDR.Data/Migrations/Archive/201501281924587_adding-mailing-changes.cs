namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingmailingchanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mailings", "Subject", c => c.String(maxLength: 1000));
            AddColumn("dbo.Mailings", "Body", c => c.String());
            DropColumn("dbo.Mailings", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Mailings", "UserId", c => c.String());
            DropColumn("dbo.Mailings", "Body");
            DropColumn("dbo.Mailings", "Subject");
        }
    }
}
