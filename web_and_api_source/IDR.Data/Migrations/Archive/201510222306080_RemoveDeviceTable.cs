namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveDeviceTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Devices", "ProcedureId", "dbo.Procedures");
            DropIndex("dbo.Devices", new[] { "ProcedureId" });
            DropTable("dbo.Devices");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureId = c.Int(nullable: false),
                        DeviceId = c.String(maxLength: 255),
                        DeviceType = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Devices", "ProcedureId");
            AddForeignKey("dbo.Devices", "ProcedureId", "dbo.Procedures", "Id", cascadeDelete: true);
        }
    }
}
