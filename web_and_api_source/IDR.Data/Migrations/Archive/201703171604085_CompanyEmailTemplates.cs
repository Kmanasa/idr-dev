namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanyEmailTemplates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "PatientEmailTemplate", c => c.String());
            AddColumn("dbo.Companies", "PhysicianEmailTemplate", c => c.String());
            AddColumn("dbo.Companies", "PhysiciansAssistantEmailTemplate", c => c.String());
            AddColumn("dbo.Companies", "PracticeAdminEmailTemplate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "PracticeAdminEmailTemplate");
            DropColumn("dbo.Companies", "PhysiciansAssistantEmailTemplate");
            DropColumn("dbo.Companies", "PhysicianEmailTemplate");
            DropColumn("dbo.Companies", "PatientEmailTemplate");
        }
    }
}
