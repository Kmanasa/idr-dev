namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibraryDuration : DbMigration
    {
        public override void Up()
        {
            // Default to 2 weeks
            AddColumn("dbo.NodeLibraries", "Duration", c => c.Int(nullable: false, defaultValue: 14));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeLibraries", "Duration");
        }
    }
}
