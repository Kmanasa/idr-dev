namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoteFacilities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RemoteFacilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        FacilityId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Facilities", t => t.FacilityId, cascadeDelete: false)
                .Index(t => t.CompanyId)
                .Index(t => t.FacilityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RemoteFacilities", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.RemoteFacilities", "CompanyId", "dbo.Companies");
            DropIndex("dbo.RemoteFacilities", new[] { "FacilityId" });
            DropIndex("dbo.RemoteFacilities", new[] { "CompanyId" });
            DropTable("dbo.RemoteFacilities");
        }
    }
}
