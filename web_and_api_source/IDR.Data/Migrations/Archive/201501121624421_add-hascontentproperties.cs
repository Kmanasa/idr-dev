namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addhascontentproperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeTypes", "HasContentProperties", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeTypes", "HasContentProperties");
        }
    }
}
