namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationSchemas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotificationResponses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationId = c.Int(nullable: false),
                        Text = c.String(maxLength: 500),
                        Trigger = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.NotificationId);
            
            CreateTable(
                "dbo.ProcedureNotificationOptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureNotificationId = c.Int(nullable: false),
                        Text = c.String(maxLength: 500),
                        Trigger = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProcedureNotifications", t => t.ProcedureNotificationId, cascadeDelete: true)
                .Index(t => t.ProcedureNotificationId);
            
            CreateTable(
                "dbo.ProcedureNotificationResponses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureNotificationId = c.Int(nullable: false),
                        ProcedureNotificationOptionId = c.Int(nullable: false),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProcedureNotificationOptions", t => t.ProcedureNotificationOptionId, cascadeDelete: false)
                .ForeignKey("dbo.ProcedureNotifications", t => t.ProcedureNotificationId, cascadeDelete: true)
                .Index(t => t.ProcedureNotificationId)
                .Index(t => t.ProcedureNotificationOptionId);
            
            AddColumn("dbo.Notifications", "MessageFullText", c => c.String());
            AddColumn("dbo.Notifications", "Repeat", c => c.Boolean(nullable: false));
            AddColumn("dbo.Notifications", "Repetitions", c => c.Int(nullable: false));
            AddColumn("dbo.Notifications", "RepetitionMinutes", c => c.Int(nullable: false));
            AddColumn("dbo.Notifications", "ResponseType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProcedureNotificationResponses", "ProcedureNotificationId", "dbo.ProcedureNotifications");
            DropForeignKey("dbo.ProcedureNotificationOptions", "ProcedureNotificationId", "dbo.ProcedureNotifications");
            DropForeignKey("dbo.ProcedureNotificationResponses", "ProcedureNotificationOptionId", "dbo.ProcedureNotificationOptions");
            DropForeignKey("dbo.NotificationResponses", "NotificationId", "dbo.Notifications");
            DropIndex("dbo.ProcedureNotificationResponses", new[] { "ProcedureNotificationOptionId" });
            DropIndex("dbo.ProcedureNotificationResponses", new[] { "ProcedureNotificationId" });
            DropIndex("dbo.ProcedureNotificationOptions", new[] { "ProcedureNotificationId" });
            DropIndex("dbo.NotificationResponses", new[] { "NotificationId" });
            DropColumn("dbo.Notifications", "ResponseType");
            DropColumn("dbo.Notifications", "RepetitionMinutes");
            DropColumn("dbo.Notifications", "Repetitions");
            DropColumn("dbo.Notifications", "Repeat");
            DropColumn("dbo.Notifications", "MessageFullText");
            DropTable("dbo.ProcedureNotificationResponses");
            DropTable("dbo.ProcedureNotificationOptions");
            DropTable("dbo.NotificationResponses");
        }
    }
}
