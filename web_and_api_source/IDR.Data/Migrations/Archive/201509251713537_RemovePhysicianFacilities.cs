namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePhysicianFacilities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PhysicianFacilityAssignments", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.PhysicianFacilityAssignments", "PhysicianId", "dbo.Physicians");
            DropIndex("dbo.PhysicianFacilityAssignments", new[] { "PhysicianId" });
            DropIndex("dbo.PhysicianFacilityAssignments", new[] { "FacilityId" });
            DropTable("dbo.PhysicianFacilityAssignments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PhysicianFacilityAssignments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhysicianId = c.Int(nullable: false),
                        FacilityId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.PhysicianFacilityAssignments", "FacilityId");
            CreateIndex("dbo.PhysicianFacilityAssignments", "PhysicianId");
            AddForeignKey("dbo.PhysicianFacilityAssignments", "PhysicianId", "dbo.Physicians", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PhysicianFacilityAssignments", "FacilityId", "dbo.Facilities", "Id", cascadeDelete: true);
        }
    }
}
