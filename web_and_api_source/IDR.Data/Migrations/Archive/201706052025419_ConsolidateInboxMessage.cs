namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConsolidateInboxMessage : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Notifications", "Message", c => c.String(maxLength: 1000));
            DropColumn("dbo.Notifications", "MessageFullText");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "MessageFullText", c => c.String());
            AlterColumn("dbo.Notifications", "Message", c => c.String(maxLength: 150));
        }
    }
}
