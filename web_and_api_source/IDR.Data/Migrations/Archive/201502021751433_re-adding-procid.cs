namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class readdingprocid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "ProcedureId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Procedures", "ProcedureId");
        }
    }
}
