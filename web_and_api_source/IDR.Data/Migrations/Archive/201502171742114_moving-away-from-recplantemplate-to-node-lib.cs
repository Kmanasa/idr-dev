namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class movingawayfromrecplantemplatetonodelib : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RecoveryPlans", "RecoveryPlanTemplateId", "dbo.RecoveryPlanTemplates");
            DropIndex("dbo.RecoveryPlans", new[] { "RecoveryPlanTemplateId" });
            AddColumn("dbo.RecoveryPlans", "MasterTemplateId", c => c.Int(nullable: false));
            CreateIndex("dbo.RecoveryPlans", "MasterTemplateId");
            AddForeignKey("dbo.RecoveryPlans", "MasterTemplateId", "dbo.NodeLibraries", "Id", cascadeDelete: true);
            DropColumn("dbo.RecoveryPlans", "RecoveryPlanTemplateId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RecoveryPlans", "RecoveryPlanTemplateId", c => c.Int(nullable: false));
            DropForeignKey("dbo.RecoveryPlans", "MasterTemplateId", "dbo.NodeLibraries");
            DropIndex("dbo.RecoveryPlans", new[] { "MasterTemplateId" });
            DropColumn("dbo.RecoveryPlans", "MasterTemplateId");
            CreateIndex("dbo.RecoveryPlans", "RecoveryPlanTemplateId");
            AddForeignKey("dbo.RecoveryPlans", "RecoveryPlanTemplateId", "dbo.RecoveryPlanTemplates", "Id", cascadeDelete: true);
        }
    }
}
