namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationDeliveryTimes : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SentNotifications", newName: "ProcedureNotifications");
            AddColumn("dbo.Notifications", "Hour", c => c.Int(nullable: false));
            AddColumn("dbo.Notifications", "Minute", c => c.Int(nullable: false));
            AddColumn("dbo.ProcedureNotifications", "Sent", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProcedureNotifications", "SendTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Notifications", "TimeToSend");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "TimeToSend", c => c.DateTime(nullable: false));
            DropColumn("dbo.ProcedureNotifications", "SendTime");
            DropColumn("dbo.ProcedureNotifications", "Sent");
            DropColumn("dbo.Notifications", "Minute");
            DropColumn("dbo.Notifications", "Hour");
            RenameTable(name: "dbo.ProcedureNotifications", newName: "SentNotifications");
        }
    }
}
