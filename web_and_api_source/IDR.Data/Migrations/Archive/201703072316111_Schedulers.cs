namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Schedulers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Schedulers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhysicianId = c.Int(),
                        CompanyId = c.Int(),
                        ProvisionalEmail = c.String(maxLength: 255),
                        UserId = c.String(maxLength: 128),
                        IsProvisional = c.Boolean(nullable: false),
                        ProvisionalKey = c.Guid(),
                        InvitationSent = c.Boolean(nullable: false),
                        FirstName = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.Physicians", t => t.PhysicianId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.PhysicianId)
                .Index(t => t.CompanyId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schedulers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Schedulers", "PhysicianId", "dbo.Physicians");
            DropForeignKey("dbo.Schedulers", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Schedulers", new[] { "UserId" });
            DropIndex("dbo.Schedulers", new[] { "CompanyId" });
            DropIndex("dbo.Schedulers", new[] { "PhysicianId" });
            DropTable("dbo.Schedulers");
        }
    }
}
