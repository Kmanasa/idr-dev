namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MailersRemoved : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Mailings", "IX_MailingEntity");
            DropTable("dbo.Mailings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Mailings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 255),
                        EntityId = c.Int(nullable: false),
                        EntityName = c.String(maxLength: 255),
                        RequestName = c.String(maxLength: 255),
                        Subject = c.String(maxLength: 1000),
                        Body = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Mailings", new[] { "EntityName", "RequestName", "EntityId" }, name: "IX_MailingEntity");
        }
    }
}
