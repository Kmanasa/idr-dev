// <auto-generated />
namespace IDR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class firstlastnamepartofbase : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(firstlastnamepartofbase));
        
        string IMigrationMetadata.Id
        {
            get { return "201501132203096_first-last-name-part-of-base"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
