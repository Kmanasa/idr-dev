namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveVantage : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.VantageDrugTradeNames", "VantageDrugId", "dbo.VantageDrugs");
            DropIndex("dbo.VantageDrugTradeNames", new[] { "VantageDrugId" });
            DropTable("dbo.VantageDrugs");
            DropTable("dbo.VantageDrugTradeNames");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.VantageDrugTradeNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VantageDrugId = c.Int(nullable: false),
                        TradeName = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VantageDrugs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VantageId = c.String(),
                        GenericName = c.String(),
                        Image = c.Binary(),
                        EnglishLeaflet = c.String(),
                        SpanishLeaflet = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.VantageDrugTradeNames", "VantageDrugId");
            AddForeignKey("dbo.VantageDrugTradeNames", "VantageDrugId", "dbo.VantageDrugs", "Id", cascadeDelete: true);
        }
    }
}
