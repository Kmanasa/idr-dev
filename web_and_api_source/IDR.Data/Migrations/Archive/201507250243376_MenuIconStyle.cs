namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MenuIconStyle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "MenuIconStyle", c => c.String(maxLength: 75));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "MenuIconStyle");
        }
    }
}
