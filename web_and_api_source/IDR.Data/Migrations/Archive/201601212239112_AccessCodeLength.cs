namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccessCodeLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Procedures", "AccessCode", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Procedures", "AccessCode", c => c.String(maxLength: 50));
        }
    }
}
