namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingtitletopropertyvaue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValues", "Title", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValues", "Title");
        }
    }
}
