namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class physicianfacililtyassignments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PhysicianFacilityAssignments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhysicianId = c.Int(nullable: false),
                        FacilityId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Facilities", t => t.FacilityId, cascadeDelete: true)
                .ForeignKey("dbo.Physicians", t => t.PhysicianId, cascadeDelete: true)
                .Index(t => t.PhysicianId)
                .Index(t => t.FacilityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhysicianFacilityAssignments", "PhysicianId", "dbo.Physicians");
            DropForeignKey("dbo.PhysicianFacilityAssignments", "FacilityId", "dbo.Facilities");
            DropIndex("dbo.PhysicianFacilityAssignments", new[] { "FacilityId" });
            DropIndex("dbo.PhysicianFacilityAssignments", new[] { "PhysicianId" });
            DropTable("dbo.PhysicianFacilityAssignments");
        }
    }
}
