namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingbindingvalueforimagebinding : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValues", "IsBoundValue", c => c.Boolean(nullable: false));
            AddColumn("dbo.PropertyValues", "BindPath", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValues", "BindPath");
            DropColumn("dbo.PropertyValues", "IsBoundValue");
        }
    }
}
