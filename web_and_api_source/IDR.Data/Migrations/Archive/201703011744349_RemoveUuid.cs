namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUuid : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProcedureAccessLogs", "Uuid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProcedureAccessLogs", "Uuid", c => c.String(maxLength: 500));
        }
    }
}
