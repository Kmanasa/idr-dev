namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProcedureNotifications", "NextAttemptTime", c => c.DateTime());
            AlterColumn("dbo.ProcedureNotifications", "Response", c => c.String(maxLength: 500));
            AlterColumn("dbo.ProcedureNotifications", "DateResponded", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProcedureNotifications", "DateResponded", c => c.String());
            AlterColumn("dbo.ProcedureNotifications", "Response", c => c.String());
            AlterColumn("dbo.ProcedureNotifications", "NextAttemptTime", c => c.DateTime(nullable: false));
        }
    }
}
