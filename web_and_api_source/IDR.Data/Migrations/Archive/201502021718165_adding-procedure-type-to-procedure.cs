namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingproceduretypetoprocedure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "ProcedureTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Procedures", "ProcedureTypeId");
            AddForeignKey("dbo.Procedures", "ProcedureTypeId", "dbo.ProcedureTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedures", "ProcedureTypeId", "dbo.ProcedureTypes");
            DropIndex("dbo.Procedures", new[] { "ProcedureTypeId" });
            DropColumn("dbo.Procedures", "ProcedureTypeId");
        }
    }
}
