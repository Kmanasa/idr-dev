namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingisphysconfig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeTypes", "IsConfigurable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeTypes", "IsConfigurable");
        }
    }
}
