namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TriggerText : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "TriggerText", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "TriggerText");
        }
    }
}
