namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class changestosupportsurgerydates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "NumberOfDays", c => c.Int());
            AddColumn("dbo.Procedures", "SurgeryDate", c => c.DateTime());
            DropColumn("dbo.Procedures", "Timeframe");
            DropColumn("dbo.Procedures", "DischargeDate");
            DropColumn("dbo.Procedures", "DaysPost");
            DropColumn("dbo.Procedures", "ProtocolEndDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Procedures", "ProtocolEndDate", c => c.DateTime());
            AddColumn("dbo.Procedures", "DaysPost", c => c.Int());
            AddColumn("dbo.Procedures", "DischargeDate", c => c.DateTime());
            AddColumn("dbo.Procedures", "Timeframe", c => c.Int());
            DropColumn("dbo.Procedures", "SurgeryDate");
            DropColumn("dbo.Procedures", "NumberOfDays");
        }
    }
}
