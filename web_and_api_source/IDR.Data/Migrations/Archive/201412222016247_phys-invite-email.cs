namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class physinviteemail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Physicians", "ProvisionalEmail", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Physicians", "ProvisionalEmail");
        }
    }
}
