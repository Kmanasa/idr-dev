namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureNotificationFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProcedureNotifications", "Procedure_Id", "dbo.Procedures");
            DropIndex("dbo.ProcedureNotifications", new[] { "Procedure_Id" });
            RenameColumn(table: "dbo.ProcedureNotifications", name: "Procedure_Id", newName: "ProcedureId");
            AlterColumn("dbo.ProcedureNotifications", "ProcedureId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProcedureNotifications", "ProcedureId");
            AddForeignKey("dbo.ProcedureNotifications", "ProcedureId", "dbo.Procedures", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProcedureNotifications", "ProcedureId", "dbo.Procedures");
            DropIndex("dbo.ProcedureNotifications", new[] { "ProcedureId" });
            AlterColumn("dbo.ProcedureNotifications", "ProcedureId", c => c.Int());
            RenameColumn(table: "dbo.ProcedureNotifications", name: "ProcedureId", newName: "Procedure_Id");
            CreateIndex("dbo.ProcedureNotifications", "Procedure_Id");
            AddForeignKey("dbo.ProcedureNotifications", "Procedure_Id", "dbo.Procedures", "Id");
        }
    }
}
