namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PracticeLicenseFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "MaxNamedUsers", c => c.Int(nullable: false));
            AddColumn("dbo.Companies", "MaxPhysicians", c => c.Int(nullable: false));
            AddColumn("dbo.Companies", "MaxFacilities", c => c.Int(nullable: false));
            AddColumn("dbo.Companies", "LicenseExpiration", c => c.DateTime(nullable: false));
            AddColumn("dbo.Companies", "Discount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Discount");
            DropColumn("dbo.Companies", "LicenseExpiration");
            DropColumn("dbo.Companies", "MaxFacilities");
            DropColumn("dbo.Companies", "MaxPhysicians");
            DropColumn("dbo.Companies", "MaxNamedUsers");
        }
    }
}
