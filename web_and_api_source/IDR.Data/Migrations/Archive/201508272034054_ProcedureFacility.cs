namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureFacility : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "FacilityId", c => c.Int(nullable: false));
            CreateIndex("dbo.Procedures", "FacilityId");
            AddForeignKey("dbo.Procedures", "FacilityId", "dbo.Facilities", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedures", "FacilityId", "dbo.Facilities");
            DropIndex("dbo.Procedures", new[] { "FacilityId" });
            DropColumn("dbo.Procedures", "FacilityId");
        }
    }
}
