namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addproceduretypesmore : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProcedureTypes", "CptCode", c => c.String(maxLength: 25));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProcedureTypes", "CptCode", c => c.String(maxLength: 10));
        }
    }
}
