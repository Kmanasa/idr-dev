namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChildTemplateId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "ChildTemplateId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "ChildTemplateId");
        }
    }
}
