namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePractices : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Physicians", "PracticeId", "dbo.Practices");
            DropForeignKey("dbo.Patients", "PracticeId", "dbo.Practices");
            DropForeignKey("dbo.PhysiciansAssistants", "PracticeId", "dbo.Practices");
            DropForeignKey("dbo.PracticeAdmins", "PracticeId", "dbo.Practices");
            DropIndex("dbo.Patients", new[] { "PracticeId" });
            DropIndex("dbo.Physicians", new[] { "PracticeId" });
            DropIndex("dbo.PhysiciansAssistants", new[] { "PracticeId" });
            DropIndex("dbo.PracticeAdmins", new[] { "PracticeId" });
            AddColumn("dbo.Companies", "HoursOfOperation", c => c.String(maxLength: 500));
            AddColumn("dbo.Companies", "CompanyType", c => c.Int(nullable: false));
            AddColumn("dbo.Patients", "CompanyId", c => c.Int());
            AddColumn("dbo.Physicians", "CompanyId", c => c.Int());
            AddColumn("dbo.PhysiciansAssistants", "CompanyId", c => c.Int());
            AddColumn("dbo.PracticeAdmins", "CompanyId", c => c.Int());
            CreateIndex("dbo.Physicians", "CompanyId");
            CreateIndex("dbo.Patients", "CompanyId");
            CreateIndex("dbo.PhysiciansAssistants", "CompanyId");
            CreateIndex("dbo.PracticeAdmins", "CompanyId");
            AddForeignKey("dbo.Physicians", "CompanyId", "dbo.Companies", "Id");
            AddForeignKey("dbo.Patients", "CompanyId", "dbo.Companies", "Id");
            AddForeignKey("dbo.PhysiciansAssistants", "CompanyId", "dbo.Companies", "Id");
            AddForeignKey("dbo.PracticeAdmins", "CompanyId", "dbo.Companies", "Id");
            DropColumn("dbo.Patients", "PracticeId");
            DropColumn("dbo.Physicians", "PracticeId");
            DropColumn("dbo.PhysiciansAssistants", "PracticeId");
            DropColumn("dbo.PracticeAdmins", "PracticeId");
            DropTable("dbo.Practices");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Practices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HoursOfOperation = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.PracticeAdmins", "PracticeId", c => c.Int());
            AddColumn("dbo.PhysiciansAssistants", "PracticeId", c => c.Int());
            AddColumn("dbo.Physicians", "PracticeId", c => c.Int());
            AddColumn("dbo.Patients", "PracticeId", c => c.Int());
            DropForeignKey("dbo.PracticeAdmins", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PhysiciansAssistants", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Patients", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Physicians", "CompanyId", "dbo.Companies");
            DropIndex("dbo.PracticeAdmins", new[] { "CompanyId" });
            DropIndex("dbo.PhysiciansAssistants", new[] { "CompanyId" });
            DropIndex("dbo.Patients", new[] { "CompanyId" });
            DropIndex("dbo.Physicians", new[] { "CompanyId" });
            DropColumn("dbo.PracticeAdmins", "CompanyId");
            DropColumn("dbo.PhysiciansAssistants", "CompanyId");
            DropColumn("dbo.Physicians", "CompanyId");
            DropColumn("dbo.Patients", "CompanyId");
            DropColumn("dbo.Companies", "CompanyType");
            DropColumn("dbo.Companies", "HoursOfOperation");
            CreateIndex("dbo.PracticeAdmins", "PracticeId");
            CreateIndex("dbo.PhysiciansAssistants", "PracticeId");
            CreateIndex("dbo.Physicians", "PracticeId");
            CreateIndex("dbo.Patients", "PracticeId");
            AddForeignKey("dbo.PracticeAdmins", "PracticeId", "dbo.Practices", "Id");
            AddForeignKey("dbo.PhysiciansAssistants", "PracticeId", "dbo.Practices", "Id");
            AddForeignKey("dbo.Patients", "PracticeId", "dbo.Practices", "Id");
            AddForeignKey("dbo.Physicians", "PracticeId", "dbo.Practices", "Id");
        }
    }
}
