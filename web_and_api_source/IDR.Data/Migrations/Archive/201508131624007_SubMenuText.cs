namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubMenuText : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "SubMenuText", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "SubMenuText");
        }
    }
}
