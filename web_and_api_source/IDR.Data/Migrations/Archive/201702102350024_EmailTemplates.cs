namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailTemplates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Alias = c.String(nullable: false, maxLength: 50),
                        Content = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailTemplates");
        }
    }
}
