namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RemoveNodeLibraryCssClass : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.NodeLibraries", "CssClass");
            DropColumn("dbo.NodeLibraries", "BuildUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NodeLibraries", "BuildUrl", c => c.String(maxLength: 1000));
            AddColumn("dbo.NodeLibraries", "CssClass", c => c.String(maxLength: 255));
        }
    }
}
