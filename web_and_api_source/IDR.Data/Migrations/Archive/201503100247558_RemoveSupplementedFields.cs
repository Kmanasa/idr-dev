namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RemoveSupplementedFields : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Nodes", "ListIconCss");
            DropColumn("dbo.Nodes", "IsSupplemented");
            DropColumn("dbo.Nodes", "SupplementMultiple");
            DropColumn("dbo.Nodes", "SupplementalLibraryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Nodes", "SupplementalLibraryId", c => c.Int());
            AddColumn("dbo.Nodes", "SupplementMultiple", c => c.Boolean(nullable: false));
            AddColumn("dbo.Nodes", "IsSupplemented", c => c.Boolean(nullable: false));
            AddColumn("dbo.Nodes", "ListIconCss", c => c.String(maxLength: 255));
        }
    }
}
