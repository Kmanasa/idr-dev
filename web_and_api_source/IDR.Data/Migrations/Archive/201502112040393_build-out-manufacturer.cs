namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class buildoutmanufacturer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Manufacturers", "IsPreferredContentProvider", c => c.Boolean(nullable: false));
            AddColumn("dbo.Manufacturers", "WebsiteUrl", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Manufacturers", "Name", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Manufacturers", "Name", c => c.String());
            DropColumn("dbo.Manufacturers", "WebsiteUrl");
            DropColumn("dbo.Manufacturers", "IsPreferredContentProvider");
        }
    }
}
