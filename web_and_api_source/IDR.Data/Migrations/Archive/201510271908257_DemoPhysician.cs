namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemoPhysician : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Physicians", "DemoPhysician", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Physicians", "DemoPhysician");
        }
    }
}
