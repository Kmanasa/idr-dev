namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NodeCopyStoredProc : DbMigration
    {
        public override void Up()
        {
            Sql(
@"

CREATE PROCEDURE CopyNodes

	@baseNodeLibraryId int, 
	@newNodeLibraryId int 
AS
BEGIN

/*

  Duplicate the node structure from one library to another

*/

DECLARE @newRows TABLE(id int, orig_id int)

MERGE INTO Nodes
USING (
	
	SELECT 
		[Id],[Name],[TypeId],[Title],[Description],[NodeId],[IsPhysicianEditable],[OverrideSourceId],[RecoveryPlanId],[Sequence],[CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[DeletedOn],[CopyOfId],[IsNew],[ChildTemplateId],[HtmlContent],[MenuIconStyle],[SubMenuText]
	FROM 
		Nodes 
	WHERE 
		NodeLibraryId = @baseNodeLibraryId

) AS cf
ON 1 = 0
WHEN NOT MATCHED THEN

  INSERT
	([NodeLibraryId],[Name],[TypeId],[Title],[Description],[NodeId],[IsPhysicianEditable],[OverrideSourceId],[RecoveryPlanId],[Sequence],[CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[DeletedOn],[CopyOfId],[IsNew],[ChildTemplateId],[HtmlContent],[MenuIconStyle],[SubMenuText]) 

  VALUES
	(@newNodeLibraryId, cf.[Name],cf.[TypeId],cf.[Title],cf.[Description],cf.[NodeId],cf.[IsPhysicianEditable],cf.[OverrideSourceId],cf.[RecoveryPlanId],cf.[Sequence],cf.[CreatedOn],cf.[CreatedBy],cf.[ModifiedOn],cf.[ModifiedBy],cf.[DeletedOn],cf.[Id],cf.[IsNew],cf.[ChildTemplateId],cf.[HtmlContent],cf.[MenuIconStyle],cf.[SubMenuText])

OUTPUT inserted.Id, cf.Id INTO @newRows(id, orig_id);

UPDATE Nodes
SET NodeId = 
( 
    SELECT 
		nr.id
    FROM 
		@newRows nr
    WHERE 
		nr.orig_id = Nodes.NodeId
)
WHERE NodeLibraryId = @newNodeLibraryId;



/*

 Duplicate the properties for the original nodes into the newly added nodes

*/

INSERT INTO
	Properties
	(Name, CssClass,Title,Value,NodeId,PropertyId,TypeId,CreatedOn,CreatedBy,ModifiedOn,ModifiedBy,DeletedOn,CopyOfId,IsNew	)
SELECT 
	p.Name, p.CssClass,p.Title,p.Value,n.Id,p.PropertyId,p.TypeId,p.CreatedOn,p.CreatedBy,p.ModifiedOn,p.ModifiedBy,p.DeletedOn,p.CopyOfId,p.IsNew
FROM 
	Properties p
INNER JOIN
	Nodes n on n.CopyOfId = p.NodeId
WHERE n.Id IN
	(SELECT ID FROM Nodes
		WHERE NodeLibraryId = 2137  
	)

END

");
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[CopyNodex]");
        }
    }
}
