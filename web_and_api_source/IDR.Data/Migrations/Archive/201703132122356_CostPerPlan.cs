namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CostPerPlan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "MaxPlans", c => c.Int(nullable: false));
            AddColumn("dbo.Companies", "CostPerPlan", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "CostPerPlan");
            DropColumn("dbo.Companies", "MaxPlans");
        }
    }
}
