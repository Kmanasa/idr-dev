namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnisformattedtopropvalue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValues", "IsFormatted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValues", "IsFormatted");
        }
    }
}
