namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TimeZone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "TimeZone", c => c.String(maxLength: 250));

            Sql("Update dbo.Companies set TimeZone = 'Mountain Standard Time' where TimeZone is null");

        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "TimeZone");
        }
    }
}
