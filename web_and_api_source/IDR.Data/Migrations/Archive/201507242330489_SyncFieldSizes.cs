namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SyncFieldSizes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Properties", "Value", c => c.String());
            AlterColumn("dbo.PropertyValues", "Value", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PropertyValues", "Value", c => c.String(maxLength: 4000));
            AlterColumn("dbo.Properties", "Value", c => c.String(maxLength: 4000));
        }
    }
}
