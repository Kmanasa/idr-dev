namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Attributes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attributes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NodeLibraryAttributes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NodeLibraryId = c.Int(nullable: false),
                        AttributeId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.AttributeId, cascadeDelete: true)
                .ForeignKey("dbo.NodeLibraries", t => t.NodeLibraryId, cascadeDelete: true)
                .Index(t => t.NodeLibraryId)
                .Index(t => t.AttributeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NodeLibraryAttributes", "NodeLibraryId", "dbo.NodeLibraries");
            DropForeignKey("dbo.NodeLibraryAttributes", "AttributeId", "dbo.Attributes");
            DropIndex("dbo.NodeLibraryAttributes", new[] { "AttributeId" });
            DropIndex("dbo.NodeLibraryAttributes", new[] { "NodeLibraryId" });
            DropTable("dbo.NodeLibraryAttributes");
            DropTable("dbo.Attributes");
        }
    }
}
