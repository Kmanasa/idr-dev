namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingnametoprocedure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "EventCode", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "ProcedureName", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Procedures", "ProcedureName");
            DropColumn("dbo.Procedures", "EventCode");
        }
    }
}
