namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class cleaningupimageforuploadmanage : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Images", "FileName");
            DropColumn("dbo.Images", "Description");
            DropColumn("dbo.Images", "Url");
            DropColumn("dbo.Images", "RepositoryId");
            DropColumn("dbo.Images", "IsEnlargeable");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Images", "IsEnlargeable", c => c.Boolean());
            AddColumn("dbo.Images", "RepositoryId", c => c.String(maxLength: 255));
            AddColumn("dbo.Images", "Url", c => c.String(maxLength: 255));
            AddColumn("dbo.Images", "Description", c => c.String(maxLength: 255));
            AddColumn("dbo.Images", "FileName", c => c.String(maxLength: 255));
        }
    }
}
