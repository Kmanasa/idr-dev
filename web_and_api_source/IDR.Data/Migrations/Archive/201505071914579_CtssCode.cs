namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CtssCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeLibraries", "CtssCode", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeLibraries", "CtssCode");
        }
    }
}
