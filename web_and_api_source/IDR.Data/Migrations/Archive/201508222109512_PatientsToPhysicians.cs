namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PatientsToPhysicians : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RecoveryPlans", "MasterTemplateId", "dbo.NodeLibraries");
            DropForeignKey("dbo.SupplementalNodeSelections", "RecoveryPlanId", "dbo.RecoveryPlans");
            DropForeignKey("dbo.SupplementalNodeSelections", "SourceNodeId", "dbo.Nodes");
            DropForeignKey("dbo.RecoveryPlans", "PhysicianId", "dbo.Physicians");
            DropForeignKey("dbo.ProcedureTypes", "RecoveryPlanId", "dbo.RecoveryPlans");
            DropForeignKey("dbo.RecoveryPlans", "SystemOverrideLibraryId", "dbo.NodeLibraries");
            DropForeignKey("dbo.Procedures", "ProcedureTypeId", "dbo.ProcedureTypes");
            DropForeignKey("dbo.Procedures", "RecoveryPlanId", "dbo.RecoveryPlans");
            DropForeignKey("dbo.RecoveryPlanTemplates", "NodeLibraryId", "dbo.NodeLibraries");
            DropForeignKey("dbo.ProcedureTypes", "RecoveryPlanTemplate_Id", "dbo.RecoveryPlanTemplates");
            DropForeignKey("dbo.Patients", "Physician_Id", "dbo.Physicians");
            DropIndex("dbo.Patients", new[] { "Physician_Id" });
            DropIndex("dbo.Procedures", new[] { "RecoveryPlanId" });
            DropIndex("dbo.Procedures", new[] { "ProcedureTypeId" });
            DropIndex("dbo.ProcedureTypes", new[] { "RecoveryPlanId" });
            DropIndex("dbo.ProcedureTypes", new[] { "RecoveryPlanTemplate_Id" });
            DropIndex("dbo.RecoveryPlans", new[] { "MasterTemplateId" });
            DropIndex("dbo.RecoveryPlans", new[] { "SystemOverrideLibraryId" });
            DropIndex("dbo.RecoveryPlans", new[] { "PhysicianId" });
            DropIndex("dbo.SupplementalNodeSelections", new[] { "RecoveryPlanId" });
            DropIndex("dbo.SupplementalNodeSelections", new[] { "SourceNodeId" });
            DropIndex("dbo.RecoveryPlanTemplates", new[] { "NodeLibraryId" });
            RenameColumn(table: "dbo.Patients", name: "Physician_Id", newName: "PhysicianId");
            AddColumn("dbo.Procedures", "StartDate", c => c.DateTime());
            AlterColumn("dbo.Patients", "PhysicianId", c => c.Int(nullable: false));
            CreateIndex("dbo.Patients", "PhysicianId");
            AddForeignKey("dbo.Patients", "PhysicianId", "dbo.Physicians", "Id", cascadeDelete: false);
            DropColumn("dbo.Patients", "MRN");
            DropColumn("dbo.Procedures", "EventCode");
            DropColumn("dbo.Procedures", "DischargeProtocol");
            DropColumn("dbo.Procedures", "ProcedureName");
            DropColumn("dbo.Procedures", "NumberOfDays");
            DropColumn("dbo.Procedures", "SurgeryDate");
            DropColumn("dbo.Procedures", "Satisfaction");
            DropColumn("dbo.Procedures", "RecoveryPlanId");
            DropColumn("dbo.Procedures", "CodeUrl");
            DropColumn("dbo.Procedures", "ShortUrl");
            DropColumn("dbo.Procedures", "ApiKey");
            DropColumn("dbo.Procedures", "HasQrCode");
            DropColumn("dbo.Procedures", "FullUrl");
            DropColumn("dbo.Procedures", "ProcedureId");
            DropColumn("dbo.Procedures", "ProcedureTypeId");
            DropTable("dbo.ProcedureTypes");
            DropTable("dbo.RecoveryPlans");
            DropTable("dbo.SupplementalNodeSelections");
            DropTable("dbo.RecoveryPlanTemplates");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RecoveryPlanTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        NodeLibraryId = c.Int(nullable: false),
                        Description = c.String(maxLength: 4000),
                        Active = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SupplementalNodeSelections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecoveryPlanId = c.Int(nullable: false),
                        SourceNodeId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecoveryPlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        MasterTemplateId = c.Int(nullable: false),
                        SystemOverrideLibraryId = c.Int(nullable: false),
                        UserId = c.String(),
                        PhysicianId = c.Int(),
                        Active = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProcedureTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CptCode = c.String(maxLength: 25),
                        FullDescription = c.String(maxLength: 1000),
                        ShortDescription = c.String(maxLength: 255),
                        RecoveryPlanId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                        RecoveryPlanTemplate_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Procedures", "ProcedureTypeId", c => c.Int(nullable: false));
            AddColumn("dbo.Procedures", "ProcedureId", c => c.Int(nullable: false));
            AddColumn("dbo.Procedures", "FullUrl", c => c.String(maxLength: 1000));
            AddColumn("dbo.Procedures", "HasQrCode", c => c.Boolean(nullable: false));
            AddColumn("dbo.Procedures", "ApiKey", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "ShortUrl", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "CodeUrl", c => c.String(maxLength: 1000));
            AddColumn("dbo.Procedures", "RecoveryPlanId", c => c.Int());
            AddColumn("dbo.Procedures", "Satisfaction", c => c.Int());
            AddColumn("dbo.Procedures", "SurgeryDate", c => c.DateTime());
            AddColumn("dbo.Procedures", "NumberOfDays", c => c.Int());
            AddColumn("dbo.Procedures", "ProcedureName", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "DischargeProtocol", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "EventCode", c => c.String(maxLength: 255));
            AddColumn("dbo.Patients", "MRN", c => c.String(maxLength: 255));
            DropForeignKey("dbo.Patients", "PhysicianId", "dbo.Physicians");
            DropIndex("dbo.Patients", new[] { "PhysicianId" });
            AlterColumn("dbo.Patients", "PhysicianId", c => c.Int());
            DropColumn("dbo.Procedures", "StartDate");
            RenameColumn(table: "dbo.Patients", name: "PhysicianId", newName: "Physician_Id");
            CreateIndex("dbo.RecoveryPlanTemplates", "NodeLibraryId");
            CreateIndex("dbo.SupplementalNodeSelections", "SourceNodeId");
            CreateIndex("dbo.SupplementalNodeSelections", "RecoveryPlanId");
            CreateIndex("dbo.RecoveryPlans", "PhysicianId");
            CreateIndex("dbo.RecoveryPlans", "SystemOverrideLibraryId");
            CreateIndex("dbo.RecoveryPlans", "MasterTemplateId");
            CreateIndex("dbo.ProcedureTypes", "RecoveryPlanTemplate_Id");
            CreateIndex("dbo.ProcedureTypes", "RecoveryPlanId");
            CreateIndex("dbo.Procedures", "ProcedureTypeId");
            CreateIndex("dbo.Procedures", "RecoveryPlanId");
            CreateIndex("dbo.Patients", "Physician_Id");
            AddForeignKey("dbo.Patients", "Physician_Id", "dbo.Physicians", "Id");
            AddForeignKey("dbo.ProcedureTypes", "RecoveryPlanTemplate_Id", "dbo.RecoveryPlanTemplates", "Id");
            AddForeignKey("dbo.RecoveryPlanTemplates", "NodeLibraryId", "dbo.NodeLibraries", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Procedures", "RecoveryPlanId", "dbo.RecoveryPlans", "Id");
            AddForeignKey("dbo.Procedures", "ProcedureTypeId", "dbo.ProcedureTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RecoveryPlans", "SystemOverrideLibraryId", "dbo.NodeLibraries", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProcedureTypes", "RecoveryPlanId", "dbo.RecoveryPlans", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RecoveryPlans", "PhysicianId", "dbo.Physicians", "Id");
            AddForeignKey("dbo.SupplementalNodeSelections", "SourceNodeId", "dbo.Nodes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SupplementalNodeSelections", "RecoveryPlanId", "dbo.RecoveryPlans", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RecoveryPlans", "MasterTemplateId", "dbo.NodeLibraries", "Id", cascadeDelete: true);
        }
    }
}
