namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addproceduretypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProcedureTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CptCode = c.String(maxLength: 25),
                        FullDescription = c.String(maxLength: 1000),
                        ShortDescription = c.String(maxLength: 255),
                        RecoveryPlanId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        RecoveryPlanTemplate_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RecoveryPlans", t => t.RecoveryPlanId, cascadeDelete: true)
                .ForeignKey("dbo.RecoveryPlanTemplates", t => t.RecoveryPlanTemplate_Id)
                .Index(t => t.RecoveryPlanId)
                .Index(t => t.RecoveryPlanTemplate_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProcedureTypes", "RecoveryPlanTemplate_Id", "dbo.RecoveryPlanTemplates");
            DropForeignKey("dbo.ProcedureTypes", "RecoveryPlanId", "dbo.RecoveryPlans");
            DropIndex("dbo.ProcedureTypes", new[] { "RecoveryPlanTemplate_Id" });
            DropIndex("dbo.ProcedureTypes", new[] { "RecoveryPlanId" });
            DropTable("dbo.ProcedureTypes");
        }
    }
}
