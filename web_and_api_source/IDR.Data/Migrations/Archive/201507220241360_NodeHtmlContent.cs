namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NodeHtmlContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "HtmlContent", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "HtmlContent");
        }
    }
}
