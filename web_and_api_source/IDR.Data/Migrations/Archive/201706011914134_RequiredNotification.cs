namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredNotification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "Required", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProcedureNotifications", "Triggered", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcedureNotifications", "Triggered");
            DropColumn("dbo.Notifications", "Required");
        }
    }
}
