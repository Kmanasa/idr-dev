namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addnewtablesforpas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PhysiciansAssistants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PracticeId = c.Int(),
                        PhysicianId = c.Int(),
                        ProvisionalEmail = c.String(maxLength: 255),
                        UserId = c.String(maxLength: 255),
                        IsProvisional = c.Boolean(nullable: false),
                        ProvisionalKey = c.Guid(),
                        InvitationSent = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Physicians", t => t.PhysicianId)
                .ForeignKey("dbo.Practices", t => t.PracticeId)
                .Index(t => t.PracticeId)
                .Index(t => t.PhysicianId);
            
            CreateTable(
                "dbo.PracticeAdmins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PracticeId = c.Int(),
                        ProvisionalEmail = c.String(maxLength: 255),
                        UserId = c.String(maxLength: 255),
                        IsProvisional = c.Boolean(nullable: false),
                        ProvisionalKey = c.Guid(),
                        InvitationSent = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Practices", t => t.PracticeId)
                .Index(t => t.PracticeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PracticeAdmins", "PracticeId", "dbo.Practices");
            DropForeignKey("dbo.PhysiciansAssistants", "PracticeId", "dbo.Practices");
            DropForeignKey("dbo.PhysiciansAssistants", "PhysicianId", "dbo.Physicians");
            DropIndex("dbo.PracticeAdmins", new[] { "PracticeId" });
            DropIndex("dbo.PhysiciansAssistants", new[] { "PhysicianId" });
            DropIndex("dbo.PhysiciansAssistants", new[] { "PracticeId" });
            DropTable("dbo.PracticeAdmins");
            DropTable("dbo.PhysiciansAssistants");
        }
    }
}
