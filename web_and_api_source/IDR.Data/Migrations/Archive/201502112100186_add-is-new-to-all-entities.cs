namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addisnewtoallentities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.AllowedTypes", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.NodeTypes", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.PropertyTypes", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Facilities", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.DisciplineEntityItems", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Disciplines", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.ImageFiles", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Images", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Mailings", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Manufacturers", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.NodeLibraries", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Nodes", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Properties", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.PropertyValues", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.PatientResponses", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Practices", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Physicians", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.PhysicianFacilityAssignments", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.Procedures", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProcedureTypes", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.RecoveryPlans", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.SupplementalNodeSelections", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.RecoveryPlanTemplates", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.PhysiciansAssistants", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.PracticeAdmins", "IsNew", c => c.Boolean(nullable: false));
            AddColumn("dbo.TreatmentBindables", "IsNew", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TreatmentBindables", "IsNew");
            DropColumn("dbo.PracticeAdmins", "IsNew");
            DropColumn("dbo.PhysiciansAssistants", "IsNew");
            DropColumn("dbo.RecoveryPlanTemplates", "IsNew");
            DropColumn("dbo.SupplementalNodeSelections", "IsNew");
            DropColumn("dbo.RecoveryPlans", "IsNew");
            DropColumn("dbo.ProcedureTypes", "IsNew");
            DropColumn("dbo.Procedures", "IsNew");
            DropColumn("dbo.PhysicianFacilityAssignments", "IsNew");
            DropColumn("dbo.Physicians", "IsNew");
            DropColumn("dbo.Practices", "IsNew");
            DropColumn("dbo.Patients", "IsNew");
            DropColumn("dbo.PatientResponses", "IsNew");
            DropColumn("dbo.PropertyValues", "IsNew");
            DropColumn("dbo.Properties", "IsNew");
            DropColumn("dbo.Nodes", "IsNew");
            DropColumn("dbo.NodeLibraries", "IsNew");
            DropColumn("dbo.Manufacturers", "IsNew");
            DropColumn("dbo.Mailings", "IsNew");
            DropColumn("dbo.Images", "IsNew");
            DropColumn("dbo.ImageFiles", "IsNew");
            DropColumn("dbo.Disciplines", "IsNew");
            DropColumn("dbo.DisciplineEntityItems", "IsNew");
            DropColumn("dbo.Contacts", "IsNew");
            DropColumn("dbo.Facilities", "IsNew");
            DropColumn("dbo.Companies", "IsNew");
            DropColumn("dbo.PropertyTypes", "IsNew");
            DropColumn("dbo.NodeTypes", "IsNew");
            DropColumn("dbo.AllowedTypes", "IsNew");
            DropColumn("dbo.Addresses", "IsNew");
        }
    }
}
