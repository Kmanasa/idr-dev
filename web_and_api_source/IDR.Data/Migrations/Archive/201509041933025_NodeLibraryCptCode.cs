namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NodeLibraryCptCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeLibraries", "CptCodes", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeLibraries", "CptCodes");
        }
    }
}
