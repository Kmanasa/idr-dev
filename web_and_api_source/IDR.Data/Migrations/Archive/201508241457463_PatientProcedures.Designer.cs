// <auto-generated />
namespace IDR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PatientProcedures : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PatientProcedures));
        
        string IMigrationMetadata.Id
        {
            get { return "201508241457463_PatientProcedures"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
