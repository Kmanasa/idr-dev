namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PatientRefactor : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Patients", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Patients", new[] { "CompanyId" });
            AddColumn("dbo.Patients", "Email", c => c.String(maxLength: 255));
            DropColumn("dbo.Patients", "CompanyId");
            DropColumn("dbo.Patients", "ProvisionalEmail");
            DropColumn("dbo.Patients", "UserId");
            DropColumn("dbo.Patients", "IsProvisional");
            DropColumn("dbo.Patients", "ProvisionalKey");
            DropColumn("dbo.Patients", "InvitationSent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "InvitationSent", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "ProvisionalKey", c => c.Guid());
            AddColumn("dbo.Patients", "IsProvisional", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "UserId", c => c.String(maxLength: 255));
            AddColumn("dbo.Patients", "ProvisionalEmail", c => c.String(maxLength: 255));
            AddColumn("dbo.Patients", "CompanyId", c => c.Int());
            DropColumn("dbo.Patients", "Email");
            CreateIndex("dbo.Patients", "CompanyId");
            AddForeignKey("dbo.Patients", "CompanyId", "dbo.Companies", "Id");
        }
    }
}
