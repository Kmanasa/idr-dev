namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NodeTypeTemplateId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeTypes", "TemplateId", c => c.Int(nullable: false));

            
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeTypes", "TemplateId");
        }
    }
}
