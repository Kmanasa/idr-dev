namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImageAlias : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImageFiles", "Alias", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ImageFiles", "Alias");
        }
    }
}
