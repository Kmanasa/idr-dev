namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class variouschangestosupportpracticeusers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Patients", "PracticeId", "dbo.Practices");
            DropIndex("dbo.Patients", new[] { "PracticeId" });
            AlterColumn("dbo.Patients", "PracticeId", c => c.Int());
            CreateIndex("dbo.Patients", "PracticeId");
            AddForeignKey("dbo.Patients", "PracticeId", "dbo.Practices", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patients", "PracticeId", "dbo.Practices");
            DropIndex("dbo.Patients", new[] { "PracticeId" });
            AlterColumn("dbo.Patients", "PracticeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Patients", "PracticeId");
            AddForeignKey("dbo.Patients", "PracticeId", "dbo.Practices", "Id", cascadeDelete: true);
        }
    }
}
