namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class uptoandincludingimages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Address1 = c.String(maxLength: 255),
                        Address2 = c.String(maxLength: 255),
                        City = c.String(maxLength: 255),
                        State = c.String(maxLength: 128),
                        PostalCode = c.String(maxLength: 15),
                        Country = c.String(maxLength: 255),
                        EntityId = c.Int(nullable: false),
                        EntityName = c.String(maxLength: 255),
                        AddressType = c.String(maxLength: 255),
                        AddressFormType = c.String(maxLength: 255),
                        AddressTitle = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.EntityName, t.AddressType, t.EntityId }, name: "IX_AddressEntity");
            
            CreateTable(
                "dbo.AllowedTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NodeTypeId = c.Int(nullable: false),
                        AllowedTypeId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeTypes", t => t.NodeTypeId, cascadeDelete: true)
                .Index(t => t.NodeTypeId);
            
            CreateTable(
                "dbo.NodeTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        UIKey = c.String(maxLength: 255),
                        Description = c.String(maxLength: 1000),
                        IsTop = c.Boolean(nullable: false),
                        AllowPhysicianEdit = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PropertyTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 1000),
                        UIKey = c.String(maxLength: 1000),
                        IsRequired = c.Boolean(nullable: false),
                        IsCompound = c.Boolean(nullable: false),
                        DomainName = c.String(maxLength: 1000),
                        General = c.String(maxLength: 1000),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        NodeType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeTypes", t => t.NodeType_Id)
                .Index(t => t.NodeType_Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        CategoryId = c.Int(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Facilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        CompanyId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        ServiceName = c.String(maxLength: 255),
                        Email = c.String(maxLength: 255),
                        OfficePhone = c.String(maxLength: 255),
                        OfficeExt = c.String(maxLength: 255),
                        CellPhone = c.String(maxLength: 255),
                        PersonalPhone = c.String(maxLength: 255),
                        EntityId = c.Int(nullable: false),
                        EntityName = c.String(maxLength: 255),
                        ContactType = c.String(maxLength: 255),
                        ContactFormType = c.String(maxLength: 255),
                        ContactTitle = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.EntityName, t.ContactType, t.EntityId }, name: "IX_AddressEntity");
            
            CreateTable(
                "dbo.DisciplineEntityItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisciplineId = c.Int(nullable: false),
                        EntityId = c.Int(nullable: false),
                        EntityName = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Disciplines", t => t.DisciplineId, cascadeDelete: true)
                .Index(t => t.DisciplineId);
            
            CreateTable(
                "dbo.Disciplines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ImageFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UniqueId = c.Guid(nullable: false),
                        FileName = c.String(maxLength: 255),
                        Image = c.Binary(),
                        EntityName = c.String(maxLength: 255),
                        EntityId = c.Int(nullable: false),
                        ImageType = c.String(maxLength: 255),
                        ImageFormType = c.String(maxLength: 255),
                        ImageTitle = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ImageType, t.EntityId, t.EntityName }, name: "IX_ImageFileEntity");
            
            CreateTable(
                "dbo.ImageLibraries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        Url = c.String(maxLength: 255),
                        RepositoryId = c.String(maxLength: 255),
                        IsEnlargeable = c.Boolean(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        ImageLibrary_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ImageLibraries", t => t.ImageLibrary_Id)
                .Index(t => t.ImageLibrary_Id);
            
            CreateTable(
                "dbo.NodeLibraries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 4000),
                        CssClass = c.String(maxLength: 255),
                        Key = c.String(maxLength: 255),
                        BuildUrl = c.String(maxLength: 1000),
                        IsSystem = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Nodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NodeLibraryId = c.Int(),
                        Name = c.String(maxLength: 255),
                        TypeId = c.Int(nullable: false),
                        Title = c.String(maxLength: 255),
                        Description = c.String(),
                        NodeId = c.Int(),
                        ListIconCss = c.String(maxLength: 255),
                        IsSupplemented = c.Boolean(nullable: false),
                        SupplementMultiple = c.Boolean(nullable: false),
                        SupplementalLibraryId = c.Int(),
                        IsPhysicianEditable = c.Boolean(nullable: false),
                        OverrideSourceId = c.Int(),
                        RecoveryPlanId = c.Int(),
                        Sequence = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId)
                .ForeignKey("dbo.NodeTypes", t => t.TypeId, cascadeDelete: true)
                .ForeignKey("dbo.NodeLibraries", t => t.NodeLibraryId)
                .Index(t => t.NodeLibraryId)
                .Index(t => t.TypeId)
                .Index(t => t.NodeId);
            
            CreateTable(
                "dbo.Properties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        CssClass = c.String(maxLength: 255),
                        Title = c.String(maxLength: 255),
                        Value = c.String(maxLength: 4000),
                        NodeId = c.Int(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                        Key = c.Guid(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.NodeId, cascadeDelete: true)
                .ForeignKey("dbo.PropertyTypes", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.NodeId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.PropertyValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderBy = c.Int(nullable: false),
                        PartKey = c.String(maxLength: 255),
                        PropertyId = c.Int(nullable: false),
                        Value = c.String(maxLength: 4000),
                        ListStyleType = c.String(maxLength: 255),
                        ListStyleUrl = c.String(maxLength: 255),
                        Sequence = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Properties", t => t.PropertyId, cascadeDelete: true)
                .Index(t => t.PropertyId);
            
            CreateTable(
                "dbo.PatientResponses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MRN = c.String(maxLength: 255),
                        FirstName = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        MiddleInitial = c.String(maxLength: 1),
                        DischargeProtocol = c.String(maxLength: 255),
                        Timeframe = c.Int(),
                        DischargeDate = c.DateTime(),
                        DaysPost = c.Int(),
                        LastResponseDate = c.DateTime(),
                        ProtocolEndDate = c.DateTime(),
                        Satisfaction = c.Int(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        Physician_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Physicians", t => t.Physician_Id)
                .Index(t => t.Physician_Id);
            
            CreateTable(
                "dbo.Physicians",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        Title = c.String(maxLength: 255),
                        ImageRef = c.String(maxLength: 255),
                        UserId = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecoveryPlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        RecoveryPlanTemplateId = c.Int(nullable: false),
                        SystemOverrideLibraryId = c.Int(nullable: false),
                        UserId = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RecoveryPlanTemplates", t => t.RecoveryPlanTemplateId)
                .ForeignKey("dbo.NodeLibraries", t => t.SystemOverrideLibraryId)
                .Index(t => t.RecoveryPlanTemplateId)
                .Index(t => t.SystemOverrideLibraryId);
            
            CreateTable(
                "dbo.SupplementalNodeSelections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecoveryPlanId = c.Int(nullable: false),
                        SourceNodeId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RecoveryPlans", t => t.RecoveryPlanId, cascadeDelete: true)
                .ForeignKey("dbo.Nodes", t => t.SourceNodeId, cascadeDelete: true)
                .Index(t => t.RecoveryPlanId)
                .Index(t => t.SourceNodeId);
            
            CreateTable(
                "dbo.RecoveryPlanTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        NodeLibraryId = c.Int(nullable: false),
                        Description = c.String(maxLength: 4000),
                        Active = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeLibraries", t => t.NodeLibraryId, cascadeDelete: true)
                .Index(t => t.NodeLibraryId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Snippets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Type = c.String(maxLength: 255),
                        Value = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Firstname = c.String(maxLength: 255),
                        Lastname = c.String(maxLength: 255),
                        Active = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RecoveryPlans", "SystemOverrideLibraryId", "dbo.NodeLibraries");
            DropForeignKey("dbo.RecoveryPlans", "RecoveryPlanTemplateId", "dbo.RecoveryPlanTemplates");
            DropForeignKey("dbo.RecoveryPlanTemplates", "NodeLibraryId", "dbo.NodeLibraries");
            DropForeignKey("dbo.SupplementalNodeSelections", "SourceNodeId", "dbo.Nodes");
            DropForeignKey("dbo.SupplementalNodeSelections", "RecoveryPlanId", "dbo.RecoveryPlans");
            DropForeignKey("dbo.Patients", "Physician_Id", "dbo.Physicians");
            DropForeignKey("dbo.PatientResponses", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.Nodes", "NodeLibraryId", "dbo.NodeLibraries");
            DropForeignKey("dbo.Nodes", "TypeId", "dbo.NodeTypes");
            DropForeignKey("dbo.PropertyValues", "PropertyId", "dbo.Properties");
            DropForeignKey("dbo.Properties", "TypeId", "dbo.PropertyTypes");
            DropForeignKey("dbo.Properties", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.Nodes", "NodeId", "dbo.Nodes");
            DropForeignKey("dbo.Images", "ImageLibrary_Id", "dbo.ImageLibraries");
            DropForeignKey("dbo.DisciplineEntityItems", "DisciplineId", "dbo.Disciplines");
            DropForeignKey("dbo.Facilities", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PropertyTypes", "NodeType_Id", "dbo.NodeTypes");
            DropForeignKey("dbo.AllowedTypes", "NodeTypeId", "dbo.NodeTypes");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RecoveryPlanTemplates", new[] { "NodeLibraryId" });
            DropIndex("dbo.SupplementalNodeSelections", new[] { "SourceNodeId" });
            DropIndex("dbo.SupplementalNodeSelections", new[] { "RecoveryPlanId" });
            DropIndex("dbo.RecoveryPlans", new[] { "SystemOverrideLibraryId" });
            DropIndex("dbo.RecoveryPlans", new[] { "RecoveryPlanTemplateId" });
            DropIndex("dbo.Patients", new[] { "Physician_Id" });
            DropIndex("dbo.PatientResponses", new[] { "Patient_Id" });
            DropIndex("dbo.PropertyValues", new[] { "PropertyId" });
            DropIndex("dbo.Properties", new[] { "TypeId" });
            DropIndex("dbo.Properties", new[] { "NodeId" });
            DropIndex("dbo.Nodes", new[] { "NodeId" });
            DropIndex("dbo.Nodes", new[] { "TypeId" });
            DropIndex("dbo.Nodes", new[] { "NodeLibraryId" });
            DropIndex("dbo.Images", new[] { "ImageLibrary_Id" });
            DropIndex("dbo.ImageFiles", "IX_ImageFileEntity");
            DropIndex("dbo.DisciplineEntityItems", new[] { "DisciplineId" });
            DropIndex("dbo.Contacts", "IX_AddressEntity");
            DropIndex("dbo.Facilities", new[] { "CompanyId" });
            DropIndex("dbo.PropertyTypes", new[] { "NodeType_Id" });
            DropIndex("dbo.AllowedTypes", new[] { "NodeTypeId" });
            DropIndex("dbo.Addresses", "IX_AddressEntity");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Snippets");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RecoveryPlanTemplates");
            DropTable("dbo.SupplementalNodeSelections");
            DropTable("dbo.RecoveryPlans");
            DropTable("dbo.Physicians");
            DropTable("dbo.Patients");
            DropTable("dbo.PatientResponses");
            DropTable("dbo.PropertyValues");
            DropTable("dbo.Properties");
            DropTable("dbo.Nodes");
            DropTable("dbo.NodeLibraries");
            DropTable("dbo.Images");
            DropTable("dbo.ImageLibraries");
            DropTable("dbo.ImageFiles");
            DropTable("dbo.Disciplines");
            DropTable("dbo.DisciplineEntityItems");
            DropTable("dbo.Contacts");
            DropTable("dbo.Facilities");
            DropTable("dbo.Companies");
            DropTable("dbo.Categories");
            DropTable("dbo.PropertyTypes");
            DropTable("dbo.NodeTypes");
            DropTable("dbo.AllowedTypes");
            DropTable("dbo.Addresses");
        }
    }
}
