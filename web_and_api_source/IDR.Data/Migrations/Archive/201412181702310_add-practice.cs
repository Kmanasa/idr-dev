namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addpractice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Practices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HoursOfOperation = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Physicians", "PracticeId", c => c.Int());
            CreateIndex("dbo.Physicians", "PracticeId");
            AddForeignKey("dbo.Physicians", "PracticeId", "dbo.Practices", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Physicians", "PracticeId", "dbo.Practices");
            DropIndex("dbo.Physicians", new[] { "PracticeId" });
            DropColumn("dbo.Physicians", "PracticeId");
            DropTable("dbo.Practices");
        }
    }
}
