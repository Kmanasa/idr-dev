namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Notifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NodeLibraryId = c.Int(nullable: false),
                        Title = c.String(maxLength: 150),
                        Message = c.String(maxLength: 150),
                        DaysOffset = c.Int(nullable: false),
                        HoursOffset = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeLibraries", t => t.NodeLibraryId, cascadeDelete: true)
                .Index(t => t.NodeLibraryId);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureId = c.Int(nullable: false),
                        DeviceId = c.String(maxLength: 255),
                        DeviceType = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId);
            
            CreateTable(
                "dbo.ProcedureInboxItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureId = c.Int(nullable: false),
                        Read = c.Boolean(nullable: false),
                        Subject = c.String(maxLength: 100),
                        Message = c.String(maxLength: 1000),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId);
            
            CreateTable(
                "dbo.SentNotifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                        Procedure_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .ForeignKey("dbo.Procedures", t => t.Procedure_Id)
                .Index(t => t.NotificationId)
                .Index(t => t.Procedure_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SentNotifications", "Procedure_Id", "dbo.Procedures");
            DropForeignKey("dbo.SentNotifications", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.ProcedureInboxItems", "ProcedureId", "dbo.Procedures");
            DropForeignKey("dbo.Devices", "ProcedureId", "dbo.Procedures");
            DropForeignKey("dbo.Notifications", "NodeLibraryId", "dbo.NodeLibraries");
            DropIndex("dbo.SentNotifications", new[] { "Procedure_Id" });
            DropIndex("dbo.SentNotifications", new[] { "NotificationId" });
            DropIndex("dbo.ProcedureInboxItems", new[] { "ProcedureId" });
            DropIndex("dbo.Devices", new[] { "ProcedureId" });
            DropIndex("dbo.Notifications", new[] { "NodeLibraryId" });
            DropTable("dbo.SentNotifications");
            DropTable("dbo.ProcedureInboxItems");
            DropTable("dbo.Devices");
            DropTable("dbo.Notifications");
        }
    }
}
