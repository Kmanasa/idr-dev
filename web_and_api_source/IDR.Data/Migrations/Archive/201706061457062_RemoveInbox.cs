namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveInbox : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProcedureInboxItems", "ProcedureId", "dbo.Procedures");
            DropIndex("dbo.ProcedureInboxItems", new[] { "ProcedureId" });
            AddColumn("dbo.ProcedureNotifications", "Read", c => c.Boolean(nullable: false));
            DropTable("dbo.ProcedureInboxItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProcedureInboxItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureId = c.Int(nullable: false),
                        Read = c.Boolean(nullable: false),
                        Subject = c.String(maxLength: 100),
                        Message = c.String(maxLength: 1000),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.ProcedureNotifications", "Read");
            CreateIndex("dbo.ProcedureInboxItems", "ProcedureId");
            AddForeignKey("dbo.ProcedureInboxItems", "ProcedureId", "dbo.Procedures", "Id", cascadeDelete: true);
        }
    }
}
