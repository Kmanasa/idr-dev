namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePropertyValues : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PropertyValues", "PropertyId", "dbo.Properties");
            DropIndex("dbo.PropertyValues", new[] { "PropertyId" });
            DropTable("dbo.PropertyValues");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PropertyValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderBy = c.Int(nullable: false),
                        PartKey = c.String(maxLength: 255),
                        PropertyId = c.Int(nullable: false),
                        Value = c.String(),
                        ListStyleType = c.String(maxLength: 255),
                        ListStyleUrl = c.String(maxLength: 255),
                        Sequence = c.Int(nullable: false),
                        Title = c.String(maxLength: 1000),
                        ColumnModelKey = c.String(maxLength: 20),
                        IsBoundValue = c.Boolean(nullable: false),
                        BindPath = c.String(maxLength: 255),
                        IsFormatted = c.Boolean(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.PropertyValues", "PropertyId");
            AddForeignKey("dbo.PropertyValues", "PropertyId", "dbo.Properties", "Id", cascadeDelete: true);
        }
    }
}
