namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class removingincorrectdatafrompatient : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PatientResponses", "Patient_Id", "dbo.Patients");
            DropIndex("dbo.PatientResponses", new[] { "Patient_Id" });
            AddColumn("dbo.Patients", "DateOfBirth", c => c.DateTime());
            DropColumn("dbo.PatientResponses", "Patient_Id");
            DropColumn("dbo.Patients", "DischargeProtocol");
            DropColumn("dbo.Patients", "Timeframe");
            DropColumn("dbo.Patients", "DischargeDate");
            DropColumn("dbo.Patients", "DaysPost");
            DropColumn("dbo.Patients", "LastResponseDate");
            DropColumn("dbo.Patients", "ProtocolEndDate");
            DropColumn("dbo.Patients", "Satisfaction");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "Satisfaction", c => c.Int());
            AddColumn("dbo.Patients", "ProtocolEndDate", c => c.DateTime());
            AddColumn("dbo.Patients", "LastResponseDate", c => c.DateTime());
            AddColumn("dbo.Patients", "DaysPost", c => c.Int());
            AddColumn("dbo.Patients", "DischargeDate", c => c.DateTime());
            AddColumn("dbo.Patients", "Timeframe", c => c.Int());
            AddColumn("dbo.Patients", "DischargeProtocol", c => c.String(maxLength: 255));
            AddColumn("dbo.PatientResponses", "Patient_Id", c => c.Int());
            DropColumn("dbo.Patients", "DateOfBirth");
            CreateIndex("dbo.PatientResponses", "Patient_Id");
            AddForeignKey("dbo.PatientResponses", "Patient_Id", "dbo.Patients", "Id");
        }
    }
}
