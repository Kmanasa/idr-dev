namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class removinguniqueidfromimagefile : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ImageFiles", "UniqueId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ImageFiles", "UniqueId", c => c.Guid(nullable: false));
        }
    }
}
