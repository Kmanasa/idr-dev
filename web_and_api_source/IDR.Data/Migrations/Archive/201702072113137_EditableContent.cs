namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditableContent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EditableContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Sequence = c.Int(nullable: false),
                        Content = c.String(),
                        InMobileMenu = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EditableContent");
        }
    }
}
