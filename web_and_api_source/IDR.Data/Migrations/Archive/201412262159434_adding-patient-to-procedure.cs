namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingpatienttoprocedure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Procedures", "Patient_Id", "dbo.Patients");
            DropIndex("dbo.Procedures", new[] { "Patient_Id" });
            RenameColumn(table: "dbo.Procedures", name: "Patient_Id", newName: "PatientId");
            AlterColumn("dbo.Procedures", "PatientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Procedures", "PatientId");
            AddForeignKey("dbo.Procedures", "PatientId", "dbo.Patients", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedures", "PatientId", "dbo.Patients");
            DropIndex("dbo.Procedures", new[] { "PatientId" });
            AlterColumn("dbo.Procedures", "PatientId", c => c.Int());
            RenameColumn(table: "dbo.Procedures", name: "PatientId", newName: "Patient_Id");
            CreateIndex("dbo.Procedures", "Patient_Id");
            AddForeignKey("dbo.Procedures", "Patient_Id", "dbo.Patients", "Id");
        }
    }
}
