namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingeventforparticularmedicaleventphysicianrelationship : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhysicianId = c.Int(nullable: false),
                        DischargeProtocol = c.String(maxLength: 255),
                        Timeframe = c.Int(),
                        DischargeDate = c.DateTime(),
                        DaysPost = c.Int(),
                        LastResponseDate = c.DateTime(),
                        ProtocolEndDate = c.DateTime(),
                        Satisfaction = c.Int(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .ForeignKey("dbo.Physicians", t => t.PhysicianId, cascadeDelete: true)
                .Index(t => t.PhysicianId)
                .Index(t => t.Patient_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "PhysicianId", "dbo.Physicians");
            DropForeignKey("dbo.Events", "Patient_Id", "dbo.Patients");
            DropIndex("dbo.Events", new[] { "Patient_Id" });
            DropIndex("dbo.Events", new[] { "PhysicianId" });
            DropTable("dbo.Events");
        }
    }
}
