namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Costs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "CostPerUser", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Companies", "CostPerPhysician", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Companies", "CostPerFacility", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "CostPerFacility");
            DropColumn("dbo.Companies", "CostPerPhysician");
            DropColumn("dbo.Companies", "CostPerUser");
        }
    }
}
