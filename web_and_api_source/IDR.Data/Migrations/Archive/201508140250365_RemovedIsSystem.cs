namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedIsSystem : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.NodeLibraries", "IsSystem");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NodeLibraries", "IsSystem", c => c.Boolean(nullable: false));
        }
    }
}
