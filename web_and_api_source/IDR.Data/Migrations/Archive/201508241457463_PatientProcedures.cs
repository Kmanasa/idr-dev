namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PatientProcedures : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.PatientResponses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PatientResponses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
