namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveReadFlag : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProcedureNotifications", "Read");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProcedureNotifications", "Read", c => c.Boolean(nullable: false));
        }
    }
}
