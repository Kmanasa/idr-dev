namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingmailing : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Mailings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 255),
                        EntityId = c.Int(nullable: false),
                        EntityName = c.String(maxLength: 255),
                        RequestName = c.String(maxLength: 255),
                        UserId = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.EntityName, t.RequestName, t.EntityId }, name: "IX_MailingEntity");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Mailings", "IX_MailingEntity");
            DropTable("dbo.Mailings");
        }
    }
}
