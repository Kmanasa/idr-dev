// <auto-generated />
namespace IDR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class MailersRemoved : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MailersRemoved));
        
        string IMigrationMetadata.Id
        {
            get { return "201702101711322_MailersRemoved"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
