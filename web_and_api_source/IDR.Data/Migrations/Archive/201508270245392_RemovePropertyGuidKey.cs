namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePropertyGuidKey : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Properties", "Key");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Properties", "Key", c => c.Guid(nullable: false));
        }
    }
}
