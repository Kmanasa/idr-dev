namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingmissingnavproptopropertytype : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PropertyTypes", "NodeType_Id", "dbo.NodeTypes");
            DropIndex("dbo.PropertyTypes", new[] { "NodeType_Id" });
            RenameColumn(table: "dbo.PropertyTypes", name: "NodeType_Id", newName: "NodeTypeId");
            AlterColumn("dbo.PropertyTypes", "NodeTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.PropertyTypes", "NodeTypeId");
            AddForeignKey("dbo.PropertyTypes", "NodeTypeId", "dbo.NodeTypes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PropertyTypes", "NodeTypeId", "dbo.NodeTypes");
            DropIndex("dbo.PropertyTypes", new[] { "NodeTypeId" });
            AlterColumn("dbo.PropertyTypes", "NodeTypeId", c => c.Int());
            RenameColumn(table: "dbo.PropertyTypes", name: "NodeTypeId", newName: "NodeType_Id");
            CreateIndex("dbo.PropertyTypes", "NodeType_Id");
            AddForeignKey("dbo.PropertyTypes", "NodeType_Id", "dbo.NodeTypes", "Id");
        }
    }
}
