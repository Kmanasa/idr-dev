namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addrecoveryplantoprocedure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "RecoveryPlanId", c => c.Int());
            CreateIndex("dbo.Procedures", "RecoveryPlanId");
            AddForeignKey("dbo.Procedures", "RecoveryPlanId", "dbo.RecoveryPlans", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedures", "RecoveryPlanId", "dbo.RecoveryPlans");
            DropIndex("dbo.Procedures", new[] { "RecoveryPlanId" });
            DropColumn("dbo.Procedures", "RecoveryPlanId");
        }
    }
}
