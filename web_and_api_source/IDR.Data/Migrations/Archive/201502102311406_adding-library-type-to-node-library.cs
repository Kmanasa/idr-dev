namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addinglibrarytypetonodelibrary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeLibraries", "LibraryType", c => c.String(maxLength: 255));

            this.Sql(sql: @"
				update nodelibraries 
				set librarytype = '" + Constants.NodeLibraryTypes.MasterTemplate + @"' 
				where name like '%Framework%'
				");
//				update nodelibraries 
//				set librarytype = '" + Constants.NodeLibraryTypes.SystemOverrideTemplate + @"' 
//				where name like 'SYS%'"


//				update nodelibraries 
//				set librarytype = '" + Constants.NodeLibraryTypes.ManufacturerTemplate + @"' 
//				where librarytype is null or librarytype = ''");
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeLibraries", "LibraryType");
        }
    }
}
