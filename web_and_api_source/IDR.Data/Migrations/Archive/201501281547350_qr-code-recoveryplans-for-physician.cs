namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class qrcoderecoveryplansforphysician : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "CodeUrl", c => c.String(maxLength: 1000));
            AddColumn("dbo.Procedures", "ShortUrl", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "ApiKey", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "HasQrCode", c => c.Boolean(nullable: false));
            AddColumn("dbo.Procedures", "FullUrl", c => c.String(maxLength: 1000));
            AddColumn("dbo.RecoveryPlans", "PhysicianId", c => c.Int());
            CreateIndex("dbo.RecoveryPlans", "PhysicianId");
            AddForeignKey("dbo.RecoveryPlans", "PhysicianId", "dbo.Physicians", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecoveryPlans", "PhysicianId", "dbo.Physicians");
            DropIndex("dbo.RecoveryPlans", new[] { "PhysicianId" });
            DropColumn("dbo.RecoveryPlans", "PhysicianId");
            DropColumn("dbo.Procedures", "FullUrl");
            DropColumn("dbo.Procedures", "HasQrCode");
            DropColumn("dbo.Procedures", "ApiKey");
            DropColumn("dbo.Procedures", "ShortUrl");
            DropColumn("dbo.Procedures", "CodeUrl");
        }
    }
}
