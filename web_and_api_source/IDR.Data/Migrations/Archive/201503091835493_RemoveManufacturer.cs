namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RemoveManufacturer : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Nodes", "ManufacturerId", "dbo.Manufacturers");
            DropIndex("dbo.Nodes", new[] { "ManufacturerId" });
            DropColumn("dbo.Nodes", "ManufacturerId");
            DropTable("dbo.Manufacturers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        IsPreferredContentProvider = c.Boolean(nullable: false),
                        WebsiteUrl = c.String(maxLength: 1000),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Nodes", "ManufacturerId", c => c.Int());
            CreateIndex("dbo.Nodes", "ManufacturerId");
            AddForeignKey("dbo.Nodes", "ManufacturerId", "dbo.Manufacturers", "Id");
        }
    }
}
