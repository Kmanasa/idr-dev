namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhysicainIdForNodeLibrary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeLibraries", "PhysicianId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeLibraries", "PhysicianId");
        }
    }
}
