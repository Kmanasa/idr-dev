// <auto-generated />
namespace IDR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class SyncFieldSizes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SyncFieldSizes));
        
        string IMigrationMetadata.Id
        {
            get { return "201507242330489_SyncFieldSizes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
