namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationTimeToSend : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "TimeToSend", c => c.DateTime(nullable: false));
            DropColumn("dbo.Notifications", "HoursOffset");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "HoursOffset", c => c.Int(nullable: false));
            DropColumn("dbo.Notifications", "TimeToSend");
        }
    }
}
