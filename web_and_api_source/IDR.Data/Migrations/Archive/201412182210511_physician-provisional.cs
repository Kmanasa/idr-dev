namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class physicianprovisional : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Physicians", "IsProvisional", c => c.Boolean(nullable: false));
            AddColumn("dbo.Physicians", "ProvisionalKey", c => c.Guid());
            AlterColumn("dbo.Physicians", "UserId", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Physicians", "UserId", c => c.String());
            DropColumn("dbo.Physicians", "ProvisionalKey");
            DropColumn("dbo.Physicians", "IsProvisional");
        }
    }
}
