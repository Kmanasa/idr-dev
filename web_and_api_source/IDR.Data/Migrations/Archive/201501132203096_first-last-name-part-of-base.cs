namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class firstlastnamepartofbase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhysiciansAssistants", "FirstName", c => c.String(maxLength: 255));
            AddColumn("dbo.PhysiciansAssistants", "LastName", c => c.String(maxLength: 255));
            AddColumn("dbo.PracticeAdmins", "FirstName", c => c.String(maxLength: 255));
            AddColumn("dbo.PracticeAdmins", "LastName", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PracticeAdmins", "LastName");
            DropColumn("dbo.PracticeAdmins", "FirstName");
            DropColumn("dbo.PhysiciansAssistants", "LastName");
            DropColumn("dbo.PhysiciansAssistants", "FirstName");
        }
    }
}
