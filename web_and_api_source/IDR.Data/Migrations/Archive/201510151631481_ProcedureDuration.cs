namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureDuration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "DurationInDays", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Procedures", "DurationInDays");
        }
    }
}
