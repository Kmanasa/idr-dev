namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingagetopatient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "Age", c => c.Int(nullable: false));
            DropColumn("dbo.Patients", "DateOfBirth");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "DateOfBirth", c => c.DateTime());
            DropColumn("dbo.Patients", "Age");
        }
    }
}
