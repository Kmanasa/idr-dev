namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveProcedureDays : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Procedures", "DurationInDays");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Procedures", "DurationInDays", c => c.Int(nullable: false));
        }
    }
}
