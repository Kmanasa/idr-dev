namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnmodelkeytopropvalue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValues", "ColumnModelKey", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValues", "ColumnModelKey");
        }
    }
}
