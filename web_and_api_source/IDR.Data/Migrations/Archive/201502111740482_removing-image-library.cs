namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class removingimagelibrary : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Images", "ImageLibrary_Id", "dbo.ImageLibraries");
            DropIndex("dbo.Images", new[] { "ImageLibrary_Id" });
            DropColumn("dbo.Images", "ImageLibrary_Id");
            DropTable("dbo.ImageLibraries");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ImageLibraries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Images", "ImageLibrary_Id", c => c.Int());
            CreateIndex("dbo.Images", "ImageLibrary_Id");
            AddForeignKey("dbo.Images", "ImageLibrary_Id", "dbo.ImageLibraries", "Id");
        }
    }
}
