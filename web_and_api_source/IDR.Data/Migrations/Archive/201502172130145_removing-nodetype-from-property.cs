namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class removingnodetypefromproperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PropertyTypes", "NodeTypeId", "dbo.NodeTypes");
            DropIndex("dbo.PropertyTypes", new[] { "NodeTypeId" });
            RenameColumn(table: "dbo.PropertyTypes", name: "NodeTypeId", newName: "NodeType_Id");
            AlterColumn("dbo.PropertyTypes", "NodeType_Id", c => c.Int());
            CreateIndex("dbo.PropertyTypes", "NodeType_Id");
            AddForeignKey("dbo.PropertyTypes", "NodeType_Id", "dbo.NodeTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PropertyTypes", "NodeType_Id", "dbo.NodeTypes");
            DropIndex("dbo.PropertyTypes", new[] { "NodeType_Id" });
            AlterColumn("dbo.PropertyTypes", "NodeType_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.PropertyTypes", name: "NodeType_Id", newName: "NodeTypeId");
            CreateIndex("dbo.PropertyTypes", "NodeTypeId");
            AddForeignKey("dbo.PropertyTypes", "NodeTypeId", "dbo.NodeTypes", "Id", cascadeDelete: true);
        }
    }
}
