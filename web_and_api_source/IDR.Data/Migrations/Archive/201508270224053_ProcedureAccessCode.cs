namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureAccessCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "CompanyId", c => c.Int());
            AddColumn("dbo.Patients", "Phone", c => c.String(maxLength: 255));
            AddColumn("dbo.Procedures", "CptCode", c => c.String(maxLength: 50));
            AddColumn("dbo.Procedures", "AccessCode", c => c.String(maxLength: 50));
            CreateIndex("dbo.Patients", "CompanyId");
            AddForeignKey("dbo.Patients", "CompanyId", "dbo.Companies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patients", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Patients", new[] { "CompanyId" });
            DropColumn("dbo.Procedures", "AccessCode");
            DropColumn("dbo.Procedures", "CptCode");
            DropColumn("dbo.Patients", "Phone");
            DropColumn("dbo.Patients", "CompanyId");
        }
    }
}
