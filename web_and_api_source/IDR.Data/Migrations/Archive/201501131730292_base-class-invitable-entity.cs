namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class baseclassinvitableentity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "ProvisionalEmail", c => c.String(maxLength: 255));
            AddColumn("dbo.Patients", "UserId", c => c.String(maxLength: 255));
            AddColumn("dbo.Patients", "IsProvisional", c => c.Boolean(nullable: false));
            AddColumn("dbo.Patients", "ProvisionalKey", c => c.Guid());
            AddColumn("dbo.Patients", "InvitationSent", c => c.Boolean(nullable: false));
            AddColumn("dbo.Physicians", "InvitationSent", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Physicians", "InvitationSent");
            DropColumn("dbo.Patients", "InvitationSent");
            DropColumn("dbo.Patients", "ProvisionalKey");
            DropColumn("dbo.Patients", "IsProvisional");
            DropColumn("dbo.Patients", "UserId");
            DropColumn("dbo.Patients", "ProvisionalEmail");
        }
    }
}
