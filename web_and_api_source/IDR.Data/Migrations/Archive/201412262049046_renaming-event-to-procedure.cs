namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class renamingeventtoprocedure : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Events", newName: "Procedures");
            DropColumn("dbo.Procedures", "LastResponseDate");
//			DropColumn("dbo.Patients", "Physician_ID");

        }
        
        public override void Down()
        {
//			AddColumn("dbo.Patients", "Physician_ID", c => c.Int(nullable: true));
			AddColumn("dbo.Procedures", "LastResponseDate", c => c.DateTime());
            RenameTable(name: "dbo.Procedures", newName: "Events");
        }
    }
}
