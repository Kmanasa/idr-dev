namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingpracticetopatient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "PracticeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Patients", "PracticeId");
            AddForeignKey("dbo.Patients", "PracticeId", "dbo.Practices", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patients", "PracticeId", "dbo.Practices");
            DropIndex("dbo.Patients", new[] { "PracticeId" });
            DropColumn("dbo.Patients", "PracticeId");
        }
    }
}
