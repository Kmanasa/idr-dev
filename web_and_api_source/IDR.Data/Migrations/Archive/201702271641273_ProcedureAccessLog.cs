namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureAccessLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProcedureAccessLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureId = c.Int(nullable: false),
                        DeviceId = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProcedureAccessLogs", "ProcedureId", "dbo.Procedures");
            DropIndex("dbo.ProcedureAccessLogs", new[] { "ProcedureId" });
            DropTable("dbo.ProcedureAccessLogs");
        }
    }
}
