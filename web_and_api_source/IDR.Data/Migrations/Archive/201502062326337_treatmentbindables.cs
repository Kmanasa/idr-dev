namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class treatmentbindables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TreatmentBindables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PathName = c.String(maxLength: 255),
                        Category = c.String(maxLength: 255),
                        IsImage = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Categories");
            DropTable("dbo.Snippets");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Snippets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Type = c.String(maxLength: 255),
                        Value = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(maxLength: 255),
                        CategoryId = c.Int(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.TreatmentBindables");
        }
    }
}
