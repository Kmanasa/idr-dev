namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SendEmailSmsFlags : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "SendEmail", c => c.Boolean(nullable: false));
            AddColumn("dbo.Procedures", "SendSms", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Procedures", "SendSms");
            DropColumn("dbo.Procedures", "SendEmail");
        }
    }
}
