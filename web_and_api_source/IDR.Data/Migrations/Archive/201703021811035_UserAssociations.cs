namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAssociations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Physicians", "User_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.PhysiciansAssistants", "User_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.PracticeAdmins", "User_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Physicians", "User_Id");
            CreateIndex("dbo.PhysiciansAssistants", "User_Id");
            CreateIndex("dbo.PracticeAdmins", "User_Id");
            AddForeignKey("dbo.PhysiciansAssistants", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.PracticeAdmins", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Physicians", "User_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Physicians", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PracticeAdmins", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PhysiciansAssistants", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.PracticeAdmins", new[] { "User_Id" });
            DropIndex("dbo.PhysiciansAssistants", new[] { "User_Id" });
            DropIndex("dbo.Physicians", new[] { "User_Id" });
            DropColumn("dbo.PracticeAdmins", "User_Id");
            DropColumn("dbo.PhysiciansAssistants", "User_Id");
            DropColumn("dbo.Physicians", "User_Id");
        }
    }
}
