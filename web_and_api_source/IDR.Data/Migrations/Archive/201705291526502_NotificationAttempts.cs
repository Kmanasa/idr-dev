namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationAttempts : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProcedureNotificationResponses", "ProcedureNotificationOptionId", "dbo.ProcedureNotificationOptions");
            DropForeignKey("dbo.ProcedureNotificationResponses", "ProcedureNotificationId", "dbo.ProcedureNotifications");
            DropIndex("dbo.ProcedureNotificationResponses", new[] { "ProcedureNotificationId" });
            DropIndex("dbo.ProcedureNotificationResponses", new[] { "ProcedureNotificationOptionId" });
            AddColumn("dbo.ProcedureNotifications", "NextAttemptTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.ProcedureNotifications", "Attempts", c => c.Int(nullable: false));
            AddColumn("dbo.ProcedureNotifications", "Response", c => c.String());
            AddColumn("dbo.ProcedureNotifications", "DateResponded", c => c.String());
            DropTable("dbo.ProcedureNotificationResponses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProcedureNotificationResponses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureNotificationId = c.Int(nullable: false),
                        ProcedureNotificationOptionId = c.Int(nullable: false),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.ProcedureNotifications", "DateResponded");
            DropColumn("dbo.ProcedureNotifications", "Response");
            DropColumn("dbo.ProcedureNotifications", "Attempts");
            DropColumn("dbo.ProcedureNotifications", "NextAttemptTime");
            CreateIndex("dbo.ProcedureNotificationResponses", "ProcedureNotificationOptionId");
            CreateIndex("dbo.ProcedureNotificationResponses", "ProcedureNotificationId");
            AddForeignKey("dbo.ProcedureNotificationResponses", "ProcedureNotificationId", "dbo.ProcedureNotifications", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProcedureNotificationResponses", "ProcedureNotificationOptionId", "dbo.ProcedureNotificationOptions", "Id", cascadeDelete: true);
        }
    }
}
