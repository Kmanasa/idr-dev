namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class manufacturersandimageispublic : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.NodeTypes", "LibraryType", c => c.String(maxLength: 255));
            AddColumn("dbo.ImageFiles", "IsPublic", c => c.Boolean(nullable: false));
            AddColumn("dbo.Nodes", "ManufacturerId", c => c.Int());
            CreateIndex("dbo.Nodes", "ManufacturerId");
            AddForeignKey("dbo.Nodes", "ManufacturerId", "dbo.Manufacturers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Nodes", "ManufacturerId", "dbo.Manufacturers");
            DropIndex("dbo.Nodes", new[] { "ManufacturerId" });
            DropColumn("dbo.Nodes", "ManufacturerId");
            DropColumn("dbo.ImageFiles", "IsPublic");
            DropColumn("dbo.NodeTypes", "LibraryType");
            DropTable("dbo.Manufacturers");
        }
    }
}
