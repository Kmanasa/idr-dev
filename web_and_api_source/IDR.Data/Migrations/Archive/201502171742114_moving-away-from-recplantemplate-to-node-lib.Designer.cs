// <auto-generated />
namespace IDR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class movingawayfromrecplantemplatetonodelib : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(movingawayfromrecplantemplatetonodelib));
        
        string IMigrationMetadata.Id
        {
            get { return "201502171742114_moving-away-from-recplantemplate-to-node-lib"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
