namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CptCodes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CptCodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        NodeLibraryId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 128),
                        DeletedOn = c.DateTime(),
                        CopyOfId = c.Int(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NodeLibraries", t => t.NodeLibraryId, cascadeDelete: true)
                .Index(t => t.NodeLibraryId);
            
            DropColumn("dbo.NodeLibraries", "CptCodes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NodeLibraries", "CptCodes", c => c.String(maxLength: 500));
            DropForeignKey("dbo.CptCodes", "NodeLibraryId", "dbo.NodeLibraries");
            DropIndex("dbo.CptCodes", new[] { "NodeLibraryId" });
            DropTable("dbo.CptCodes");
        }
    }
}
