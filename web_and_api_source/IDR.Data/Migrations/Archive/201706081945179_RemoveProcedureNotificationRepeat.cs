namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveProcedureNotificationRepeat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProcedureNotifications", "Repeat", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcedureNotifications", "Repeat");
        }
    }
}
