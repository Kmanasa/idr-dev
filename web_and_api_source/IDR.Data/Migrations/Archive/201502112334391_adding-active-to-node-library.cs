namespace IDR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class addingactivetonodelibrary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NodeLibraries", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NodeLibraries", "Active");
        }
    }
}
