namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MobileLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProcedureAccessLogs", "Model", c => c.String(maxLength: 500));
            AddColumn("dbo.ProcedureAccessLogs", "Platform", c => c.String(maxLength: 500));
            AddColumn("dbo.ProcedureAccessLogs", "Uuid", c => c.String(maxLength: 500));
            AddColumn("dbo.ProcedureAccessLogs", "Version", c => c.String(maxLength: 500));
            AddColumn("dbo.ProcedureAccessLogs", "Manufacturer", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcedureAccessLogs", "Manufacturer");
            DropColumn("dbo.ProcedureAccessLogs", "Version");
            DropColumn("dbo.ProcedureAccessLogs", "Uuid");
            DropColumn("dbo.ProcedureAccessLogs", "Platform");
            DropColumn("dbo.ProcedureAccessLogs", "Model");
        }
    }
}
