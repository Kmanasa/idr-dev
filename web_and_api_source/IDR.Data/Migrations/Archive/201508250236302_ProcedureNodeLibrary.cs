namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureNodeLibrary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "NodeLibraryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Procedures", "NodeLibraryId");
            AddForeignKey("dbo.Procedures", "NodeLibraryId", "dbo.NodeLibraries", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedures", "NodeLibraryId", "dbo.NodeLibraries");
            DropIndex("dbo.Procedures", new[] { "NodeLibraryId" });
            DropColumn("dbo.Procedures", "NodeLibraryId");
        }
    }
}
