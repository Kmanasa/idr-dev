// <auto-generated />
namespace IDR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class CallCenterComment : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CallCenterComment));
        
        string IMigrationMetadata.Id
        {
            get { return "201802062208091_CallCenterComment"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
