namespace IDR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFacilityGroupToCallCenterPracticeReferencesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallCenterPracticeReferences", "FacilityGroup", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallCenterPracticeReferences", "FacilityGroup");
        }
    }
}
