﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class NodeType : AuditEntity
	{
		public NodeType()
		{
			Properties = new List<PropertyType>();
		}
		[StringLength(255)]
		public string Name { get; set; }

		[StringLength(255)]
		public string UIKey { get; set; }

		[StringLength(1000)]
		public string Description { get; set; }

		public virtual List<PropertyType> Properties { get; set; }

		public virtual List<AllowedType> AllowedTypes { get; set; }

		public bool IsTop { get; set; }

		public bool HasContentProperties { get; set; }

        public int TemplateId { get; set; }

		/// <summary>
		/// Can the physician edit the content?
		/// </summary>
		public bool AllowPhysicianEdit { get; set; }

		[StringLength(255)]
		public string LibraryType { get; set; }

		/// <summary>
		/// Can the physician make selections
		/// </summary>
		public bool IsConfigurable { get; set; }
	}
}
