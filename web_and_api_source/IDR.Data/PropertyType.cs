﻿
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class PropertyType : AuditEntity
	{
		[StringLength(255)]
		public string Name { get; set; }

		[StringLength(1000)]
		public string Description { get; set; }

		[StringLength(1000)]
		public string UIKey { get; set; }

		public bool IsRequired { get; set; }

		public bool IsCompound { get; set; }

		[StringLength(1000)]
		public string DomainName { get; set; }

		[StringLength(1000)]
		public string General { get; set; }

	}
}
