﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDR.Data
{
	public class Contact : AuditEntity
	{
		[StringLength(255)]
		public string FirstName { get; set; }

		[StringLength(255)]
		public string LastName { get; set; }

		[StringLength(255)]
		public string ServiceName { get; set; }

		[StringLength(255)]
		public string Email { get; set; }

		[StringLength(255)]
		public string OfficePhone { get; set; }

		[StringLength(255)]
		public string OfficeExt { get; set; }

		[StringLength(255)]
		public string CellPhone { get; set; }

		[StringLength(255)]
		public string PersonalPhone { get; set; }

		[Index("IX_AddressEntity", 3)]
		public int EntityId { get; set; }

		[StringLength(255)]
		[Index("IX_AddressEntity", 1)]
		public string EntityName { get; set; }

		[StringLength(255)]
		[Index("IX_AddressEntity", 2)]
		public string ContactType { get; set; }

		[StringLength(255)]
		public string ContactFormType { get; set; }

		[StringLength(255)]
		public string ContactTitle { get; set; }

	}
}
