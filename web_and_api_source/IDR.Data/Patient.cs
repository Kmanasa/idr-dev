﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class Patient : AuditEntity
    {
        public Patient()
        {
            Procedures = new List<Procedure>();
		}

        public int? CompanyId { get; set; }
        public int PhysicianId { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }
        [StringLength(255)]
        public string LastName { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(50)]
        public string Identifier { get; set; }
        [StringLength(255)]
        public string Phone { get; set; }
        [StringLength(1)]
        public string MiddleInitial { get; set; }
        public int Age { get; set; }
        [StringLength(255)]
        public string MedicalRecordNumber { get; set; }

        public virtual Company Company { get; set; }
        public virtual Physician Physician { get; set; }
		public virtual List<Procedure> Procedures { get; set; }
    }
}
