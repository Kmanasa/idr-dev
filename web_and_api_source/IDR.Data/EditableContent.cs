﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDR.Data
{
    [Table(name: "EditableContent")]
	public class EditableContent : AuditEntity
	{
		[StringLength(50)]
        [Required]
		public string Title { get; set; }

        public int Sequence { get; set; }

        public string Content { get; set; }

        public bool InMobileMenu { get; set; }
	}
}
