﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class OnDemandMessage : AuditEntity
    {
        public int PatientId { get; set; }
        public string Phone { get; set; }
        public string AccessCode { get; set; }
        public string MessageType { get; set; }
        public string MessageContent { get; set; }
    }
}