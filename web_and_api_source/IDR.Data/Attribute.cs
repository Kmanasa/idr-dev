﻿
using System.Collections.Generic;

namespace IDR.Data
{
    public class Attribute : AuditEntity
    {
        public Attribute()
        {
            NodeLibraryAttributes = new List<NodeLibraryAttribute>();
        }

        public string Name { get; set; }

        public virtual List<NodeLibraryAttribute> NodeLibraryAttributes { get; set; }
    }

    public class NodeLibraryAttribute : AuditEntity
    {
        public int NodeLibraryId { get; set; }
        public int AttributeId { get; set; }

        public virtual Attribute Attribute { get; set; }
        public virtual NodeLibrary NodeLibrary { get; set; }
    }
}
