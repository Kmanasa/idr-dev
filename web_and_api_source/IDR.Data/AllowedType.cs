﻿namespace IDR.Data
{
	public class AllowedType : AuditEntity
	{
		public NodeType Type { get; set; }

		public int NodeTypeId { get; set; }

		public int AllowedTypeId { get; set; }
	}
}
