﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public interface IAuditEntity
    {
        void Audit(string userId);
    }

    public class AuditEntity : Entity, IAuditEntity
    {
        public AuditEntity()
        {
            CreatedOn = DateTime.UtcNow;
            ModifiedOn = DateTime.UtcNow;
        }

        public void Audit(string userId)
        {
            var now = DateTime.UtcNow;

            if (string.IsNullOrEmpty(CreatedBy))
            {
                CreatedBy = userId ?? "api@idr.com";
                CreatedOn = now;
            }

            ModifiedBy = userId ?? "api@idr.com";
            ModifiedOn = now;
        }

        [ScaffoldColumn(false)]
        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? ModifiedOn { get; set; }

        [StringLength(128)]
        [ScaffoldColumn(false)]
        public string ModifiedBy { get; set; }
    }
}
