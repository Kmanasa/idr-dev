﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IDR.web.Models;

namespace IDR.Data
{
    public class InvitableEntity : AuditEntity
    {

        public Company Company { get; set; }

        public int? CompanyId { get; set; }

        [StringLength(255)]
        public string ProvisionalEmail { get; set; }

        [StringLength(255)]
        public string UserId { get; set; }

        public bool IsProvisional { get; set; }

        public Guid? ProvisionalKey { get; set; }

        public bool InvitationSent { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }
    }
}