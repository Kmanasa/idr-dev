﻿using IDR.web.Models;

namespace IDR.Data
{
	public class CallSiteMobileUser : InvitableEntity
	{
		public virtual ApplicationUser User { get; set; }
	}
}