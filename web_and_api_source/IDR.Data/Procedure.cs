﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class Procedure : AuditEntity
    {
        public Procedure()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ProcedureNotifications = new List<ProcedureNotification>();
            //InboxItems = new List<ProcedureInboxItem>();
            ProcedureAccessLog = new List<ProcedureAccessLog>();
        }

        public int PhysicianId { get; set; }
        public int PatientId { get; set; }
        public int FacilityId { get; set; }
        public int NodeLibraryId { get; set; }

        [StringLength(50)]
        public string CptCode { get; set; }

        [StringLength(100)]
        public string AccessCode { get; set; }

        public DateTime? StartDate { get; set; }

        public bool SendEmail { get; set; }
        public bool SendSms { get; set; }

        public virtual Physician Physician { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual Facility Facility { get; set; }
        public virtual NodeLibrary NodeLibrary { get; set; }

        public virtual List<ProcedureNotification> ProcedureNotifications { get; set; }
        //public virtual List<ProcedureInboxItem> InboxItems { get; set; }
        public virtual List<ProcedureAccessLog> ProcedureAccessLog { get; set; }
    }

    public class ProcedureAccessLog
    {
        public int Id { get; set; }
        public int ProcedureId { get; set; }
        [StringLength(500)]
        public string DeviceId { get; set; }
        public DateTime TimeStamp { get; set; }
        [StringLength(500)]
        public string Model { get; set; }
        [StringLength(500)]
        public string Platform { get; set; }
        [StringLength(500)]
        public string Version { get; set; }
        [StringLength(500)]
        public string Manufacturer { get; set; }
        public virtual Procedure Procedure { get; set; }
    }
}
