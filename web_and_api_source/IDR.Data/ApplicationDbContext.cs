﻿using System;
using System.Collections.Generic;
using System.Configuration;
using IDR.web.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using IDR.Data.CallCenter;

namespace IDR.Data
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext() : base(
			"IdrDataContext",
			////Tenancy.GetConnectionForHost(System.Web.HttpContext.Current?.Request?.Url?.Host ?? "IdrDataContext"),
			throwIfV1Schema: false)
		{
		}

		public ApplicationDbContext(string connectionString) : base(connectionString, throwIfV1Schema: false)
		{
			var cs = connectionString;
		}

		public static ApplicationDbContext Create(string host)
		{
			var cs = Tenancy.GetConnectionForHost(host);
			return new ApplicationDbContext(cs);
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Physician>().HasOptional(p => p.User).WithOptionalDependent(u => u.Physician);
			modelBuilder.Entity<PracticeAdmin>().HasOptional(p => p.User).WithOptionalDependent(u => u.PracticeAdmin);
			modelBuilder.Entity<PhysiciansAssistant>().HasOptional(p => p.User)
				.WithOptionalDependent(u => u.PhysiciansAssistant);
			modelBuilder.Entity<CallSiteAdmin>().HasOptional(p => p.User).WithOptionalDependent(u => u.SiteAdmin);
			base.OnModelCreating(modelBuilder);
		}

		public virtual DbSet<Attribute> Attributes { get; set; }
		public virtual DbSet<NodeLibraryAttribute> NodeLibraryAttributes { get; set; }
		public virtual DbSet<NodeLibrary> NodeLibraries { get; set; }
		public virtual DbSet<Node> Nodes { get; set; }
		public virtual DbSet<CptCode> CptCodes { get; set; }
		public virtual DbSet<Notification> Notifications { get; set; }
		public virtual DbSet<NotificationResponse> NotificationResponses { get; set; }
		public virtual DbSet<EditableContent> EditableContent { get; set; }
		public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
		public virtual DbSet<Property> Properties { get; set; }
		public virtual DbSet<NodeType> NodeTypes { get; set; }
		public virtual DbSet<PropertyType> PropertyTypes { get; set; }
		public virtual DbSet<AllowedType> AllowedTypes { get; set; }
		public virtual DbSet<Image> Images { get; set; }
		public virtual DbSet<Physician> Physicians { get; set; }
		public virtual DbSet<Patient> Patients { get; set; }
		public virtual DbSet<PhysiciansAssistant> PhysiciansAssistants { get; set; }
		public virtual DbSet<PracticeAdmin> PracticeAdmins { get; set; }
		public virtual DbSet<CallSiteAdmin> CallSiteAdmins { get; set; }
		public virtual DbSet<CallSiteMobileUser> CallSiteMobileUsers { get; set; }
		public virtual DbSet<Scheduler> Schedulers { get; set; }
		public virtual DbSet<ProcedureNotification> ProcedureNotifications { get; set; }
		public virtual DbSet<Discipline> Disciplines { get; set; }
		public virtual DbSet<DisciplineEntityItem> DisciplineEntityItems { get; set; }
		public virtual DbSet<Company> Companies { get; set; }
		public virtual DbSet<Facility> Facilities { get; set; }
		public virtual DbSet<RemoteFacility> RemoteFacilities { get; set; }
		public virtual DbSet<Contact> Contacts { get; set; }
		public virtual DbSet<Address> Addresses { get; set; }
		public virtual DbSet<ImageFile> ImageFiles { get; set; }
		public virtual DbSet<Procedure> Procedures { get; set; }
		public virtual DbSet<ProcedureAccessLog> ProcedureAccessLog { get; set; }
		public virtual DbSet<TreatmentBindable> TreatmentBindables { get; set; }
        public virtual DbSet<OnDemandMessage> OnDemandMessages { get; set; }
        public virtual DbSet<PDFFile> PDFFiles { get; set; }

        // Call Center
        public virtual DbSet<CallCenter.CallCenter> CallCenters { get; set; }
		public virtual DbSet<CallCenterPracticeReference> CallCenterPracticeReferences { get; set; }
		public virtual DbSet<CallCenterScheduledCall> CallCenterScheduledCalls { get; set; }
		public virtual DbSet<CallCenterProcedure> CallCenterProcedures { get; set; }
		public virtual DbSet<CallCenterComment> CallCenterComments { get; set; }
		public virtual DbSet<CallCenterPatient> CallCenterPatients { get; set; }
		public virtual DbSet<CallCenterPayor> CallCenterPayors { get; set; }
		public virtual DbSet<CallCenterProvider> CallCenterProviders { get; set; }
		public virtual DbSet<CallCenterProviderFacility> CallCenterProviderFacilities { get; set; }
		public virtual DbSet<CallCenterCommentType> CallCenterCommentTypes { get; set; }
		public virtual DbSet<CallCenterProcedureTimer> CallCenterProcedureTimers { get; set; }
		public virtual DbSet<Report> Reports { get; set; }
	}

	public static class Tenancy
	{
		private static readonly Dictionary<string, string> _hostConnections = new Dictionary<string, string>();

		static Tenancy()
		{
			try
			{
				var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["IdrTenants"].ConnectionString);
				cn.Open();
				var tenants = cn.Query<TenantConnection>("select * from TenantConnections");
				cn.Close();

				foreach (var tenant in tenants)
				{
					_hostConnections.Add(tenant.Host.ToLowerInvariant(), tenant.ConnectionString);
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Unable to load IDR Tenant Connections", ex);
			}
		}

		public static string GetConnectionForHost(string host)
		{
			return _hostConnections.First(t => t.Key == host.ToLowerInvariant()).Value;
		}
	}

	internal class TenantConnection
	{
		public string Host { get; set; }
		public string ConnectionString { get; set; }
	}
}
