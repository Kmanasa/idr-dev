﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class CptCode : AuditEntity
    {
        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        public int NodeLibraryId { get; set; }
        public NodeLibrary NodeLibrary { get; set; }
    }
}