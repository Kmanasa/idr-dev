﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class DisciplineEntityItem : AuditEntity
	{
		public int DisciplineId { get; set; }

		public Discipline Discipline { get; set; }

		public int EntityId { get; set; }

		[StringLength(255)]
		public string EntityName { get; set; }
	}
}
