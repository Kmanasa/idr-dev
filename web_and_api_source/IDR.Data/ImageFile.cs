﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDR.Data
{
	public class ImageFile : AuditEntity
	{
		[StringLength(255)]
		public string FileName { get; set; }

		public byte[] Image { get; set; }

		[Index("IX_ImageFileEntity", 3)]
		[StringLength(255)]
		public string EntityName { get; set; }

        [StringLength(500)]
        public string Alias { get; set; }

		[Index("IX_ImageFileEntity", 2)]
		public int EntityId { get; set; }

		[Index("IX_ImageFileEntity", 1)]
		[StringLength(255)]
		public string ImageType { get; set; }

		[StringLength(255)]
		public string ImageFormType { get; set; }

		[StringLength(255)]
		public string ImageTitle { get; set; }

		public bool IsPublic { get; set; }
	}
}
