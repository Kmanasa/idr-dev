﻿namespace IDR.Data
{
    public class RemoteFacility : AuditEntity
    {
        public int CompanyId { get; set; }
        public int FacilityId { get; set; }

        public Facility Facility { get; set; }
        public Company Company { get; set; }
    }
}
