﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDR.Data
{
	public class Address : AuditEntity
	{
		[StringLength(255)]
		public string Name { get; set; }

		[StringLength(255)]
		public string Address1 { get; set; }

		[StringLength(255)]
		public string Address2 { get; set; }
	
		[StringLength(255)]
		public string City { get; set; }

		[StringLength(128)]
		public string State { get; set; }

		[StringLength(15)]
		public string PostalCode { get; set; }

		[StringLength(255)]
		public string Country { get; set; }

		[Index("IX_AddressEntity", 3)]
		public int EntityId { get; set; }

		[StringLength(255)]
		[Index("IX_AddressEntity", 1)]
		public string EntityName { get; set; }

		[StringLength(255)]
		[Index("IX_AddressEntity", 2)]
		public string AddressType { get; set; }

		[StringLength(255)]
		public string AddressFormType { get; set; }

		[StringLength(255)]
		public string AddressTitle { get; set; }

	}
}
