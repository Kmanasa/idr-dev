﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class Discipline : AuditEntity
    {
		public Discipline()
		{
			EntityItems = new List<DisciplineEntityItem>();
		}

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

		public virtual List<DisciplineEntityItem> EntityItems { get; set; }
    }
}
