﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class Node : AuditEntity
	{
		public Node()
		{
			Properties = new List<Property>();

			Nodes = new List<Node>();
		}

		public int? NodeLibraryId { get; set; }

		[StringLength(255)]
		public string Name { get; set; }

		public int TypeId { get; set; }
		public NodeType Type { get; set; }

		[StringLength(255)]
		public string Title { get; set; }

		public List<Property> Properties { get; set; }

		public List<Node> Nodes { get; set; }

        public string HtmlContent { get; set; }

		public string Description { get; set; }

		[StringLength(255)]
		public string SubMenuText { get; set; }

        public int? NodeId { get; set; }

        public int? ChildTemplateId { get; set; }

		public bool IsPhysicianEditable { get; set; }

		public int? OverrideSourceId { get; set; }

		public int? RecoveryPlanId { get; set; }

		public int Sequence { get; set; }

		public string ImageData { get; set; }

        [StringLength(75)]
        public string MenuIconStyle { get; set; }
	}
}
