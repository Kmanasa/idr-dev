using System;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }

        public DateTime? DeletedOn { get; set; }

        public int? CopyOfId { get; set; }

        public bool IsNew { get; set; }
    }
}