﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class EmailTemplate : AuditEntity
    {
        [StringLength(50)]
        [Required]
        public string Title { get; set; }

        [StringLength(50)]
        [Required]
        public string Alias { get; set; }

        public string Content { get; set; }
    }
}