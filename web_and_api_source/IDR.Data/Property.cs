﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class Property : AuditEntity
	{
        public Property()
        {
        }

        [StringLength(255)]
		public string Name { get; set; }

		[StringLength(255)]
		public string CssClass { get; set; }

		[StringLength(255)]
		public string Title { get; set; }

		public string Value { get; set; }

		//public List<PropertyValue> Values { get; set; }

		public Node Node { get;set; }
		public int NodeId { get; set; }

		public int PropertyId { get; set; }

		public PropertyType Type { get; set; }

		public int TypeId { get; set; }

		//public Guid Key { get; set; }
	}
}
