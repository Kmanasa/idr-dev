﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using IDR.Data;

namespace IDR.web.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

		[StringLength(255)]
		public string Firstname { get; set; }

		[StringLength(255)]
		public string Lastname { get; set; }

		public bool Active { get; set; }
	    public bool Bilingual { get; set; }

		
		public virtual Physician Physician { get; set; }
        public virtual PracticeAdmin PracticeAdmin { get; set; }
        public virtual PhysiciansAssistant PhysiciansAssistant { get; set; }
	    public virtual CallSiteAdmin SiteAdmin { get; set; }

        // Used by OAuth Provider for bearer tokens
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
	}
}