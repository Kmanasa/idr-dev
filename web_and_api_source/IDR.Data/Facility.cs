﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class Facility : AuditEntity
	{
		[StringLength(255)]
		public string Name { get; set; }


		public int CompanyId { get; set; }

        public Company Company { get; set; }

        public string Division { get; set; }

    }
}
