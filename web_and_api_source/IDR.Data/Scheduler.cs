﻿using IDR.web.Models;

namespace IDR.Data
{
    public class Scheduler : InvitableEntity
    {
        public virtual ApplicationUser User { get; set; }

        public Physician Physician { get; set; }

        public int? PhysicianId { get; set; }
    }
}