﻿namespace IDR
{
	public static class Constants
	{
		public const string MOBILE_APP_NODE_TEMPLATE = "node_NodeLibrary_TreeItem";

		public const string FORM_NODE_TEMPLATE = "node_Node_TreeItem";

		public static class ContactFormTypes
		{
			public const string Person = "PERSON";

			public const string Service = "SERVICE";

			public const string Simple = "SIMPLE";
		}

		public static class AddressFormTypes
		{
			public const string Service = "SERVICE";

			public const string Simple = "SIMPLE";
		}

		public static class ImageFormTypes
		{
			public const string Basic = "BASIC";

			public const string Logo = "LOGO";

			public const string Portrait = "PORTRAIT";
		}

		public static class NodeLibraryTypes
		{
			public const string SystemOverrideTemplate = "OVERRIDE";
            public const string MasterTemplate = "master-template";
            public const string ReusableTemplate = "content-template";
		}
	}
}
