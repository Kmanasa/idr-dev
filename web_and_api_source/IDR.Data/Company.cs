﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class Company : AuditEntity
	{
	    public Company()
	    {
	        Facilities = new List<Facility>();
            Physicians = new List<Physician>();
            RemoteFacilities = new List<RemoteFacility>();
	    }

		[StringLength(255)]
		public string Name { get; set; }
        [StringLength(500)]
        public string HoursOfOperation { get; set; }
        [StringLength(250)]
        public string TimeZone { get; set; }

        public int MaxNamedUsers { get; set; }
        public int MaxPhysicians { get; set; }
        public int MaxFacilities { get; set; }
        public int MaxPlans { get; set; }

        public decimal CostPerUser { get; set; }
        public decimal CostPerPhysician { get; set; }
        public decimal CostPerFacility { get; set; }
        public decimal CostPerPlan { get; set; }

        public string PatientEmailTemplate { get; set; }
        public string PhysicianEmailTemplate { get; set; }
        public string PhysiciansAssistantEmailTemplate { get; set; }
        public string PracticeAdminEmailTemplate { get; set; }
        public string CallSiteAdminEmailTemplate { get; set; }
        public string CallSiteMobileUserEmailTemplate { get; set; }
        public int PayorId { get; set; }

        public string Division { get; set; }

        public DateTime LicenseExpiration { get; set; }
        public decimal Discount { get; set; }

        public CompanyType CompanyType { get; set; }

		public virtual List<Facility> Facilities { get; set; }

        public virtual List<Physician> Physicians { get; set; }

        public virtual List<RemoteFacility> RemoteFacilities { get; set; } 
	}

    public enum CompanyType
    {
        Group = 0,
        Practice = 1,
        SurteryCenter = 2
    }
}
