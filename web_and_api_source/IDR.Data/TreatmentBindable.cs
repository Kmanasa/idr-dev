﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
	public class TreatmentBindable : AuditEntity
	{
		[StringLength(255)]
		public string PathName { get; set; }

		[StringLength(255)]
		public string Category { get; set; }

		public bool IsImage { get; set; }
	}
}
