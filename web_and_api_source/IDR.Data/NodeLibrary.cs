﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class NodeLibrary : AuditEntity
	{
		public NodeLibrary()
		{
		    // ReSharper disable DoNotCallOverridableMethodsInConstructor
			Nodes = new List<Node>();
            Attributes = new List<NodeLibraryAttribute>();
            CptCodes = new List<CptCode>();
		    Notifications = new List<Notification>();
		}

		[StringLength(255)]
		public string Name { get; set; }

		[StringLength(4000)]
		public string Description { get; set; }

        [StringLength(255)]
		public string Key { get; set; }

        [StringLength(255)]
        public string CtssCode { get; set; }

        public int Duration { get; set; }

        public List<CptCode> CptCodes { get; set; }

        public int? PhysicianId { get; set; }

		public virtual List<Node> Nodes { get; set; }

		[StringLength(255)]
		public string LibraryType { get; set; }

		public bool Active { get; set; }

        public virtual List<NodeLibraryAttribute> Attributes { get; set; }
        public virtual List<Notification> Notifications { get; set; }
	}
}
