﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenterProcedure : AuditEntity
	{
		public CallCenterProcedure()
		{
			CallCenterScheduledCalls = new List<CallCenterScheduledCall>();
			CallCenterProcedureTimers = new List<CallCenterProcedureTimer>();
		}


		public int CallCenterId { get; set; }
		public int ProcedureId { get; set; }
		public int? CallCenterPayorId { get; set; }
		public int? PhysicianId { get; set; }
		public int? AnesthesiologistId { get; set; }
		public int? NurseId { get; set; }

		public bool SpanishSpeaking { get; set; }
		public bool Outpatient { get; set; }

		[StringLength(256)]
		public string PumpSerial { get; set; }
		public bool IsDualCath { get; set; }
		[StringLength(256)]
		public string PumpSerial2 { get; set; }
		public bool Cancelled { get; set; }
		public bool Completed { get; set; }
		public bool NoMonitoring { get; set; }
		public bool DoNotDisturb { get; set; }
		public DateTime InitialContactDate { get; set; }
		public DateTime DiscontinueDate { get; set; }

		public int CptId { get; set; }
		public int PracticeFacilityId { get; set; }
		public bool DeliveredAoB { get; set; }
		public bool RemindedAoB { get; set; }

		#region InfuSystem Fields
		[StringLength(256)]
		public string InfuPumpModel { get; set; }
		public bool InfuCck { get; set; }
		public bool InfuHaveConsent { get; set; }
		[StringLength(256)]
		public string InfuTreatmentPlan { get; set; }
		public decimal InfuRate { get; set; }
		public int InfuVolume { get; set; }
		public bool InfuImSafetyFeatures { get; set; }
		#endregion

		public virtual CallCenterProvider Physician { get; set; }
		public virtual CallCenterProvider Anesthesiologist { get; set; }
		public virtual CallCenterProvider Nurse { get; set; }
		public virtual CallCenter CallCenter { get; set; }
		public virtual CallCenterPayor CallCenterPayor { get; set; }
		public virtual List<CallCenterScheduledCall> CallCenterScheduledCalls { get; set; }
		public virtual List<CallCenterProcedureTimer> CallCenterProcedureTimers { get; set; }

		public int Satisfaction { get; set; }
		public DateTime DeliveredAoBDate { get; set; }
	}

	public enum PatientGender
	{
		Male = 0,
		Female = 1,
		NonBinary
	}
}