﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenterProvider : AuditEntity
	{
		public CallCenterProvider()
		{
			CallCenterProviderFacilities = new List<CallCenterProviderFacility>();
		}

		[StringLength(100)]
		public string FirstName { get; set; }
		[StringLength(100)]
		public string LastName { get; set; }
		[StringLength(20)]
		public string Title { get; set; }
		[StringLength(40)]
		public string Suffix { get; set; }

		public ProviderType ProviderType { get; set; }
		public virtual List<CallCenterProviderFacility> CallCenterProviderFacilities { get; set; }
	}

	public class CallCenterProviderFacility : AuditEntity
	{
		public int CallCenterProviderId { get; set; }

		public int FacilityId { get; set; }

		public CallCenterProvider CallCenterProvider { get;set;}
	}

	public enum ProviderType
	{
		Physician = 0,
		Anesthesiologist = 1,
		Nurse = 2,
	}
}