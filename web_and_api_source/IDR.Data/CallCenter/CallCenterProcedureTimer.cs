﻿using System;

namespace IDR.Data.CallCenter
{
	public class CallCenterProcedureTimer : AuditEntity
	{
		public int CallCenterProcedureId { get; set; }
		public int ProcedureId { get; set; }

		public string AccessCode { get; set; }
		public double Rate { get; set; }
		public bool Safe { get; set; }
		public int Duration { get; set; }
		public string Unit { get; set; }
		public DateTime? End { get; set; }
		public DateTime Start { get; set; }
		public decimal InitialVolume { get; set; }
		public decimal RecordedVolume { get; set; }
		public decimal? CCV { get; set; }
        public bool NotificationSent { get; set; }
	    public int ActualDuration { get; set; }

        public CallCenterProcedure CallCenterProcedure { get; set; }
	}
}