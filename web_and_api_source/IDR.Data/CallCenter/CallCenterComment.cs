﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDR.Data.CallCenter
{
	public class CallCenterComment : AuditEntity
	{
		public int CallCenterProcedureId { get; set; }
		public int CallCenterScheduledCallId { get; set; }
		public int? CallCenterCommentTypeId { get; set; }

        [Column(TypeName = "VARCHAR(MAX)")]
        public string Comment { get; set; }
		//public CallType CallType { get; set; }

		public virtual CallCenterCommentType CallCenterCommentType { get; set; }

		public CallCenterProcedure CallCenterProcedure { get; set; }
		public CallCenterScheduledCall CallCenterScheduledCall { get; set; }

	}

	public class CallCenterCommentType : AuditEntity
	{
		public int Id { get; set; }
		public int Index { get; set; }
		[StringLength(50)]
		public string Title { get; set; }
	}
}