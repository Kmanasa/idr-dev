﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenter : AuditEntity
	{
        [StringLength(100)]
		public string Name { get; set; }
	}
}
