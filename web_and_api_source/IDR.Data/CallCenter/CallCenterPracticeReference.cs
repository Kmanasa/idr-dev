﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenterPracticeReference : AuditEntity
	{
		public int CallCenterId { get; set; }

		public int PracticeId { get; set; }

		public int StartCalls { get; set; }
		public int EndCalls { get; set; }
		public int SatisfactionInitial { get; set; }
		public int SatisfactionFinal { get; set; }
		public bool Passive { get; set; }
		public string Notes { get; set; }

		[StringLength(20)]
		public string PracticeCode { get; set; }

		[StringLength(20)]
		public string InfuFacilityId { get; set; }
		[StringLength(200)]
		public string InfuFacilityGroupName { get; set; }
		public bool InfuImSafetyFeatures { get; set; }

		public virtual CallCenter CallCenter { get; set; }
        public string FacilityGroup { get; set; }
        public string Addl_URL { get; set; }
    }
}