﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenterScheduledCall : AuditEntity
	{
		public int CallCenterProcedureId { get; set; }

		public CallStatus CallStatus { get; set; }
		public int CallDay { get; set; }
		public int CallMonth { get; set; }
		public int CallYear { get; set; }
		public int Index { get; set; }
		public bool Assigned { get; set; }
		[StringLength(256)]
		public string AssignedUserId { get; set; }

		public DateTime CallTime { get; set; }
		public CallType CallType { get; set; }

		// Assessment
		public int? PainScoreCurrent { get; set; }

		public int Perscription24Hour { get; set; }
		public int Otc24Hour { get; set; }
        public int DoseButton { get; set; }

        public int? TolerableToilet { get; set; }
		public int? TolerableConsumption { get; set; }
		public int? TolerableSleeping { get; set; }
		public int? TolerableWalking { get; set; }
		public int? TolerableDressing{ get; set; }
		public int? TolerableStairs { get; set; }
		public int? TolerablePtOt{ get; set; }
        public int? TolerableReturnToWork { get; set; }
        public bool? EffectiveUse { get; set; }

        public virtual CallCenterProcedure CallCenterProcedure { get; set; }
	}

	public enum CallStatus
	{
		Scheduled = 1,
		Completed = 2,
		CompletedMobile = 3,
		Discharged = 4
	}

	public enum CallType
	{
		Unknown = 0,
		Assessment = 1,
		HotlineNote = 2,
		PatientReported = 3,
		General = 4
	}

    public enum AssessmentValues
    {
        Tolerable = 0,
        SlightlyDifficult = 1,
        Intolerable = 2

    }
}