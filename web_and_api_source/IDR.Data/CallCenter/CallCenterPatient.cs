﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenterPatient : AuditEntity
	{
		public int CallCenterProcedureId { get; set; }

		public int PatientId { get; set; }
		public PatientGender Gender  { get; set; }
		[StringLength(256)]
		public string AltContact { get; set; }
		[StringLength(256)]
		public string AltPhone { get; set; }

		public bool Mobile { get; set; }

		[StringLength(20)]
		public string InfuPatientId { get; set; }
		

		public virtual CallCenterProcedure CallCenterProcedure { get; set; }
	}
}