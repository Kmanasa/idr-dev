﻿using System.ComponentModel.DataAnnotations;

namespace IDR.Data.CallCenter
{
	public class CallCenterPayor : AuditEntity
	{
		[StringLength(100)]
		public string Title { get; set; }
	}
}