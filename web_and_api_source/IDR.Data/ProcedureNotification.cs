﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class ProcedureNotification : AuditEntity
    {
        public ProcedureNotification()
        {
            ProcedureNotificationOptions = new List<ProcedureNotificationOption>();
        }

        public int ProcedureId { get; set; }
        public int NotificationId { get; set; }
        public bool Sent { get; set; }
        public DateTime SendTime { get; set; }

        public DateTime? NextAttemptTime { get; set; }
        public int Attempts { get; set; }
        public bool Triggered { get; set; }
        public bool Repeat { get; set; }

        [StringLength(500)]
        public string Response { get; set; }
        public DateTime? DateResponded { get; set; }

        public virtual Procedure Procedure { get; set; }
        public virtual Notification Notification { get; set; }

        public virtual List<ProcedureNotificationOption> ProcedureNotificationOptions { get; set; }
    }

    public class ProcedureNotificationOption : Entity
    {
        public int ProcedureNotificationId { get; set; }
        [StringLength(500)]
        public string Text { get; set; }
        public bool Trigger { get; set; }
    }
}