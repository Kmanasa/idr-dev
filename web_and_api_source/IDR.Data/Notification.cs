﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class Notification : AuditEntity
    {
        public Notification()
        {
            NotificationResponses = new List<NotificationResponse>();
        }

        public int NodeLibraryId { get; set; }

        [StringLength(150)]
        public string Title { get; set; }
        [StringLength(1000)]
        public string Message { get; set; }

        public int DaysOffset { get; set; }
        public int Hour{ get; set; }
        public int Minute{ get; set; }

        [StringLength(1000)]
        public string TriggerText { get; set; }
        public bool Required { get; set; }
        public bool Repeat { get; set; }
        public int Repetitions { get; set; }
        public int RepetitionMinutes { get; set; }
        public int ResponseType { get; set; }

        public virtual NodeLibrary NodeLibrary { get; set; }
        public virtual List<NotificationResponse> NotificationResponses { get; set; }
    }

    public class NotificationResponse : Entity
    {
        public int NotificationId { get; set; }
        [StringLength(500)]
        public string Text { get; set; }
        public bool Trigger { get; set; }
    }
}