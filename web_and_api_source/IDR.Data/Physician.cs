﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IDR.web.Models;

namespace IDR.Data
{
    public class Physician : InvitableEntity
    {
        public Physician()
        {
            Patients = new List<Patient>();
        }

        public virtual ApplicationUser User { get; set; }


        [StringLength(255)]
        public string Title { get; set; }

        public List<Patient> Patients { get; set; }

        [StringLength(255)]
        public string ImageRef { get; set; }

        public bool DemoPhysician { get; set; }
	}
}
