﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDR.Data
{
    public class PDFFile : AuditEntity
    {
        public string FileName { get; set; }
        public byte[] PDF { get; set; }
        public int CompanyId { get; set; }
        public string Alias { get; set; }
        public double FileSize { get; set; }
        public int PageCount { get; set; }
    }
}