﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using IDR.Data;
using IDR.Messaging;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace WebJobQueueEvents
{
    public class NotificationQueueService
    {
        private string _currentCs;
        private ApplicationDbContext _db;
        private readonly CloudQueueClient _queueClient;
        private readonly TextWriter _log;

        public NotificationQueueService(TextWriter log)
        {
            _log = log;

            var storageString = CloudConfigurationManager.GetSetting("StorageConnectionString");
            var storageAccount = CloudStorageAccount.Parse(storageString);

            // Create the queue client
            _queueClient = storageAccount.CreateCloudQueueClient();
        }

        public void Run()
        {
            var queue = _queueClient.GetQueueReference("procedurenotifications");
            queue.CreateIfNotExists();

	        var hosts = CloudConfigurationManager.GetSetting("TenantHosts").Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

			var cn = new SqlConnection(CloudConfigurationManager.GetSetting("IdrTenantsConnectionString"));
	        cn.Open();
	        var connections = cn.Query<string>("select distinct ConnectionString from TenantConnections where Host in @hosts", new{ hosts }).ToList();
	        cn.Close();


			foreach (var cs in connections)
			{
				_log.WriteLine("Notification Service: Using connection " + cs);
				_currentCs = cs;
				_db = new ApplicationDbContext(cs);
				EnqueueNewMessages(queue);
	        }
        }

        public void EnqueueNewMessages(CloudQueue queue)
        {
            var now = DateTime.UtcNow;

            _log.WriteLine($"Running EnqueueNewMessages at {now} UTC");

            try
            {
                // Get unsent notifications 
                var notifications = _db.ProcedureNotifications
                    .Where(n =>
                        // Not sent, ready for 1st send
                        (n.Sent == false && n.SendTime < now) ||
                        // Sent, not responded
                        (n.Notification.Required && n.Response == null && n.Attempts < 3 && n.NextAttemptTime < now))
                    .OrderBy(n => n.SendTime)
                    .ToList();

                _log.WriteLine($"Total new notifications found: {notifications.Count}");

                foreach (var n in notifications)
                {
                    _log.WriteLine($"Sending new notification {n.NotificationId} for code {n.Procedure.AccessCode} with send time {n.SendTime.ToLongTimeString()} at UTC {now.ToLongTimeString()}");

                    var notif = n.Notification;

                    var note = new ProcedurePushNotification
                    {
                        ProcedureId = n.ProcedureId,
                        ProcedureCode = n.Procedure.AccessCode,
                        Message = notif.Message,
						Connection = _currentCs
                    };

                    var message = new CloudQueueMessage(JsonConvert.SerializeObject(note));
                    queue.AddMessage(message);

                    // Flag the notification as sent
                    n.Sent = true;

                    var notifConfig = n.Notification;

                    // Flag the next attempt
                    n.Attempts++;
                    n.NextAttemptTime = n.SendTime.AddMinutes(notifConfig.RepetitionMinutes);

                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.WriteLine("NotificationQueueService exception at {0}", DateTime.UtcNow);
                _log.WriteLine("===");
                _log.Write(ex.Message);
            }
        }

        public void EneueRetryMessages(CloudQueue queue)
        {
            var now = DateTime.UtcNow;

            _log.WriteLine($"Running EneueRetryMessages at {now} UTC");

            try
            {
                // Get unsent notifications 
                var notifications = (from pn in _db.ProcedureNotifications
                                     join n in _db.Notifications on pn.NotificationId equals n.Id
                                     where pn.Sent && pn.NextAttemptTime != null && 
                                        pn.NextAttemptTime < now &&
                                        pn.Attempts < n.Repetitions &&
                                        pn.DateResponded == null
                                     orderby pn.SendTime
                                     select pn)
                    .ToList();

                _log.WriteLine($"Total notifications found: {notifications.Count}");

                foreach (var n in notifications)
                {
                    _log.WriteLine($"Sending notification {n.NotificationId} for code {n.Procedure.AccessCode} with send time {n.SendTime.ToLongTimeString()} at UTC {now.ToLongTimeString()}");

                    var notif = n.Notification;

                    var note = new ProcedurePushNotification
                    {
                        ProcedureId = n.ProcedureId,
                        ProcedureCode = n.Procedure.AccessCode,
                        Message = notif.Message
                    };

                    var message = new CloudQueueMessage(JsonConvert.SerializeObject(note));
                    queue.AddMessage(message);
                    
                    // Flag the notification as sent
                    n.Sent = true;
                    n.Attempts++;

                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.WriteLine("NotificationQueueService exception at {0}", DateTime.UtcNow);
                _log.WriteLine("===");
                _log.Write(ex.Message);
            }
        }
    }
}
