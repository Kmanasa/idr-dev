﻿using System;
using System.IO;
using Microsoft.Azure.WebJobs;

namespace WebJobQueueEvents
{
    public class Functions
    {
        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called queue
        [NoAutomaticTrigger]
        public static void ManualTrigger(TextWriter log)
        {
            try
            {
                var start = DateTime.UtcNow;
                log.WriteLine("IDR event queue service started at {0}", DateTime.UtcNow);

                try
                {
                    log.WriteLine("Running notification queue at {0}", DateTime.UtcNow);
                    new NotificationQueueService(log).Run();
                }
                catch (Exception ex)
                {
                    log.Write(ex.Message + Environment.NewLine + ex.StackTrace);
                }

                try
                {
                    log.WriteLine("Running missing AoB service at {0}", DateTime.UtcNow);
                    new MissingAobService(log).Run();
                }
                catch (Exception ex)
                {
                    log.Write(ex.Message + Environment.NewLine + ex.StackTrace);
                }

                try
                {
                    log.WriteLine("Running RRV service at {0}", DateTime.UtcNow);
                    new NotifyMissingRrvServive(log).Run();
                }
                catch (Exception ex)
                {
                    log.Write(ex.Message + Environment.NewLine + ex.StackTrace);
                }

                var elapsed = DateTime.UtcNow - start;
                log.WriteLine("IDR event queue service completed at {0}", elapsed.TotalMilliseconds);
            }
            catch (Exception ex)
            {
                log.Write(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }
    }
}
