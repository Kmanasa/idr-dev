﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Dapper;
using IDR.Data;
using IDR.Migrations;
using Microsoft.Azure;
using Twilio;
using Twilio.Rest.Messaging.V1;
using Twilio.Rest.Messaging.V1.Service;
using Twilio.Rest.Api.V2010.Account;

namespace WebJobQueueEvents
{
	public class MissingAobService
	{
		private readonly TextWriter _log;
		private ApplicationDbContext _db;

	    private static readonly string Sid = ConfigurationManager.AppSettings["TwilioSid"];
	    private static readonly string Token = ConfigurationManager.AppSettings["TwilioToken"];
	    // private static readonly string TwilioPhone = ConfigurationManager.AppSettings["TwilioPhone"];

        public MissingAobService(TextWriter log)
		{
			_log = log;
		}

		public void Run()
		{
			try
			{
				var hosts = CloudConfigurationManager.GetSetting("TenantHosts")
					.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

				var cn = new SqlConnection(CloudConfigurationManager.GetSetting("IdrTenantsConnectionString"));
				cn.Open();
				var connections = cn.Query<string>("select distinct ConnectionString from TenantConnections where Host in @hosts",
					new {hosts}).ToList();
				cn.Close();

				foreach (var cs in connections)
				{
					_log.WriteLine("AOB Service: Using connection " + cs);
					_db = new ApplicationDbContext(cs);
					SendReminders();
				}
			}
			catch (Exception ex)
			{
				_log.Write(ex.Message);
			}
		}

		private void SendReminders()
		{
			var fiveDaysAgo = DateTime.UtcNow.AddDays(-5);
			// Find all procedures without an AoB, no reminder send, and 5 days elapsed.
			var ccProcs = _db.CallCenterProcedures.Where(p => p.DeliveredAoB == false &&
			                                                  p.RemindedAoB == false &&
			                                                  p.CreatedOn < fiveDaysAgo)
				.ToList();

			var patients = new List<Patient>();

			foreach (var ccProc in ccProcs)
			{
				ccProc.RemindedAoB = true;
				var proc = _db.Procedures.FirstOrDefault(p => p.Id == ccProc.ProcedureId);

				if (proc != null)
				{
					var patient = _db.Patients.FirstOrDefault(p => p.Id == proc.PatientId);

					if (patient != null)
					{
						patients.Add(patient);
					}
				}
			}

			_db.SaveChanges();

			foreach (var patient in patients.Where(p => !string.IsNullOrWhiteSpace(p.Phone)))
			{
                // Commented out due to production issues
                //SendAoBSms(patient);
			}
		}

	    private void SendAoBSms(Patient patient)
	    {
	        var text = "We haven't received your pump consent form. Open InfuSystem Mobile app and tap Sign Benefits Form to complete.";

	        try
	        {
	            var regex = new Regex("[^0-9]");
	            var phone = regex.Replace(patient.Phone, "");
                // var twilio = new TwilioRestClient(Sid, Token);

                // twilio.SendMessage(TwilioPhone, phone, message, "");

                TwilioClient.Init(Sid, Token);

                var message = MessageResource.Create(
                body: text,
                messagingServiceSid: ConfigurationManager.AppSettings["MessagingServiceSid"],
                to: new Twilio.Types.PhoneNumber(phone)
                );

            }
	        catch (Exception ex)
	        {
	            var a = ex;
	        }

        }
	}
}