﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Dapper;
using IDR.Data;
using IDR.Data.CallCenter;
using IDR.Services;
using Microsoft.Azure;

namespace WebJobQueueEvents
{
	public class NotifyMissingRrvServive
	{
		private readonly TextWriter _log;
		private ApplicationDbContext _db;

		public NotifyMissingRrvServive(TextWriter log)
		{
			_log = log;
		}

		public void Run()
		{
			try
			{
				var hosts = CloudConfigurationManager.GetSetting("TenantHosts")
					.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

				var cn = new SqlConnection(CloudConfigurationManager.GetSetting("IdrTenantsConnectionString"));
				cn.Open();
				var connections = cn.Query<string>("select distinct ConnectionString from TenantConnections where Host in @hosts",
					new { hosts }).ToList();
				cn.Close();

				foreach (var cs in connections)
				{
					_log.WriteLine("RRV Service: Using connection " + cs);

					_db = new ApplicationDbContext(cs);
					SendWarnings();
				}
			}
			catch (Exception ex)
			{
				_log.Write(ex.Message);
			}
		}

		private void SendWarnings()
		{
			var oneHourAgo = DateTime.UtcNow.AddHours(-1);
			// Find all procedures without an AoB, no reminder send, and 5 days elapsed.
			var elapsedTimers = _db.CallCenterProcedureTimers.Where(t => !t.NotificationSent &&
			                                                             t.End == null &&
			                                                             t.RecordedVolume == 0 &&
			                                                             t.Start < oneHourAgo)
				.ToList();

			

			var service = new NonResponsiveMessageService(_db);

			foreach (var timer in elapsedTimers)
			{
				//timer.NotificationSent = true;

				service.SendNonResponsiveWarning(timer);
			}

			_db.SaveChanges();
		}
	}
}